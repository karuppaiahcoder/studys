---
                FILE: how_to_change_table_fields_in_rails.md
         DESCRIPTION:
              AUTHOR: Karuppaiah K
             PROJECT: zivah5.0
        CREATED DATE: 09/08/2019
---

1.[How to generate the model for a table?](#generate-model)

2.[How to generate the migration file?](#generate-migration-file)

3.[How to execute the migration file?](#excute-the-migration-file)

4.[How to remove the table,model and migration files?](#remove-table-model-and-migration-file)

5.[How to add a column?](#add-column)

6.[How to rename a column](#rename-column)

7.[How to remove a column?](#remove-column)

8.[How to change a data type for a column? How to set ENUM](#change-the-data-type-for-a-column)

9.[How to add and remove the 'not null' constrain to a field?](#add-or-remove-not-null)

10.[How to add a 'default' values to a field?](#add-default-value)

11.[How to add and remove the unuique ID?](#add-and-remove-unique-id)

12.[How to add foreign key to a field?](#add-foreign-key)

13.[How to remove the foreign key to a field?](#remove-foreign-key)



#### Generate model

* We can generate the model by refering following rails command.

```
rails g model model_name field_name-1:datatype field_name-n:datatype
```

* Here we will get the migration file after executing the above command. After that we need to add the all constrain into the migration. After that we need to run the 'rake db:migrate' command to create the table into database

* Example,

```
$ rails g model query_mapping listview_management_id:integer dashboard_id:integer query:text next_listview_id:integer
```


#### Generate migration file 

* We can generate the migration file by referring following rails command.

```
rails g migration <migration file name>
```

* Here the migration file name should be related to table name and what change we are going to do. If we follow those things while generation migration file will help us in future. But its not mandatory which means we can use any name for that migration, but should not exist one.

* Example

```
database@zivah5:~/databasemanagement/src/FrontEnd/db/migrate\n$ rails g migration changeStationDetails
Running via Spring preloader in process 9162
      invoke  active_record
      create    db/migrate/20190731064811_change_station_details.rb
database@zivah5:~/databasemanagement/src/FrontEnd/db/migrate\n$
```

#### Excute the migration file

* We have use the rake command to execute the migration files.

```
rake db:migrate
```

* It will execute the migration files which are created newly and apply the all changes into database.
* Example,

```
$ rake db:migrate
```
* And also we can migrate with specific migration file, by using the version.

```
rake db:migrate:up VERSION=<version of the migration file>
```
* Here we need to give the migration file version value. That might bee in frond of the migration file. For example,

```
20190608095325_create_memory_usage_logs.rb
```

* Here the first integer values consider as version value. So we need to use that values like below.

```
$ rake db:migrate:up VERSION=20190608095325
```


#### Remove table model and migration file

* We have follow the following steps one by one to remove the model,migration file and tables from database.

1. First we need to remove the table from database, we can remove the table by using rails command.

```
rake db:migrate:down VERSION=<integer value>
```

* Here we need to give the migration file version value. That might bee in frond of the migration file. For example,

```
20190608095325_create_memory_usage_logs.rb
```

* Here the first integer values consider as version value. So we need to use that values like below.

```
$ rake db:migrate:down VERSION=20190608095325
```

* Now the table will remove from the database.

2. Second we need remove the model and migration file. For that we have use the following rails commands.

```
rails destroy model <model name>
```

* Here we have give the model name

* The above command will remove the model and migration files which are related to that model.

* Example,

```
$ rails destroy model disk_usage_log
```


#### Methods to modify the table details

* Note: We need to following methods inside the migration files

##### Add column

* We can add a field to particular table by using the add_column method.

```
add_column :table_name, :field_name, :data_type
```

* Example,

```
$ add_column :license_details, :user_service_count, :integer
```

##### Rename column

* We can rename the particular field in a table by using the rename_column method.

```
rename_column :table_name, :old_field_name, :new_field_name
```

* Example,

```
rename_column :customer_details, :primay_number, :primary_number
rename_column :call_logs, :projects_id, :project_id

```

##### Remove column

* We can remove the particular field in a table by using the remove_column method.

```
remove_column :table_name, :field_name
```

* Example,

```
remove_column :voice_file_name_configs, :location_id
remove_column :keywords_configurations, :location_id
```

##### Change the data type for a  column

* We can change the data type for a particular filed in a table by using the change_column method.

```
change_column :table_name, :field_name, :data_type

change_column :table_name, :field_name,:data_type, using: 'field_name::data_type'
```

* Example,

```
change_column :user_profiles, :dob, :date

change_column :call_logs, :customer_id,:integer, using: 'customer_id::integer'
```

* Here we can change the data type for a filed. We have two ways to change the data type.  

* If we need to change the data type as integer or enum type we need to use second way.

* ENUM

	* We can create and set the enum type in migration file.

```
        execute <<-SQL
                CREATE TYPE user_gender AS ENUM ('MALE', 'FEMALE', 'TRANSGENDER');
        SQL
        change_column :ldap_users, :gender, :user_gender, using: 'gender::user_gender'
```
	* First we need to create the ENUM type then we need use that type in change_column method.

##### Add or Remove not null

* We can add or remove the 'not null' constrain to the field by using the change_column method.

* Add 'not null' constrain

```
change_column :table_name, :field_name, :data_type, :null => false
```

* Remove 'not null' constrain

```
change_column :table_name, :field_name, :data_type, :null => true
```
* Example,

```
change_column :license_details, :active_count, :integer, :null => false
change_column :license_details, :active_count, :integer, :null => true
```

##### Add default value

* We can add or remove the 'default' values to the filed by using the change_column method.

```
change_column :table_name, :field_name, :data_type, :default => default_value
```

* Example,

```
change_column :license_details, :is_unlimited, :boolean, :default => false
change_column :license_details, :is_unlimited, :integer, :default => 1
```

##### Add and remove unique id

* We can add an unique constrain for a field.

```
add_index :table_name, :field_name, unique: true
```

* Also we can set compination of field an unique id

```
add_index :table_name, [:field_name-1, :field_name-2, :Field_name-n], unique: true
```

* Example,

```
add_index :station_details, :station_name, unique: true
add_index :station_details, [:recorder_id, :station_name], unique: true
```

* Same like we can remove the unique constrain to a filed and compination of the field by using 'remove_index' method.

```
remove_index :table_name, :field_name
remove_index :table_name, [:field_name-1,:field_name-2, :field_name-n]
```

* Example,

```
remove_index :station_details, :station_name
remove_index :station_details, [:station_name,:station_id]
```

##### Add foreign key

* We can add the foreign to a field in migration file

```
add_foreign_key :table_name-1,:table_name-2,column: :field_name, primary_key: :field_name ,on_delete: :cascade
```

* Here 'table_name-1' is the table name which have the forign key and 'column: :field_name' is a field name of the foreign key table

* Then 'table_name-2' is the table name which have the primary key and 'primary_key: :field_name' is a field name of the primary key table

* Which means table_name1 refer the table_name-2. Finally we need to give the 'on_delete: :cascade' option. It is optional.

* Example,

```
add_foreign_key :call_logs, :customer_details, column: :customer_id
add_foreign_key :query_mappings,:listview_managements,column: :listview_management_id, primary_key: :id ,on_delete: :cascade
```

##### Remove foreign key

* We can remove the foreign key by using the 'remove_foreign_key' method.

```
remove_foreign_key :table_name, name: "name of the foreign key"

```

* Here we need to give the name of the foreign key which is created by psql. Which means we can get the foreign key from psql by executing the '\d table_name' command.

* Example,

```
remove_foreign_key :online_status_br_logs, name: "fk_rails_6c5d839345"

        foreign key name : "fk_rails_6c5d839345" FOREIGN KEY (restore_log_id) REFERENCES br_backup_logs(id) ON DELETE CASCADE
```


