#include<stdio.h>
main()
{
        float fahrenheit,celsius;
        float lower=0,upper=300,step=20;
        fahrenheit=upper;
        printf("Fahrenheit\tCelsius\n");
        while(fahrenheit>=lower)
        {
                celsius=(5.0/9.0)*(fahrenheit-32.0);
                printf("   %0.f\t\t  %.2f\n",fahrenheit,celsius);
                fahrenheit=fahrenheit-step;
        }
}
