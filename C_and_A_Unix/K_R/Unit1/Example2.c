/*Exercise 1-2. 
		Experiment to find out what happens when prints's argument string contains \c, where c is some character not listed above.
*/

#include<stdio.h>
main()
{
	printf("Hello, WOrld\n");
	printf("Hello, WOrld\t");
	printf("Hello, WOrld\b");
	printf("Hello, WOrld\a");
	printf("Hello, WOrld\r");
	printf("Hello, WOrld\\");
	printf("Hello, WOrld\v");
	printf("Hello, WOrld\'");
	printf("Hello, WOrld\"");
	printf("Hello, WOrld\?");
	printf("Hello, WOrld\vEnd");
}
