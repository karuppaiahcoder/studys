#include <stdio.h>
#include <stdlib.h>

int main()
{
   char str[30] = "010 This is test";
   char *ptr;
   long ret;

   ret = strtol(str, &ptr, 8);

   printf("The number(unsigned long integer) is %o\n", ret);
   printf("String part is |%s|\n", ptr);

   return(0);
}
