#include<stdio.h>
#include<stdlib.h>
void *func(void);
void func2(void);
void func1(void);
main()
{

	int a=10;

	atexit((void*)func);	//register the function in execute the program terminate time.
	atexit(func1);	//Same for this function register.
	atexit(func2);	//Same for this function register.
			//But last register function execute the first which first register last execute.
	a=getchar();
	printf("%c\n",a);
//	abort();	//The abort function is terminate the program , not delete and flush anything.
	a=getchar();
	printf("%c\n",a);
}
void *func()
{
	printf("E.........1\n");
	exit(0);
}
void func1()
{
	printf("E.........2\n");
	exit(0);
}

void func2()
{
	printf("E.........3\n");
	exit(0);
}
