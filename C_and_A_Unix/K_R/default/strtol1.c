#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

int main(int argc, const char * argv[])
{
    char value[10];
    char *eptr;
    long result;

    strcpy(value, " 123");

    result = strtol(value, &eptr, 10);
    if (result == 0)
    {
        printf("Conversion error occurred: %d", errno);

        exit(0);
    }

    printf("%ld decimal\n", result);

    strcpy(value, "0x19e");

    result = strtol(value, &eptr, 16);

    if (result == 0)
    {
        printf("Conversion error occurred: %d", errno);

        exit(0);
    }

    printf("%lx hexadecimal\n", result);

    return 0;
}
