#include<stdio.h>
#include<assert.h>  //This function include the assert function or macro.
main()
{
	char num='a';

	printf("number is %c\n",num);
	
	assert(num);  //The assert is a macro check the variable value non zero.

	num = 0;

	printf("number is %c\n",num);

	assert(num);	//This time Terminate the program, because num is zero.
	return 0;
}
