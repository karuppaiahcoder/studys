//Write a cross-referencer that prints a list of all words in a document, and for each word, a list of the line numbers on which it occurs. Remove noise words like ``the,'' ``and,'' and so on.
/***********************Algorithm********************/
/*
	+This program concept was skip the noise words.
	+Then count the word occur lines and list.
	+Then first write the one structure in store the noise words.
	+Then each time match the our get value noise or non noise value.
	+Noise word in skip the word.

*/

#include<stdio.h>
#include<ctype.h>
#include<string.h>
#include<stdlib.h>
#define MAX 100

struct nois
{
	char *word;
}noisword[]={
		{"a"},
		{"about"},
		{"and"},
		{"any"},
		{"are"},
		{"be"},
		{"but"},
		{"by"},
		{"come"},
		{"can"}
	    };

#define NOIS (sizeof noisword/sizeof noisword[0])

struct tnode
{
	char *word;		
	char count[100];		
	int cnt;
	struct tnode *left;	//left child
	struct tnode *right;	//right child
};

int binsearch(char *s,struct nois tab[],int n);
struct tnode *addtree(struct tnode *,char *);
void tree_print(struct tnode *);
int get_word(char *s,int lim);

int line=1;

main(int argc,char *argv[])
{
	struct tnode *root;
	char word[MAX]={};

	root='\0';
	while(get_word(word,MAX)!=EOF)
	{
		if((binsearch(word,noisword,NOIS))==-1)
			if(isalpha(word[0]))
				root = addtree(root,word);
	}
	tree_print(root);
}

int binsearch(char *s,struct nois tab[],int n)
{
        int cnt=0,low=0,high=n-1,mid=0;

        while(low<=high)
        {
                mid=(low+high)/2;

                if((cnt=strcmp(s,tab[mid].word))<0)
                        high=mid-1;
                else if(cnt>0)
                        low=mid+1;
                else
                        return mid;
        }
        return -1;
}

struct tnode *talloc(void);
char *str_dup(char *s);

struct tnode *addtree(struct tnode *p,char *w)
{
	int cnt=0,i=0;
	
	if(p=='\0')
	{
		p=talloc();
		p->word=str_dup(w);
		p->count[0]=line+48;
		p->cnt=1;
		p->left=p->right='\0';
	}
	else if((cnt = strcmp(w,p->word))==0)
	{
		
		i=p->cnt++;
		if(p->count[i-1]!=(line+48))
		{
			if(line>9)
			{
				p->count[i++]=',';
				p->count[i++]=(line/10)+48;
				p->count[i]=(line%10)+48;
				p->cnt++;
			}
			else
			{
				printf("Fun %d	%d	%s\n",line,i,p->word);
				p->count[i++]=',';
				p->count[i]=line+48;
			}
		}
		else
			p->cnt--;
	}
	else if(cnt < 0)
		p->left = addtree(p->left,w);
	else
		p->right = addtree(p->right,w);
	
	return p;
}

char *str_dup(char *s)
{
	char *p;

	p=(char *) malloc(strlen(s)+1);
	if(p!=NULL)
		strcpy(p,s);
	return p;
}

struct tnode *talloc(void)
{
	return (struct tnode *)malloc(sizeof(struct tnode));
}

void tree_print(struct tnode *p)
{
	if(p!='\0')
	{
		tree_print(p->left);
		printf("\n\"%s\" This word occur the line numbers= %s\n",p->word,p->count);
		tree_print(p->right);
	}
}

int get_ch();
void unget_ch(int c);

int get_word(char *s,int lim)
{
        char c,c1,*w=s;

       L: while((c=get_ch())==' '||c=='\t') ;

	if(c=='\n')
	{
		line++;
		goto L;
	}

        if(c!=EOF)
                *w++=c;
        if(!isalpha(c))
        {
                *w='\0';
                return c;
        }

        for(;--lim;w++)
        {
                if(!isalpha(*w=get_ch()))
                {
                        unget_ch(*w);
                        break;
                }
        }
        *w='\0';
        return s[0];
}

char buff[MAX]={},buf=0;

int get_ch()
{
        return buf>0?buff[--buf]:getchar();
}

void unget_ch(int c)
{
        buff[buf++]=c;
}

