//Our version of getword does not properly handle underscores, string constants, comments, or preprocessor control lines. Write a better version.

/***************Algorithm*****************/
/*

	+Count the key word program.
	+But this version program not handle the underscores, string constants, comment and preprocessor.
	+So we handle the all statements.
	+Namely develop the version to handle the all statements.
	+write the five conditions.
	+First one is our get value double cord in skip the value to next come double cord.
	+Second one Is our get value single  line command in skip the value to next come new line.
	+third one Is our get value multi line command in skip the value to next come close multi command symbol.
	+Fourth one is our get value underscores in skip the next come value for any one white space.
	+Last one condition for Is our get value # is don't skip joint the next word.

		47 is asscii '/'.
*/

#include<stdio.h>
#include<string.h>
#define MAX 100
#define NKEYS (sizeof keytab/sizeof keytab[0])

struct key
{
	char *word;
	int count;
}keytab[]={
		{"#define",0},
		{"#include",0},
		{"auto",0},
		{"break",0},
		{"case",0},
		{"const",0},
		{"continue",0},
		{"default",0},
		{"double",0},
		{"float",0},
		{"int",0},
		{"register",0},
		{"static",0},
		{"switch",0}
	};
		/*{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{}};*/

int get_word(char *s,int lim);
int binsearch(char *s,struct key *,int lim);

#include<ctype.h>
main()
{
	int n=0;
	char word[MAX]={};
	while(get_word(word,MAX)!=EOF)
	{
		if(isalpha(word[0])||word[0]=='#')
		{
			if((n=binsearch(word,keytab,NKEYS))>=0)
				keytab[n].count++;

		}
	}
	for(n=0;n<NKEYS;n++)
		if(keytab[n].count>0)
			printf("%d	%s\n",keytab[n].count,keytab[n].word);
	
}

int binsearch(char *s,struct key tab[],int n)
{
	int cnt=0,low=0,high=n-1,mid=0;
	
	while(low<=high)
	{
		mid=low+(high/2);

		if((cnt=strcmp(s,tab[mid].word))<0)
			high=mid-1;
		else if(cnt>0)
			low=mid+1;
		else
			return mid;
	}
	return -1;
}

int get_ch();
void unget_ch(int c);

int get_word(char *s,int lim)
{
	char c,*w=s,com=0,str=0,c1;
	
	while(isspace(c1=c=get_ch())) ;

	if(c=='_')
		while(!isspace(c=get_ch()));

	if(c=='\"')
		while((c=get_ch())!='\"') ;

	if(c==47&&((c=get_ch())==47||c=='*'))
	{
		if(c==47)
			while((c=get_ch())!='\n');
		else
			while((c=get_ch()))
			{
				if(c=='*'&&(c=get_ch())=='/')
					break;
				else if(c=='*')
					unget_ch(c);
			}
	}
	else if(c1==47)
	{	
		unget_ch(c);
		c='/';
	}

	if(c!=EOF)
		*w++=c;
	if(!isalpha(c)&&c!='#')
	{
		*w='\0';
		return c;
	}
	if(c=='#')
	{
		while((c=get_ch())!='\n')
			*w++=c;
		*w='\0';
		return s[0];
	}
        for(;--lim;w++)
        {
                if(!isalpha(*w=get_ch()))
                {
                        unget_ch(*w);
                        break;
                }
        }
        *w='\0';
	return s[0];
}

char buff[MAX]={},buf=0;

int get_ch()
{
	return buf>0?buff[--buf]:getchar();
}

void unget_ch(int c)
{
	buff[buf++]=c;
}
