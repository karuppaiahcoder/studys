// structure 

#include<stdio.h>

struct point
{
	int x;
	int y;
}p1={5,6},p2={1,2};

struct rect
{
	struct point pt1;
	struct point pt2;
};

struct point makepoint(int x,float y) 
{
	struct point temp;
	temp.x=x;
	temp.y=y;
	return temp;
};


struct point addpoint(struct point p1,struct point p2)
{
	p1.x += p2.x;
	p1.y += p2.y;
	return p1;
};

#define min(a, b) ((a) < (b) ? (a) : (b))
#define max(a, b) ((a) > (b) ? (a) : (b))
struct rect canonrect(struct rect r)
{
	struct rect temp;
	temp.pt1.x = min(5,8);
	temp.pt1.y = min(3,4);
	temp.pt2.x = max(6,0);
        temp.pt2.y = max(1,2);
	printf("%d	%d	%d	%d\n",temp.pt1.x,temp.pt1.y,temp.pt2.x,temp.pt2.y);
	return temp;
}



main()
{
	struct rect screen,r;
	struct point middle,*p;
	p=&middle;
	screen.pt1=makepoint(0,0);
	screen.pt2=makepoint(10,10);

	p1=addpoint(p1,p2);

	p=makepoint((screen.pt1.x + screen.pt2.x)/2,(screen.pt1.y + screen.pt2.y)/2);
			
	printf("%d	%d\n",(*p).x,(*p).y);
	printf("%d	%d\n",p1.x,p1.y);

	r=canonrect(r);
	printf("%d	%d\n",r.pt1.x,r.pt2.y);
}
