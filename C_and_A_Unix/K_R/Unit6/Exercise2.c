//Write a program that reads a C program and prints in alphabetical order each group of variable names that are identical in the first 6 characters, but different somewhere thereafter. Don't count words within strings and comments. Make 6 a parameter that can be set from the command line.

/********************Algorithm********************/
/*
	+That program concept was create the node and tree in use the structure.
	+Then first create the one structure and declare the structure members.
	+Then get the input by get_word function.
	+First get the identical number in march the number of character.
	+Then that value can be set from command line arguments. No put the value in set the default value 6.
	+Then match the other word in number of character match or non match.
	+Match mean print the alphabetical order.
	+Non match in skip the word.
*/

#include<stdio.h>
#include<ctype.h>
#include<string.h>
#include<stdlib.h>
#define MAX 100

struct tnode
{
	char *word;		
	int count;		
	struct tnode *left;	//left child
	struct tnode *right;	//right child
};

struct tnode *addtree(struct tnode *,char *);
void tree_print(struct tnode *);
int get_word(char *s,int lim);

main(int argc,char *argv[])
{
	struct tnode *root;
	char word[MAX]={},p=0,n=6,temp[MAX]={};

	if(argc==2&&*(argv++))
		n=atoi(argv[0]);
	printf("\nYour identical in the first '%d' characters\n\n",n);

	root='\0';
	while(get_word(word,MAX)!=EOF)
	{
		if(p==0)
			strcpy(temp,word);
		p=str_cmp(temp,word,n);
		if(p>0)
			if(isalpha(word[0]))
				root = addtree(root,word);
	}
	tree_print(root);
}

int str_cmp(char *temp,char *word,int len)//Match the word in occur the first array position.
{
	int i=0;
	while(len-->0)
		if(*temp++!=*word++)
			return -1;
	return 1;
}

struct tnode *talloc(void);
char *str_dup(char *s);

struct tnode *addtree(struct tnode *p,char *w)
{
	int cnt=0;
	
	if(p=='\0')
	{
		p=talloc();
		p->word=str_dup(w);
		p->count=1;
		p->left=p->right='\0';
	}
	else if((cnt = strcmp(w,p->word))==0)
		p->count++;
	else if(cnt < 0)
		p->left = addtree(p->left,w);
	else
		p->right = addtree(p->right,w);
	
	return p;
}

char *str_dup(char *s)
{
	char *p;

	p=(char *) malloc(strlen(s)+1);
	if(p!=NULL)
		strcpy(p,s);
	return p;
}

struct tnode *talloc(void)
{
	return (struct tnode *)malloc(sizeof(struct tnode));
}

void tree_print(struct tnode *p)
{
	if(p!='\0')
	{
		tree_print(p->left);
		printf("%4d	%s\n",p->count,p->word);
		tree_print(p->right);
	}
}

int get_ch();
void unget_ch(int c);

int get_word(char *s,int lim)
{
        char c,c1,*w=s;

        while(isspace(c=get_ch())) ;

 	if(c=='\"')
                while((c=get_ch())!='\"') ;

        if(c==47&&((c=get_ch())==47||c=='*'))
        {
                if(c==47)
                        while((c=get_ch())!='\n');
                else
                        while((c=get_ch()))
                        {
                                if(c=='*'&&(c=get_ch())=='/')
                                        break;
                                else if(c=='*')
                                        unget_ch(c);
                        }
        }
        else if(c1==47)
        {
                unget_ch(c);
                c='/';
        }


        if(c!=EOF)
                *w++=c;
        if(!isalpha(c))
        {
                *w='\0';
                return c;
        }

        for(;--lim;w++)
        {
                if(!isalpha(*w=get_ch()))
                {
                        unget_ch(*w);
                        break;
                }
        }
        *w='\0';
        return s[0];
}

char buff[MAX]={},buf=0;

int get_ch()
{
        return buf>0?buff[--buf]:getchar();
}

void unget_ch(int c)
{
        buff[buf++]=c;
}

