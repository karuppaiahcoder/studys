//Pointer to structure program can use the pointer in structure.


#include <stdio.h>
#include <ctype.h>
#include <string.h>
#define MAX 100

struct key
{
        char *word;
        int count;
}keytab[]={
                {"auto",0},
                {"break",0},
                {"case",0},
                {"const",0},
                {"continue",0},
                {"default",0},
                {"double",0},
                {"float",0},
                {"int",0},
                {"register",0},
                {"static",0},
                {"switch",0}
        };

int getword(char *, int);
struct key *binsearch(char *, struct key *, int);
/* count C keywords; pointer version */
main()
{
	char word[MAX];
	struct key *p;
	while (get_word(word, MAX) != EOF)
		if (isalpha(word[0]))
			if ((p=binsearch(word, keytab, 12)) != NULL)
				p->count++;
	for (p = keytab; p < keytab + 12; p++)
		if (p->count > 0)
			printf("%4d %s\n", p->count, p->word);
	return 0;
}
/* binsearch: find word in tab[0]...tab[n-1] */

struct key *binsearch(char *word, struct key *tab, int n)
{
	int cond;
	struct key *low = &tab[0];
	struct key *high = &tab[n];
	struct key *mid;
	while (low < high) 
	{
		mid = low + (high-low) / 2;
		if ((cond = strcmp(word, mid->word)) < 0)
			high = mid;
		else if (cond > 0)
			low = mid + 1;
		else
			return mid;
	}
	return NULL;
}

int get_ch();
void unget_ch(int c);

int get_word(char *s,int lim)
{
        char c,*w=s;

        while(isspace(c=get_ch())) ;

        if(c!=EOF)
                *w++=c;
        if(!isalpha(c))
        {
                *w='\0';
                return c;
        }

        for(;--lim;w++)
        {
                if(!isalpha(*w=get_ch()))
                {
                        unget_ch(*w);
                        break;
                }
        }
        *w='\0';
        return s[0];
}

char buff[MAX]={},buf=0;

int get_ch()
{
        return buf>0?buff[--buf]:getchar();
}

void unget_ch(int c)
{
        buff[buf++]=c;
}

