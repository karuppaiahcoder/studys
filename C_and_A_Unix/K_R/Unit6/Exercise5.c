//Write a function undef that will remove a name and definition from the table maintained by lookup and install.

/**************************Algorithm**********************/
/*
	+Add the get_word, string duplicate and get_ch and unget_ch function to program.
	+First get the multiple definition and names.

		Example:
				MAX 100
				TABLE 300
				SIZE 40
	+So use the get word function.
	+Then add the values to table I can use the install function in add the definition.
	+Then The install function get the location to store the value or name in the table.
	+First We develop the one structure from create the hash table.
	+Check the all value to store the hash table in structure.

	+All ways perfect in write the one undef function in use for Un-definition  the name or remove the name from hash table.
	+Wrote the one choose the option in add the name to table. Other one options in remove the name to table.
	+This time select the option one is call the install function to add the name from table.
	+select the other one option is call the undef function to remove the name from table.
*/

#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<ctype.h>
#define MAX 100
#define HASH 101
int get_word(char *s,char *t,int lim);
struct nlist *install(char *name,char *defn);
struct nlist *lookup(char *s);
char *str_dup(char *s);
unsigned hash(char *s);
struct nlist *undef(char *s);
static struct nlist *hastab[HASH]={};
struct nlist
{
	struct nlist *next;
	char *name;
	char *defn;
};

main()
{
        int n,i,opt=1;
        char word[MAX]={},defi[MAX]={};
	struct nlist *root;
        while(get_word(word,defi,MAX)!=EOF)
        {
		if(opt==1)
			root=install(word,defi);
		else if(opt==2)
			undef(word);
		else
			printf("Please select the correct option\n");

		printf("\n\n....Your table values....\n\n");
	        for(i=0;i<101;i++)
	                for(root=hastab[i];root!='\0';root=root->next)
	                        printf("#define %s      %s\n\n",root->name,root->defn);
		
		printf("Select the option \n\t1.Add the name to table.\n\t2.Delete the name to table\n");
		scanf("%d",&opt);
	}

}

struct nlist *undef(char *s)
{
        struct nlist *np,*p;

        for(p=np=hastab[hash(s)];np!=NULL;np=np->next)
        {
                if(strcmp(s,np->name)==0)
                {
                        if(np==p)
                                hastab[hash(s)]=NULL;
                        else
                                p->next=np->next;
                        return;
                 }
                else
                        p=np;
        }
}

char *str_dup(char *s);

struct nlist *install(char *name,char *defn)
{
	struct nlist *np;
	unsigned  hashval;
	
	if((np=lookup(name))==NULL)
	{
		np=(struct nlist *)malloc(sizeof(*np));
		if(np==NULL||(np->name=str_dup(name))==NULL)
			return NULL;
		hashval=hash(name);
		np->next=hastab[hashval];
		hastab[hashval]=np;	
	}
	else
		free((void *) np->defn);
	if((np->defn=str_dup(defn))==NULL)
		return NULL;
	return np;
}
char *str_dup(char *s)
{
        char *p;

        p=(char *) malloc(strlen(s)+1);
        if(p!=NULL)
                strcpy(p,s);

        return p;
}


unsigned hash(char *s)
{
	unsigned has;
	
	for(has=0;*s!='\0';s++)
		has=*s+31*has;

	return has%HASH;
}

struct nlist *lookup(char *s)
{
	struct nlist *np;

	for(np=hastab[hash(s)];np!=NULL;np=np->next)
		if(strcmp(s,np->name)==0)
			return np;
	return NULL;
}

int get_ch();
void unget_ch(int c);

int get_word(char *s,char *t,int lim)
{
        char c,*w=s,*x=t,max=lim;

	printf("Enter the variable and value....\n");
        while(isspace(c=get_ch())) ;

        if(c!=EOF)
                *w++=c;
        if(!isalpha(c))
        {
                *w='\0';
                return c;
        }

        for(;--lim;w++)
        {
                if(!isalpha(*w=get_ch()))
                {
                        unget_ch(*w);
                        break;
                }
        }
        *w='\0';

       while(isspace(c=get_ch())) ;

        if(c!=EOF)
                *x++=c;
        if(!isdigit(c))
        {
                *x='\0';
                return c;
        }

        for(;--max;x++)
        {
                if(!isdigit(*x=get_ch()))
                {
                        unget_ch(*x);
                        break;
                }
        }
        *x='\0'; 
	return s[0];
}

char buff[MAX]={},buf=0;

int get_ch()
{
        return buf>0?buff[--buf]:getchar();
}

void unget_ch(int c)
{
        buff[buf++]=c;
}

