//Structure self-referential..

#include<stdio.h>
#include<ctype.h>
#include<string.h>
#include<stdlib.h>
#define MAX 100

struct tnode
{
	char *word;		
	int count;		
	struct tnode *left;	//left child
	struct tnode *right;	//right child
};

struct tnode *addtree(struct tnode *,char *);
void tree_print(struct tnode *);
int get_word(char *s,int lim);

main()
{
	struct tnode *root;
	char word[MAX]={};
	
	root='\0';
	while(get_word(word,MAX)!=EOF)
	{
		if(isalpha(word[0]))
			root = addtree(root,word);
	}
	tree_print(root);
}


struct tnode *talloc(void);
char *str_dup(char *s);

struct tnode *addtree(struct tnode *p,char *w)
{
	int cnt=0;
	
	if(p=='\0')
	{
		p=talloc();
		p->word=str_dup(w);
		p->count=1;
		p->left=p->right='\0';
	}
	else if((cnt = strcmp(w,p->word))==0)
		p->count++;
	else if(cnt < 0)
		p->left = addtree(p->left,w);
	else
		p->right = addtree(p->right,w);
	
	return p;
}

char *str_dup(char *s)
{
	char *p;

	p=(char *) malloc(strlen(s)+1);
	if(p!=NULL)
		strcpy(p,s);
	return p;
}

struct tnode *talloc(void)
{
	return (struct tnode *)malloc(sizeof(struct tnode));
}

void tree_print(struct tnode *p)
{
	if(p!='\0')
	{
		tree_print(p->left);
		printf("%4d	%s\n",p->count,p->word);
		tree_print(p->right);
	}
}

int get_ch();
void unget_ch(int c);

int get_word(char *s,int lim)
{
        char c,*w=s;

        while(isspace(c=get_ch())) ;

        if(c!=EOF)
                *w++=c;
        if(!isalpha(c))
        {
                *w='\0';
                return c;
        }

        for(;--lim;w++)
        {
                if(!isalpha(*w=get_ch()))
                {
                        unget_ch(*w);
                        break;
                }
        }
        *w='\0';
        return s[0];
}

char buff[MAX]={},buf=0;

int get_ch()
{
        return buf>0?buff[--buf]:getchar();
}

void unget_ch(int c)
{
        buff[buf++]=c;
}

