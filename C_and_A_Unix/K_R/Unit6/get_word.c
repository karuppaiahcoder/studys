//Get word program in use structure.

#include<stdio.h>
#include<string.h>
#define MAX 100
#define NKEYS (sizeof keytab/sizeof keytab[0])

struct key
{
	char *word;
	int count;
}keytab[]={
		{"auto",0},
		{"break",0},
		{"case",0},
		{"const",0},
		{"continue",0},
		{"default",0},
		{"double",0},
		{"float",0},
		{"int",0},
		{"register",0},
		{"static",0},
		{"switch",0}
	};
		/*{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{}};*/

int get_word(char *s,int lim);
int binsearch(char *s,struct key *,int lim);

#include<ctype.h>
main()
{
	int n;
	char word[MAX]={};
	while(get_word(word,MAX)!=EOF)
	{
		if(isalpha(word[0]))
		{
			if((n=binsearch(word,keytab,NKEYS))>=0)
				keytab[n].count++;

			printf("%s	%d\n",word,n);
		}
		for(n=0;n<NKEYS;n++)
			if(keytab[n].count>0)
				printf("%d	%s\n",keytab[n].count,keytab[n].word);
	}
}

int binsearch(char *s,struct key tab[],int n)
{
	int cnt=0,low=0,high=n-1,mid=0;
	
	while(low<=high)
	{
		mid=(low+high)/2;

	printf("low=%d	high=%d	mid=%d	%s\n",low,high,mid,tab[mid].word);
		if((cnt=strcmp(s,tab[mid].word))<0)
			high=mid-1;
		else if(cnt>0)
			low=mid+1;
		else
			return mid;

	//printf("low=%d	high=%d	mid=%d	%s\n",low,high,mid,tab[mid].word);
	}
	return -1;
}

int get_ch();
void unget_ch(int c);

int get_word(char *s,int lim)
{
	char c,*w=s;
	
	while(isspace(c=get_ch())) ;

	if(c!=EOF)
		*w++=c;
	if(!isalpha(c))
	{
		*w='\0';
		return c;
	}

	for(;--lim;w++)
	{
		if(!isalpha(*w=get_ch()))
		{
			unget_ch(*w);
			break;
		}
	}
	*w='\0';
	return s[0];
}

char buff[MAX]={},buf=0;

int get_ch()
{
	return buf>0?buff[--buf]:getchar();
}

void unget_ch(int c)
{
	buff[buf++]=c;
}
