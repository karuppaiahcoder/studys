#include<stdio.h>
struct point
{
	char a;		//1 byte
	double b;	//8 byte
	float c;	//4 byte 
}p1;			//total 1+8+4=13

struct point1
{
	double a; 	//8 byte
	float b;	//4 byte
	char c;		//1 byte
}p2;			//total 8+4+1=13

main()
{
	printf("point_size=%d\npoint_1_size=%d\n",sizeof(struct point),sizeof(struct point1));
	/*Can't print the 13 instead of print the 16 why? Please Explain you.*/
	/*
		+The structure memory allocation was dynamically. 
		+So The structure allocate the all variable size.
		+But that memory allocation is dynamic so allocate the low level size variable in max size for between variable size.
		Example:
			This program allocate the double data type variable size in 8 byte.
			Next allocate the integer data type variable 4, Here between data type is int so allocate the character data type variable memory size in 4 byte total structure size in 16.
			It's structure  padding in C.
	*/
}
