#include<stdio.h>
#include<ctype.h>
#define SIZE 100
int get_int(int *x);
int get_ch();
int Unget_ch(char a);
int main()
{
	int array[SIZE]={},n,c;
	for(n=0;n<SIZE&&(c=get_int(&array[n]))!=EOF;n++)
	{
		if(c=='~')
			;
		else
			printf("%d\n",array[n]);
	}
}

int get_int(int *sp)
{
	int c,sign;
	
	while(isspace(c=get_ch()));
	
	if(c!=EOF&&c!='+'&&c!='-'&&!isdigit(c))
	{
		Unget_ch(c);
		return '~';
	}
	sign=(c=='-')?-1:1;
	if(c=='-'||c=='+')
		c=get_ch();
	for(*sp=0;isdigit(c);c=get_ch())
		*sp=(10* *sp)+(c-'0');
	*sp*=sign;
	if(c!=EOF)
		Unget_ch(c);
	else
		return c;
	return 1;
}

int buf=0;
char buff[SIZE]={};

int get_ch()
{
	return getchar();
}

int Unget_ch(char c)
{
	buff[buf++]=c;
}
