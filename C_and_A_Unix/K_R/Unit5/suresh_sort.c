#include<stdio.h>
#include<string.h>
int sort(int s[],int left,int right);
int swap(int s[],int i,int j);

main()
{
	int s[]={5,8,3,1,4,11,9,10,7,2,1,12,1,5,23},i;

	sort(s,0,14);

	for(i=0;i<15;i++)
		printf("%d\n",s[i]);
}

int sort(int s[],int left,int right)
{
	int l=left,r=right,i,k,p=0;
	if(left>right)
		return;
	for(i=left;i<right;i++)
	{
		if(s[l] > s[++l])
			p=swap(s,l-1,l);
		if(s[r] < s[--r])
			p=swap(s,r,r+1);
	}
	if(p)
		sort(s,left+1,right-1);
}

int swap(int s[],int i,int j)
{
	int temp;
	temp = s[i];
	s[i] = s[j];
	s[j] = temp;
	return 1;
}
