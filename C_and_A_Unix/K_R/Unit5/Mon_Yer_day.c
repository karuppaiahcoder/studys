//Calculate the year and month and day.

#include<stdio.h>
int Day(int year,int month,int day);
void Month(int year,int day,int *pmoth,int *pday);

static char ptr[2][13]={{0,31,28,31,30,31,30,31,31,30,31,30,31},{0,31,29,31,30,31,30,31,31,30,31,30,31}};

int main()
{
	int y=2015,d=25,m=2,i=0,pm=0,pd=0;

	if(d>31)
		i=printf("Day only 31, So please Enter the with in 31\n");
	if(m>12)
		i=printf("Month only 12, So please Enter the with in 12\n");

	if(!i)
	{
		if((i=Day(y,m,d))>0)
		{
			printf("%d\n",i);
			Month(2015,366,&pm,&pd);
			printf("%d\t%d\n",pm,pd);
		}
	}

}

int Day(int year,int month,int day)
{
	int i,leap,day1;

	leap=year%4==0&&year%100!=0||year%400==0;

	day1=ptr[leap][month];

	if(day1>=day)
	{
		for(i=1;i<month;i++)
			day+=ptr[leap][i];
		return day;
	}
	else
		printf("Error\n");
	return 0;
}

void Month(int year,int day,int *pmonth,int *pday)
{
	int i,leap,day1;

        leap=year%4==0&&year%100!=0||year%400==0;

	if(leap)
		day1=366;
	else
		day1=365;
	if(day1>=day)
	{
		for(i=1;day > ptr[leap][i];i++)
				day-=ptr[leap][i];

		*pmonth=i;
		*pday=day;
	}
	else
		printf("Error\n");
}
