//Make dcl recover from input errors.

/**************************Algorithm*********************/
/*
	+Only error recover program so first understand the program and functions.
	+The main function is call the two sub functions.
	+The get_token function is get the tokens is one by one.
	+That function keep the token is three method. First method is get the name of the tokens.
	+Data, variable name and storage class in all tokens is get the name method.
	+Next method was get the parenthesis called "()" .
	+Next method was get the brackets in called "[]".
	+Other tokens in return the direct in the function.

	+I recover the three error in the program.
	+First one skip the parenthesis missing message and next one skip the brackets missing message.
*/

#include<stdio.h>
#include<string.h>
#include<ctype.h>
#define MAX 1000

enum {NAME,PARENS,BRACKETS};

void dcl(void);
void dirdcl(void);
int get_token(void);

int token_type,p,a;
char token[MAX];
char name[MAX];
char data_type[MAX];
char out[MAX];
int get_ch();
void unget_ch(int c);

char def[MAX]="value";
main()
{
	
	while(get_token() !=EOF)
	{
		if(token_type=='\n' && token[0]=='\0')
			break;

//		printf("main:>%s\n",token);
		strcpy(data_type,token);	
		out[0]='\0';
		dcl();
		if(token_type!='\n')
			printf("Syntax Error!!!!\n");

		printf("%s: %s %s\n", name, out, data_type);

		token[0]='\0';
	}	
}

void dirdcl(void)
{
	int type,i;

//		printf("dir:>%s\n",token);
	if(token_type == '(')
	{
		dcl();
		if(token_type !=')')
			printf("error: missing )\n");
	}
	else if(token_type==NAME)
	{
		if(token_type==NAME)
			strcpy(name,token);
	}
	else
	{
		printf("error: expected name or (dcl)\n");
		strcpy(name,def);		
		if(token_type=='[')
			for(i=strlen(token)-1;i>=0;i--)
				unget_ch(token[i]);
                        strcat(out," array");
                        strcat(out,token);
                        strcat(out," of");
	}
	
	while(type==BRACKETS||(type=get_token())==PARENS||type==BRACKETS||type=='('||type=='['||type==';')
	{
  //              printf("dir:>%s 	type=%c\n",token,type);
		if(type=='[')
		{
			printf("error: missing ]\n");
			strcat(token,"]");
		}
		if(type=='(')
		{
                        printf("error: missing )\n");
			type=PARENS;
		}
		if(type==PARENS)
			strcat(out," function returning");
		else if(type!=';')
		{
			strcat(out," array");
			strcat(out,token);
			strcat(out," of");
		}
	}
}

void dcl(void)
{
	int i;
	
	for(i=0;get_token() == '*';i++);
//	printf("dcl:>%s\n",token);
	dirdcl();
	while(i-->0)
		strcat(out," pointer to");
}


int get_token(void)
{
	int c;
	char *p=token;

	while((c=get_ch())==' '||c=='\t');

	if(c ==	'(')
	{
		if((c=get_ch())==')')
		{
			strcpy(token,"()");
			return token_type=PARENS;
		}
		else
		{
			unget_ch(c);
			return token_type='(';
		}
	}
	else if(c == '[')
	{
		for(*p++=c;(*p++=c=get_ch())!=']'&&c!='\n'&&c!=EOF;);
	
		*p='\0';
		if(c!=']')
		{
			unget_ch(c);	
			if(c=='\n')
				*(p-1)='\0';
			return token_type='[';
		}
		return token_type=BRACKETS;
	}
	else if(isalpha(c))
	{
		for(*p++=c;isalnum(c=get_ch());)
			*p++=c;
		*p='\0';
		unget_ch(c);
		return token_type=NAME;
	}
	else
	{
		return token_type=c;
	}
}



int buf=0;
char buff[MAX];

int get_ch()
{
	return (buf>0)?buff[--buf]:getchar();
}

void unget_ch(int c)
{
	buff[buf++]=c;
}
