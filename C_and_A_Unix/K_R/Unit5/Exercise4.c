//Write the function strend(s,t), which returns 1 if the string t occurs at the end of the string s, and zero otherwise.
/****************************Algorithm********************************/
/*
	+Get the two strings it one string is a pattern. 
	+Check the pattern in place the other one string which place is trailing.
	+Place in return value for 1 Not in return value for zero.
	+First write the string end function , That function usage search the pattern is other string.
*/

#include<stdio.h>
#define MAX 50
int strend(char *s,char *p);
main()
{
	char str1[MAX]={},pattern[MAX]={};
	int i=0,j=0,c;

	printf("Enter the string:\n");
	while((c=getchar())!='\n')
		str1[i++]=c;

	printf("Enter the search pattern:\n");
	while((c=getchar())!='\n')
		pattern[j++]=c;

	printf("Call the string end function search the trailing pattern:\n");
	
	i=strend(str1,pattern);
	printf("%d\n",i);
		
}

int strend(char *s,char *p)
{
	char *source=p;
	while(*s++);    //This time pointer go on destination address for string.

	while(*source++); //This time pointer go on destination address for pattern.

	while(source>=p) 
		if(*source--!=*s--) /*This process check the pattern non match in trailing place. Non 
					match in return value for zero.*/
			return 0;
	return 1;			//perfect match in return value for one.
}
