//Write a pointer version of the function strcat that we showed in Chapter 2: strcat(s,t) copies the string t to the end of s.

/*****************************************Algorithm****************************/
/*
	+The default function string cat is joint the two string to one string, and not affect the second array.
	+I same function write bur not use the array it other one variable use is pointer.
	+That pointer variable act the array.
	+First  calculate the first array length to end of address to point the first pointer variable.
	+This time That variable point destination address. Next joint the second pointer value to first pointer. 
	+Namely not affect the second pointer value.
*/

#include<stdio.h>
#define MAX 50
void str_cat(char *s,char *p);
int main()
{
	char str1[MAX]={},str2[MAX]={};
	int i=0,j=0,c;

	printf("\tString cat function\n1.Enter the first string:\n");
	while((c=getchar())!='\n')
		str1[i++]=c;

	printf("2.Enter the second string:\n");
	while((c=getchar())!='\n')
		str2[j++]=c;

	printf("\nCall the string cat function to joint the two string:\n");

	str_cat(str1,str2);
	printf("%s\n",str1);
}

void str_cat(char *s,char *p)
{
	if(*s)
		while(*++s);

	while(*s++=*p++);
}
