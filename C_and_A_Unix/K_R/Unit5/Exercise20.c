//Expand dcl to handle declarations with function argument types, qualifiers like const, and so on.

/**************************Algorithm*********************/
/*
	+The get_token function is get the tokens is one by one.
        +That function keep the token is three method. First method is get the name of the tokens.
        +Data, variable name and storage class in all tokens is get the name method.
        +Next method was get the parenthesis called "()" .
        +Next method was get the brackets in called "[]".
        +Other tokens in return the direct in the function.

	+That program concept is write the program in function argument word description.
	+first handle the qualifiers in register and conts and auto.
	+Next check the variable type and array or pointer.

*/

#include<stdio.h>
#include<string.h>
#include<ctype.h>
#define MAX 1000

enum {NAME,PARENS,BRACKETS};

void dcl(void);
void dirdcl(void);
int get_token(void);

int token_type,p,a;
char token[MAX];
char name[MAX];
char data_type[MAX];
char out[MAX],OUT[MAX];
int get_ch();
void unget_ch(int c);

char def[MAX]="value";
main()
{
	while(get_token() !=EOF)
	{
		if(token_type=='\n' && token[0]=='\0')
			break;

		strcpy(data_type,token);	
		out[0]='\0';
		dcl();
		if(token_type!='\n')
			printf("Syntax Error!!!!\n");

		printf("%s:%s %s %s\n", name,OUT,out, data_type);

		name[0]=data_type[0]=out[0]=OUT[0]=token[0]='\0';
	}	
}

void dirdcl(void)
{
	int type,i;

	if(token_type == '(')
	{
		dcl();
		if(token_type !=')')
			printf("error: missing )\n");
	}
	else if(token_type==NAME)
	{
		if(token_type==NAME)
			strcpy(name,token);
	}
	else
	{
		printf("error: expected name or (dcl)\n");
		strcpy(name,def);		
		if(token_type=='[')
			for(i=strlen(token)-1;i>=0;i--)
				unget_ch(token[i]);
                        strcat(out," array");
                        strcat(out,token);
                        strcat(out," of");
	}
	
	char qual[MAX]={},out1[MAX]={};
	char data1[MAX]={},var[MAX]={};
	char poin[MAX]={},qual1=0,temp[MAX]={};

	while((type=get_token())==PARENS||type==BRACKETS||type=='('||type=='['||type==';')
	{
		if(type=='[')
		{
			printf("error: missing ]\n");
			strcat(token,"]");
		}
		if(type=='(')
		{
	  	   P:	while((type=get_token())==NAME)
			{
				printf("%d\n",type);
				if(strcmp(token,"register")==0&&!qual1)
				{
					strcpy(qual,token);
					strcat(qual," is qualifier of "); 
					qual1=1;
				}
				else if(strcmp(token,"const")==0&&!qual1)
				{
					strcpy(qual,token);
					strcat(qual," is qualifier of ");
					qual1=1;
				}
				else if(strcmp(token,"void")==0&&!qual1)
				{
					strcpy(qual,token);
					strcat(qual," is qualifier of ");
					qual1=1;
				}
				else
				{
					if(!qual1)
					{
						strcpy(qual,"auto qualifier of ");
						qual1=1;
					}
					strcpy(data1,token);
				    while((type=get_token())==NAME||type=='*'||type==BRACKETS||type==',')
					{
						if(type==NAME)
							strcpy(var,token);
						else if(type==BRACKETS||type=='[')
		                                {
		                                        if(type=='[')
		                                        {
		                                                printf("error: missing ]\n");
		                                                strcat(token,"]");
		                                        }
		                                        strcat(out1," array");
		                                        strcat(out1,token);
		                                }
						else if(type=='*')
							strcat(poin," pointer to");
					if(type==',')
					{
						sprintf(temp,"%s %s %s%s argument type is %s ",qual,var,poin,out1,data1);
						strcat(OUT,temp);
						goto P;
					}
					}
					 if(type==')')
                                         {  
                                                type==PARENS;
						break;
					 }

				}
			}
			sprintf(temp,"%s %s %s%s argument type is %s ",qual,var,poin,out1,data1);
			strcat(OUT,temp);
			if(type!=')')
        		{
	                	printf("error: missing )\n");
				type=PARENS;
			}
			else	
				type=PARENS;
		}
		if(type==PARENS)
			strcat(out," function returning");
		else if(type!=';')
		{
			strcat(out," array");
			strcat(out,token);
			strcat(out," of");
		}
	}
}

void dcl(void)
{
	int i;
	
	for(i=0;get_token() == '*';i++);
	
	dirdcl();
	while(i-->0)
		strcat(out," pointer to");
}


int get_token(void)
{
	int c;
	char *p=token;

	while((c=get_ch())==' '||c=='\t');

	if(c ==	'(')
	{
		if((c=get_ch())==')')
		{
			strcpy(token,"()");
			return token_type=PARENS;
		}
		else
		{
			unget_ch(c);
			return token_type='(';
		}
	}
	else if(c == '[')
	{
		for(*p++=c;(*p++=c=get_ch())!=']'&&c!='\n'&&c!=EOF;);
	
		*p='\0';
		if(c!=']')
		{
			unget_ch(c);	
			if(c=='\n')
				*(p-1)='\0';
			return token_type='[';
		}
		return token_type=BRACKETS;
	}
	else if(isalpha(c))
	{
		for(*p++=c;isalnum(c=get_ch());)
			*p++=c;
		*p='\0';
		unget_ch(c);
		return token_type=NAME;
	}
	else
	{
		return token_type=c;
	}
}



int buf=0;
char buff[MAX];

int get_ch()
{
	return (buf>0)?buff[--buf]:getchar();
}

void unget_ch(int c)
{
	buff[buf++]=c;
}
