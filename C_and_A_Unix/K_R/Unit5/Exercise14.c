//Modify the sort program to handle a -r flag, which indicates sorting in reverse (decreasing) order. Be sure that -r works with -n.

/**********************Algorithm*******************/
/*

	+First get the more then lines and character or numbers.
	+That process in use the get line function and read line function.
	+That function in get the line one by one and store the get line in one by one in the pointer array.
	+The program main point is pass the function address to Q_sort function.
	+The program was sort the numeric or alpha order.
	+The program select the any one order in get the option is command line arguments.
	+-n is sort the numeric order.
	+No arguments in sort the alpha order.

	+Then use the to function in string compare and numeric compare function.
	+Take the two function in use the sorting order.
	
	+Next we develop the same function in command line argument value -r in sort the revers order.
	+First check the argument count and check the argument value -r is true in sort the revers order.
	+It's use the one static global variable . That variable value 1 is sort the reverse order.
	+That variable only increment the command line value -r. 
*/

#include<stdio.h>
#include<string.h>
#define MAX 2000

int Read_line(char *ptr[],int line);
void Write_line(char *ptr[],int line);
void q_sort(void *ptr[],int left,int right,int (*comp)(void*,void*));
int num_cmp(char *s,char *p);
int get_line(char s[],int lim);
int str_cmp(char *s,char *p);
char *ptr[MAX]={};
static int revers=0;
main(int argc,char *argv[])
{
	int nline,numeric=0;
	
	if(argc>1)
		if(argc==2 && strcmp(argv[1],"-n")==0)
			numeric=1;
		else if(argc==3 && strcmp(argv[2],"-n")==0)
			numeric=1;
	if(argc>1 && strcmp(argv[1],"-r")==0)
	{
		revers=1;
	}

	if((nline=Read_line(ptr,MAX))>=0)
	{
		q_sort((void **)ptr,0,nline-1,(int (*)(void*,void*))(numeric?num_cmp:str_cmp));
		Write_line(ptr,nline);
	}
	else
		printf("Input too big to sort\n");
}

int str_cmp(char *s,char *p)
{
        int i;
        for(;*p==*s;s++,p++)
	{
                if(*s=='\0')
                        return 0;
	}
        return (*s-*p)<0?-1:1;
}

static char str[MAX]={};
static char *buf=str;

int Read_line(char *line[],int lim)
{
        int lin=0,len,i=0;
        char *p;
        while((len=get_line(str,MAX))>1)
        {
                if((str+MAX-buf)>=len)
                {
                        buf+=len;
                        p=buf-len;
                }
                else
                        p=0;

                if((p)>0&&i<MAX)
                        ptr[i++]=p;
                else
                        return -1;
        }
        return i;
}


int get_line(char *s,int lim)
{
        int c;
        s=buf;
        char *p=s;

        while((c=getchar())!='\n'&&c!=EOF)
                *s++=c;

        *s++='\0';
        return (s-p);
}

void Write_line(char *line[],int lim)
{
        int i=0;
        for(i=0;i<lim;i++)
                printf("\t%s\n",line[i]);
}

#include<stdlib.h>

int num_cmp(char *s1,char *s2)
{
	double v1,v2;

	v1=atof(s1);
	v2=atof(s2);

	if(v1<v2)
		return -1;
	else if(v1>v2)
		return 1;
	else
		return 0;
}

void swap(void *v[],int i, int j)
{
	void *temp;
	temp=v[i];
	v[i]=v[j];
	v[j]=temp;
}

void q_sort(void *v[],int left,int right,int (*comp)(void *,void *))
{
	int i,last;
	
	if(left>=right)
		return;
	swap(v,left,(left+right)/2);

	last=left;

	for(i=left+1;i<=right;i++)
		if(revers==1)
		{
			if((*comp)(v[i],v[left])>0)
				swap(v,++last,i);
		}
		else
		{
			if((*comp)(v[i],v[left])<0)
				swap(v,++last,i);
		}

	swap(v,left,last);

	q_sort(v,left,last-1,comp);
	q_sort(v,last+1,right,comp);
}
