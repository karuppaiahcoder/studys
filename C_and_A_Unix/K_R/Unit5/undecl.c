/**************************Algorithm*********************/
/*
	+
*/

#include<stdio.h>
#include<string.h>
#include<ctype.h>
#define MAX 1000

enum {NAME,PARENS,BRACKETS};


int get_token(void);

int token_type;
char token[MAX];
char name[MAX];
char out[MAX];

main()
{

	int type;
	char temp[MAX];
	while (get_token() != EOF) 
	{
		strcpy(out, token);

		while ((type = get_token()) != '\n')
			if (type == PARENS || type == BRACKETS)
				strcat(out, token);
			else if (type == '*') 
			{
				sprintf(temp, "(*%s)", out);
				strcpy(out, temp);
			} 
			else if (type == NAME) 
			{
				sprintf(temp, "%s %s", token, out);
				strcpy(out, temp);
			} 
			else
				printf("invalid input at %s\n", token);

		printf("%s\n",out);
	}
}


int get_ch();
void unget_ch(int c);

int get_token(void)
{
	int c;
	char *p=token;

	while((c=get_ch())==' '||c=='\t');

	if(c ==	'(')
	{
		if((c=get_ch())==')')
		{
			strcpy(token,"()");
			return token_type=PARENS;
		}
		else
		{
			unget_ch(c);
			return token_type='(';
		}
	}
	else if(c == '[')
	{
		for(*p++=c;(*p++=get_ch())!=']';) ;
	
		*p='\0';
		return token_type=BRACKETS;
	}
	else if(isalpha(c))
	{
		for(*p++=c;isalnum(c=get_ch());)
			*p++=c;
		*p='\0';
		unget_ch(c);

		return token_type=NAME;
	}
	else
		return token_type=c;
}



int buf=0;
char buff[MAX];

int get_ch()
{
	return (buf>0)?buff[--buf]:getchar();
}

void unget_ch(int c)
{
	buff[buf++]=c;
}
