/**************************Algorithm*********************/
/*
	+
*/

#include<stdio.h>
#include<string.h>
#include<ctype.h>
#define MAX 1000

enum {NAME,PARENS,BRACKETS};

void dcl(void);
void dirdcl(void);
int get_token(void);

int token_type,p;
char token[MAX];
char name[MAX];
char data_type[MAX];
char out[MAX];

char def[MAX]="value";
main()
{
	
	while(get_token() !=EOF)
	{
		strcpy(data_type,token);	
		out[0]='\0';
		dcl();
		if(token_type!='\n')
			printf("Syntax Error!!!!\n");
		if(strcmp(name,def)==0&&p)
			printf("error:missed variable name add the default variable name.\n",p=0);
		printf("%s: %s %s\n", name, out, data_type);
	}	
}

void dirdcl(void)
{
	int type,a=0;

	if(token_type == '(')
	{
		dcl();
		if(token_type !=')')
			printf("error: missing )\n");
	}
	else if(token_type==NAME||token_type==BRACKETS)
	{
		if(token_type==NAME)
			strcpy(name,token);
		else if(token_type==BRACKETS)
		{
			strcpy(name,def);
			p=a=1;
		}
	}
	else
	{
		strcpy(name,def);
		p=1;
	}
	
	while((token_type==BRACKETS&&a)||(type=get_token())==PARENS||type==BRACKETS)
	{
		if(type==PARENS)
			strcat(out," function returning");
		else
		{
			strcat(out," array");
			strcat(out,token);
			strcat(out," of");
		}
		if(token_type==BRACKETS&&a==1)
		{
			token_type=0;
			a=0;
		}
	}
}

void dcl(void)
{
	int i;
	
	for(i=0;get_token() == '*';i++);
	  
//	printf("%d\n",i);

	dirdcl();
	while(i-->0)
		strcat(out," pointer to");
}

int get_ch();
void unget_ch(int c);

int get_token(void)
{
	int c;
	char *p=token;

	while((c=get_ch())==' '||c=='\t');

	if(c ==	'(')
	{
		if((c=get_ch())==')')
		{
			strcpy(token,"()");
			return token_type=PARENS;
		}
		else
		{
			unget_ch(c);
			return token_type='(';
		}
	}
	else if(c == '[')
	{
		for(*p++=c;(*p++=get_ch())!=']';) ;
	
		*p='\0';
		return token_type=BRACKETS;
	}
	else if(isalpha(c))
	{
		for(*p++=c;isalnum(c=get_ch());)
			*p++=c;
		*p='\0';
		unget_ch(c);

		return token_type=NAME;
	}
	else
		return token_type=c;
}



int buf=0;
char buff[MAX];

int get_ch()
{
	return (buf>0)?buff[--buf]:getchar();
}

void unget_ch(int c)
{
	buff[buf++]=c;
}
