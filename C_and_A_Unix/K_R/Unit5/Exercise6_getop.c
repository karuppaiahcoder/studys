
/********Algorithm**********/
/*
	+get_op() function:

		-This function get the value one bye one.
		-Return value for separate the integer and character and others.
		-This function terminate take the value End of the file.
*/

#include<stdio.h>
#define MAX 100
#define NUMBER 0
int getop(char *s);
int getch();
int ungetch(char c);
main()
{
	char str1[MAX]={},type;
	
	while((type=getop(str1))!=EOF)
	{
		printf("%s\n",str1);
	}

}

int getop(char *s)
{
	int c;

	while((*s=c=getch())==' '||c=='\t');

	*++s='\0';

	if(!isdigit(c)&&c!='.')
		return c;

	if(isdigit(c))
		while(isdigit(*s++=c=getch()));
	if(c=='.')
		while(isdigit(*s++=c=getch()));

	*--s='\0';
	if(c!=EOF)
		ungetch(c);
	return NUMBER;
}

char s[MAX]={};
char *buff=s;

int getch()
{
	return (*buff-1)>0?*--buff:getchar();
}

int ungetch(char c)
{
	*buff++=c;
}
