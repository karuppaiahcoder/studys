#include<stdio.h>
#define ALLOCSIZE 10000 
static char allocbuf[ALLOCSIZE];
static char *allocp = allocbuf; 
char *alloc(int n);
void afree(char *p); 

int main()
{
	char *p;
	int n=10;
	printf("starting %u\n",allocbuf);
	printf("n %d , return %u\n",n,p=alloc(n));
	printf("allocp %u\n",allocp);

	afree(p);
	printf("allocp %u\n",allocp);
}

char *alloc(int n)
{
	if (allocbuf + ALLOCSIZE - allocp >= n) 
	{ 
		allocp += n;
		return allocp - n; 
	} 
	else
		return 0;
}

void afree(char *p) 
{
	if (p >= allocbuf && p < allocbuf + ALLOCSIZE)
		allocp = p;
}
