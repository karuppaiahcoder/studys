//Write the program expr, which evaluates a reverse Polish expression from the command line, where each operator or operand is a separate argument. For example,
/*****************Algorithm****************/
/*
	+Write the revers policy calculator with command line arguments.
	+First get the in put format is revers policy format.
	+Check the number or operator or other character.
	+Number in convert the character to double or float value and push the stack.
	+Operator in get the two stack value in use the pop function. Then continue process to operator process.
	+Then other character in print the error message.
	+Important work for any time check the stack memory size. Size full in print the error message.
*/

#include<stdio.h>
#include<stdlib.h>
#define MAX 1000
void push(double n);
double pop();

main(int argc,char *argv[])
{
	int i=0;
	double n;
	while(--argc>0&&*++argv)
	{
		extern int buf;
		switch(**argv)
		{
			case '0'...'9':  		//Check the value 0,1,2,3,4,5,6,7,8 and 9.
				push(atof(*argv));
				break;
			case '+':
				push(pop()+pop());
				break;
			case '-':
				if(buf>=2)
				{
					n=pop();
					push(pop()-n);
				}
				break;
			case '/':
				n=pop();
				if(n>0)
					push(pop()/n);
				else
					printf("Invalid divider operation\n");
				break;
			case 'x':
				n=pop();
				push(pop()*n);
				break;
			default:
				printf("unknown operator");
				break;
		}
	}
	printf("%f\n",pop());
}

double buff[MAX]={};
int buf=0;

void push(double n)
{
//	printf("%f\n",n);
	if(buf<MAX)
		buff[buf++]=n;
	else
		printf("Stack full Can't push the value %f\n",n);
}
 
double pop()
{
	if(buf>0)
		return buff[--buf];
	else
		printf("Stack empty......\n");
	return 0;
}

