//Write getfloat, the floating-point analog of getint. What type does getfloat return as its function value?

#include<stdio.h>
#include<ctype.h>
#define SIZE 100
double get_int(double *x);
int get_ch();
int Unget_ch(char a);
int main()
{
	double array[SIZE]={};
	int c,n;
	printf("Enter the double values:\n");
	for(n=0;n<SIZE&&(c=get_int(&array[n]))!=EOF;n++)
	{
		if(c=='~')
			;
		else
			printf("%.10f\n",array[n]);
	}
}

double get_int(double *sp)
{
	int c;
	double power=1.0,sign;
	static double posi=0,neg=0;

	while(isspace(c=get_ch()));
	
	if(c!=EOF&&c!='+'&&c!='-'&&!isdigit(c))
	{
		Unget_ch(c);
		return '~';
	}

	sign=(c=='-')?-1.0:1.0;

	if(posi)
	{
		sign=1*sign;
		posi=0;
	}
	else if(neg)
	{
		sign=-1*sign;
		neg=0;
	}

	if(c=='-'||c=='+')
		c=get_ch();

	for(*sp=0;isdigit(c);c=get_ch())
		*sp=(10*(*sp))+(c-'0');

	//printf("%.10f\n",*sp);

	if(c=='.')
	{
		c=get_ch();

		for(power=1.0;isdigit(c);c=get_ch())
		{
			*sp=(10*(*sp))+(c-'0');
			power*=10;
		}
	}

	//printf("%.10lf	pow=%.10lf\n",*sp,power);

	*sp=sign* *sp/power;

	if(c!=EOF)
	{
		if(c=='-')
			neg=-1;
		else if(c=='+')
			posi=1;
		Unget_ch(c);
	}
	else
		return c;
	return 1;
}

int buf=0;
char buff[SIZE]={};

int get_ch()
{
	return getchar();
}

int Unget_ch(char c)
{
	buff[buf++]=c;
}
