
/*
* calculate the string length so write the string length function return type is integer.
* compare the two string, So write the string compare function return type us integer.
* copy the string to string, So write the string copy function return type is void.
*/

#include<stdio.h>
void str_cpy(char *s,char *p);
int str_len(char *s);
int str_cmp(char *s,char *p);
int str_cat(char *s,char *p);
#define MAX_SIZE 50
main()
{
	char str1[MAX_SIZE]="I am string";
	char str2[MAX_SIZE]=" I am string";
	char str3[MAX_SIZE]="I am array";

	str_cpy(str1,str3);
		printf("%s\n",str1);
	printf("%d\n",str_len(str2));

	printf("%d\n",str_cmp(str1,str3));

	str_cat(str1,str2);
		printf("%s\n",str1);
}

void str_cpy(char *s,char *p)
{
	while(*s++=*p++);  //This time assign the p value to s. Then increase the location. 
			   //This loop terminate the p variable point the null '\0' value.
}

int str_len(char *s)
{
	char *p=s;     //This time copy the pointer first address to *p pointer. Next check the value No
                       //equal to null.
	while(*p++);   

	return p-s;    //Return the old address to new address subtraction value.
}

int str_cmp(char *s,char *p)
{
	int i;

	for(;*p==*s;s++,p++)
		if(*s=='\0')
			return 0;
	return (*s-*p)<0?-1:1;
}

int str_cat(char *s,char *p)
{
	while(*++s);

	while(*s++=*p++);
}
