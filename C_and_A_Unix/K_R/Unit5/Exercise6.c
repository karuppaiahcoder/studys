//Rewrite appropriate programs from earlier chapters and exercises with pointers instead of array indexing. Good possibilities include getline (Chapters 1 and 4), atoi, itoa, and their variants (Chapters 2, 3, and 4), reverse (Chapter 3), and strindex and getop (Chapter 4).


/************************************Algorithm**********************************/
/*
	-All ready find the all function but rewrite the all function with use the pointer.

	+Get line function:
		-This function get the one line. 
		-It function terminate the input value for new line character.

	+Integer to character function:
		-This function convert the character value to integer value.
		-Then Handle the negative symbols.

	+character to integer function:
		-This function convert the integer to character.
		-Then handle the longest negative value.

	+Reverse function:
		-This function only usage for reverse the string.
		-That function revers the all string.

	+String index function:
		-This function take the two arguments.
		-One of the argument is string second one patter or index string.
		-This function match the all string to pattern.
		-Pattern perfect match in return the matching start location. Non match in return negative value -1.

	+Get_top function:
		-

*/

#include<stdio.h>
#include<ctype.h>
#define MAX 100
int get_line(char *s,int p);
int atoi(char *s);
int itoa(int a,char *s);
int reverse(char *s);
int str_index(char *s,char *p);
main()
{
	char str1[MAX]={},str2[MAX]={};
	int num,i;
	printf("Enter the integer numbers\n");
	while((get_line(str1,MAX))>0)	//Call the get line function.
	{
		num=atoi(str1);		//Call the character to integer conversion function.
		printf("Convert the character to integer= %d\n",num);

		itoa(num,str2);		//Call the integer to character conversion function.
		printf("Convert the integer to character reverse order= %s\n",str2);

		reverse(str2);		//Call the reverse function.
		printf(" %s\n",str2);

		char array1[MAX]={},pattern[MAX]={};

		printf("Enter the string\n");
			get_line(array1,MAX);

		printf("Enter the string search index\n");
			get_line(pattern,MAX);

		if((num=str_index(array1,pattern))>=0)	//Call the string index function.
			printf("Index match position = %d\n",num);
		else
			printf("Index can't match = %d\n",num);

		for(i=0;str1[i]!='\0';i++)		//Overt write the all string location to null.
			str1[i]=str2[i]='\0';

		printf("Enter the integer numbers\n");
	}

}


int str_index(char *s,char *p)
{
	char *t,*k,*i=s;

	for(;*s!='\0';s++)
		for(t=s,k=p;*k==*t;k++,t++)
			if((k-p)>0&&*k=='\0')
				return  s-i;
	return -1;
}


int get_line(char *s,int lim)
{
	int c;

	while(lim-->0&&(c=getchar())!=EOF&&c!='\n')
		*s++=c;
	if(c=='\n')
		*s++=c;
	*s='\0';
	return *(s-1);
}

int atoi(char *s)
{
	int num,sign;

	while(isspace(*s))
		s++;

	sign=*s=='-'?-1:1;

	if(*s=='-'||*s=='+')
		s++;

	for(num=0;isdigit(*s);s++)
		num=10*num+(*s-'0');
	
	return sign*num;
}

int itoa(int a,char *s)
{
	int sign;
	
	sign=(a<0)?-1:1;

	do
	{
		*s++=(a%10) * sign +'0';
	}
	while((a/=10));

	if(sign<0)
		*s++='-';
}

#include<string.h>
int reverse(char *s)
{
	int j=strlen(s)-1,i,temp;
	for(i=0;j>i;i++,j--)
	{
		temp=*(s+i);
		*(s+i)=*(s+j);
		*(s+j)=temp;
	}
}
