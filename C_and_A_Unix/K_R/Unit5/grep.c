//write the grep program.

#include<stdio.h>
#include<string.h>
#define MAX 100
int get_line(char *s,int lim);
main(int argc,char *argv[])
{
	char line[MAX]={};
	
	if(argc!=2)
		printf("Usage : Find pattern\n");
	else
	{
		while(get_line(line,MAX)>0)	
			if(strstr(line,argv[1])!=NULL)
				printf("%s",line);
	}
}


int get_line(char *s,int lim)
{

	char *p=s;
	while(--lim>0&&(*s++=getchar())!='\n');

	*s='\0';	
	return s-p;
}
