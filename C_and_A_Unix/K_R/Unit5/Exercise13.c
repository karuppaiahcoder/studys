/*Write the program tail, which prints the last n lines of its input. By default, n is set to 10, let us say, but it can be changed by an optional argument so that
				tail -n
prints the last n lines. The program should behave rationally no matter how unreasonable the input or the value of n. Write the program so it makes the best use of available storage; lines should be stored as in the sorting program of Section 5.6, not in a two-dimensional array of fixed size. */

/***********************Algorithm*********************/
/*
	+Write the tail command function.
	+That function print the last number of lines. That line get the command line arguments.
	+So first get the multi lines, in use the get line function and read line function.
	+which function in get line get the line one by one. Then The read line function store the line one by one.
	+finally check the line and print the tail lines.
	+Ofter check the tail value up to read lines. It's like been the tail value store the default value read line count.
	+If don't put the tail value in take the default value 10. But tail command is must.
*/


#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#define LINE 100
#define MAX 10000
int Read_line(char *a[],int lim);
void Write_line(char *s[],int tail,int line);
int get_line(char *s,int lim);
char *line[LINE]={};
main(int argc,char *argv[])
{
	int lin=0,i,tail=-1;
	if(argc==3)
	{
		if(*(++argv)[0]=='-')
			if(tail*=atoi(*argv));
	}
	else if(argc==2)
	{
		if(strcmp(*(++argv),"tail")==0)
			tail*=-10;
	}
		
	if(tail>0)
	{
		while((lin=Read_line(line,LINE))>0)
		{
			if(tail<lin)
				tail=lin-tail;
			else if(tail>=lin)
				tail;

			Write_line(line,tail,lin);
		}
	}
	else
		printf("Please Enter the perfect format tail command \n\tExample: tail -10\n");
}

void Write_line(char *s[],int tail,int line)
{
	int i=0;

	printf("Tail lines:\n\n");
	for(;tail<line;tail++)
		printf("\t%s\n",s[tail]);
}

static char str[MAX]={};
static char *buf=str;

int Read_line(char *ptr[],int lim)
{
	int len=0,i=0;
	char *p;
	while((len=get_line(str,lim))>1)
	{
		if((str+MAX-buf)>len)
		{
			buf+=len;
			p=buf-len;
		}	
		else 
			p=0;
		if(p>0&&i<lim)
			ptr[i++]=p;
		else
		{
			printf("Line over\n");
			return -1;
		}
		

	}
	return i;
}

int get_line(char *s,int lim)
{
	int c;
	s=buf;	
	char *p=s;
	while((c=getchar())!='\n'&&c!=EOF)
		*s++=c;	
	*s++='\0';
	return (s-p);
}
