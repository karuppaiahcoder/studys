//Add the -d (``directory order'') option, which makes comparisons only on letters, numbers and blanks. Make sure it works in conjunction with -f.


/***********************Algorithm*******************/
/*

	+First get the more then lines and character or numbers.
	+That process in use the get line function and read line function.
	+That function in get the line one by one and store the get line in one by one in the pointer array.
	+The program main point is pass the function address to Q_sort function.
	+The program was sort the numeric or alpha order.
	+The program select the any one order in get the option is command line arguments.
	+ (-N) is sort the numeric order.
	+No arguments in sort the alpha order.

	+Then use the to function in string compare and numeric compare function.
	+Take the two function in use the sorting order.
	
	+Next we develop the same function in command line argument value -r in sort the revers order.
	+First check the argument count and check the argument value -r is true in sort the revers order.
	+It's use the one static global variable . That variable value 1 is sort the reverse order.
	+That variable only increment the command line value -r. 

	+Next we develop the same program in find the -f option.
	+That option not the case sensitive.
	+I will develop the same function. Then same process for check the command line argument get the -f in assign the fold variable value 1 or zero.
	+Use the to lower default function in same for upper and lower.
	
	+Next we develop the same program in find the -d option.
	+That handle the only letters,numbers and blanks to use sorting. Other character avoid.
*/

#include<stdio.h>
#include<string.h>
#define MAX 2000

int Read_line(char *ptr[],int line);
void Write_line(char *ptr[],int line);
void q_sort(void *ptr[],int left,int right,int (*comp)(void*,void*));
int num_cmp(char *s,char *p);
int get_line(char s[],int lim);
int str_cmp(char *s,char *p);
char *ptr[MAX]={};
static int revers=0,numeric=0,fold=0,dir=0,field=0;
main(int argc,char *argv[])
{
	int nline,c;
	if(argc<6)	
	{
		while(--argc&&(*++argv)[0]=='-')
			while(c=*++argv[0])
			{
				switch(c)
				{
					case 'n':
						numeric=1;
						break;
					case 'f':
						fold=1;
						break;
					case 'r':
						revers=1;
						break;
					case 'd':
						dir=1;
						break;
					case 'k':
					   L:	if(isdigit(*++argv[0]))
						{
							field=(10*field)+(*argv[0]-'0');
							goto L;
						}
						else
						{
							--argv;
							break;
						}
					default:
						argc=0;
						break;
				}
			}
    printf("Numeric=%d	Revers=%d	Fold=%d	Dir=%d	field=%d\n",numeric,revers,fold,dir,field);
		if((nline=Read_line(ptr,MAX))>=0)
		{
			q_sort((void **)ptr,0,nline-1,(int (*)(void*,void*))(numeric?num_cmp:str_cmp));
			Write_line(ptr,nline);
		}
		else
			printf("Input too big to sort\n");
	}
	else
		printf("Only one commands allowed -n -f -r  -d\n");
}

int field_s(char *s,char *p,char *s1,char *p1);	

int str_cmp(char *s,char *p)
{
	char str1[MAX]={},str2[MAX]={};

	if(field)
	{
		field_s(s,p,str1,str2);
		s=str1;p=str2;
	}
	while(*s!='\0')
	{
		while(ispunct(*s)&&dir) s++;
		while(ispunct(*p)&&dir) p++;
		
		if(!((fold)?tolower(*s)==tolower(*p):*s==*p))
			return (fold)?tolower(*s)-tolower(*p):*s-*p;
		s++;p++;
	}

	return 0;
}

int field_s(char *s,char *p,char *s1,char *p1)
{
	int i=0,j=0;

	while(*s!='\0')
	{
		if(isspace(*s)&&!isspace(*(s-1)))
			i++;
		if(i==(field-1)&&!isspace(*s))
			*s1++=*s;
		if(i==field)
			break;
		s++;
	}
	*s1='\0';
        while(*p!='\0')
        {
                if(isspace(*p)&&!isspace(*(p-1)))
                        j++;
                if(j==(field-1)&&!isspace(*p))
                        *p1++=*p;
                if(j==field)
                        break;
		*p++;
        }
        *p1='\0';
}


static char str[MAX]={};
static char *buf=str;

int Read_line(char *line[],int lim)
{
        int lin=0,len,i=0;
        char *p;
        while((len=get_line(str,MAX))>1)
        {
                if((str+MAX-buf)>=len)
                {
                        buf+=len;
                        p=buf-len;
                }
                else
                        p=0;

                if((p)>0&&i<MAX)
                        ptr[i++]=p;
                else
                        return -1;
        }
        return i;
}


int get_line(char *s,int lim)
{
        int c;
        s=buf;
        char *p=s;

        while((c=getchar())!='\n'&&c!=EOF)
                *s++=c;

        *s++='\0';
        return (s-p);
}

void Write_line(char *line[],int lim)
{
        int i=0;
        for(i=0;i<lim;i++)
                printf("\t%s\n",line[i]);
}

#include<stdlib.h>

int num_cmp(char *s1,char *s2)
{
	double v1,v2;
	char str1[MAX]={},str2[MAX]={};

	 if(field)
         {
        	field_s(s1,s2,str1,str2);
                s1=str1;s2=str2;
         }

	v1=atof(s1);
	v2=atof(s2);

	if(v1<v2)
		return -1;
	else if(v1>v2)
		return 1;
	else
		return 0;
}

void swap(void *v[],int i, int j)
{
	void *temp;
	temp=v[i];
	v[i]=v[j];
	v[j]=temp;
}

void q_sort(void *v[],int left,int right,int (*comp)(void *,void *))
{
	int i,last;
	
	if(left>=right)
		return;
	swap(v,left,(left+right)/2);

	last=left;

	for(i=left+1;i<=right;i++)
		if(revers==1)
		{
			if((*comp)(v[i],v[left])>0)
				swap(v,++last,i);
		}
		else
		{
			if((*comp)(v[i],v[left])<0)
				swap(v,++last,i);
		}

	swap(v,left,last);

	q_sort(v,left,last-1,comp);
	q_sort(v,last+1,right,comp);
}
