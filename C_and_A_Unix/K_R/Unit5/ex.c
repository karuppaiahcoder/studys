//Modify undcl so that it does not add redundant parentheses to declarations.


/**************************Algorithm*********************/
/*
	+This program only error recover is skip the redundant parentheses.
	+First understand the undcl function.
	+The get_token function is get the tokens is one by one.
        +That function keep the token is three method. First method is get the name of the tokens.
        +Data, variable name and storage class in all tokens is get the name method.
        +Next method was get the parenthesis called "()" .
        +Next method was get the brackets in called "[]".
        +Other tokens in return the direct in the function.

	+The undcl function convert the word descriptions to declarations.
	+With example was x () * [] * () char to char (*(*x())[])()

	+That program add the extra parentheses is token value '*' so That place modify the program.


*/

#include<stdio.h>
#include<string.h>
#include<ctype.h>
#define MAX 1000

enum {NAME,PARENS,BRACKETS};


int get_token(void);

int token_type;
char token[MAX];
char name[MAX];
char out[MAX];

main()
{

	int type,a=0;
	char temp[MAX];
	while (get_token() != EOF)   //x () * [] * () char 
	{
		strcpy(out, token);

		while ((type = get_token()) != '\n')
		{
			if (type == PARENS || type == BRACKETS)
			{
				if(a)
				{
					sprintf(temp,"(%s)",temp);		
					a=0;
				}
				strcat(out, token);
			}
			else if (type == '*') 
			{
				sprintf(temp, "*%s", out);
				strcpy(out, temp);	
				a=1;
			} 
			else if (type == NAME) 
			{
				sprintf(temp, "%s %s", token, out);
				strcpy(out, temp);
			} 
			else
				printf("invalid input at %s\n", token);
		}

		printf("%s\n",out);
	}
}


int get_ch();
void unget_ch(int c);

int get_token(void)
{
	int c;
	char *p=token;

	while((c=get_ch())==' '||c=='\t');

	if(c ==	'(')
	{
		if((c=get_ch())==')')
		{
			strcpy(token,"()");
			return token_type=PARENS;
		}
		else
		{
			unget_ch(c);
			return token_type='(';
		}
	}
	else if(c == '[')
	{
		for(*p++=c;(*p++=get_ch())!=']';) ;
	
		*p='\0';
		return token_type=BRACKETS;
	}
	else if(isalpha(c))
	{
		for(*p++=c;isalnum(c=get_ch());)
			*p++=c;
		*p='\0';
		unget_ch(c);
		return token_type=NAME;
	}
	else
		return token_type=c;
}



int buf=0;
char buff[MAX];

int get_ch()
{
	return (buf>0)?buff[--buf]:getchar();
}

void unget_ch(int c)
{
	buff[buf++]=c;
}
