//write the program in sort the line.

/*****************Algorithm**************/
/*
	+Get line function:

		-This function only get the string or one line.
		-That function terminate the stats for newline.
		-This function take the two arguments. 
		-One type character for array pointer variable.
		-Other one limit of get value and type integer.
		-It's function return type was integer.

	+Read_line function:
		
		-This function get the more then lines in the memory.
		-This function allowed the all value for without and of file.
		-Then that function get the memory by allocation function.
		-That function take the two arguments. 
		-The one argument type is character and other one integer.
		-Then this function return type was integer.

	+memory allocation function:

		-The function was pointer type function.
		-This function allocate the length of memory to store the value.
		-This function take the one arguments. 
		-That arguments type is integer. 
		-That argument value for how much memory of allocation value.
		-This function first check available of memory. 
		-Available in allocate the memory. 
		-Non available in return null value.
		-Because this function return type is character.

	+Q_sort function:

		-This function is sort the lines.
		-Take the three arguments.
		-Then one arguments array pointer it character type.
		-Then other two arguments type is integer. 
		-Because That variables hold the array of first location and last location.
		-The function check the all lines and character one by one. 
		-So I want use the string comparison default function.
		-The lines can't match in call the swap function. 
		-Then two time call the swap function automatically.

	+Swap function:

		-This function swap the particular two position values.
		-So this function take the three arguments.
		-One of the arguments be array pointer with character type.
		-Other two arguments is get the one argument is since location value other one get the other one location of value.
		-So finally swap the two location values.

	+Write_line function:

		-This function only print the group of lines.
		-This function take the two arguments.
		-One argument is array pointer variable type is character.
		-Other one argument type is integer , That variable hold the line count or total line count.
	
*/

#include<stdio.h>
#define LINE 100
int get_line(char *s,int lim);
int Read_line(char *a[],int lim);
void Write_line(char *a[],int lim);
void qsort(char *a[],int left,int right);
char *alloc(int a);
char *lineptr[LINE]={};
int main()
{
	int nline=0,i;
	if((nline=Read_line(lineptr,LINE))>=0)
	{
		printf("Without Line  sorting \n\n");
		Write_line(lineptr,nline);
		
		qsort(lineptr,0,nline-1);
		printf("\nWith Line sorting\n\n");
		Write_line(lineptr,nline);
	}
	else
		printf("Error\n");
}

#include<string.h>
#define MAX 100000
static char str[MAX]={};
static char *buf=str;
char *alloc(int num)
{
	if((str+MAX-buf)>=num)
	{
		buf+=num;
		return buf-num;
	}
	else
		return 0;
}

int Read_line(char *line[],int lim)
{
	int lin=0,len,i=0;
	char *p;
	while((len=get_line(str,MAX))>1)
	{
		if((p=alloc(len))>0&&i<LINE)
			lineptr[i++]=p;
		else
			return -1;
	}
	return i;
}

int get_line(char *s,int lim)
{
	int c;
	s=buf;
	char *p=s;
	while((c=getchar())!='\n'&&c!=EOF)
		*s++=c;

	*s++='\0';
	return (s-p);
}
void swap(char *line[],int i,int j);

void qsort(char *line[],int left,int right)
{
	int i=0,last;
	if(left>=right)
		return;

	swap(line,left,(left+right)/2);

	last=left;	
	
	for(i=left+1;i<=right;i++)
		if(strcmp(line[i],line[left])<0)
			swap(line,++last,i);

	swap(line,left,last);

	qsort(line,left,last-1);
	qsort(line,last+1,right);

}

void swap(char *line[],int i,int j)
{
	char *temp;

	temp=line[i];

	line[i]=line[j];

	line[j]=temp;
}

void Write_line(char *line[],int lim)
{
	int i=0;
	for(i=0;i<lim;i++)
		printf("\t%s\n",line[i]);
}
