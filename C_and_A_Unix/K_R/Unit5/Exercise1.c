//As written, getint treats a + or - not followed by a digit as a valid representation of zero. Fix it to push such a character back on the input.

#include<stdio.h>
#include<ctype.h>
#define SIZE 100
int get_int(int *x);
int get_ch();
int Unget_ch(char a);
int main()
{
	int array[SIZE]={},n,c;
	printf("Enter the Integer values....\n");
	for(n=0;n<SIZE&&(c=get_int(&array[n]))!=EOF;n++)
	{
		if(c=='~')
			;
		else
			printf("%d\n",array[n]);
	}
	extern char buff[SIZE];

	printf("Your Without integer values:\n%s\n",buff);
}
int get_int(int *sp)
{
	int c,sign;
	static int posi=0,neg=0;
	while(isspace(c=get_ch()));
	
	if(c!=EOF&&c!='+'&&c!='-'&&!isdigit(c))
	{
		Unget_ch(c);
		return '~';
	}
	sign=(c=='-')?-1:1;
	if(posi)
	{
		sign=1*sign;
		posi=0;
	}
	else if(neg)
	{
		sign=-1*sign;
		neg=0;
	}

	if(c=='-'||c=='+')
		c=get_ch();
	
	for(*sp=0;isdigit(c);c=get_ch())
		*sp=(10* *sp)+(c-'0');
	*sp*=sign;
	if(c!=EOF)
	{
		if(c=='-')
			neg=-1;
		else if(c=='+')
			posi=1;
		else
			Unget_ch(c);
	}
	else
		return c;
	return 1;
}

int buf=0;
char buff[SIZE]={};

int get_ch()
{
	return getchar();
}

int Unget_ch(char c)
{
	buff[buf++]=c;
}
