//Modify the program entab and detab (written as exercises in Chapter 1) to accept a list of tab stops as arguments. Use the default tab settings if there are no arguments.

/**************Algorithm***************/
/*
	+create the two function are de_tab and End_tab.
	+The de_tab in delete the tab or convert the tab to space.
	+Then End_tab in add the tab or convert the space to tab.
	+Get the tab stop value to command line arguments.
		Example: ./a.out 12 3 5 8

		-first tab stop value 12, next 3 last one stop value 8. Then ofter time stop value 8.
	+Then any value not enter in take the tab default value 8.
	+One time convert the tab to space or space to tab in get the next tab stop value to pointer array.
*/

#include<stdio.h>
#include<stdlib.h>
#define MAX 100
int End_tab(char *s,int n,int lim,int tab,int *);
int De_tab(char *s,int n,int lim);
main(int argc,char *argv[])
{
	char str[MAX]={},c;
	int i=0,tab_co=8,ch=0,use,ps=0,tab=0,pt=0,I;

	if(((--argc)-1)>0)
	{
			tab_co=atoi(*(argv+2));
		printf("Tab stop=%d\n",tab_co);
	

		if(strcmp(*++argv,"entab")==0)	
		{
			tab=2;
			++argv;
		}
	
		else if(strcmp(*argv++,"detab")==0)
			tab=1;
		printf("%d\n",tab);
	}

	while((c=getchar())!=EOF)
	{
		if(tab==2)
		{
			if(c==' ')
			{
				ps++;
			}
			else if(c!=' ')
			{
				if(ps>0)
				{
					if(ps+ch>=tab_co)
						ps+=ch;
					else
						ps=ch-ps;
					i=End_tab(str,i,ps,tab_co,&pt);
				}
		
				if(pt==1)		//get the tab stop position value.
					if(--argc>1)
		                                if(**++argv)
		                                        if(use=atoi(*argv))
		                                        {       
		                                                printf("Tab stop=%d\n",use);
		                                                tab_co=use;     pt=0;
		                                        }
				if(ps)
				{
					ps=0;
					ch=0;
				}
				if(c!='\n')
					ch++;
				str[i++]=c;
			}
		}
		else if(tab==1)
		{
			if(c=='\t'&&(tab_co-ch)>0)
			{
				if((--argc)>0)			//get the tab stop position value.
					if(**++argv)
						if(use=atoi(*argv))
						{
							printf("Tab stop=%d\n",use);
							tab_co=use;	
						}
						
				printf("J");
				i=De_tab(str,i,tab_co-ch);
				ch=0;
			}
			else
			{
				str[i++]=c;
				if(c!='\n')
					ch++;
				if(ch==tab_co)
					ch=0;
			}
		
		}
		else
			printf("Please type the entab or detab\n");
	}
	printf("%s\n",str);
}

int End_tab(char *s,int n,int sp,int tab,int *p)
{
	L:if(sp>=tab)
		while((sp-=tab)>=0)
		{
			*(s+n)='@';
			n++;
			*p=1;
			goto L;
		}
	while(--sp>0)
	{
		*(s+n)='+';
		n++;
	}
	return n;
}


int De_tab(char *s,int n,int lim)
{
	while(lim)
	{
		*(s+n)='*';
		n++;lim--;
	}
	return n;
}
