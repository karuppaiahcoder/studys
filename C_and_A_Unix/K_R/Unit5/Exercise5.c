//Write versions of the library functions strncpy, strncat, and strncmp, which operate on at most the first n characters of their argument strings. For example, strncpy(s,t,n) copies at most n characters of t to s. Full descriptions are in Appendix B.

/*********************************Algorithm***************************************/
/*
	+string copy function:
		-That function take the three arguments in type was to argument  character , 
		 Then other argument is integer..
		-That return type for char.
		-Copy the string 2 to string 1 but number of character only.

	+string concatenation function:
		-That function take the three arguments in type was two arguments character,
		 Then other one argument is integer.
		-That function return for string 1.
		-That function concatenate the number of character string two to string one.

	+string comparison function:
		-That function take the three arguments in type was two arguments character,
                 Then other one argument is integer.
		-That function return value for integer.
		-That function compare the number of character string one to string two.
*/

#include<stdio.h>
#define MAX 50
char str_ncpy(char *s,char *p,int loc);
char str_ncat(char *s,char *p,int loc);
int str_ncmp(char *s,char *p,int loc);
main()
{
	char str1[MAX]={},str2[MAX]={};
	int i=0,j=0,c,loc;
	printf("Enter the string one:\n");
	while((c=getchar())!='\n')
		str1[i++]=c;

	printf("Enter the second string:\n");
	while((c=getchar())!='\n')
		str2[j++]=c;

	printf("Enter the location:\n");
	scanf("%d",&loc);
	
	str_ncpy(str1,str2,loc);
	printf("%s\n",str1);

	str_ncat(str1,str2,loc);
	printf("%s\n",str1);

	i=str_ncmp(str1,str2,loc);
	printf("%d\n",i);

}


char str_ncpy(char *s,char *p,int loc)
{
	while(loc-->0)
		*s++=*p++;
}

char str_ncat(char *s,char *p,int loc)
{
	while(*++s);

	while(loc-->0)
		*s++=*p++;
}

int str_ncmp(char *s,char *p,int loc)
{
	while(loc-->0)
		if(*s++!=*p++)
			return *(s-1)-*(p-1);
	return 0;
}
