//Find program.

#include<stdio.h>
#include<string.h>
#define MAX 100
int ge_line(char *s,int lim);

main(int argc,char *argv[])
{
	int line_no=0,c,except=0,num=0;
	char line[MAX]={};
	while(--argc>0 && (*++argv)[0]=='-')
		while(c=*++argv[0])
		{
			switch(c)
			{
				case 'x':
					except=1;
					break;
				case 'n':
					num=1;
					break;
				default:
					printf("Find: Illegal option %c\n",c);
					argc=0;
					break;
			}
		}
	if(argc!=1)
		printf("Usage : Find pattern\n");
	else
	{
		while(get_line(line,MAX)>0)
		{
			line_no++;

			if( (strstr(line,*argv)!=NULL) !=except)
			{
				if(num)
					printf("Line :%d ",line_no);
				printf("%s",line);
			}
		}
	}

}

int get_line(char *s,int lim)
{

        char *p=s;
        while(--lim>0&&(*s++=getchar())!='\n'&&*(s-1)!=EOF);

        *s='\0';
        return (s-p)-1;
}

