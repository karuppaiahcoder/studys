//Revise minprintf to handle more of the other facilities of printf.
/****************************Algorithm***********************/
/*
	+
*/

#include<stdarg.h>
#include<stdio.h>
void minprintf(char *argv,...);
main()
{
	char a;
	a='a';
	minprintf("%c%c%d",a,'c',12);
}

void minprintf(char *argv,...)
{
	va_list arg;
	char *p,*ptr,c,*j;
	int a;
	double b;
	unsigned int A;

	va_start(arg,argv);

	for(p=argv; *p ;p++)
	{
		if(*p!='%')
		{
			putchar(*p);
			continue;
		}
		switch(*++p)
		{
			case 'd':
			case 'i':
				a=va_arg(arg,int);
				printf("%d",a);
				break;
		
			case 'f':
				b=va_arg(arg,double);
				printf("%f",b);
				break;
		
			case 'o':
				a=va_arg(arg,int);
				printf("%o",a);
				break;

			case 'x':
			case 'X':
				a=va_arg(arg,int);
				if(*p=='X')
					printf("%X",a);
				else
					printf("%x",a);
				break;

			case 'e':
			case 'E':
				b=va_arg(arg,double);
                                if(*p=='E')
                                        printf("%E",b);
                                else
                                        printf("%e",b);
                                break;

			case 'u':
				A=va_arg(arg,unsigned);
				printf("%u",A);
				break;

			case 'g':
                        case 'G':
                                b=va_arg(arg,double);
                                if(*p=='G')
                                        printf("%G",b);
                                else
                                        printf("%g",b);
                                break;

			case 'c':
				c=va_arg(arg,int);
				putchar(c);
				break;

			case 's':
				for(ptr=va_arg(arg,char *); *ptr ; ptr++)
					putchar(*ptr);
				break;
	
			case 'p':
				ptr=va_arg(arg,void *);
				printf("%p",ptr);
				break;

			default:
				putchar(*p);
				break;
		}
	}
	va_end(arg);
}
