//Rewrite the postfix calculator of Chapter 4 to use scanf and/or sscanf to do the input and number conversion.

#include<stdio.h>
#include<stdlib.h>
#define MAX 100
void push(double c);
double pop();
main()
{
	char array[MAX]={},digit[MAX]={},digit1[MAX]={},op[MAX]={};
	while(gets(array)>0)
	{
		if(sscanf(array,"%s %s %s",digit,digit1,op)==3)
		{
			push(atof(digit));
			push(atof(digit1));
			if(op[0]=='+')
				printf("%f\n",pop()+pop());
			else if(op[0]=='-')
				printf("%f\n",pop()-pop());
			else if(op[0]=='*')
				printf("%f\n",pop()*pop());
		}
		else
			printf("Wrong format please follow the format 1 2 +\n");
		
	}
}


double buff[MAX];
int i;

void push(double c)
{
	buff[i++]=c;
}

double pop()
{
	if(i>0)
		return buff[--i];
	else
	{
		printf("Stack empty\n");
		return 0;
	}
}
