#include<stdio.h>
#include<stdarg.h>

void minscanf(char *arg,...);

main()
{
	int a;
	char a1[100]={};
	double d;

	minscanf("%s",a1);
	printf("main  %s\n",a1);
}

void minscanf(char *argv,...)
{
	va_list arg;

	va_start(arg,argv);

	int *a,*b;
	char *c;
	double *d;
	for(;*argv!='\0';argv++)
	{
		if(*argv!='%')
		{
			continue;
		}

		switch(*++argv)
		{
			case 'd':
			case 'i':
			case 'u':
				a=va_arg(arg,int *);
				if(*argv=='u')
					scanf("%u",a);
				else
					scanf("%d",a);
				break;

			case 'o':
			case 'x':
				b=va_arg(arg,int *);
				if(*argv=='x')
					scanf("%x",b);
				else
					scanf("%o",b);
				break;

			case 's':
			case 'c':
				c=va_arg(arg,char *);
				if(*argv=='s')
					scanf("%s",c);
				else
					scanf("%c",c);				
				break;

			case 'e':
			case 'f':
			case 'g':
				d=va_arg(arg,double *);
				scanf("%lf",d);
				break;
		}
	}

}
