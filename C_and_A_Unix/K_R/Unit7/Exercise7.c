//Modify the pattern finding program of Chapter 5 to take its input from a set of named files or, if no files are named as arguments, from the standard input. Should the file name be printed when a matching line is found?

/******************Algorithm*****************/
/*
	+Implemented the pattern matching program in can use the strstr function in find the pattern.
	+Use the file gets and file puts function in can get and print the value.
        +Next get the one pattern and one or more then file names in the command line.
        +Not Enter the pattern or file name in the command line in get the file name in use the  stander input.
	+Get the line one by one in the file.
	+Then check the pattern in any line is match. Present in print the line with file name.
        +This process continue to each files.
        +Write the one split function in used for split the file name.
	+This program input method was ,
				./a.out pattern  file_1 file_2 file_3 file_n

*/

#include<stdio.h>
#include<string.h>
#include<ctype.h>
#define MAX 100

char *f_gets(char *s,int lim,FILE *iop);
int f_puts(char *s,FILE *oop);
char *split(char *s,char *p);

main(int argc,char *argv[])
{
	char pattern[20]={},*pat,*s,str[MAX]={},file_name[MAX]={},file1[MAX]={},*name;
	FILE *file;
	if(argc>2)
	{
		pat=*++argv;
		printf("%s",pat);
		while(*++argv!='\0')
		{
			if((file=fopen(*argv,"r"))!=NULL)
			{
				while((s=f_gets(str,MAX,file))!=NULL&&s!='\0')
					if(strstr(s,pat)!=NULL)
						printf("%s: %s\n",*argv,str);
			}
			else
				printf("Cat:Can't open %s\n",*argv);
		}
	}
	else
	{
		if(argc==1)
		{
			printf("Enter the pattern\n");
			pat=f_gets(pattern,MAX,stdin);
			*(pat+(strlen(pat)-1))='\0';
		}
	 	else
			pat=*++argv;	

		printf("Enter the file names\n");
		gets(file_name);
		name=&file_name[0];
		while((name=split(name,file1))!=NULL)
		{
			if((file=fopen(file1,"r"))!=NULL)
			{
				while((s=f_gets(str,MAX,file))!=NULL&&s!='\0')
	                                if(strstr(s,pat)!=NULL)
	                                        printf("%s: %s\n",file1,str);
			}
			else
				printf("Cat:Can't open %s\n",file1);
		}
	}
}

char *split(char *s,char *p)
{
        if(*s=='\0')
                return;
        while(isspace(*s))
                s++;
        while(!isspace(*p++=*s++))
        {
                if(*s=='\0')
                        break;
        }
        if(isspace(*(s-1)))
                *(p-1)='\0';
        else
                *p='\0';
        return (*s=='\n'&&s==p)? NULL : s;
}

char *f_gets(char *s,int lim,FILE *iop)
{
        register int c;
        register char *p=s;

        while(--lim>0 && (c=getc(iop))!=EOF)
                if((*p++=c)=='\n')
                        break;

        *p='\0';
        return ((c==EOF)&&(p==s))? NULL : s;
}

int f_puts(char *s,FILE *oop)
{
        int c;

        while(c=*s++)
                putc(c,oop);

        return ferror(oop) ? EOF : 0 ;
}
