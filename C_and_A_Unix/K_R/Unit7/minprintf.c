#include<stdarg.h>
#include<stdio.h>
void minprintf(char *argv,...);
main()
{
	minprintf("%s","vijay\n");
}

void minprintf(char *argv,...)
{
	va_list arg;
	char *p,*ptr;
	int a;
	double b;

	va_start(arg,argv);

	for(p=argv; *p ;p++)
	{
		if(*p!='%')
		{
			putchar(*p);
			continue;
		}
		switch(*++p)
		{
			case 'd':
				a=va_arg(arg,int);
				printf("%d",a);
				break;

			case 'f':
				b=va_arg(arg,double);
				printf("%f",b);
				break;

			case 's':
				for(ptr=va_arg(arg,char *); *ptr ; ptr++)
					putchar(*ptr);
				break;

			default:
				putchar(*p);
				break;
		}
	}
	va_end(arg);
}
