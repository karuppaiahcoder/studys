//Write a program to print a set of files, starting each new one on a new page, with a title and a running page count for each file.

/******************Algorithm*****************/
/*
	+Use the file gets and file puts function in can get and print the value.
	+Next get the one or more then file names in the command line.
	+Not Enter the file name in the command line in get the file name in use the  stander input.
	+Next the print the line with line number.
	+Next implemented the program in print the page number in after ten line.
	+This process continue to each time.

	+Write the one split function in used for split the file name.
		input method 
			./a.out file_1 file_2 file_3 file_n 
*/

#include<stdio.h>
#include<string.h>
#include<ctype.h>
#define MAX 100

char *f_gets(char *s,int lim,FILE *iop);
int f_puts(char *s,FILE *oop);
char *split(char *s,char *p);

main(int argc,char *argv[])
{
	char *s,str[MAX]={},file_name[MAX]={},file1[MAX]={},*name;
	FILE *file;
	int page=1,line=1;
	if(argc>1)
	{
		while(*++argv!='\0')
		{
			if((file=fopen(*argv,"r"))!=NULL)
			{
				while((s=f_gets(str,MAX,file))!=NULL&&s!='\0')
				{
					if(line==1||line==11)
					{
					    printf("\nFile name:%s..............Page:%d\n",*argv,page);
						line=1;page++;
					}
					printf("\t%d: %s",line,str);
					line++;
				}
			}
			else
				printf("Cat:Can't open %s\n",*argv);
			line=1;page=1;
		}
	}
	else
	{
		printf("Enter the file names\n");
		gets(file_name);
		name=&file_name[0];
		while((name=split(name,file1))!=NULL)
		{
			printf("File %s\n",file1);
			if((file=fopen(file1,"r"))!=NULL)
			{
				while((s=f_gets(str,MAX,file))!=NULL&&s!='\0')
				{
 					if(line==1||line==11)
                                        {
                                            printf("\nFile name:%s..............Page:%d\n\n",file1,page);
                                                line=1;page++;
                                        }
                                        printf("\t%d: %s",line,str);
                                        line++;
				}
			}
			else
				printf("Cat:Can't open %s\n",file1);
			line=1;page=1;
		}
	}
}

char *split(char *s,char *p)
{
	if(*s=='\0')
		return;
	while(isspace(*s))
		s++;
	while(!isspace(*p++=*s++))
	{
	        if(*s=='\0')
        	        break;
	}
	if(isspace(*(s-1)))	
		*(p-1)='\0';
	else
		*p='\0';
	return (*s=='\n'&&s==p)?NULL:s;
}

char *f_gets(char *s,int lim,FILE *iop)
{
        register int c;
        register char *p=s;

        while(--lim>0 && (c=getc(iop))!=EOF)
                if((*p++=c)=='\n')
                        break;

        *p='\0';
        return ((c==EOF)&&(p==s))? NULL : s;
}

int f_puts(char *s,FILE *oop)
{
        int c;

        while(c=*s++)
                putc(c,oop);

        return ferror(oop) ? EOF : 0 ;
}
