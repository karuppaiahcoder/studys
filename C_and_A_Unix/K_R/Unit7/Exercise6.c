//Write a program to compare two files, printing the first line where they differ.

#include<stdio.h>
#include<string.h>
#define MAX 100

char *f_gets(char *s,int lim,FILE *iop);
int f_puts(char *s,FILE *oop);

main(int argc,char *argv[])
{
	FILE *file1,*file2;
	char str1[MAX]={},str2[MAX]={},*s,*s1,a=6;

	if(argc==3)
	{
		if((file1=fopen(argv[1],"r"))==NULL)
		{
			printf("Can't open the file: %s\n",argv[1]);
			goto L;
		}
		if((file2=fopen(argv[2],"r"))==NULL)
		{
			printf("Can't open the file: %s\n",argv[2]);
			goto L;
		}

		while(--a>0)
		{
			s=f_gets(str1,MAX,file1);
			s1=f_gets(str2,MAX,file2);
			if(strcmp(s,s1)!=0)
			{
				printf("%s ",argv[1]);
				f_puts(s,stdout);
				printf("%s ",argv[2]);
				f_puts(s1,stdout);
				break;
			}
		}
	}
	else
	L:	printf("please give the two files.\n");
}

char *f_gets(char *s,int lim,FILE *iop)
{
        register int c;
        register char *p=s;

        while(--lim>0 && (c=getc(iop))!=EOF)
                if((*p++=c)=='\n')
                        break;

        *p='\0';
        return ((c==EOF)&&(p==s))? NULL : s;
}

int f_puts(char *s,FILE *oop)
{
        int c;

        while(c=*s++)
                putc(c,oop);

        return ferror(oop) ? EOF : 0 ;
}


