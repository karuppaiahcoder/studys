	.file	"Exercise1.c"
	.section	.rodata
.LC0:
	.string	"./a.out"
.LC1:
	.string	"./A.OUT"
.LC2:
	.string	"Error"
	.text
	.globl	main
	.type	main, @function
main:
.LFB0:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	pushl	%edi
	pushl	%esi
	andl	$-16, %esp
	subl	$32, %esp
	movl	12(%ebp), %eax
	movl	(%eax), %eax
	movl	%eax, %edx
	movl	$.LC0, %eax
	movl	$8, %ecx
	movl	%edx, %esi
	.cfi_offset 6, -16
	.cfi_offset 7, -12
	movl	%eax, %edi
	repz cmpsb
	seta	%dl
	setb	%al
	movl	%edx, %ecx
	subb	%al, %cl
	movl	%ecx, %eax
	movsbl	%al, %eax
	testl	%eax, %eax
	jne	.L2
	movl	$1, 24(%esp)
	jmp	.L3
.L2:
	movl	12(%ebp), %eax
	movl	(%eax), %eax
	movl	%eax, %edx
	movl	$.LC1, %eax
	movl	$8, %ecx
	movl	%edx, %esi
	movl	%eax, %edi
	repz cmpsb
	seta	%dl
	setb	%al
	movl	%edx, %ecx
	subb	%al, %cl
	movl	%ecx, %eax
	movsbl	%al, %eax
	testl	%eax, %eax
	jne	.L3
	movl	$2, 24(%esp)
.L3:
	cmpl	$2, 24(%esp)
	jg	.L4
	jmp	.L5
.L7:
	cmpl	$1, 24(%esp)
	jne	.L6
	movl	28(%esp), %eax
	movl	%eax, (%esp)
	call	toupper
	movl	%eax, (%esp)
	call	putchar
	jmp	.L5
.L6:
	movl	28(%esp), %eax
	movl	%eax, (%esp)
	call	tolower
	movl	%eax, (%esp)
	call	putchar
.L5:
	call	getchar
	movl	%eax, 28(%esp)
	cmpl	$-1, 28(%esp)
	jne	.L7
	jmp	.L9
.L4:
	movl	$.LC2, (%esp)
	call	puts
.L9:
	leal	-8(%ebp), %esp
	popl	%esi
	.cfi_restore 6
	popl	%edi
	.cfi_restore 7
	popl	%ebp
	.cfi_def_cfa 4, 4
	.cfi_restore 5
	ret
	.cfi_endproc
.LFE0:
	.size	main, .-main
	.ident	"GCC: (Ubuntu/Linaro 4.6.3-1ubuntu5) 4.6.3"
	.section	.note.GNU-stack,"",@progbits
