//Write a program that will print arbitrary input in a sensible way. As a minimum, it should print non-graphic characters in octal or hexadecimal according to local custom, and break long text lines.
/*******************Algorithm****************/
/*
	+This program is convert the non graphic character to octal or hexadecimal value.
	+First check the values are non graphical or graphical value.
	+Use the one default function in find the non graphical value.
	+Then get the convert option in command line argument.
*/
#include<stdio.h>
#define OCT 8
#define HAX 15
#define MAX 100
main(int argc,char *argv[])
{
	int opt=0,count=0,c;
	if(argc==2)
		if((*++argv)[0]=='-')
			if(*++argv[0]=='o')
				opt=OCT;
			else if(*argv[0]=='x')
				opt=HAX;
	while((c=getchar())!=EOF&&count++<MAX)
	{
		if(iscntrl(c)||c==' ')
		{
			if(opt==OCT)
			{
				printf("\\%o",c);
			}
			else if(opt==HAX)
			{
				printf("\\%0x",c);
			}
			
			if(c=='\n')
			{
				putchar(c);	
				count=0;
			}				
		}
		else
			putchar(c);
	}
}
