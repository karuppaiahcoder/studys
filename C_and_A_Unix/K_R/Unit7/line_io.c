#include<stdio.h>
#include<string.h>
#define MAX 100

int get_line(char *s,int lim);
char *f_gets(char *s,int lim,FILE *iop);
int f_puts(char *s,FILE *oop);

main()
{
	char line[MAX]={};
	
	get_line(line,MAX);

	f_puts(line,stdout);

}

int f_puts(char *s,FILE *oop)
{
	int c;
	
	while(c=*s++)
		putc(c,oop);

	return ferror(oop) ? EOF : 0 ;
}

int get_line(char *s,int lim)
{
	if((f_gets(s,lim,stdin))==NULL)
		return 0;
	else
		return strlen(s);
}

char *f_gets(char *s,int lim,FILE *iop)
{
	register int c;
	register char *p=s;	
	
	while(--lim>0 && (c=getc(iop))!=EOF)
		if((*p++=c)=='\n')
			break;

	*p='\0';
	return ((c==EOF)&&(p==s))? NULL : s;
}
