//Write a program that converts upper case to lower or lower case to upper, depending on the name it is invoked with, as found in argv[0].

/****************Algorithm******************/
/*
	+Convert the lower to upper case or upper case to lower case.
	+The convert option in choose the our object file format.
	+Example for our object file is ./a.out in convert the lower to upper, And ./A.OUT means convert upper case to lower case.
	+Then I will get the object name is use the command line argument variable argv[0].
	+Finally convert the case in use to default function are tolower and toupper.
*/

#include<stdio.h>
#include<string.h>
main(int argc,char *argv[0])
{
	int c,found;
	if(!strcmp(argv[0],"./a.out"))
		found=1;

	else if(!strcmp(argv[0],"./A.OUT"))
		found=2;

	if(found<=2)
	{
		while((putchar(found==1?toupper(getchar()):putchar(tolower(getchar()))))!=EOF)
			;
	}	
	else
		printf("Error\n");
}
