//Add commands for handling variables. (It's easy to provide twenty-six variables with single-letter names.) Add a variable for the most recently printed value.

#include<stdio.h>
#include<stdlib.h>
#include<ctype.h>
#include<math.h>
#define MAX 100
#define NUMBER '0'
#define STRING 'A'
#define ASSIN '='
double push(double val);
double pop();
int get();
int unget(char c);
double float_mode(double a,double b);
double pop1();
double fun(char s[]);
main()
{
	char str[MAX]={};
	int type=0,i,j;
	double op1=0,op2=0,assign[MAX]={};
	print();
	printf("Calc>: ");
	while((type=get_char(str))!=EOF)
	{
		extern int sp;
		switch(type)
		{
			case ASSIN:
					j=str[0]-'a';
					assign[j]=atof(str+1);
					break;
			case STRING:
					if(fun(str))
						;
					else
						goto L;
					break;
			case NUMBER:
					push(atof(str));
					break;
			case '+':
					if(sp>=2)
                                        {
                                                op1=pop();
                                                push(pop()+op1);
                                        }
					break;
			case '-':
					if(sp>=2)
					{
						op1=pop();
						push(pop()-op1);
					}
					break;
			case '/':
					op1=pop();
					if(op1!=0.0)
						push(pop() / op1);
					else
		                                printf("error: zero divisor\n");
					break;
			case '*':
					op1=pop();
					push(pop() * op1);
					break;
			case '%':
					op1=pop();
					if(op1!=0.0)
						push(float_mode(pop(),op1));
					else
                                                printf("error: zero divisor\n");
					break;
			case 'P':
					if(pop1())
						printf("Stack top value : %f\n",pop1());
					else
						printf("error: Stack empty Not pop the value\n");
					break;
			case 'C':
					for(;(i=clear())=='\0';);
					printf("Clear the stack\n");
					break;
			case 'S':
					Swap();
					printf("Swap the top two value in the stack\n");
					break;
			case 'D':
					duplicate();
 				  printf("Copy the duplicate value and store into stack top position\n");
					break;	
			case '\n':
					printf("Answer:%f\n",pop1());
					printf("Calc>: ");
					break;
			L:default:
					if(str[1]=='\0')	
					{
						j=str[0]-'a';
						if(assign[j])
							push(assign[j]);
						else
							goto END;

					}
	                                else
				END:		printf("error: unknown command \"%s\"\n", str);
					break;
		}
	}
}

double fun(char s[])
{
	double f,f1;
	if(strcmp(s,"sin")==0)
		push(sin(pop()));

	else if(strcmp(s,"pow")==0)
		push(pow(pop(),pop()));

	else if(strcmp(s,"exp")==0)
		push(exp(pop()));

	else
		return 0.0;
}

print()
{
	printf("\nAdd the four commands.\n\n\t1.P->print the stack top value\n");
	printf("\t2.C->Clear the stack\n");
	printf("\t3.D->Create the duplicate value\n");
	printf("\t4.S->Swap the top two value into stack\n");
	printf("\nAdd the three functions.\n\n\t1.sin - return the sin value\n");
	printf("\t2.exp - return the Exponential value.\n");
	printf("\t3.pow - return the Power value.\n");
  printf("\nAdd the value assignment to single character variable only use the Lower case latter\n");
	printf("\n\tThe assignment method \"a=10\"\n\n");
}

double float_mode(double a,double b)
{
	int a1=(int)a,b1=(int)b;
	return (a-b*(a1/b1));	
}

int sp=0;
double array[MAX];

double push(double val)
{
	if(sp<MAX)
		array[sp++]=val;
	else 
                printf("error: stack full, can't push %g\n", val);
}

clear()
{
	if(sp>0)
		return (array[--sp]='\0');
	else
		return 1.0;
}
double pop1()
{
	if(sp>0)
		return array[sp-1];
	else
		return 0.0;
}

double pop()
{
	if(sp>0)
		return array[--sp];
	else
	{
                printf("error: stack empty\n");
		return 0.0;
	}
}

Swap()
{
	double temp=0.0;
	temp=array[sp-1];
	array[sp-1]=array[sp-2];
	array[sp-2]=temp;
}

duplicate()
{
	double temp=array[sp-1];
		array[sp++]=temp;
}

int use;
int get_char(char str[])
{
	char c,c1;
	int i=0;
	while((str[0]=c=get())==' '||c=='\t');

	str[1]='\0';

	if(c=='-')
		use++;
	else if(c=='\n')
		use=0;
	if(!isdigit(c)&&!islower(c)&&c!='.')
		return c;
	if(islower(c))
	{
		char c1;
		while(islower(str[++i]=c=get()));
		if(c==' '||c=='\t')
	       	        while((c=get())==' '||c=='\t');
		str[i]='\0';
		if(c=='='&&i==1)
		{
			char c1;
	
	       	        while((c=get())==' '||c=='\t');
			if(c=='-'||c=='+'||isdigit(c))
				str[i++]=c;

			while(isdigit(str[i++]=c=get()));
			if(c=='.')
		                while(isdigit(str[i++]=c=get()));

			str[i]='\0';
			if(c!=EOF)
				unget(c);
			return ASSIN;
		}

		if(c!=EOF)
			unget(c);
		return STRING;
	}
	i=0;
 	if(isdigit(c)&&use)
	{
		c1=str[0];
		str[0]='-';
		str[++i]=c1;
		use=0;
	}
	while(isdigit(str[++i]=c=get()));

	if(c=='.')
		while(isdigit(str[++i]=c=get()));

	str[i]='\0';
	if(c!=EOF);
		unget(c);

	return NUMBER;
}

char buff[MAX];
int j;
int get()
{
	return (j>0)?buff[--j]:getchar();
}

int unget(char c)
{
	if(j<MAX)
		buff[j++]=c;
	else
                printf("unget: too many characters\n");
}
