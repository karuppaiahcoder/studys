#include<stdio.h>
#include<stdlib.h>
#include <ctype.h>
#define MAX 100
#define NUMBER '0'
double push(double val);
double pop();
int get();
int unget(char c);
double float_mode(double a,double b);
main()
{
	char str[MAX]={};
	int type=0;
	double mul=0;
	while((type=get_char(str))!=EOF)
	{
		switch(type)
		{
			case NUMBER:
					push(atof(str));
					break;
			case '+':
					push(pop() + pop());
					break;
			case '-':
					push(pop() - pop());
					break;
			case '/':
					mul=pop();
					if(mul!=0.0)
						push(pop() / mul);
					else
		                                printf("error: zero divisor\n");
					break;
			case '*':
					mul=pop();
					push(pop() * mul);
					break;
			case '%':
					mul=pop()
					if(mul!=0.0)
						push(float_mode(pop(),mul));
					else
                                                printf("error: zero divisor\n");
					break;
			case '\n':
					printf("%f\n",pop());
					break;
			default:
	                                printf("error: unknown command %s\n", str);
					break;
		}
	}
}

double float_mode(double a,double b)
{
	int a1=(int)a,b1=(int)b;
	return (a-b*(a1/b1));	
}

double array[MAX];
int i=0;
double push(double val)
{
	if(i<MAX)
		array[i++]=val;
	else
                printf("error: stack full, can't push %g\n", val);
}

double pop()
{
	if(i>0)
		return array[--i];
	else
	{
                printf("error: stack empty\n");
		return 0.0;
	}
}

int get_char(char str[])
{
	char c;
	int i;
	printf("Calc> ");
	while((str[0]=c=get())==' '||c=='\t');

	str[1]='\0';
	if(!isdigit(c)&&c!='.'&&c!='-'&&c!='+')
		return c;
        i=0;
	while(isdigit(str[++i]=c=get()));

	if(c=='.')
		while(isdigit(str[++i]=c=get()));

	str[i]='\0';
	if(c!=EOF);
		unget(c);

	return NUMBER;
}

char buff[MAX];
int j;
int get()
{
	return (j>0)?buff[--j]:getchar();
}

int unget(char c)
{
	if(j<MAX)
		buff[j++]=c;
	else
                printf("unget: too many characters\n");
}

/*#include<stdio.h>
#include<string.h>
#define MAX 200
double atof(char s[]);
main()
{
	char line[200]={};
	double n=0;
	while(get_line(line)>0)
	{
		n+=atof(line);
		printf("n=%f\n",n);	
	}
}

double atof(char s[])
{
	double val,power;
	int i=0,sign;
	for(i=0;isspace(s[i]);i++);
	sign=(s[i]=='-')?-1:1;
	if(s[i]=='-'||s[i]=='+')
		i++;
	for(val=0.0;isdigit(s[i]);i++)
		val=10.0*val+(s[i]-'0');
	if(s[i]=='.')
		i++;
	for(power=1.0;isdigit(s[i]);i++)
	{
		val=10.0*val+(s[i]-'0');
		power*=10;
	}
	return ((sign*val)/power);
}

int get_line(char s[])
{
	int i=0,c;
	while(i<MAX && (c=getchar())!=EOF && c!='\n')
		s[i++]=c;
	if(c=='\n')
		s[i++]='\n';
	s[i]='\0';
	return i;
}*/

/*#include<stdio.h>
#define MAX 1000
int get_line(char s[],int lim);
int str_index(char s[],char s1[]);
main()
{
	char line[MAX]={},pattern[10]="vijay";
	int n=0;
	while(get_line(line,MAX)>0)
		if(str_index(line,pattern)>=0)
		{
			printf("%s",line);
			n++;
		}
	printf("n=%d\n",n);
}

int get_line(char s[],int lim)
{
	int c,i=0;
	while(--lim>0&&(c=getchar())!=EOF&&c!='\n')
		s[i++]=c;
	if(c=='\n')
		s[i++]=c;
	s[i]='\0';
	return i;
}

int str_index(char s[],char s1[])
{
	int i,j,k;
	for(i=0;s[i]!='\0';i++)
	{
		for(j=i,k=0;s1[k]!='\0'&&s[j]==s1[k];j++,k++)
			;
		if(k>0&&s1[k]=='\0')
			return i;
	}
	return -1;
}*/
