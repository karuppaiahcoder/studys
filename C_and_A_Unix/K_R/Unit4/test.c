#include <stdio.h> 
#include <stdlib.h> 
#define MAXOP 100
#define NUMBER '0'
int getop(char []);
void push(double);
double pop(void);
main()
{
	int type;
	double op2;
	char s[MAXOP];
	while ((type = getop(s)) != EOF) 
	{
		printf("%d\n",type);
		switch (type) 
		{
			case NUMBER:
				push(atof(s));
				break;
			case '+':
				push(pop() + pop());
				break;
			case '*':
				push(pop() * pop());
				break;
			case '-':
				op2 = pop();
				push(pop() - op2);
				break;
			case '/':
				op2 = pop();
				if (op2 != 0.0)
					push(pop() / op2);
				else
					printf("error: zero divisor\n");
				break;
			case '\n':
				printf("\t%.8g\n", pop());
				break;
			default:
				printf("error: unknown command %s\n", s);
				break;
		}
	}
	return 0;
}


#define MAXVAL 100
int sp = 0;
double val[MAXVAL];
void push(double f)
{
	if (sp < MAXVAL)
		val[sp++] = f;
	else
		printf("error: stack full, can't push %g\n", f);
}

double pop(void)
{
	if (sp > 0)
	return val[--sp];
	else 
	{
		printf("error: stack empty\n");
		return 0.0;
	}
}


#include <ctype.h>
int getch(void);
void ungetch(int);
int getop(char s[])
{
	int i, c;
	while ((s[0] = c = getch()) == ' ' || c == '\t')
		;
	s[1] = '\0';
	if (!isdigit(c) && c != '.')
		return c;
	i = 0;
	if (isdigit(c))
		while (isdigit(s[++i] = c = getch()))
			;
	if (c == '.')
		while (isdigit(s[++i] = c = getch()))
			;
	s[i] = '\0';
	if (c != EOF)
		ungetch(c);
	return NUMBER;
}


#define BUFSIZE 100
char buf[BUFSIZE];
int bufp = 0;
int getch(void) 
{
	return (bufp > 0) ? buf[--bufp] : getchar();
}
void ungetch(int c)
{
	if (bufp >= BUFSIZE)
		printf("ungetch: too many characters\n");
	else
		buf[bufp++] = c;
}




/*#include <stdio.h>
#include <ctype.h>

double atof(char s[]);

int main(void)
{
	printf("%lf\n", atof("123.45e+4"));

}

double atof(char s[])
{
	double val, power, base, p;
	int i, sign, exp;

	for (i = 0; isspace(s[i]); i++)
		;
	sign = (s[i] == '-') ? -1 : 1;
	if (s[i] == '-' || s[i] == '+')
		++i;
	for (val = 0.0; isdigit(s[i]); i++)
		val = 10.0 * val + (s[i] - '0');
	if (s[i] == '.')
		i++;
	for (power = 1.0; isdigit(s[i]); i++) {
		val = 10.0 * val + (s[i] - '0');
		power *= 10.0;		
	}
	if (s[i] == 'e' || s[i] == 'E')
		i++;
	else
		return sign * val/power;
	base = (s[i] == '-') ? 0.1 : 10.0;//  10^(-n) = 1/10^n = (1/10)^n = (0.1)^n 
	if (s[i] == '+' || s[i] == '-')
		i++;	
	for (exp = 0; isdigit(s[i]); i++) 
		exp = 10 * exp + (s[i] - '0');
	for (p = 1; exp > 0; --exp)
		p = p * base;
printf("%f\n",p);
	return (sign * (val/power)) * p;
}*/

/*#include<stdio.h>
#include<ctype.h>
main()
{
        char c;
        while((c=getchar())!=EOF)
        {
                if(isspace(c))
                        printf("|%c| is a white-space character\n",c);
                else if(isdigit(c))
                        printf("|%c| is a digit\n",c);
                else if(isxdigit(c))
                        printf("|%c| is a hexa digit\n",c);
                else
                        printf("|%c| is a not digit and space\n",c);
        }
}*/

/*#include<stdio.h>
#include<ctype.h>
main()
{
        char c;
        while((c=getchar())!=EOF)
        {
                if(isxdigit(c))
                        printf("|%c| is a hexa digit\n",c);
                else
                        printf("|%c| is not a hexa digit\n",c);
        }
}*/

/*#include <stdio.h>
#define MAXLINE 1000
int get_line(char line[], int max);
int strindex(char source[], char searchfor[]);
char pattern[] = "ould";
main()
{
	char line[MAXLINE];
	int found = 0;
	while (get_line(line, MAXLINE) > 0)
		if (strindex(line, pattern) >= 0) 
		{
			printf("%s", line);
			found++;
		}
	return found;
}


int get_line(char s[], int lim)
{
	int c, i;
	i = 0;
	while (--lim > 0 && (c=getchar()) != EOF && c != '\n')
		s[i++] = c;
	if (c == '\n')
		s[i++] = c;
	s[i] = '\0';
	return i;
}


int strindex(char s[], char t[])
{
	int i, j, k;
	for (i = 0; s[i] != '\0'; i++) 
	{
		for (j=i, k=0; t[k]!='\0' && s[j]==t[k]; j++, k++)
			;
		if (k > 0 && t[k] == '\0')
			return i;
	}
	return -1;
}
*/
