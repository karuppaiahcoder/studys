//Add the commands to print the top elements of the stack without popping, to duplicate it, and to swap the top two elements. Add a command to clear the stack.

/*
	Algorithm:

		-

*/

#include<stdio.h>
#include<stdlib.h>
#include <ctype.h>
#define MAX 100
#define NUMBER '0'
double push(double val);
double pop();
int get();
int unget(char c);
double float_mode(double a,double b);
double pop1();
main()
{
	char str[MAX]={};
	int type=0,i;
	double use=0;
	print();
	while((type=get_char(str))!=EOF)
	{
	//	printf("return = %d\n",type);
		switch(type)
		{
			case NUMBER:
					push(atof(str));
					break;
			case '+':
					push(pop() + pop());
					break;
			case '-':
					push(pop() - pop());
					break;
			case '/':
					use=pop();
					if(use!=0.0)
						push(pop() / use);
					else
		                                printf("error: zero divisor\n");
					break;
			case '*':
					use=pop();
					push(pop() * use);
					break;
			case '%':
					use=pop();
					if(use!=0.0)
						push(float_mode(pop(),use));
					else
                                                printf("error: zero divisor\n");
					break;
			case 'p':
					if(pop1())
						printf("Stack top value : %f\n",pop1());
					else
						printf("error: Stack empty Not pop the value\n");
					break;
			case 'c':
					for(;(i=clear())=='\0';);
					printf("Clear the stack\n");
					break;
			case 's':
					Swap();
					printf("Swap the top two value in the stack\n");
					break;
			case 'd':
					duplicate();
 				  printf("Copy the duplicate value and store into stack top position\n");
					break;	
			case '\n':
					printf("Answer:%f\n",pop1());
					//for(;(i=clear())=='\0';);
					printf("Calc>: ");
					break;
			default:
	                                printf("error: unknown command %s\n", str);
					break;
		}
	}
}

print()
{
	printf("Add the four commands\n\t1.p->print the stack top value\n");
	printf("\t2.c->Clear the stack\n");
	printf("\t3.d->Create the duplicate value\n");
	printf("\t4.s->Swap the top two value into stack\n");
}

double float_mode(double a,double b)
{
	int a1=(int)a,b1=(int)b;
	return (a-b*(a1/b1));	
}

double array[MAX];
int i=0;
double push(double val)
{
	if(i<MAX)
		array[i++]=val;
	else
                printf("error: stack full, can't push %g\n", val);
//	printf("1...push a[%d]=%f\n",i-1,array[i-1]);
}

clear()
{
	if(i>0)
		return (array[--i]='\0');
	else
		return 1.0;
}
double pop1()
{
	if(i>0)
		return array[i-1];
	else
		return 0.0;
}

double pop()
{
//	printf("2...pop return a[%d]=%f\n",i-1,array[i-1]);
	if(i>0)
		return array[--i];
	else
	{
                printf("error: stack empty\n");
		return 0.0;
	}
}

Swap()
{
	double temp=0.0;
	temp=array[i-1];
	array[i-1]=array[i-2];
	array[i-2]=temp;
}

duplicate()
{
	double temp=array[i-1];
		array[i++]=temp;
}

int use;
int get_char(char str[])
{
	char c,c1;
	int i=0;
	while((str[0]=c=get())==' '||c=='\t');

	str[1]='\0';
//	printf("------ [%c] -----\n",c);
	if(c=='-')
		use++;
	else if(c=='\n')
		use=0;

	if(!isdigit(c)&&c!='.')
		return c;
	i=0;
 	if(isdigit(c)&&use)
	{
		c1=str[0];
		str[0]='-';
		str[++i]=c1;
		use=0;
	}
	while(isdigit(str[++i]=c=get()));

	if(c=='.')
		while(isdigit(str[++i]=c=get()));

	str[i]='\0';
	if(c!=EOF);
		unget(c);

	return NUMBER;
}

char buff[MAX];
int j;
int get()
{
	return (j>0)?buff[--j]:getchar();
}

int unget(char c)
{
	if(j<MAX)
		buff[j++]=c;
	else
                printf("unget: too many characters\n");
}
