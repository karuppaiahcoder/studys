//Write a recursive version of the function reverse(s), which reverses the string s in place.

#include<stdio.h>
#include<string.h>
main()
{
	char str[30]={};
	int i,j;
	scanf("%s",str);
	i=0;j=strlen(str)-1;
	revers(str,i,j);
	printf("%s\n",str);
}

revers(char str[],int i,int j)
{
	int use;
	if(i<j)
	{
		use=str[i];
		str[i]=str[j];
		str[j]=use;
		revers(str,i+1,j-1);
	}
}
