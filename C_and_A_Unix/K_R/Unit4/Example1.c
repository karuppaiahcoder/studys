//Write the function strindex(s,t) which returns the position of the rightmost occurrence of t in s, or -1 if there is none.

/*
	Algorithm:

		-Write the two functions, The first function is get the input in lines, Then next one function is find the pattern in the one group of string.
		-First create the variables in count the patter match and store the string and pattern.
		-Next the get function is get the one group of string and pattern, And then call the string index function to find the pattern including the string.
		-The string_index function find the last pattern match in first character location returning.
		-So This function return for right most occurrence pattern first location.

		Example:

			Hello sir, how are sir. What are you doing? ->Is a string.

			sir ->Is a pattern,

			So return for last or right most occurrence pattern first location.

			So return value for 18.
*/

#include<stdio.h>
#include<string.h>
#define MAX 500
int get_line(char str[],int a,char s[]);
int str_index(char str[],char t[]);
main()
{
	int count=0,loc=0;
	char str[MAX]={},pattern[MAX]={};
	while(get_line(str,MAX,pattern)>0)
		if((loc=str_index(str,pattern))>=0)
		{
		    printf("Match the pattern to string\"%s\"\n\n\"Right\" most location=%d\n",str,loc);
			count++;
		}
	printf("count=%d\n",count);
}

int get_line(char s1[],int lim,char s2[])
{
	int i=0,j=0,c;
	for(i=strlen(s1)-1,j=strlen(s2)-1;j>=0||i>=0;i--,j--)  //Store the all location null.
		s1[i]=s2[j]='\0';

	i=j=0;
	printf("Enter a string:\n");

	while(--lim>0 && (c=getchar())!=EOF && c!='\n') //Get the group of string.
		s1[j++]=c;
	s1[j]='\0';
	printf("Enter a search pattern:\n");  

	while((c=getchar())!='\n')     //Get the pattern
		s2[i++]=c;
	return j;
}

int str_index(char s1[],char s2[])
{
	int i,j,k;
	for(i=strlen(s1)-1;i>=0;i--)
	{
		for(j=i,k=strlen(s2)-1;k>=0&&s1[j]==s2[k];j--,k--);  //Find the pattern including string.
		if(k<0)
			return (i-(strlen(s2)-1));  //Return for pattern first location in string.
	}
	return -1;  //Return EOF in not match the character.
}
