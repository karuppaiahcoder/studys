//Exercise 4-3. Given the basic framework, it's straightforward to extend the calculator. Add the modulus (%) operator and provisions for negative numbers.

/*
	Algorithm:

		-write the five functions,

		get():
			-Get the number and operators.
			-Then skip the white space.
			-Next This function return type is int so only return the integer value.
			-This function in including use the to functions , are getch and ungetch.

				-This function usage for get the value one bye one and store the any one value in the buffer array.
				-Because That function not store the without number and operator so, We can use the ungetch function.
				
		push():
			-The push function only store the double values in the array.
			-I has use the one default function in atof to convert the string to double value.
		pop():
			-The pop function usage for pop the array value one by one.

		float_mode():

			-That function is find the remainder in the floating point numbers.

*/


#include<stdio.h>
#include<stdlib.h>
#include <ctype.h>
#define MAX 100
#define NUMBER '0'
double push(double val);
double pop();
int get();
int unget(char c);
double float_mode(double a,double b);
main()
{
	char str[MAX]={};
	int type=0;
	double use=0;
	while((type=get_char(str))!=EOF)
	{
	//	printf("return = %d\n",type);
		extern sp;
		switch(type)
		{
			case NUMBER:
					push(atof(str));
					break;
			case '+':
					if(sp>=2)
					{
						use=pop();
						push(pop() + use);
					}
					break;
			case '-':
					if(sp>=2)
					{
						use=pop();
						push(pop() - use);
					}
					break;
			case '/':
					use=pop();
					if(use!=0.0)
						push(pop() / use);
					else
		                                printf("error: zero divisor\n");
					break;
			case '*':
					use=pop();
					push(pop() * use);
					break;
			case '%':
					use=pop();
					if(use!=0.0)
						push(float_mode(pop(),use));
					else
                                                printf("error: zero divisor\n");
					break;
			case '\n':
					printf("Answer :%f\n",pop());
					break;
			default:
	                                printf("error: unknown command %s\n", str);
					break;
		}
	}
}

double float_mode(double a,double b)
{
	int a1=(int)a,b1=(int)b;
	return (a-b*(a1/b1));	
}

double array[MAX];
int sp=0;
double push(double val)
{
	if(sp<MAX)
		array[sp++]=val;
	else
                printf("error: stack full, can't push %g\n", val);
//	printf("1...push a[%d]=%f\n",i-1,array[i-1]);
}

double pop()
{
//	printf("2...pop return a[%d]=%f\n",i-1,array[i-1]);
	if(sp>0)
		return array[--sp];
	else
	{
                printf("error: stack empty\n");
		return 0.0;
	}
}
int use;
int get_char(char str[])
{
	char c,c1;
	int i=0;
	while((str[0]=c=get())==' '||c=='\t');

	str[1]='\0';
//	printf("------ [%c] -----\n",c);
	if(c=='-')
		use++;
	else if(c=='\n')
		use=0;

	if(!isdigit(c)&&c!='.')
		return c;
	i=0;
 	if(isdigit(c)&&use)
	{
		c1=str[0];
		str[0]='-';
		str[++i]=c1;
		use=0;
	}
	while(isdigit(str[++i]=c=get()));

	if(c=='.')
		while(isdigit(str[++i]=c=get()));

	str[i]='\0';
	if(c!=EOF);
		unget(c);

	return NUMBER;
}

char buff[MAX];
int j;
int get()
{
	return (j>0)?buff[--j]:getchar();
}

int unget(char c)
{
	if(j<MAX)
		buff[j++]=c;
	else
                printf("unget: too many characters\n");
}
