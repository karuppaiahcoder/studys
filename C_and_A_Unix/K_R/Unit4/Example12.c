//Adapt the ideas of printd to write a recursive version of itoa; that is, convert an integer into a string by calling a recursive routine.


#include<stdio.h>
int Itoa(int n, char s[]);
int i=0;
main()
{
        char s[20]={};
        int n=-2147483648;
        Itoa(n,s);
        printf("\nAnswer: %s\n\n",s);
}

int Itoa(int n, char s[])
{
	static int sign=0;
	if(n!=0)
	{
		if(n<0)	
		{
			sign=-1;
			if(s[0]!='-')
				s[i++]='-';
		}
		if(n/10)
			Itoa(n/10,s);
		s[i++]=(sign*(n%10))+'0';
	}
}
