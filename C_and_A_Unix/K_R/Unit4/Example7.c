//Write a routine ungets(s) that will push back an entire string onto the input. Should ungets know about buf and bufp, or should it just use ungetch?

#include<stdio.h>
#define MAX 100
int get_ch(char a[]);
int unget_s(char b[]);
int unget_ch();
main()
{
	char str[MAX]={};
	while(get_ch(str))
	{
		extern use;
		if(use==0)
			printf("Answer :%s\n",str);
	}
}

int use=0;

int get_ch(char str[])
{
	int i=0,c,j;
	while((str[i++]=c=unget_ch())!='\n'&&c!=EOF);

	str[i]='\0';

	if(use)
		unget_s(str);

	if(c==EOF)
		return 0;
	else
		return 1;
}

int buf;
char buff[MAX];

int unget_ch()
{
	if(buf>0&&!(use=0))
	{
		buff[buf]='\0';
		return buff[--buf];
	}
	else if(use=1)
		return getchar();
}

int unget_s(char str[])
{
	int i,j;
	for(i=0;str[i]!='\0';i++);
	for(buf=0,i=i-1;i>=0;buf++,i--)
	{
		if(buf<MAX)
		{
			buff[buf]=str[i];
			str[i]='\0';
		}
		else
		{
			printf("Stack Full Don't store the %s\n",str+i);
			break;
		}
	}
	buff[buf]='\0';
	printf("buffer=%s\n",buff);
}
