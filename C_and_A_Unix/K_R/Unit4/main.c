#include<stdio.h>
#include<stdlib.h>
#include <ctype.h>
#include "calc.h"
main()
{
        char str[MAX]={};
        int type=0;
        double use=0;
        while((type=get_char(str))!=EOF)
        {
        //      printf("return = %d\n",type);
                extern sp;
                switch(type)
                {
                        case NUMBER:
                                        push(atof(str));
                                        break;
                        case '+':
                                        if(sp>=2)
                                        {
                                                use=pop();
                                                push(pop() + use);
                                        }
                                        break;
                        case '-':
                                        if(sp>=2)
                                        {
                                                use=pop();
                                                push(pop() - use);
                                        }
                                        break;
                        case '/':
                                        use=pop();
                                        if(use!=0.0)
                                                push(pop() / use);
                                        else
                                                printf("error: zero divisor\n");
                                        break;
                        case '*':
                                        use=pop();
                                        push(pop() * use);
                                        break;
                        case '%':
                                        use=pop();
                                        if(use!=0.0)
                                                push(float_mode(pop(),use));
                                        else
                                                printf("error: zero divisor\n");
                                        break;
                        case '\n':
                                        printf("Answer :%f\n",pop());
                                        break;
                        default:
                                        printf("error: unknown command %s\n", str);
                                        break;
                }
        }
}

double float_mode(double a,double b)
{
        int a1=(int)a,b1=(int)b;
        return (a-b*(a1/b1));
}

