//Extend atof to handle scientific notation of the form 123.45e-6 where a floating-point number may be followed by e or E and an optionally signed exponent.

/*
	Algorithm:

		-Write the two function in  get the double value for string, and convert the string to floating value.

		-Then atof function in convert the ascii to floating value.

		123.45e-6

		-First skip the white spaces in begging of the string.
		-Next find the sign negative or positive.
		-Declare the double variables in store the double value and power value.

		-Next split the only numbers and dot, (123.45) and find the power value in the number (100).
			12345.0 is a number,
			100.0 is power value 

				eg:
					12345.0/100=123.45
		-Next calculate the exponent value e-6.
			e^-6 = 10^-6 =0.000001.
		-Finally return the double value, (sign*(12345.0/100.0))/0.000001.
		-Answer for 0.00012345.

*/

#include<stdio.h>
#include<ctype.h>
#define MAX 200
double atof(char s[]);
int get_line(char str[],int lim);
main()
{
	char str[MAX]={};
	double n=0;
	while((get_line(str,MAX))>0)
	{
			n=atof(str);
			printf("Double=%g\n",n);
	}
}

double atof(char s[])
{
        double val,power,base,powe1;
        int i=0,sign,use;

        for(i=0;isspace(s[i]);i++);

        sign=(s[i]=='-')?-1:1;

        if(s[i]=='-'||s[i]=='+')
                i++;

        for(val=0.0;isdigit(s[i]);i++)
                val=10.0*val+(s[i]-'0');

        if(s[i]=='.')
                i++;

        for(power=1.0;isdigit(s[i]);i++)
        {
                val=10.0*val+(s[i]-'0');
                power*=10;
        }

	if(s[i]=='e'||s[i]=='E')
		i++;
	else
		return ((sign*val)/power);

	base=(s[i]=='-')?0.1:10.0;

	if(s[i]=='-'||s[i]=='+')
		i++;

	for(use=0;isdigit(s[i]);i++)
		use=10.0*use+(s[i]-'0');

	for(powe1=1.0;use>0;--use)
		powe1=powe1*base;
	printf("%f\n",powe1);

	return (sign*(val/power))*powe1;
}		


int get_line(char str[],int lim)
{
	int c,i=0,a=0,b=0,e=0,d=0;
	while((c=getchar())!='\n')
	{
	    if((c>='0'&&c<='9')||((c=='e'||c=='E')&&(a<1))||(c=='-'&&b<1)||(c=='+'&&e<1)||(c=='.'&&d<1))
		{
			if(c=='-')
				 b++;
	
			else if(c=='e')
				 a++;

			else if(c=='+')
				e++;

			else if(c=='.')
				d++;

			str[i++]=c;
		}
		else
			return 0;
	}
	if(c=='\n')
		str[i++]='\n';
	str[i]='\0';
	return i;
}
