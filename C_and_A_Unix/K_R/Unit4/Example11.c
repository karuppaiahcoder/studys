//Modify getop so that it doesn't need to use ungetch. Hint: use an internal static variable.

#include<stdio.h>
#include<stdlib.h>
#include <ctype.h>
#define MAX 100
#define NUMBER '0'
double push(double val);
double pop();
double float_mode(double a,double b);
main()
{
	char str[MAX]={};
	int type=0;
	double use=0;
	while((type=get_char(str))!=EOF)
	{
		extern sp;
		switch(type)
		{
			case NUMBER:
					push(atof(str));
					break;
			case '+':
					if(sp>=2)
					{
						use=pop();
						push(pop() + use);
					}
					break;
			case '-':
					if(sp>=2)
					{
						use=pop();
						push(pop() - use);
					}
					break;
			case '/':
					use=pop();
					if(use!=0.0)
						push(pop() / use);
					else
		                                printf("error: zero divisor\n");
					break;
			case '*':
					use=pop();
					push(pop() * use);
					break;
			case '%':
					use=pop();
					if(use!=0.0)
						push(float_mode(pop(),use));
					else
                                                printf("error: zero divisor\n");
					break;
			case '\n':
					printf("Answer :%f\n",pop());
					break;
			default:
	                                printf("error: unknown command %s\n", str);
					break;
		}
	}
}

double float_mode(double a,double b)
{
	int a1=(int)a,b1=(int)b;
	return (a-b*(a1/b1));	
}

double array[MAX];
int sp=0;
double push(double val)
{
	if(sp<MAX)
		array[sp++]=val;
	else
                printf("error: stack full, can't push %g\n", val);
}

double pop()
{
	if(sp>0)
		return array[--sp];
	else
	{
                printf("error: stack empty\n");
		return 0.0;
	}
}
int use;
int get_char(char str[])
{
	static buf=0;
	char c=0,c1;
	int i=0;
	if(buf>0)
	{
		c=buf;
		buf=0;
	}
	if(c==' '||c=='\t'||c==0)
		while((str[0]=c=getchar())==' '||c=='\t');

	str[1]='\0';
	if(c=='-')
		use++;
	else if(c=='\n')
		use=0;

	if(!isdigit(c)&&c!='.')
		return c;
	i=0;
 	if(isdigit(c)&&use)
	{
		c1=str[0];
		str[0]='-';
		str[++i]=c1;
		use=0;
	}
	while(isdigit(str[++i]=c=getchar()));

	if(c=='.')
		while(isdigit(str[++i]=c=getchar()));

	str[i]='\0';
	if(c!=EOF);
		buf=c;

	return NUMBER;
}

