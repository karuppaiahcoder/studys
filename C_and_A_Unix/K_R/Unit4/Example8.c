//Suppose that there will never be more than one character of pushback. Modify getch and ungetch accordingly.

#include<stdio.h>
#define MAX 100
int get_ch();
int unget_s(char b);
int unget_ch(char b);
main()
{
	char str[MAX]={},c,i;
	while((c=get_ch())!='\n'&&c!=EOF)
	{
		str[i++]=c;
		c=get_ch();
		unget_ch(c);
	}
	if(c=='\n')
		str[i]='\n';
	printf("%s\n",str);
}

int use=0;
int buf;

int get_ch()
{
	char temp;
	if(buf>0)
	{
		temp=buf;
		buf=0;
	}
	else
		temp=getchar();
	return temp;
}

int unget_ch(char c)
{
	if(buf!=0)
		printf("More than character can't store the buffer.\n");
	else
		buf=c;
}
