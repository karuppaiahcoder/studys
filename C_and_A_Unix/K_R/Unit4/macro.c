#include<stdio.h>
//#define swap(t,x,y) { t a; a=x; x=(t)y; y=a;}
#define SWAP(t,x,y)  void swap(t x,t y)                                           \
                                        {t temp;                                   \
                                         temp=x;                                    \
                                         x=y;                                        \
                                         y=temp;                                      \
                                        }
main()
{
	int x=128,y=-129;
	float s=-129,f=128;
	SWAP(float,x,y);
	printf("%f	%f\n%f	     %f\n",x,y,s,f);
}
/*#include <stdio.h>

#define swap(t, x, y) 	do {t safe ## x ## y; safe ## x ## y=x; x=y; y=safe ## x ## y;} while (0)

int main(void) {
	int ix, iy;
	double dx, dy;
	char *px, *py;
	
	ix = 128;
	iy = -129;
	printf("integers before swap: %d and %d\n", ix, iy);
	swap(char, ix, iy);
	printf("integers after swap: %d and %d\n", ix, iy);
	
	dx = 123.0;
	dy = 321.0;
	printf("doubles before swap: %g and %g\n", dx, dy);
	swap(double, dx, dy);
	printf("integers after swap: %g and %g\n", dx, dy);
	
	px = "hello";
	py = "world";
	printf("pointers before swap: %s and %s\n", px, py);
	swap(char *, px, py);
	printf("integers after swap: %s and %s\n", px, py);

	return 0;
}*/
