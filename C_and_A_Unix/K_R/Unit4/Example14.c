//Define a macro swap(t,x,y) that interchanges two arguments of type t. (Block structure will help.)

#include<stdio.h>
#define swap(t,x,y) { t a=x; x=(t)y; y=a; }
int main(void)
{
    int x,y;
    x=2;
    y=3;
    swap(int,x,y);
    printf("x=%d \t y=%d\n",x,y);
}
