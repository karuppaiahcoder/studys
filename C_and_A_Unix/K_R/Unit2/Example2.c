// Write a loop equivalent to the for loop above without using && or ||.

#include<stdio.h>
#define lim 50
main()
{
	char array[lim]={},c;
	int i;
	for(i=0;(c=getchar())!=EOF;i++)
	{
		if(i<(lim-1))
		{
			if(c!='\n')
				array[i]=c;
			else
				break;
		}
		else
			break;
	}
	printf("%s\n",array);
}
/*main()
{
	int i,lim=10;
	char a[50]={},c;
	for(i=0;i<lim-1&&(c=getchar())!='\n'&&c!=EOF;++i)
		a[i]=c;
}*/

