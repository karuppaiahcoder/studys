//Write a function htoi(s), which converts a string of hexadecimal digits (including an optional 0x or 0X) into its equivalent integer value. The allowable digits are 0 through 9, a through f, and A through F.

#include<stdio.h>
main()
{
	char array[15]={},c=0;
	int i=0,use=0;
	printf("Enter the Hexadecimal value must begin with 0x...\n");
	while((c=getchar())!='\n')
	{
		if(use==2)
		{
			if(c>='A'&&c<='F')
		                c=c+32;
			array[i++]=c;
		}
		if(((c=='0'&&use==0)||((c=='x'||c=='X')&&use==1))&&use<=1)
			use++;
	}
	c=Decimal_convert(array);
	if(c&&array[0])
		Convert(array,i);
	else if(c==0||use<=1)
		printf("Non valid hexadecimal value\n");
}

Convert(char str[],int l)
{
	unsigned long long int num=0,mask=0,i=0,sum;
	while((l--)>0)
	{
		sum=1;
		for(i=1;i<=mask;i++)
		{
			sum=sum*16;
		}
		if(mask==0)
			num=str[l];
		else
			num=(str[l]*sum)+num;
		mask++;
	}
	printf("Answer:%llu\n",num);
}

int Decimal_convert(char str[])
{
	int i,use=0;
	for(i=0;str[i]!='\0';i++)
	{
		if(str[i]>='0'&&str[i]<='9')
			use=48;
		else if(str[i]>='a'&&str[i]<='f')
			use=87;
		else
			return 0;
		str[i]-=use;
	}
	return 1;
}
