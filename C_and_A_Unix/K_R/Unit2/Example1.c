//Write a program to determine the ranges of char, short, int, and long variables, both signed and unsigned, by printing appropriate values from standard headers and by direct computation. Harder if you compute them: determine the ranges of the various floating-point types.

#include<stdio.h>
#include<limits.h>
#include<float.h>
main()
{
	printf("\nThe character bit value (CHAR_BIT)   			= %d\n", CHAR_BIT);
	printf("maximum number of bytes (MB_LEN_MAX) 			= %d\n", MB_LEN_MAX);
	printf("The minimum value of char (CHAR_MIN)   			= %+d\n", CHAR_MIN);
	printf("The maximum value of char (CHAR_MAX)   			= %+d\n", CHAR_MAX);
	printf("The minimum value of short char (SCHAR_MIN)  		= %+d\n", SCHAR_MIN);
	printf("The maximum value of short char (SCHAR_MAX)  		= %+d\n", SCHAR_MAX);
	printf("The maximum value of unsigned char (UCHAR_MAX)  		= %u\n",  UCHAR_MAX);

	printf("\nThe minimum value of short int (SHRT_MIN)   		= %+d\n", SHRT_MIN);
	printf("The maximum value of short int (SHRT_MAX)   		= %+d\n", SHRT_MAX);
	printf("The maximum value of unsigned short int (USHRT_MAX)  	= %u\n",  USHRT_MAX);

	printf("\nThe minimum value of int (INT_MIN)			= %+d\n", INT_MIN);
	printf("The maximum value of int (INT_MAX)    			= %+d\n", INT_MAX);
	printf("The maximum value of unsigned int (UINT_MAX)   		= %u\n",  UINT_MAX);
	
	printf("\nThe minimum value of long int (LONG_MIN)   		= %+ld\n", LONG_MIN);
	printf("The maximum value of long int (LONG_MAX)   		= %+ld\n", LONG_MAX);
	printf("The maximum value of unsigned long int (ULONG_MAX)  	= %lu\n",  ULONG_MAX);

	printf("\nThe minimum value of long long int (LLONG_MIN)  	= %+lld\n", LLONG_MIN);
	printf("The maximum value of long long int (LLONG_MAX)  		= %+lld\n", LLONG_MAX);
	printf("The maximum value of unsigned long long int (ULLONG_MAX) = %llu\n",  ULLONG_MAX);

	printf("\nThe minimum value of float (FLT_MIN) 			= %e\n",FLT_MIN);
        printf("The maximum value of float (FLT_MAX) 			= %e\n\n",FLT_MAX);

	printf("\nThe minimum value of double (DBL_MIN)			= %e\n",DBL_MIN);
	printf("The minimum value of double (DBL_MAX)			= %e\n",DBL_MAX);

}
