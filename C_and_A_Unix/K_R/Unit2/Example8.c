//Write a function rightrot(x,n) that returns the value of the integer x rotated to the right by n positions.


#include<stdio.h>
#define BIT 32
int Shift_bit(int val,int posi);
main()
{
	int val,posi;
	printf("Enter the number:\n");
	scanf("%d",&val);
	printf("Enter the number of right most bit:\n");
	scanf("%d",&posi);
	Shift_bit(val,posi);
}

int Shift_bit(int val,int posi)
{
	int num=0;
	num=val<<(BIT-posi);
	num|=(val>>posi);
	printf("%d",num);
}
