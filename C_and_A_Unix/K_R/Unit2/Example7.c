//Write a function invert(x,p,n) that returns x with the n bits that begin at position p inverted (i.e., 1 changed into 0 and vice versa), leaving the others unchanged.


#include<stdio.h>
main()
{
	int val,posi,nbit,get=0,get1=0,num=0;
	printf("Enter the value:\n");
	scanf("%d",&val);
	printf("Enter the GET BIT position:\n");
	scanf("%d",&posi);
	printf("Enter the number of bit:\n");
	scanf("%d",&nbit);
	get1=Invert(~val,posi,nbit);
        get=Invert(val,posi,nbit);
        val^=(get<<(posi+1-nbit));
	val|=(get1<<(posi+1-nbit));
	printf("GET the value:%d\nAnswer:%d\n",get,val);
}

Invert(int val,int posi,int nbit)
{
	int i=0;
        i=(val>>(posi+1-nbit))&~(~0<<nbit);
	return i;
}
