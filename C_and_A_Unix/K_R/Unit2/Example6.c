//Write a function setbits(x,p,n,y) that returns x with the n bits that begin at position p set to the rightmost n bits of y, leaving the other bits unchanged.


#include<stdio.h>
main()
{
        unsigned int fval,posi,nbit,sval;
	printf("Enter the first unsigned integer number:\n");
	scanf("%d",&fval);
	printf("Enter the second unsigned integer number:\n");
	scanf("%d",&sval);
	printf("Enter the GET BIT position:\n");
	scanf("%d",&posi);
	printf("Enter the number of bit:\n");
	scanf("%d",&nbit);
        set_bit(fval,posi,nbit,sval);
}

set_bit(int fval,int posi, int nbit,int sval)
{
        unsigned int i=0,mask=sval,num=0,j,use=posi;
        i=(fval>>(posi+1-nbit))&~(~0<<nbit);
	printf("get bit:%d\n",i);
	for(j=nbit;j>0;j--)
	{
		mask&=(sval&(~(1<<(use))));
		use--;
	}
	printf("mask for second value:%d\n",mask);
  	num=(mask)|(i<<((posi+1)-nbit));
	printf("Answer:%d\n",num);
}
