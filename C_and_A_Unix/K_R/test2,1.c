#include<stdio.h>
int i=4;          /* Global definition   */
main()
{
	i++;          /* Global variable     */
	func();
	printf( "Value of i = %d -- main function\n", i );
}

func()
{
	int i=10;     /* Local definition */
	i++;          /* Local variable    */
	printf( "Value of i = %d -- func() function\n", i );
}

