//In a two's complement number representation, our version of itoa does not  handle the largest negative number, that is, the value of n equal to -(2wordsize-1). Explain why not. Modify it to print that value correctly, regardless of the machine on which it runs.


#include<stdio.h>
#include<string.h>
int Itoa(int n, char s[]);
int revers(char s[]);
main()
{
	char s[20]={};
	int n=-2147483648;
	Itoa(n,s);
	printf("\nAnswer: %s\n\n",s);
}

int Itoa(int n, char s[])
{
	int i=0, sign,use=0;
	int max=-2147483647;
	use=(n<max)?1:0;
	if(use)
		n+=1;
	if ((sign = n) < 0)
		n = -n;
	do 
	{
		s[i++] = n % 10 + '0'+use;
		use=0;
	} 
	while ((n /= 10) > 0);
	if (sign < 0)
		s[i++] = '-';
	s[i] = '\0';
	revers(s);
}

int revers(char s[])
{   
        int c, i, j;
        for (i = 0, j = strlen(s)-1; i < j; i++, j--) 
        {
                c = s[i];
                s[i] = s[j];
                s[j] = c;
        }
}
