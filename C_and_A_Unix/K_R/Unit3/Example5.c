//Write the function itob(n,s,b) that converts the integer n into a base b character representation in the string s. In particular, itob(n,s,16) formats s as a hexadecimal integer in s.


#include<stdio.h>
#include<string.h>
int Itoa(int n, char s[],int i);
int revers(char s[]);
main()
{
	char s[20]={};
	int n=10,base=8;
	if(base>1&&17>base)
	{
		Itoa(n,s,base);
		        printf("\nAnswer: %s\n\n",s);
	}
	else
		printf("This base value invalid\n");
}

int Itoa(int n, char s[],int base)
{
	int i=0, sign,use=0;
	if ((sign = n) < 0)
		n = -n;
	do 
	{
		use=n%base;
		s[i++] = ((use>9)?(use-10+'A'):(use+'0'));
	} 
	while (n /= base);
	if (sign < 0&&base>2)
		s[i++] = '-';
	s[i] = '\0';
	revers(s);
}

int revers(char s[])
{   
        int c, i, j;
        for (i = 0, j = strlen(s)-1; i < j; i++, j--) 
        {
                c = s[i];
                s[i] = s[j];
                s[j] = c;
        }
}
