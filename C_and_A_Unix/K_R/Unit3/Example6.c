//Write a version of itoa that accepts three arguments instead of two. The third argument is a minimum field width; the converted number must be padded with blanks on the left if necessary to make it wide enough.


#include<stdio.h>
#include<string.h>
int Itoa(int n, char s[],int i);
void shellsort(char v[], int n);


main()	
{
	char s[20]={};
	int n=-12345,base=10;
	Itoa(n,s,base);
	printf("\nAnswer: %s\n\n",s);
}

 
int Itoa(int n, char s[],int base)
{
	int i=0, sign,use=0,j= -n;
	if ((sign = n) < 0)
		n = -n;
	do 
	{
		s[i++]=(n%10)+'0';
	} 
	while (n /= 10);
	if (sign < 0&&base>2)
		s[i++] = '-';
	while(i<base)
		s[i++]=' ';
	s[i] = '\0';
	 shellsort(s,i);
}


void shellsort(char v[], int n)
{
        int gap=0, i=0, j=0, temp=0;
        for (gap = n/2; gap > 0; gap /= 2)
        {
                for (i = gap; i < n; i++)
                {
                        for (j=i-gap; j>=0 && v[j]>v[j+gap]; j-=gap) 
                        {
                                temp = v[j];
                                v[j] = v[j+gap];
                                v[j+gap] = temp;
                        }
                }
        }
}

