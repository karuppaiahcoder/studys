//Write a function escape(s,t) that converts characters like newline and tab into visible escape sequences like \n and \t as it copies the string t to s. Use a switch. Write a function for the other direction as well, converting escape sequences into the real characters.

#include<stdio.h>
main()
{
	char str1[100]={},str2[100],c;
	int i=0;
	printf("Enter a string :\n");
	while((c=getchar())!=EOF)
		str1[i++]=c;
	Escape(str1,str2);
	printf("\nVISIBLE THE '\\n' and '\\t':\n\n%s\n\n",str2);
}

Escape(char str1[],char str2[])
{
	int i=0,j=0;
	while(str1[i]!='\0')
	{
		switch(str1[i])
		{
			case '\t':
			{
				str2[j++]='\\';
				str2[j++]='t';
				break;
			}
			case '\n':
			{	
				str2[j++]='\\';
				str2[j++]='n';
				break; 
			}
			case '\\':
			{
				switch(str1[i+1])
				{
					case 't':
						str2[j++]='\t';
						break;
					case 'n':
						str2[j++]='\n';
						break;
					default:
						goto L;
				}
				i++;
				break;
			}
		L:	default:
				str2[j++]=str1[i];
		}
		i++;
	}
	str2[j]='\0';
}
