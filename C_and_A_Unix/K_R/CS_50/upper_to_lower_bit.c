#include <stdio.h>
#include <string.h>

int main()
{
    char array[]="programming";
    int i,j;

    for(i=0,j=strlen(array);i<j;i++)
        array[i] = array[i] & (~32);
    
    printf("Lower to upper [%s]\n",array);

    for(i=0,j=strlen(array);i<j;i++)
        array[i] = array[i] | 32;

    printf("Upper to lower [%s]\n",array);
}
