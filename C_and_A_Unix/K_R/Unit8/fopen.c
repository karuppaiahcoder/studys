#define NULL 0
#define EOF (-1)
#define BUFSIZ 1024
#define OPEN_MAX 20

enum flags{
               _READ = 01,
               _WRITE =02,
               _UNBUF =04,
                _EOF  =010,
               _ERR = 020
          };

typedef struct _obuf
{
        int cnt;
        char *ptr;
        char *base;
        int flag;
        int fd;
}FILE;

FILE  _iob[OPEN_MAX]={
                     {0,(char *)0,(char *)0,_READ,0},         //STDIN
                     {0,(char *)0,(char *)0,_WRITE,1},        //STDOUT
                     {0,(char *)0,(char *)0,_WRITE|_UNBUF,2},  //STDERR
                     };

#define stdin (&_iob[0])
#define stdout (&_iob[1])
#define stderr (&_iob[2])

int _fillbuf(FILE *);
int _flushbuf(int ,FILE *);

#define feof(p)         (((p)->flag & _EOF)!=0)
#define ferror (p)      (((p)->flag & _ERR)!=0)
#define fileno (p)      ((p)->fd)

#define getc(p) 	(--(p)->cnt >= 0? (unsigned char) *(p)->ptr++ : _fillbuf(p))
#define putc(x,p)  	(--(p)->cnt >= 0 ? *(p)->ptr++ = (x) : _flushbuf((x),p))
#define getchar() 	getc(stdin)
#define putcher(x) 	putc((x), stdout)
#define PERMS 0666

#include<fcntl.h>
#include<stdlib.h>
FILE *fopen(char *name,char *mode);
void filecopy(FILE *ifp, FILE *ofp);
main()
{
	FILE *fp,*fp1;

	fp=fopen("Exercise1.c","r");
	fp1=fopen("vijay.c","w");
	filecopy(fp,fp1);
}

void filecopy(FILE *ifp, FILE *ofp)
{
        int c;
        while((c=getc(ifp))!=EOF)
	{
                //printf("%c",
		putc(c,ofp);//);
	}
	//printf("%s",ofp->base);
	_flushbuf(c,ofp);
	//printf("[1].%s",ofp->base);
}

int _flushbuf(int val,FILE *fp)
{
	int bufsize,len;

	if((fp->flag & (_WRITE|_EOF|_ERR)) !=_WRITE)
		return EOF;

	bufsize = (fp->flag & _UNBUF)? 1 : BUFSIZ;
		
	if(fp->base == NULL)
	{
			if((fp->base = (char *) malloc(bufsize)) == NULL)
				return EOF;
		fp->ptr = fp->base;
		fp->cnt = bufsize;
	
		*fp->ptr++ = val;
		
		return *fp->ptr-1;
	}
	else
	{
		printf("%d	%c\n",val,val);
		*fp->ptr=val;		
		printf("%d	%c\n",*fp->ptr,*fp->ptr);
		write(fp->fd,fp->base,strlen(fp->base)-1);
		free(fp->base);
		fp->base=NULL;
		return val;
	}
}

int _fillbuf(FILE *fp)
{
	int bufsize;
	
	if((fp->flag & (_READ|_EOF|_ERR)) !=_READ)
		return EOF;

	bufsize = (fp->flag & _UNBUF)? 1 : BUFSIZ;

	if (fp->base == NULL)
		if ((fp->base = (char *) malloc(bufsize)) == NULL)
			return EOF;
	
	fp->ptr = fp->base;
	fp->cnt = read(fp->fd, fp->ptr, bufsize);
	if (--fp->cnt < 0) 
	{
		if (fp->cnt == -1)
			fp->flag |= _EOF;
		else
			fp->flag |= _ERR;

		fp->cnt = 0;
		return EOF;
	}

	return (unsigned char) *fp->ptr++;
}



FILE *fopen(char *name,char *mode)
{
	int fd;
	FILE *fp;
	
	if(*mode!='r' && *mode!='a' && *mode!='w')
		return NULL;

	for(fp=_iob;fp<_iob+OPEN_MAX;fp++)
		if((fp->flag & (_READ | _WRITE))==0)
			break;

	if(fp>=_iob+OPEN_MAX)
		return NULL;

	if(*mode=='w')
		fd=creat(name,PERMS);

	else if(*mode=='a')
	{
		if((fd=open(name,O_RDONLY,0))==-1)
			fd=creat(name,PERMS);
		lseek(fd,0L,2);
	}
	else
		fd=open(name,O_RDONLY,0);

	if(fd==-1)
		return NULL;

	fp->fd=fd;
	fp->cnt=0;
	fp->base=NULL;
	fp->flag=(*mode == 'r')? _READ:_WRITE;
	
	return fp;

}


