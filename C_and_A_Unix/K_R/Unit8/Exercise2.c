// Rewrite fopen and _fillbuf with fields instead of explicit bit operations. Compare code size and execution speed.
/**********************************Algorithm*****************************/
/*
	+First develop the one structure with four fields.
	+Next write the enumeration variable in use to assign the constant values.
	+Create the one File typedef structure name.
	+Next create the getc and putc functions in macro in access the bill buffer and flush buffer functions. 
	+Because hat function is read  the file and store the buffer and copy the buffer to file.
	+Then add the file copy function, fill buffer function and flush buffer unction.

	+Next open the one old file in read mode if use the fopen function.
	+This time the fopen function return the one file pointer value. That value store the one file pointer variable.
	+Then create the one new file in write mode use the fopen function and take the that file pointer value.

	+Next call the getc function, that function read the file and store the all value to buffer in use the fill buffer function.
	+Next call the getc function that write the all values to output buffer not new file.
	+Then other one time call the fill buffer function to write the all values to new file address.
	+The getc and putc unction return type is integer.

	+Then so far, we all use the only bit wish operator in the program next change the bit wish to field operations.
	+So I will add the five field in the structure.
	+Then store the all file descriptions and file open mode and count and then error information to structure filed.
	+Then run the program.

*/


#define NULL 0
#define EOF (-1)
#define BUFSIZ 1024
#define OPEN_MAX 20

enum flags{
               _READ = 01,
               _WRITE =02,
               _UNBUF =04,
                _EOF  =010,
               _ERR = 020
          };

typedef struct _obuf
{
        int cnt;
        char *ptr;
        char *base;
        int fd;
	
	int read;
	int write;
	int unbuf;
	int eof;
	int err;
}FILE;

FILE  _iob[OPEN_MAX]={
                     {0,(char *)0,(char *)0,0,_READ,0,0,0,0},         //STDIN
                     {0,(char *)0,(char *)0,1,0,_WRITE,0,0,0},        //STDOUT
                     {0,(char *)0,(char *)0,2,0,_WRITE,_UNBUF,0,0},  //STDERR
                     };

#define stdin (&_iob[0])
#define stdout (&_iob[1])
#define stderr (&_iob[2])

int _fillbuf(FILE *);
int _flushbuf(int ,FILE *);

#define feof(p)         (((p)->flag & _EOF)!=0)
#define ferror (p)      (((p)->flag & _ERR)!=0)
#define fileno (p)      ((p)->fd)

#define getc(p) 	(--(p)->cnt >= 0? (unsigned char) *(p)->ptr++ : _fillbuf(p))
#define putc(x,p)  	(--(p)->cnt >= 0 ? *(p)->ptr++ = (x) : _flushbuf((x),p))
#define getchar() 	getc(stdin)
#define putcher(x) 	putc((x), stdout)
#define PERMS 0666

#include<fcntl.h>
#include<stdlib.h>
#include<string.h>
FILE *fopen(char *name,char *mode);
void filecopy(FILE *ifp, FILE *ofp);
main()
{
	FILE *fp,*fp1;

	fp=fopen("Exercise1.c","r");
	fp1=fopen("vijay.c","w");
	filecopy(fp,fp1);
}

void filecopy(FILE *ifp, FILE *ofp)
{
        int c;
        while((c=getc(ifp))!=EOF)
		putc(c,ofp);
	//printf("%s",ofp->base);
	_flushbuf(EOF,ofp);
	//printf("[1].%s",ofp->base);
}

int _flushbuf(int val,FILE *fp)
{
	int bufsize,len;

	if(fp->write !=_WRITE)
		return EOF;

	bufsize = (fp->unbuf == _UNBUF)? 1 : BUFSIZ;
		
	if(fp->base == NULL)
	{
			if((fp->base = (char *) malloc(bufsize)) == NULL)
				return EOF;
		fp->ptr = fp->base;
		fp->cnt = bufsize;
	
		*fp->ptr++ = val;
		
		return *fp->ptr-1;
	}
	else
	{
		*fp->ptr=val;		
		write(fp->fd,fp->base,strlen(fp->base)-1);
		free(fp->base);
		fp->base=NULL;
		return val;
	}
}

int _fillbuf(FILE *fp)
{
	int bufsize;
	
	if(fp->read !=_READ)
		return EOF;

	bufsize = (fp->unbuf == _UNBUF)? 1 : BUFSIZ;

	if (fp->base == NULL)
		if ((fp->base = (char *) malloc(bufsize)) == NULL)
			return EOF;
	
	fp->ptr = fp->base;
	fp->cnt = read(fp->fd, fp->ptr, bufsize);
	if (--fp->cnt < 0) 
	{
		if (fp->cnt == -1)
			fp->eof = _EOF;
		else
			fp->err = _ERR;

		fp->cnt = 0;
		return EOF;
	}

	return (unsigned char) *fp->ptr++;
}



FILE *fopen(char *name,char *mode)
{
	int fd;
	FILE *fp;
	
	if(*mode!='r' && *mode!='a' && *mode!='w')
		return NULL;

	for(fp=_iob;fp<_iob+OPEN_MAX;fp++)
		if(fp->read==0 && fp->write==0)
			break;

	if(fp>=_iob+OPEN_MAX)
		return NULL;

	if(*mode=='w')
		fd=creat(name,PERMS);

	else if(*mode=='a')
	{
		if((fd=open(name,O_RDONLY,0))==-1)
			fd=creat(name,PERMS);
		lseek(fd,0L,2);
	}
	else
		fd=open(name,O_RDONLY,0);

	if(fd==-1)
		return NULL;

	fp->fd=fd;
	fp->cnt=0;
	fp->base=NULL;
	if(*mode=='r')
		fp->read=_READ;
	else
		fp->write=_WRITE;
	
	return fp;

}


