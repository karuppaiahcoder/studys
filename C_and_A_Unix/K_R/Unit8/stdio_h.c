#include<stdio.h>

#define null 0
#define eof (-1)
#define bufsiz 1024
#define open_max 20

enum flags{
		read = 01,
		write =02,
		unbuf =04,
		_eof  =010,
		err = 020
	  };

typedef struct iobuf
{
	int cnt;
	char *ptr;
	char *base;
	int flag;
	int fd;
}file;

extern file iob[open_max]={
				{0,(char *)0,(char *)0,read,0},		//STDIN
				{0,(char *)0,(char *)0,write,1},	//STDOUT
				{0,(char *)0,(char *)0,write|unbuf,2},	//STDERR
  			  };

#define STDIN (&iob[0])
#define STDOUT (&iob[1])
#define STDERR (&iob[2])

int fillbuf(file *);
int flushbuf(int ,file *);

#define FEOF(p)		(((p)->flag & _eof)!=0)
#define FERROR (p) 	(((p)->flag & err)!=0)
#define FILENO (p)	((p)->fd)


