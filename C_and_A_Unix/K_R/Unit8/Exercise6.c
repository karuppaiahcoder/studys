//The standard library function calloc(n,size) returns a pointer to n objects of size size, with the storage initialized to zero. Write calloc, by calling malloc or by modifying it.

/*********************************Algorithm*****************************/
/*
	+
*/


#include<stdio.h>
#include<string.h>
//#include<stdlib.h>
#define NALLOC 1024

typedef long Align;

union header
{
	struct 
	{
		union header *ptr;
		unsigned size;
	}s;
	Align x;
};

typedef union header Header;

static Header base;
static Header *freep=NULL;

void *malloc(unsigned int nbytes);
void _free(void *ap);
Header *morecore(unsigned nu);

void *calloc(unsigned int n,int size);
main()
{
	char *ptr;
	int a=3;
//	ptr=(int *)malloc(3992);

	ptr=(char *)calloc(a,4);

	printf("main == %u	%d\n",ptr,*ptr);	

	while(1)
	{
		printf("%d [%u]\n",*ptr,ptr);
		ptr++;
		getchar();
	}
}


void *calloc(unsigned int n,int size)
{
	char *ptr;
	unsigned int num=(n*size);
	ptr=malloc(num);
	printf("calloc==%u\n",ptr);
	while(num-->0)
	{
		*(ptr+num)=5;
		printf("%d.......\n",num);
	}
	return ptr;
}

void *malloc(unsigned int nbytes)
{
	Header *p,*prevp;
	unsigned nunits;
	nunits = (nbytes+sizeof(Header)-1)/sizeof(Header)+1;
	printf("1....\n");
	if((prevp = freep) == NULL)
	{
		printf("2.....\n");
		base.s.ptr = freep = prevp = &base;
		base.s.size = 0;

	}

	for(p = prevp->s.ptr; ; prevp =p, p=p->s.ptr)
	{
		printf("3....\n");

		if(p->s.size >= nunits)
		{
			printf("4....\n");
			if(p->s.size == nunits)
			{
				printf("5....\n");
				prevp->s.ptr = p->s.ptr;
			}
			else
			{
				printf("6......\n");
				p->s.size -= nunits;
				p+=p->s.size;
				p->s.size = nunits;
			}
			freep = prevp;

			printf("%u\n",freep->s.ptr);
			return (void *) (p+1);
		}
		if(p == freep)
		{
			printf("7......\n");
			if((p = morecore(nunits)) == NULL)
				return NULL;
		}
	}
}


Header *morecore(unsigned nu)
{
	char *cp;
	Header *up;

	if(nu < NALLOC)
	{
		printf("8....\n");
		nu = NALLOC;
	}
	cp = (char *)sbrk(nu * sizeof(Header));
	
	if(cp == (char *) -1)
		return NULL;
	printf("9.......\n");

	up = (Header *) cp;
	up->s.size = nu;
	_free((void *) (up+1));
	return freep;
}

void _free(void *ap)
{
	Header *bp,*p;
	
	bp = (Header *) ap-1;
	printf("10,......\n");

	for(p = freep; !(bp > p && bp <p->s.ptr);p = p->s.ptr)
	{
		printf("11......\n");
		if(p >= p->s.ptr && (bp > p || bp < p->s.ptr))
			break;
	}
	if(bp + bp->s.size == p->s.ptr)
	{
		//Create the bp filed child
		printf("12.......\n");
		bp->s.size += p->s.ptr->s.size;
		bp->s.ptr = p->s.ptr->s.ptr;
	}
	else
	{
		printf("13........\n");
		bp->s.ptr = p->s.ptr;
	}

	if(p + p->s.size == bp)
	{
		printf("14......\n");
		p->s.size += bp->s.size;
		p->s.ptr = bp->s.ptr;
	}
	else
		p->s.ptr = bp;
	printf("15.......\n");
	freep = p;
}
