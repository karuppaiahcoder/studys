//Rewrite the program cat from Chapter 7 using read, write, open, and close instead of their standard library equivalents. Perform experiments to determine the relative speeds of the two versions.

#include<stdio.h>
#include<fcntl.h>
main(int argc,char *argv[])
{
	int discri;
	if(argc==1)
	{
		file_copy(0,1);
	}
	else
	{
		while(argc-->1)
		{
			if((discri=open(*++argv,O_RDONLY,0))<0)
				printf("Can't open the file %s\n",*argv);
			else
				file_copy(discri,1);
		}
	}	
}

int file_copy(int input,int output)
{
	char buf[BUFSIZ];
        int n;
        while ((n = read(input,buf,BUFSIZ)) > 0)
                  write(output,buf,n);
}
