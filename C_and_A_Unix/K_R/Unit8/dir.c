//#include <sys/types.h>
//#include <sys/stat.h>
//#include <time.h>
#include <stdio.h>
#include <string.h>
#include<fcntl.h>
#include<stdlib.h>
#define MAX_PATH 1024

#define NAME_MAX 14

typedef struct 
{
	long ino;
	char name[NAME_MAX+1];
	unsigned long  d_off;
        unsigned short d_reclen;
        char           d_name[];  
}Dirent;

typedef struct 
{
	int fd;
	Dirent d;
}DIR;

DIR *opendir(char *dirname);
Dirent *readdir(DIR *dfd);
void closedir(DIR *dfd);
void fsize(char *);
void dirwalk(char *, void (*fcn)(char *));

main(int argc, char **argv)
{
	if (argc == 1) 
		fsize(".");
	else
		while (--argc > 0)
			fsize(*++argv);
}

void fsize(char *name)
{
	struct stat stbuf;
	if (stat(name, &stbuf) == -1) 
	{
		fprintf(stderr, "fsize: can't access %s\n", name);
		return;
	}
	if((stbuf.st_mode & S_IFMT) == S_IFDIR)
		dirwalk(name, fsize);

	printf("%8ld %s\n", stbuf.st_size, name);
}

void dirwalk(char *dir, void (*fcn)(char *))
{
	char name[MAX_PATH];

	Dirent *dp;
	DIR *dfd;

	if((dfd = opendir(dir)) == NULL) 
	{
		fprintf(stderr, "dirwalk: can't open %s\n", dir);
		return;
	}
	while ((dp = readdir(dfd)) != NULL) 
	{
		printf("EEEE\n");
		if(strcmp(dp->name, ".") == 0 || strcmp(dp->name, ".."))
			continue;
		if (strlen(dir)+strlen(dp->name)+2 > sizeof(name))
			fprintf(stderr, "dirwalk: name %s %s too long\n",dir, dp->name);
		else 
		{
			sprintf(name, "%s/%s", dir, dp->name);
			(*fcn)(name);
		}
	}

	closedir(dfd);
}

DIR *opendir(char *dirname)
{
	int fd;
	struct stat stbuf;
	DIR *dp;

	if ((fd = open(dirname, O_RDONLY, 0)) == -1 || fstat(fd, &stbuf) == -1 || (stbuf.st_mode & S_IFMT) != S_IFDIR || (dp = (DIR *) malloc(sizeof(DIR))) == NULL)
		return NULL;

	printf("1_V_I_J_A_Y\n");
	dp->fd = fd;
	return dp;
}
#ifndef DIRSIZ
#define DIRSIZ 14
#endif
struct dirent 
{
	ino_t d_ino;
	char d_name[DIRSIZ]; 
};

Dirent *readdir(DIR *dp)
{
	struct dirent dirbuf; 
	static Dirent d;
	off_t base;
	char buff[BUFSIZ]={};
	int 

	
	//while (()!=-1) 
	//{
	/*	if (dirbuf.d_ino == 0) 
		{
			continue;
		}
	*/

		printf("EEE\n",dirbuf.d_name);
	//	d.ino = dirbuf.d_ino;
	//	strncpy(d.name, dirbuf.d_name, DIRSIZ);
	//	d.name[DIRSIZ] = '\0'; 
//		return &d;
	//	break;
	//}
	return NULL;
}


void closedir(DIR *dp)
{
	printf("3_V_I_J_A_Y\n");
	if (dp) 
	{
		close(dp->fd);
		free(dp);
	}
}

