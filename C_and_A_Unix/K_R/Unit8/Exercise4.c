
/*The standard library function

	int fseek(FILE *fp, long offset, int origin)

is identical to lseek except that fp is a file pointer instead of a file descriptor and return value
is an int status, not a position. Write fseek. Make sure that your fseek coordinates properly
with the buffering done for the other functions of the library.
*/


/**********************************Algorithm*****************************************/

/*
	+First develop the one structure with four fields.
        +Next write the enumeration variable in use to assign the constant values.
        +Create the one File typedef structure name.
        +Next create the getc and putc functions in macro in access the bill buffer and flush buffer functions. 
        +Because hat function is read  the file and store the buffer and copy the buffer to file.
        +Then add the file copy function, fill buffer function and flush buffer unction.
        +Next open the one old file in read mode if use the fopen function.
        +This time the fopen function return the one file pointer value. That value store the one file po
inter variable.
        +Then create the one new file in write mode use the fopen function and take the that file pointer value.
        +Next call the getc function, that function read the file and store the all value to buffer in us
e the fill buffer function.
        +Next call the getc function that write the all values to output buffer not new file.
        +Then other one time call the fill buffer function to write the all values to new file address.
        +The getc and putc unction return type is integer.
	+Then develop the fflush function, That function is clear the out put stream buffer values.
	+So check the file pointer in point the any one file location. Pointing means check the mode write or others.
	+Write mode in copy the buffer to on file, or return the EOF.
	+Next the file pointer null meats clear the all output stream in the program.
	+Next develop the fclose function, That function close the file.
	+Then check the mode in write mean copy the buffer to file next clear the all values.
*/

#define NULL 0
#define EOF (-1)
#define BUFSIZ 1024
#define OPEN_MAX 20

enum flags{
               _READ = 01,
               _WRITE =02,
               _UNBUF =04,
                _EOF  =010,
               _ERR = 020
          };

typedef struct _obuf
{
        int cnt;
        char *ptr;
        char *base;
        int flag;
        int fd;
}FILE;

FILE  _iob[OPEN_MAX]={
                     {0,(char *)0,(char *)0,_READ,0},         //STDIN
                     {0,(char *)0,(char *)0,_WRITE,1},        //STDOUT
                     {0,(char *)0,(char *)0,_WRITE|_UNBUF,2},  //STDERR
                     };

#define stdin (&_iob[0])
#define stdout (&_iob[1])
#define stderr (&_iob[2])

int _fillbuf(FILE *);
int _flushbuf(int ,FILE *);

#define feof(p)         (((p)->flag & _EOF)!=0)
#define ferror (p)      (((p)->flag & _ERR)!=0)
#define fileno (p)      ((p)->fd)

#define getc(p) 	(--(p)->cnt >= 0? (unsigned char) *(p)->ptr++ : _fillbuf(p))
#define putc(x,p)  	(--(p)->cnt >= 0 ? *(p)->ptr++ = (x) : _flushbuf((x),p))
#define getchar() 	getc(stdin)
#define putcher(x) 	putc((x), stdout)
#define PERMS 0666

#include<fcntl.h>
#include<stdlib.h>
FILE *fopen(char *name,char *mode);
void filecopy(FILE *ifp, FILE *ofp);
int fflush(FILE *fp);
int fseek(FILE *fp,long int offset,int whence);
main()
{
	FILE *fp,*fp1=NULL;

	fp=fopen("file_1","r");
	fp1=fopen("file_2","w");
	filecopy(fp,fp1);
}
void filecopy(FILE *ifp, FILE *ofp)
{
        int c;
	FILE *fp;

	fp=fopen("file_3","r");

        while((c=getc(ifp))!=EOF)
	{
		putc(c,ofp);
	}

	fseek(ofp,8,SEEK_SET);

	while((c=getc(fp))!=EOF)
	{
		putc(c,ofp);
	}
	_flushbuf(EOF,ofp);	

//	ofp=NULL;  	
//	fflush(ofp);

//	fclose(ofp);

	//_flushbuf(c,ofp);
	//printf("[1].%s",ofp->base);
}

int fseek(FILE *fp,long int offset,int whence)
{
	if(whence==SEEK_SET)
	{
		fp->ptr=fp->base;
		fp->ptr+=offset;
	}
	else if(whence==SEEK_CUR)
	{
		fp->ptr+=offset;
	}
	else if(whence==SEEK_END)
        {
		fp->ptr=fp->base;
		fp->ptr+=strlen(fp->base)-1;
        }
	else
		return EOF;

	return 0;
}

int fclose(FILE *fp)
{
	if(fp==NULL)
		return EOF;

	if((fp->flag & (_WRITE|_EOF|_ERR)) ==_WRITE)
		_flushbuf(EOF,fp);

	free(fp->base);
	fp->cnt=0;
	fp->ptr=fp->base;
	fp->ptr=NULL;
	fp->base=NULL;
	fp->flag=0;
	fp->fd=0;

	return 0;
}

int fflush(FILE *fp)
{
	if(fp!=NULL)
	{
		if((fp->flag & (_WRITE|_EOF|_ERR)) !=_WRITE)
			return EOF;
		else if((_flushbuf(EOF,fp))>0)
		{
			return 0;
		 }
		else
			return EOF;
	}
	else
	{
		for(fp=_iob ; fp < _iob+OPEN_MAX ; fp++)
		{
			if((fp->flag & (_WRITE|_EOF|_ERR)) ==_WRITE)
				_flushbuf(EOF,fp);
		}
	}
	return 0;
}

int _flushbuf(int val,FILE *fp)
{
	int bufsize,len;

	if((fp->flag & (_WRITE|_EOF|_ERR)) !=_WRITE)
		return EOF;

	bufsize = (fp->flag & _UNBUF)? 1 : BUFSIZ;

	if(fp->base == NULL)
	{
			if((fp->base = (char *) malloc(bufsize)) == NULL)
				return EOF;
		fp->ptr = fp->base;
		fp->cnt = bufsize;
	
		*fp->ptr++ = val;
		
		return *fp->ptr-1;
	}
	else
	{
		*fp->ptr=val;		
		write(fp->fd,fp->base,strlen(fp->base)-1);
		free(fp->base);
		fp->base=NULL;
		return val;
	}
}

int _fillbuf(FILE *fp)
{
	int bufsize;
	
	if((fp->flag & (_READ|_EOF|_ERR)) !=_READ)
		return EOF;

	bufsize = (fp->flag & _UNBUF)? 1 : BUFSIZ;

	if (fp->base == NULL)
		if ((fp->base = (char *) malloc(bufsize)) == NULL)
			return EOF;
	
	fp->ptr = fp->base;
	fp->cnt = read(fp->fd, fp->ptr, bufsize);
	if (--fp->cnt < 0) 
	{
		if (fp->cnt == -1)
			fp->flag |= _EOF;
		else
			fp->flag |= _ERR;

		fp->cnt = 0;
		return EOF;
	}

	return (unsigned char) *fp->ptr++;
}



FILE *fopen(char *name,char *mode)
{
	int fd;
	FILE *fp;
	
	if(*mode!='r' && *mode!='a' && *mode!='w')
		return NULL;

	for(fp=_iob;fp<_iob+OPEN_MAX;fp++)
		if((fp->flag & (_READ | _WRITE))==0)
			break;

	if(fp>=_iob+OPEN_MAX)
		return NULL;

	if(*mode=='w')
		fd=creat(name,PERMS);

	else if(*mode=='a')
	{
		if((fd=open(name,O_RDONLY,0))==-1)
			fd=creat(name,PERMS);
		lseek(fd,0L,2);
	}
	else
		fd=open(name,O_RDONLY,0);

	if(fd==-1)
		return NULL;

	fp->fd=fd;
	fp->cnt=0;
	fp->base=NULL;
	fp->flag=(*mode == 'r')? _READ:_WRITE;
	
	return fp;

}


