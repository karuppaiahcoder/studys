//malloc accepts a size request without checking its plausibility; free believes that the block it is asked to free contains a valid size field. Improve these routines so they make more pains with error checking.

/*********************************Algorithm*****************************/
/*
	+
*/


#include<stdio.h>
#include<string.h>
//#include<stdlib.h>
#define NALLOC 1024

typedef long Align;

union header
{
	struct 
	{
		union header *ptr;
		unsigned size;
	}s;
	Align x;
};

typedef union header Header;

static Header base;
static Header *freep=NULL;

void *malloc(unsigned int nbytes);
void _free(void *ap);
void *b_free(void *s,unsigned max);
Header *morecore(unsigned nu);

int main() 
{
	char x[12]="purush";
	char *a;
	int i;
	b_free(x, sizeof(x));
	a = (char*) malloc(sizeof(x));
	printf("%s",a);		


	return;

/*
	for (i = 0; i < 12; i++)
		a[i] = i;
	for (i = 0; i < 12; i++)
		printf("%d ", a[i]);

	printf("\n");
*/	return 0; 
}

void *b_free(void *s,unsigned max)
{
	unsigned units;
	Header *up;
	units= max/sizeof(Header)+1;
	printf("b_free  %d\n",units);
	if(units<=1)
		return NULL;	
	if(freep==NULL)
        {
                base.s.ptr = freep = &base;
                base.s.size = 0;
        }

	printf("1........%s\n",(char *)s);
//	up=(Header *) s;

	up->s.ptr=s;
	up->s.size=units;
	printf("2........%s\n",(char *)s);
	_free((void*)(up+1));
	return ((void*)up+1);
}

void *malloc(unsigned int nbytes)
{
	Header *p,*prevp;
	unsigned nunits;
	nunits = (nbytes+sizeof(Header)-1)/sizeof(Header)+1;
	if((prevp = freep) == NULL)
	{
		base.s.ptr = freep = prevp = &base;
		base.s.size = 0;
	}

	for(p = prevp->s.ptr; ; prevp =p, p=p->s.ptr)
	{

	printf("%d > = %d\n",p->s.size, nunits);
		if(p->s.size >= nunits)
		{
		printf("1...\n");
			if(p->s.size == nunits)
				prevp->s.ptr = p->s.ptr;
			else
			{
				p->s.size -= nunits;
				p+=p->s.size;
				p->s.size = nunits;
			}
			freep = prevp;
			return (void *) (p+1);
		}
                printf("2...\n");
		if(p == freep)
			if((p = morecore(nunits)) == NULL)
				return NULL;
	}
}


Header *morecore(unsigned nu)
{
	char *cp;
	Header *up;

	if(nu < NALLOC)
		nu = NALLOC;
	else
		return NULL;
	cp = (char *)sbrk(nu * sizeof(Header));
	
	if(cp == (char *) -1)
		return NULL;

	up = (Header *) cp;
	up->s.size = nu;
	_free((void *) (up+1));
	return freep;
}

void _free(void *ap)
{
	Header *bp,*p;
	
	bp = (Header *) ap-1;

	for(p = freep; !(bp > p && bp <p->s.ptr);p = p->s.ptr)
		if(p >= p->s.ptr && (bp > p || bp < p->s.ptr))
			break;
	if(bp + bp->s.size == p->s.ptr)
	{
		//Create the bp filed child
		bp->s.size += p->s.ptr->s.size;
		bp->s.ptr = p->s.ptr->s.ptr;
	}
	else
		bp->s.ptr = p->s.ptr;

	if(p + p->s.size == bp)
	{
		p->s.size += bp->s.size;
		p->s.ptr = bp->s.ptr;
	}
	else
		p->s.ptr = bp;
	freep = p;
}
