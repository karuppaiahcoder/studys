#include <stdio.h>
#include <string.h>
#include<fcntl.h>
#include<stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/syscall.h>
#define _GNU_SOURCE
#define MAX_PATH 1024
#define NAME_MAX 14

typedef struct
{
	unsigned long d_ino;
        unsigned long  d_off;
        unsigned short d_reclen;      
        char           d_name[];  
}dirent;

typedef struct
{
        int fd;
        dirent d;
}DIR;


DIR *open_dir(char *dirname);
void closedir(DIR *dp);
int read_dir(DIR *dfd,char *name);

main(int argc,char *argv[])
{
	DIR *dp;
	if(argc==1)
	{
		dp=open_dir(".");
		strcpy(*argv,".");
	}
	else if(argc==2)
		if((dp=open_dir(*++argv))==NULL)
		{
			printf("Can't open the file %s\n",*argv);
			return;
		}
		read_dir(dp,*argv);

		info(*argv);	
}

DIR *open_dir(char *dirname)
{
        int fd;
        struct stat stbuf;
        DIR *dp;

	if((fd = open(dirname, O_RDONLY, 0)) == -1 || fstat(fd, &stbuf) == -1 || (stbuf.st_mode & S_IFMT) != S_IFDIR || (dp = (DIR *) malloc(sizeof(DIR))) == NULL)
        	return NULL;

        dp->fd = fd;
        return dp;
}

int read_dir(DIR *dfd,char *name)
{
	char buff[BUFSIZ]={},temp[100]={};
	dirent *fp;
	DIR *dp;
	int len=0,base=0;

	len=syscall(SYS_getdents,dfd->fd,buff,BUFSIZ);

	while(base < len)
	{
		fp = (dirent *) (buff+base);
		base+=fp->d_reclen;

		if(strcmp(fp->d_name,".")==0 || strcmp(fp->d_name,"..")==0)
				continue;

		sprintf(temp,"%s/%s",name,fp->d_name);

		if((dp=open_dir(temp))!=NULL)
		{
			read_dir(dp,temp);
			closedir(dp);
		}

		info(temp);
	}
}

info(char*temp)
{
	struct stat val;
	stat(temp,&val);	
	printf("\n. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .\n");
	printf("\nFILE : %s",temp);
	if((val.st_mode & S_IFMT) == S_IFDIR)
		printf("	\tDirectory\n");
	else
		printf("	\tRegular File\n");
	printf("FILE size : %ld\n",val.st_size);
	printf("I_node : %ld\n",val.st_ino);
}



void closedir(DIR *dp)
{
        printf("3_V_I_J_A_Y\n");
        if (dp)
        {
                close(dp->fd);
                free(dp);
        }
}
