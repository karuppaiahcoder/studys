/*
   1.First I get the input from user

   2.I check the input validation.

   3.if give a -R option so,it will do recursively.

   4.Otherwise I will do chmod current given file only .

   output format:

 */

#include<stdio.h>
#include<string.h>
#include <sys/stat.h>
#include <unistd.h>
#include<dirent.h>
int valid_num(char val[]);
int valid_file(char val[]);
void recur(char file[],char perm[]);
int main(int argc,char *argv[])
{

	struct stat bf;
	if(argc==3)				//operation of without -R option
	{
		if((valid_num(argv[1])))
		{
			if((valid_file(argv[2])))
				chmod(argv[2],strtol(argv[1],0,8));		//change the mode of given file
			else
				printf("No such file or directory :%s\n",argv[2]);	
		}
		else
		{
			printf("Not valid permission %s\n",argv[1]);
			printf("chmod:Usage [a.out][-R] [perms] [filename]\n");
		}
	}
	else if(argc==4)		//operation of with -R option
	{

		if((valid_num(argv[2]))==1 && (strcmp(argv[1],"-R"))==0)
		{
			if((valid_file(argv[3])))
			{
				stat(argv[3],&bf);			//verify the given file or directory
				if(S_ISDIR(bf.st_mode))		//if directory call recur function
					recur(argv[3],argv[2]);	
					chmod(argv[3],strtol(argv[2],0,8));	//changing the mode of given file
			}
			else
				printf("No such file or directory :%s\n",argv[3]);	
		}
		else
			printf("chmod:Usage [a.out][-R] [perms] [filename]\n");

	}
	else
		printf("chmod:Usage [a.out][-R] [perms] [filename]\n");

}
int valid_num(char val[])	//check the value number or not
{
	int i,len;
	len=strlen(val);
	for(i=0;i<len;i++)
	{
		if(isdigit(val[i]))
			;
		else 
			return 0;
	}
	return 1;
}
int valid_file(char val[])	//check the file present or not
{
	if((access(val,F_OK))==0)
		return 1;
	else
		return 0;
}

void recur(char file[],char perm[])
{
	DIR *d1;
	char temp[4096];
	strcpy(temp,file);
	struct dirent *f1;
	struct stat bf;
	d1=opendir(temp);	
	chdir(temp);
	while((f1=readdir(d1))!=NULL)
	{

		if((strcmp(f1->d_name,"."))==0 || (strcmp(f1->d_name,".."))==0)	//skip the entry .. and .
			continue;
		stat(f1->d_name,&bf);
		if(S_ISDIR(bf.st_mode))		//if a directory call recur function recursively
		{	
			recur(f1->d_name,perm);
			chmod(f1->d_name,strtol(perm,0,8)); //changing the permission of the directory	
			continue;
		}	
		chmod(f1->d_name,strtol(perm,0,8)); //changing the permission of the file	
	}
	chdir("..");
	return;	
}




