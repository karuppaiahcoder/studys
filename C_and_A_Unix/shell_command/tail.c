#include<stdio.h>
#include<fcntl.h>
#include<stdlib.h>
#include<string.h>
char *load_buf(int fd);
int main(int argc,char *argv[])
{
	int ifd,size=1,i=1,k,opt=0,tail=10;
	char *buf,*lines[1024]={};
	char stack[1024]={};
	if(argc==3)
	{
		if(!strcmp(argv[1],"-f"))
			opt=1;
		else if(isdigit(*argv[1]))
			tail=atoi(argv[1]);
		argc--;
		i++;
	}
	if(argc==2 && tail>0)
	{
		ifd=open(argv[i],O_RDONLY);
		i=0;
		if(ifd!=-1)
		{
			while((buf=load_buf(ifd))!=NULL)
			{
				lines[i]=buf;
				i++;
			}
			for(k=i-tail;k<i;k++)
				printf("%s",lines[k]);
			if(opt==1)
				while(1)
					if((size=read(ifd,stack,1024))>0)
						write(1,stack,size);
		}
	}
	close(ifd);
}

char *load_buf(int fd)
{
	char c,buf[1024]={},*a;
	int size=1,i=0;
	while(read(fd,&c,1))
	{
		buf[i++]=c;
		if(c=='\n')
			break;
	}
	if(i>0)
	{
		a=(char *)malloc(strlen(buf)+1);
		strcpy(a,buf);
		return a;
	}
	else
		return NULL;
}
