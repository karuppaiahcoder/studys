/*1.Implement the find command

  2. option -name,-type,-cmin,-mmin

 */

#include<stdio.h>
#include<string.h>
#include <unistd.h>
#include <sys/stat.h>
#include<dirent.h>
#include<stdlib.h>
#include<ctype.h>

char dirpath[4096][4096],fipath[4096][4096],filter[2044][2044];		//declare file array directory array
int di=0,fi=0,clob=0,p=0;
void recur(char file[]);						//find the all the files recursively
void typech(char t[]);							//-type option
void change(char ch[],int n);						//-cmin and -mmin option
void search(char file[],int n);						//-name and -iname option
int main(int argc,char *argv[])
{


	int i=1;
	struct stat bf;
	int name=0,type=0,cmin=0,mmin=0,iname=0,path=0;
	char file[1000],ifile[1000],t[100],ch[100],mod[100];
	while(i<argc)
	{

		if((strcmp(argv[i],"-name"))==0)				
		{
			name++;
			if(i+1<argc)
				strcpy(file,argv[++i]);
			else
			{
				printf("find: missing argument to `-name'\n");
				return;
			}
		}
		else if((strcmp(argv[i],"-iname"))==0)				
		{
			iname++;
			if(i+1<argc)
				strcpy(ifile,argv[++i]);
			else
			{
				printf("find: missing argument to `-iname'\n");
				return;
			}
		}
		else if((strcmp(argv[i],"-type"))==0)				
		{
			type++;
			if(i+1<argc)
				strcpy(t,argv[++i]);
			else
			{	
				printf("find: missing argument to `-type'\n");
				return;
			} 	

		}
		else if((strcmp(argv[i],"-cmin"))==0)				
		{
			cmin++;
			if(i+1<argc)
				strcpy(ch,argv[++i]);
			else
			{
				printf("find: missing argument to `-cmin'\n");
				return;
			}

		}
		else if((strcmp(argv[i],"-mmin"))==0)				
		{
			mmin++;
			if(i+1<argc)
				strcpy(mod,argv[++i]);
			else
			{	
				printf("find: missing argument to `-mmin'\n");
				return;
			}	
		}
		else if((strcmp(argv[i],"-P"))==0 && i==1)
		{
			p=1;	
		}
		else if((strcmp(argv[i],"-L"))==0 && i==1)
		{
			p=2;
		}
		else
		{
			if(i==1)
			{
				path=1;
				i++;
				continue;	
			}
			else if(i==2 && ((strcmp(argv[1],"-P"))==0 || (strcmp(argv[1],"-L"))==0)) 
			{
			    path=2;
			    i++;
			    continue;	
			}
			printf("Usage [./a.out][[-P][-L]][options]\n");
			return;
		}
		i++;
	}

	if(path==1 || path==2)
	{
		if((access(argv[path],F_OK))==0)
		{
			stat(argv[path],&bf);
			if(S_ISDIR(bf.st_mode))
				recur(argv[path]);	
		}
		else
		  {	
			printf("No such Path:%s\n",argv[path]);	
			exit(1);
		  }
	}
	else
		recur(".");
	if(type==1)
		typech(t);	//find type of file 	
	if(name==1)
		search(file,1);
	else if(iname==1)
		search(ifile,2);
	if(cmin==1)
		change(ch,1);
	if(mmin==1)
		change(mod,2);
	for(i=0;i<clob;i++)
		printf("%s\n",filter[i]);

}
void recur(char file[])
{
	DIR *d1;
	char temp[2046];
	strcpy(temp,file);
	struct dirent *f1;
	struct stat bf;
	char buf[2046];
	d1=opendir(temp);
	chdir(temp);
	while((f1=readdir(d1))!=NULL)
	{

		if((strcmp(f1->d_name,"."))==0 || (strcmp(f1->d_name,".."))==0)        //skip the entry .. and .
			continue;
		lstat(f1->d_name,&bf);
		if(p==2)
			stat(f1->d_name,&bf);
		if(S_ISDIR(bf.st_mode))                //if a directory call recur function recursively
		{
			sprintf(dirpath[di++],"%s/%s",getwd(buf),f1->d_name);
			strcpy(filter[clob++],dirpath[di-1]);
			recur(f1->d_name);
			continue;
		}
		sprintf(fipath[fi++],"%s/%s",getwd(buf),f1->d_name);
		strcpy(filter[clob++],fipath[fi-1]);
	}
	chdir("..");
	return;
}

void typech(char t[])
{
	int i,j=0;
	struct stat bf;
	if((strcmp(t,"f"))==0)				//file search
	{
		for(i=0;i<fi;i++)
		{
			lstat(fipath[i],&bf);
			if(p==2)
			     stat(fipath[i],&bf);		
			if(S_ISREG(bf.st_mode))
				strcpy(filter[j++],fipath[i]);
		}  	
	}
	else if((strcmp(t,"d"))==0)			//dir search
	{
		for(i=0;i<di;i++)
		{
			lstat(dirpath[i],&bf);
			if(p==2)
				stat(dirpath[i],&bf);
			if(S_ISDIR(bf.st_mode))	
				strcpy(filter[j++],dirpath[i]);
		}
	}
	else if((strcmp(t,"l"))==0)			//link search
	{
		for(i=0;i<fi;i++)
		{
			lstat(fipath[i],&bf);
			if(S_ISLNK(bf.st_mode))
				strcpy(filter[j++],fipath[i]);
		}
	}
	else if((strcmp(t,"b"))==0 || (strcmp(t,"c"))==0 || (strcmp(t,"p"))==0 || (strcmp(t,"s"))==0)	//block and character etc..
	{
		for(i=0;i<fi;i++)
		{
			lstat(fipath[i],&bf);
			if(p==2)
			      stat(fipath[i],&bf);
			if(S_ISBLK(bf.st_mode) || S_ISFIFO(bf.st_mode) ||S_ISSOCK(bf.st_mode) || S_ISCHR(bf.st_mode))
				strcpy(filter[j++],fipath[i]);   
		}

	}
	else
	{
		printf("Plese give valid format:%s\n",t); 	
		exit(1);
	}

	clob=j; 
}



void search(char file[],int n)
{
	char temp[2046],temp1[2046],temp2[2046][2046];
	int i,len,j,k,s=0;
	for(i=0;i<clob;i++)
	{	
		k=0;
		len=strlen(filter[i]);
		strcpy(temp,filter[i]);		
		for(j=len-1;temp[j]!='/';j--)
			temp1[k++]=temp[j];
		temp1[k]='\0';
		len=strlen(temp1);
		k=0;
		for(j=len-1;j>=0;j--)
			temp[k++]=temp1[j]; 
		temp[k]='\0';
		if(n==1 && (strcmp(file,temp))==0)
			strcpy(temp2[s++],filter[i]);	
		else if(n==2 && (strcasecmp(file,temp))==0)
			strcpy(temp2[s++],filter[i]);	

	}

	for(i=0;i<clob;i++)
	{
		strcpy(filter[i],temp2[i]);
		clob=s;
	}

}

void change(char ch[],int n)
{
	
	struct stat bf;
	time_t t1;

	char temp[2046][2046];
	int i,a=-1,j=0;
	for(i=0;i<clob;i++)
	{
		stat(filter[i],&bf);
		if(p==2)
			stat(filter[i],&bf);
		if(ch[0]=='+')					//find mmin and cmin using +
		{
			a=atoi(&ch[1]);
			if(a>=0)
			{
				time(&t1);

				if(n==1) 	
				{
					if(bf.st_ctime <t1-(a*60))
						strcpy(temp[j++],filter[i]);	
				}
				else
				{
					if(bf.st_mtime <t1-(a*60))
						strcpy(temp[j++],filter[i]);
				}
			}
			else
			{
				printf("Please give valid format:%d\n",a);
				exit(1);
			}

		}

		else if(ch[0]=='-')			//find mmin and cmin file using -
		{
			a=atoi(&ch[1]);
			if(a>=0)
			{
				time(&t1);
				if(n==1) 
				{       
					if(bf.st_ctime >t1-(a*60) ) //&& bf.st_ctime < t1)      
						strcpy(temp[j++],filter[i]);
				}
				else
				{
					if(bf.st_mtime >t1-(a*60) ) //&& bf.st_mtime < t1)
						strcpy(temp[j++],filter[i]);
				}		
			}
			else
			{
				    printf("Please give valid format:%d\n",a);
				exit(1);
			}

		}
		else if(isdigit(ch[0]))		//find mmin and cmin file exactly	
		{
			a=atoi(ch);

			if(a>=0)
			{	
				time(&t1);
				if(n==1)
				{	
					if(bf.st_ctime/60 == (t1-(a*60))/60)
						strcpy(temp[j++],filter[i]);
				}
				else
				{

					if(bf.st_mtime/60 == (t1-(a*60))/60)
						strcpy(temp[j++],filter[i]);
				}
			}
			else
			{
				printf("Please give valid format:%d\n",a);
				exit(1);
			}


		}
		else
		{
			printf("Please give valid format:%s\n",ch);
			exit(1);		
		}
	}
	
	for(i=0;i<j;i++)
		strcpy(filter[i],temp[i]);	
  clob=j;
}
