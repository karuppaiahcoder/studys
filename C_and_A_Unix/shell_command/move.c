#include<stdio.h>
#include <sys/stat.h>
#include<fcntl.h>
#include <unistd.h>
#define BUFSIZE 9
int main(int argc,char *argv[])
{
	int ifd,ofd,size=1;
	if(argc==3)
	{
		ifd=open(argv[1],O_RDONLY);
		ofd=creat(argv[2],0700);
		if(ifd!=-1&&ofd!=-1)
		{
			while(size)
			{
				char buf[BUFSIZE]={};
				size=read(ifd,buf,BUFSIZE);
				write(ofd,buf,size);
			}
			unlink(argv[1]);
		}else if(ifd!=-1)
			printf("%s :Not a file or directory\n",argv[3]);
		else
			printf("%s :Not a file or directory\n",argv[1]);
			
	}else
		printf("Give input format like this     ./a.out file_1 file_2\n");
	close(ifd);
	close(ofd);
}
