#include<stdio.h>

int Index_sequential_search(int ptr[],int val,int size);

int main()
{
	int array[20]={},max,srh,i;
	printf("Enter the size of index:");
	scanf("%d",&max);

	for(i=0;i<max;i++)
		scanf("%d",&array[i]);

	printf("Enter the searching number:");
	scanf("%d",&srh);
	
	if((i=Index_sequential_search(array,srh,max))>=0)
		printf("Your searching number %d . Array location %d\n\n",array[i],i);
	else
		printf("Can't occur the  %d number\n",srh);
}

int Index_sequential_search(int ptr[],int val,int size)
{
	int i;
	for(i=0;i<size;i++)
		if(ptr[i]==val)
			return i;
	return -1;
}
