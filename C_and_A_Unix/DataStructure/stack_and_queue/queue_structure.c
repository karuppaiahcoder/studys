#include<stdlib.h>
#include<stdio.h>

typedef struct 
{
	int max;
	int rear;
	int front;
	int size;
	int *element;
}Queue;

int Enqueue(int a);
int Dequeue();

Queue * Create_Queue(int max);

Queue *q;

main()
{
	q=Create_Queue(10);

	Enqueue(5);

	printf("%d\n",Dequeue());
}

int Dequeue()
{
	if(q->size==0)
		printf("Queue is empty!!!\n");
	else
	{
		q->size--;

		if(q->front>=q->max)
			q->front=0;

		return q->element[q->front++];
	}
}

int Enqueue(int value)
{

	if(q->size==q->max)
		printf("Queue full !!!\n");
	else
	{
		q->size++;
		q->rear=q->rear+1;
		if(q->rear>=q->max)
			q->rear=0;
		q->element[q->rear]=value;
	}

}

Queue * Create_Queue(int max)
{
	q =(Queue *)  malloc(sizeof(Queue));
	q->element=(int *) malloc(sizeof(int)*max);
	q->max=max;
	q->rear=-1;
	q->front=0;
	q->size=0;
	return q;
}
