#include<stdio.h>
#include<stdlib.h>
#define MAX 200

typedef struct 
{
	int capacity;
	int top;
	int *elements;
}Stack;

Stack* Create_Stack(int size);

void push(int value);
int pop();

Stack *S;

main()
{
	S=Create_Stack(10);
	push(7);
	int a=pop();
	printf("Stack pop value:%d\n",a);
}

int pop()
{
	if(S->top==0)
	{
		printf("Stack empty\n");
		return 0;
	}
	else
	{
		S->top--;
		//printf("%d\n", stack->elements[stack->top]);
		return S->elements[S->top--];
	}
}

void push(int value)
{
	if(S->top>=S->capacity)
	{
		printf("Stack full Can't push that value %d\n",value);
	}
	else
	{
		S->elements[S->top]=value;	
		S->top++;
	}
}

Stack* Create_Stack(int size)
{
	Stack *S;

	S = (Stack*)malloc(sizeof(Stack));

	S->capacity=size;
	S->top=0;

	S->elements=(int *) malloc(sizeof(int)*size);

	return S;
}
