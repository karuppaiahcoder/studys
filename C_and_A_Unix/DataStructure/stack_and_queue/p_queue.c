#include<stdio.h>
#include<stdlib.h>

typedef struct pqueue
{
	int *data;
	int size;
	int max;
	int front;
	int rear;
}QUE;

QUE *Queue_store;
QUE * create_queue(int num);

int main()
{
	int val=0,num,a=0;
	printf("Enter the queue size\n");
	scanf("%d",&num);

	Queue_store=create_queue(num);
	
	while(1)
	{
		printf("1.insert\n");
		printf("2.delete\n");
		printf("3.display\n");
		printf("4.priority \n");
		printf("5.exit\n");

		scanf("%d",&num);
		
		switch(num)
		{
			case 1:
				printf("Enter the value\n");
				scanf("%d",&val);
				
				a=insert(Queue_store,val);
				if(a)
					printf("value inserted\n");
				else
					printf("value not inserted\n");
				break;

			case 2:
				delete(Queue_store,num);
				break;

			case 3:
				display(Queue_store);
				break;

			case 4:
				priority(Queue_store);
				break;
		
			case 5:
				printf("Thank you!!!\n");
				exit(0);
			
			default:

				printf("Invalid option\n");
				break;
		}
	}
}


QUE * create_queue(int num)
{
	Queue_store=(QUE *)malloc(sizeof(QUE));

	Queue_store->data=(int *)malloc(sizeof(int)*num);

	Queue_store->size=0;
	Queue_store->max=num;
	Queue_store->front=0;
	Queue_store->rear=0;
}


int insert(QUE *ptr,int num)
{
	if(ptr->rear==ptr->max)
	{
		printf("queue full\n");
		return 0;
	}
	else
	{
		ptr->size++;
//		ptr->rear;
		ptr->data[ptr->rear++]=num;		
	}
}

int delete(QUE *ptr)
{
	if(ptr->size==0)
		printf("queue is Empty\n");
	else
	{
		ptr->size--;
		printf("%d ",ptr->front++);
		printf("deleted success	\n");
	}
	
}

int display(QUE* ptr)
{
	int i=ptr->front;
	while(i!=ptr->rear)
	{
		printf("%d  ",ptr->data[i++]);
	}
	putchar('\n');	
}

int priority(QUE *pp)
{

	int i=0,j=0;	
	int op=0,tmp1=pp->front,tmp2=pp->rear,temp=0;
	printf("1.ascending\n");
	printf("2.descending\n");
	scanf("%d",&op);
	printf("tmp1=%d\ntemp2=%d\n",tmp1,tmp2);
	for(;tmp1<tmp2;tmp1++)
	{
		for(i=j=tmp1;j<tmp2;j++)
		{
			printf("i=%d\nj=%d\n",i,j);
			if(op==1)
			{
				if(pp->data[i]>pp->data[j])
				{
					temp=pp->data[i];
					pp->data[i]=pp->data[j];
					pp->data[j]=temp;
				}
			}
			else
			{
				if(pp->data[i]<pp->data[j])
				{
					temp=pp->data[i];
					pp->data[i]=pp->data[j];
					pp->data[j]=temp;
				}

			}
			display(pp);
		}
	}
}
