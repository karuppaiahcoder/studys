#include<stdio.h>
#define MAX 100
main()
{
	int array[MAX]={},max,i;

	printf("Enter the size:");
	scanf("%d",&max);

	for(i=0;i<max;i++)
		scanf("%d",&array[i]);

	heap_sort(array,max);

	for(i=0;i<max;i++)
		printf("%d ",array[i]);
}

int heap_sort(int ptr[],int size)
{
	int temp,use=0;
	while(size>0)
	{
		if(((size-1)%2)==0)
			use=1;
		else
			use=2;

		int last=size-1,p=0,l=0,r=0,c=0;
		while(last>0)	
		{
			if(use==1)
				p=(last-use)/2;
			else
				p=last/2;

			l=2*p+1;
			r=2*p+2;
			if(r<=(size-1))
			{
				if(ptr[l]>ptr[r])
					c=l;
				else
					c=r;
			}else
				c=l;
			last-=use;

			if(ptr[c]>ptr[p])
			{
				int tmp=ptr[p];
				ptr[p]=ptr[c];
				ptr[c]=tmp;
			}
		}

		temp=ptr[0];
		ptr[0]=ptr[size-1];
		ptr[size-1]=temp;
		size--;
	}
}
