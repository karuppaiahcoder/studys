#include<stdio.h>
#include<stdlib.h>
#define MAX 50
typedef struct t
{
	int val;
	struct t *L;
	struct t *R;
}TREE;
int sort(TREE *root,int ptr[]);
int Binary_tree_sort(int ptr[],int size);
TREE *Create_tree(TREE *root,int val);

main()
{
        int array[MAX]={},max,i;

        printf("Enter the size:");
        scanf("%d",&max);

        for(i=0;i<max;i++)
                scanf("%d",&array[i]);

	Binary_tree_sort(array,max);

        for(i=0;i<max;i++)
                printf("%d ",array[i]);
	printf("\n");
}

int Binary_tree_sort(int ptr[],int size)
{
	int i;

	TREE *root=NULL;
	for(i=0;i<size;i++)
		root=Create_tree(root,ptr[i]);
	sort(root,ptr);
}

TREE *Create_tree(TREE *root,int val)
{
	if(root==NULL)
	{
		root=(TREE *)malloc(sizeof(TREE));
		root->L=root->R=NULL;
		root->val=val;
	}else if(root->val<val)
	{
		root->R=Create_tree(root->R,val);
	}else
	{
		root->L=Create_tree(root->L,val);
	}
	return root;
}

sort(TREE *root,int ptr[])
{
	static i=0;

	if(root!=NULL)
	{
		sort(root->L,ptr);
		ptr[i++]=root->val;
		sort(root->R,ptr);
	}
}
