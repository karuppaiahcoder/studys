#include<stdio.h>
#define MAX 1000
void swap(int v[],int left,int right);
void q_sort(int v[],int left,int right);
int main()
{
	int i,j,arr[MAX],tot=0;
	printf("Enter total number of elements\n");
	scanf("%d",&tot);

	printf("Enter %d numbers\n",tot);
	for(i=0;i<tot;i++)
		scanf("%d",&arr[i]);

	q_sort(arr,0,tot-1);

	for(j=0;arr[j]!=0;j++)
		printf("%d\n",arr[j]);
}	

void q_sort(int v[],int left,int right)
{
	int i=0,last=0;
	if(left>=right)
		return;

	swap(v,left,(left+right)/2);

	last=left;
	
	for(i=left+1;i<right;i++)
		if(v[i]<v[left])
			swap(v,++last,i);
	swap(v,left,last);

	qsort(v,left,last-1);
	qsort(v,last+1,right);
}


void swap(int v[],int left,int right)
{
	int tmp=0;
	
	tmp=v[left];
	v[left]=v[right];
	v[right]=tmp;
}			
