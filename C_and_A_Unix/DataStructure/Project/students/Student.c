#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#define MAX 100
typedef struct Stud 
{
	char *name;	
	int reg_no;
	char *f_name;
	int age;
	char *gender;
	char *DOB;
	char *depart;
	char *sem;
	char *bg;
	long long int m_num;
	char *city;
	struct M
	{
		int sub1;
		int sub2;
		int sub3;
		int sub4;
		int sub5;
		int sub6;
		int sub7;
	}mark;

	struct Stud *next;	//Create the single linked list to same students names.
}Students;

char *gender[]={"Male","Female","Other",""};
char *depart[]={"DME","DCE","DCT","DEEE",""};
char *bg[]={"A+","A-","B+","B-","AB+","AB-","O+","O-",""};
char *sem[]={"-I","-II","-III","-IV","-V","-VI",""};

Students *location(Students *tap[],char *ptr);
int add_stu(Students *tab[],char *p);
int str_cmp(char *ptr1,char *ptr2);
char *alloc(int size);
int check(Students *tab[],int reg_no);
Students *search(Students *tab[]);
int sem_mark();
Students *find(int a,int val,Students *temp[]);
void details(Students *node);

Students *Stud_tab[MAX];	//Students entry table array.

int printer=0;

int main()
{
	Students *temp;
	int opt=0,exit=1;

	while(1)
	{
		printf("\n\n===============/*Welcome To st.joseph College*/============\n\n");
		printf("1.Add the Student details\n");
		printf("2.View All Student details\n");
		printf("3.Search\n");
		printf("4.exit\n");
		scanf("%d",&opt);
		getchar();
		if(opt==1)
		{		
			while(exit==1)
			{
				char name[MAX]={};
				printf("\nNAME\t\t\t\t:");
				gets(name);
				add_stu(Stud_tab,name);
				printf("\n1.Next\n(!1).back\n");
				scanf("%d",&exit);	
				getchar();
			}
			exit=1;
		}else if(opt==2)
		{
			print(Stud_tab);	
		}else if(opt==3)
		{
			temp=search(Stud_tab);	
			if(temp!=NULL)
				details(temp);
		}else if(opt==4)
		{
			printf("Thank you!!!!\n");
			break;
		}
	}
}

void details(Students *node)	//Print the single students full data 
{
	printf("\n\n==========/*****Student Data and Information*****/==========\n\n");

	printf("\t\tRegister Number             : %d\n",node->reg_no);
	printf("\t\tStudent Name                : %s\n",node->name);
	printf("\t\tFather Name                 : %s\n",node->f_name);
	printf("\t\tStudent Age                 : %d\n",node->age);
	printf("\t\tStudent Gender              : %s\n",node->gender);
	printf("\t\tDate Of Birth               : %s\n",node->DOB);
	printf("\t\tDepartment and Semester     : %s%s\n",node->depart,node->sem);
	printf("\t\tBlood Group                 : %s\n",node->bg);
	printf("\t\tMobile Number               : %lld\n",node->m_num);
	printf("\t\tCity                        : %s\n",node->city);
	printf("\n====================*This Semester Marks*====================\n\n");
	printf("\t\tSubject-1                   : %d\n",node->mark.sub1);
	printf("\t\tSubject-2                   : %d\n",node->mark.sub2);
	printf("\t\tSubject-3                   : %d\n",node->mark.sub3);
	printf("\t\tSubject-4                   : %d\n",node->mark.sub4);
	printf("\t\tSubject-5                   : %d\n",node->mark.sub5);
	printf("\t\tSubject-6                   : %d\n",node->mark.sub6);
	printf("\t\tSubject-7                   : %d\n",node->mark.sub7);
	printf("\n========================*All The Best*========================\n\n");
}

Students *search(Students *tab[])
{
	int val=0,opt,us=0;
	Students *temp[MAX]={},*p;
	if(tab[0]==NULL)
	{
		printf("\n\nStudents data empty !!!!\n\n");
		return NULL;
	}
	while(1)
	{
		printf("Available searching:\n");
		printf("\t1.Reg-No\n\t2.Student Name\n\t3.Father Name\n\t4.Department\n\t5.DOB\n\t6.Back\n\n");
		scanf("%d",&opt);
		getchar();
		printer=0;
		if(opt>=3&&5>=opt)
			printer=1;
	L:	switch(opt)
		{
			case 1:			//Reg-No
				while(1)
				{	
					printf("Enter Register Number: ");
					scanf("%d",&val);
					if(us==1)
						us=check(temp,val);
					if(us>=0)
						p=find(opt,val,temp);
					else
					{
						printf("Register number is doest not match!!!!!!!\n");
						goto L;
					}
					if(p!=NULL)
						return p;
					else
						printf("Not match the register number\n\n------->Try again!!! \n");
				}
				break;
			case 2:			//Student Name
				printf("Enter the Student Name: ");
				p=find(opt,val,temp);
				if(p==NULL)
	                                    printf("Not match the Student Name\n\n------->Try again!!! \n");
				else
					print(temp);
				break;
			case 3:			//Father Name
				printf("Enter the Father Name: ");
				p=find(opt,val,temp);
				if(p==NULL)
				     printf("Not match the Father Name\n\n------->Try again!!! \n");
                                else
                                	print(temp);
				break;
			case 4:			//Department 
				printf("Enter the Department Name: ");
				p=find(opt,val,temp);
				if(p==NULL)
				     printf("Not match the Department Name\n\n------->Try again!!! \n");
				else
				        print(temp);
				break;
			case 5:			//DOB 
				printf("Enter the Data Of Birth (dd/mm/yyyy): ");
				p=find(opt,val,temp);
				if(p==NULL)
				     printf("Not match the Data Of Birth\n\n------->Try again!!! \n");
				else
				        print(temp);
				break;
			case 6:
				return NULL;
				break;
			default:
				printf("Non-valid option!!!!\n");
				break;
		}
		if((p!=NULL&&p->next!=NULL)||(p!=NULL&&temp[1]!=NULL))	//Check the multi matching string
		{
			us=opt=1;
			goto L;
		}
		else if(p!=NULL)
			return temp[0];
	}
}

Students *find(int opt,int val,Students *array[])
{
	Students *temp;
	int i,j=0;
	char p[25]={};
	switch(opt)
	{
		case 1:
			for(i=0;Stud_tab[i]!=NULL;i++)		//search Reg_no
		        {
                		for(temp=Stud_tab[i];temp!=NULL;temp=temp->next)
		                {
					if(temp->reg_no==val)
					{
						array[j++]=temp;
						return array[0];
					}
				}
			}
			return NULL;
			break;
		case 2:
			gets(p);			//search Student Name
			for(i=0;Stud_tab[i]!=NULL;i++)
			{
			        for(temp=Stud_tab[i];temp!=NULL;temp=temp->next)
			        {
			                if(str_cmp(temp->name,p)==0)
					{
			                        array[j++]=temp;
						break;
					}
			        }
			}
			break;
		case 3:
			gets(p);			//search Father Name
			for(i=0;Stud_tab[i]!=NULL;i++)
			{
			        for(temp=Stud_tab[i];temp!=NULL;temp=temp->next)
			        {
			                if(str_cmp(temp->f_name,p)==0)
			                        array[j++]=temp;
			        }
			}			
			break;
		case 4:
			gets(p);			//search Departments
			for(i=0;Stud_tab[i]!=NULL;i++)
			{
			        for(temp=Stud_tab[i];temp!=NULL;temp=temp->next)
			        {
			                if(str_cmp(temp->depart,p)==0)
			                        array[j++]=temp;
			        }
			}
			break;
		case 5:
			gets(p);			//search Data of Birth 
			for(i=0;Stud_tab[i]!=NULL;i++)
			{
			        for(temp=Stud_tab[i];temp!=NULL;temp=temp->next)
			        {
			                if(str_cmp(temp->DOB,p)==0)
			                        array[j++]=temp;
			        }
			}
			break;
	}
	if(j==0)
		return NULL;
	else
		return array[0];
}

									
print(Students *tab[])	//Print the all student informations.
{
	Students *temp;
	int i,tmp;
	printf("\n\n\nName\t\tRe-No\tDepartment   Father\t     Age\t  DOB\t    Gender\t  Mobile     City\n");
	for(i=0;tab[i]!=NULL;i++)
	{
		for(temp=tab[i];temp!=NULL;temp=temp->next)
		{
			printf("%s",temp->name);
			tmp=16-(strlen(temp->name));
			while(tmp-->0)
				printf(" ");
			printf("%d\t",temp->reg_no);
			printf("%s\t      ",temp->depart);
			printf("%s",temp->f_name);
			tmp=16-(strlen(temp->f_name));
			while(tmp-->0)
				printf(" ");
   			printf("%d\t",temp->age);
			printf("%s    ",temp->DOB);
			printf("%s\t",temp->gender);
			printf("%lld  ",temp->m_num);
			printf("%s\n",temp->city);
			if(printer==1)
				break;
		}
	}
}

int check(Students *tab[],int reg_no)	//Check the register number already exit
{
	int i;
	Students *temp;
	for(i=0;tab[i]!=NULL;i++)
                for(temp=tab[i];temp!=NULL;temp=temp->next)
			if(temp->reg_no==reg_no)
				return 0;
	return 1;
}

int add_stu(Students *tab[],char *p)
{
	Students *node=location(tab,p);
	int temp,us;
	if(node==NULL)
	{
		return;
	}
	else
	{
		node->name=alloc(strlen(p));//address allocation 
		strcpy(node->name,p);
		node->next=NULL;
		
	 R:	printf("\n\nReg-No(Four digit)\t\t:");  	//Reg-No
		scanf("%d",&temp);
		getchar();
		if(temp>=1000&&temp<=9999&&(us=check(tab,temp))==1)	//The check function to check the register number 
			node->reg_no=temp;				// already exit
		else
		{
			if(isalpha(temp))
				while((getchar())!='\n');
			else if(us==0)
	 			printf("This Register number already Exits !!!!\n");
			printf("Try again\n");
			goto R;
		}

		printf("\n\nFather Name\t\t\t:");			//F_Name;
		gets(p);
		node->f_name=alloc(strlen(p));
		strcpy(node->f_name,p);
		printf("\n");

	A:	printf("\nAge(minimum 18)\t\t\t:");				//Age
		scanf("%d",&temp);
		if(temp>=18)
			node->age=temp;
		else
		{
			printf("Try again\n");
                        goto A;
		}

		while(1)				//Gender
		{
			printf("\n\nChoose the number (1-3) Gender:\n");
			printf("1.Male	\t2.Female	\t3.Other\n");
			scanf("%d",&temp);
			if(temp>=1&&3>=temp)
			{
				node->gender=alloc(strlen(gender[temp-1]));//Get the memory space to store the gender 
				strcpy(node->gender,gender[temp-1]);	//The gender[] array have the three format gender
				break;
			}
			else
				printf("Non-Valid option!!\n------------Try again!!!!!!!\n\n");
		}
		
		printf("\n\nDOB (dd/mm/yyyy)\t\t:");					//DOB 
		getchar();
		gets(p);
		node->DOB=alloc(strlen(p));		//Store the Data Of birth in string
		strcpy(node->DOB,p);

		while(1)
		{
			printf("\n\nChoose the number (1-4) Course:\n");
			printf("1.DME	\t3.DCT\n");
			printf("2.DCE	\t4.DEEE\n");
			scanf("%d",&temp);
			if(temp>=1&&4>=temp)
			{
				node->depart=alloc(strlen(depart[temp-1]));	//Get the memory space to store the department
                                strcpy(node->depart,depart[temp-1]);	//The depart[] array have the three department name
				break;
			}else
				printf("Non-Valid option!!\n------------Try again!!!!!!!\n\n");
		}

                while(1)
                {
                        printf("\n\nChoose the number (1-6) Semester:\n");
                        printf("1.I   \t3.III\t5.V\n");
                        printf("2.II   \t4.IV\t6.VI\n");
                        scanf("%d",&temp);
                        if(temp>=1&&6>=temp)
                        {
                                node->sem=alloc(strlen(sem[temp-1]));	//Get the memory space to store semester
                                strcpy(node->sem,sem[temp-1]);		//The sem[] array have the all semester format
                                break;
                        }else
                                printf("Non-Valid option!!\n------------Try again!!!!!!!\n\n");
                }

		while(1)
		{
			printf("\nChoose the number (1-8) Blood Group:\n");
			printf("1.A+\t2.A-\t3.B+\t4.B-\n");
			printf("5.AB+\t6.AB-\t7.O+\t8.O-\n");
			scanf("%d",&temp);
			if(temp>=1&&8>=temp)
                        {
                                node->bg=alloc(strlen(bg[temp-1]));	//Get the memory space to store blood group 
                                strcpy(node->bg,bg[temp-1]);		//The bg[] array have the all blood group format
				break;
                        }else
                                printf("Non-Valid option!!\n------------Try again!!!!!!!\n\n");
		}

		while(1)
		{
			printf("\nMobile Number (10 digits)\t:");		//Mobile Number
			long long int num;
			scanf("%lld",&num);
			if(num>=1000000000&&9999999999>=num)		//Check the mobile number must 10 digits
			{
				node->m_num=num;
				break;
			}else
				printf("Your Entered Mobile number doesn't having 10 digits\n----------Try again!!!\n");
		}

 	    printf("\n\nCity\t\t\t\t:");			//City
		getchar();
		gets(p);
		node->city=alloc(strlen(p));	
		strcpy(node->city,p);
		printf("\n");
		printf("Enter the 7 subject marks:\n\n");	//Get the 7 subject marks
		printf("Subject_1\t\t\t:");
		node->mark.sub1=sem_mark();
		printf("Subject_2\t\t\t:");
		node->mark.sub2=sem_mark();
		printf("Subject_3\t\t\t:");
		node->mark.sub3=sem_mark();
		printf("Subject_4\t\t\t:");
		node->mark.sub4=sem_mark();
		printf("Subject_5\t\t\t:");
		node->mark.sub5=sem_mark();
		printf("Subject_6\t\t\t:");
		node->mark.sub6=sem_mark();
		printf("Subject_7\t\t\t:");
		node->mark.sub7=sem_mark();
	}
}

int sem_mark()
{
	int tmp;
	while(1)
	{
		scanf("%d",&tmp);
		if(tmp>=0&&100>=tmp)	//Check the mark max and min (0-100)
			return tmp;
		else
			printf("Maximum (0 to 100) mark\n------>Try Again!!!\n");
	}
}


char *alloc(int size)		//This function allocate the address to all Structure pointer members to store the string
{
	char *p;
	p=(char *)malloc(size);
	return p;
}

int str_cmp(char *ptr1,char *ptr2)
{
	while(toupper(*ptr1)==toupper(*ptr2))
	{
		if(*ptr1=='\0'&&*ptr2=='\0')
			return 0;	
		ptr1++;ptr2++;
	}
	return 1;
}

Students *location(Students *tab[],char *ptr)
{
	int i=0;
	Students *node,*tmp;

	while(tab[i]!=NULL&&str_cmp(tab[i]->name,ptr)!=0)
		i++;

	if(tab[i]!=NULL)
	{
		tmp=tab[i];
		while(tmp->next!=NULL)
			tmp=tmp->next;

		node=tmp->next=(Students *)malloc(sizeof(Students));
		node->next=NULL;
	}
	else
	{
		node=tab[i]=(Students *)malloc(sizeof(Students));
		node->next=NULL;
	}
	return node;
}
