#include<stdio.h>
#include<ctype.h>
#include<stdlib.h>
#include<string.h>
#define MAX 500

typedef struct
{
	char *fname;
	char *fdata;
}files;

typedef struct node
{
	char *dir_name;
	int fcount;
	int dcount;
	files *file[MAX];
	struct node *dir_link[MAX];
	struct node *prev;
}dir;

dir *USER=NULL,*temp=NULL;
char path[100]={};

char *alloc(char *name)
{
	char *ptr=(char *)malloc(strlen(name));
	strcpy(ptr,name);
	return ptr;
}

clean()
{
	system("reset");
}

dir *Create_dir(char *name)
{
	dir *node;
	node=(dir *)malloc(sizeof(dir));
	node->dir_name=alloc(name);
	node->dcount=node->fcount=0;
	node->prev=node->dir_link[0]=NULL;
	return node;
}

char *comd[]={"ls","cat","mkdir","cd","rmdir","rm","pwd","cp","type","help","clear","exit",""};

int find_command(char *com)
{
	int i;
	for(i=0;i<12;i++)
		if(!strcmp(comd[i],com))
			return i+1;
	return 0;
}

int exist(dir *node,char *name)
{
	int i=0;
	while(i<node->fcount)
		if(!strcmp(node->file[i++]->fname,name))
			return i;
	i=0;
	while(i<node->dcount)
		if(!strcmp(node->dir_link[i++]->dir_name,name))
			return i;
	return 0;
}

int cat(dir *node,char *list)
{
	int i=0,j=0,len=strlen(list);
	char file[50]={};
//	while(isspace(list[i])) i++;
	if(list[i]=='>')
	{
		i++;
		while(isspace(list[i])) i++;

		while(!isspace(list[i]) && i<len) 
			file[j++]=list[i++];
		if(exist(node,file))
		{
			printf("This name already exist.....\n");
			return ;
		}
		char temp[50]={},c;
		j=0;
		if(i==len)
		{
			while((c=getchar())!=EOF)
				temp[j++]=c;
			node->file[node->fcount]=(files *)malloc(sizeof(files));
			node->file[node->fcount]->fname=alloc(file);
			node->file[node->fcount]->fdata=alloc(temp);
			node->fcount++;
		}else{
			//copy;
		}
	}else{
		int succ;
		while(i<len)
		{
			j=0;
			while(isspace(list[i])) 
				i++;
			while(!isspace(list[i]) && i<len) 
				file[j++]=list[i++];
			for(j=0,succ=0;node->fcount > j;j++)
				if(!strcmp(node->file[j]->fname,file))
					succ=printf("%s\n",node->file[j]->fdata);
			if(succ==0)
				printf("%s Is not file\n",file);
		}
	}
	
}

void mkdir(dir *node,char *list)
{
	int len=strlen(list),i=0,j=0;	
	char dir_name[MAX]={};
	while(i<len)
	{	
		j=0;
		while(isspace(list[i])) 
			i++;
		while(!isspace(list[i]) && i<len) 
			dir_name[j++]=list[i++];
		if(exist(node,dir_name))
		{
			printf("This name already exist.....\n");
			continue;
		}
		node->dir_link[node->dcount]=Create_dir(dir_name);
		node->dir_link[node->dcount]->prev=node;
		node->dcount++;
	}
}

void ls(dir *node)
{
	int i=0;
	while(i<node->fcount)
		printf("%s\t",node->file[i++]->fname);
	i=0;
	while(i<node->dcount)
		printf("%s/\t",node->dir_link[i++]->dir_name);
	printf("\n");
}

dir *cd(dir *node,char *list)
{
	int len=strlen(list),i=0,j=0,pos,use=0;	
	char dir_name[MAX]={},tpath[50]={},ttpath[50]={};
	dir *ptr=node;
	if(len==0||(list[i+1]=='/'&&len==1))
	{
		ptr=USER;
		sprintf(ttpath,"%s:~",USER->dir_name);	
		len=0;
	}
	while(i<len)
	{	
		j=0;
		while(isspace(list[i])) 
			i++;
		if(list[i]=='/'&&isalnum(list[i+1])||use==1)
		{
			i++;
			if(use==0)
			{
				ptr=USER;
				sprintf(ttpath,"%s:~",USER->dir_name);
			}
			use=1;
			while(!isspace(list[i]) && i<len && list[i]!='/') 
				dir_name[j++]=list[i++];
			printf("%s\n",dir_name);
			if((pos=exist(ptr,dir_name)) && (pos-1)<ptr->dcount)
			{
				ptr=ptr->dir_link[pos-1];
				sprintf(tpath,"%s/%s",ttpath,dir_name);
				sprintf(ttpath,"%s",tpath);
			}
			else{
				printf("Not Exist in the directory\n");
				ptr=node;
				break;
			}
		}/*else if(list[i]=='.'||use==2)
		{
			use=2;
			if(list[i+1]=='/')
			{
				i+=2;
			}else if(list[i+1]=='.'&&list[i+2]=='/')
			{
				i+=3;
			}else if(list[i]=='\0')
				ptr=USER;
		}
		//while(!isspace(list[i]) && i<len) 
		//	dir_name[j++]=list[i++];*/
	}
	if(ptr!=node)
		sprintf(path,"%s",ttpath);
	return ptr;

}

int terminal(char name[])
{
	if(name[0]=='\0')
		return 0;
	char command[30]={};
	int i=0,opt;
	while(isspace(name[i])) i++;
	while(isalpha(name[i]))
		command[i]=name[i++];
	while(isspace(name[i])) i++;
	opt=find_command(command);
	switch(opt)
	{
		case 1:		//ls
			ls(temp);
			break;
		case 2:		//cat
			cat(temp,&name[i]);
			break;
		case 3:		//mkdir
			mkdir(temp,&name[i]);
			break;
		case 4:		//cd
			printf("[cd]:\n");
			temp=cd(temp,&name[i]);
			break;
		case 5:		//rmdir
			printf("[rmdit]:\n");
			break;
		case 6:		//rm
			printf("[rm]:\n");
			break;
		case 7:		//pwd
			printf("[pwd]:\n");
			break;
		case 8:		//cp
			printf("[cp]:\n");
			break;
		case 9:		//type
			printf("[type]:\n");
			break;
		case 10:	//help
			printf("[help]:\n");
			break;
		case 11:	//clear
			printf("[clear]:\n");
			break;
		case 12:	//exit
			printf("[exit]:\n");
			return 1;
			break;
		default:	//command not found
			printf("Command Not found\n");
			break;
	}
	return 0;
}


int main()
{
	char name[20]={};
	int exit=0;
	printf("\n\n\t\t\tWelcome To Virtual File system\n");
	printf("\n\n\t\tEnter user name :");
	gets(name);
	temp=USER=Create_dir(name);
	USER->prev=temp;
	clean();
	printf("\n\n\n\n\n\n\n\n\t\t\t\tHello Mr/Ms %s Your root directory name %s \n",name,name);
	sleep(3);
	clean();
	sprintf(path,"%s:~",name);
	clean();
	while(1)
	{
		printf("%s$ ",path);
		gets(name);
		exit=terminal(name);
		if(exit==1)
		{
			printf("Now Erase your all files !!!!\n");
			int i=0;
			for(i=0;i<5;i++)
			{
				sleep(1);
				printf(". ");
				fflush(stdout);
			}
			clean();
			printf("\n\n\n\t\t--+--+--+--+--+--+--[ Thank you ]--+--+--+--+--+--+--\n\n\n");
			break;
		}
	}
}

