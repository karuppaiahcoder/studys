#include<stdio.h>
#include<stdlib.h>

typedef struct tree
{
        int data;
	int hight;
        struct tree *left;
        struct tree *right;
}TREE;

TREE *Binary_tree(TREE *root,int data);
void print(TREE *root);
TREE *Left_to_right_rotation(TREE *root);
TREE *Right_to_left_rotation(TREE *root);

TREE *ROOT=NULL;

int main()
{
	int data=0,opt=1;

	while(opt==1)
	{
		if(opt==1)
		{
			printf("Enter the Number: ");
			scanf("%d",&data);
			ROOT=Binary_tree(ROOT,data);
			print(ROOT);
		}
		printf("1.Add\t(any) Exit\nOption: ");
		scanf("%d",&opt);
	}
}


TREE *Binary_tree(TREE *root,int data)
{
        if(root==NULL)
        {
                root=(TREE *)malloc(sizeof(TREE));
                root->data=data;
                root->left=root->right=NULL;
		root->hight=1;
		printf("tree [%p]->%d\n",root,root->data);
        }else if(root->data>data)
                root->left=Binary_tree(root->left,data);
        else
                root->right=Binary_tree(root->right,data);
	
	if(root->left!=NULL || root->right!=NULL)		//Balance the node hight
	{
		int lef=0,rig=0,bal=0;
		if(root->left!=NULL && root->right!=NULL)	//Find the longest hight in child.
		{
			if(root->left->hight >= root->right->hight)
				root->hight=root->left->hight+1;
			else
				root->hight=root->right->hight+1;
			lef=root->left->hight;
			rig=root->right->hight;

		}else if(root->left!=NULL)
			lef=(root->hight=root->left->hight+1)-1;
		else
			rig=(root->hight=root->right->hight+1)-1;
		
		bal=lef-rig;

		if(bal>1)						
		{
			printf("left	%d\n",root->data);
			if((root->left!=NULL) && root->left->data > data)	
			{
				printf("left to left\n");
				root=Left_to_right_rotation(root);
			}
			else
			{				
				printf("left_right_left\n");
				root->left=Right_to_left_rotation(root->left);
				root=Left_to_right_rotation(root);
			/*	root->hight+=1;
				root->right->hight-=2;
				root->left->hight-=1;
			*/
			}
		}
		else if(bal<-1)
		{					

			if((root->right!=NULL) && root->right->data < data)	
			{
				root=Right_to_left_rotation(root);
			}
			else
			{							
				root->right=Left_to_right_rotation(root->right);
				root=Right_to_left_rotation(root);
			/*	root->hight+=1;
				root->left->hight-=2;
				root->right->hight-=1;
			*/
			}
		}
	}

        return root;
}
int max(TREE *left ,TREE *right)
{
	if(left!=NULL && right!=NULL)
	{
		if(left->hight <=right->hight)
			return right->hight+1;
		else 
			return left->hight+1;	
	}
	else if(left==NULL&&right!=NULL)
		return right->hight+1;
	else if(left!=NULL&&right==NULL)
		return left->hight+1;
	else
		return 1;
}
TREE *Left_to_right_rotation(TREE *root)
{
	TREE *parent,*left,*right,*temp;
	parent=left=right=temp=NULL;

	left=root->left;
	right=left->right;
	left->right=root;
	root->left=right;
	parent=left;
	
	left->right->hight=max(left->right->left,left->right->right);
	parent->hight=max(parent->left,parent->right);
	return parent;
}

TREE *Right_to_left_rotation(TREE *root)
{
	TREE *parent,*left,*right,*temp;
	parent=left=right=temp=NULL;

	right=root->right;
	left=right->left;
	right->left=root;
	root->right=left;
	parent=right;	
	right->left->hight=max(right->left->left,right->left->right);
	parent->hight=max(parent->left,parent->right);
	return parent;
}

void print(TREE *root)
{
        if(root!=NULL)
        {
      printf("\n\nparent      [%p]    left[%p]        right[%p]\n\n",root,root->left,root->right);
                print(root->left);
                printf("\n%d ->%d\n",root->data,root->hight);
                print(root->right);
        }
}
