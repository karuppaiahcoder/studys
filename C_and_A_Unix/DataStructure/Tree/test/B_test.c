#include <stdio.h>
#include <stdlib.h>
#define MAX 4
#define MIN 2

typedef struct btreeNode {
        int val[MAX + 1], count;
        struct btreeNode *link[MAX + 1];
}B;

void splitNode (int val, int *pval, int pos, B *node,B *child, B **newNode);
void insertion(int val);
int setValueInNode(int val, int *pval,B *node,B **child);
void addValToNode(int val, int pos, B *node,B *child);
void splitNode (int val, int *pval, int pos, B *node,B *child, B **newNode);

struct btreeNode *root;

int main()
{
	insertion(val);
}

B *createNode(int val,B *child) 
{
        B *newNode;
        newNode = (B *)malloc(sizeof(B));
        newNode->val[1] = val;
        newNode->count = 1;
        newNode->link[0] = root;
        newNode->link[1] = child;
        return newNode;
}


void insertion(int val) 
{
        int flag, i;
        B *child;

        flag = setValueInNode(val, &i, root, &child);
        if(flag)
                root = createNode(i, child);
}


int setValueInNode(int val, int *pval,B *node,B **child) 
{

        int pos;
        if (!node) 
	{
                *pval = val;
                *child = NULL;
                return 1;
        }

        if (val < node->val[1]) {
                pos = 0;
        } else {
                for (pos = node->count;(val < node->val[pos] && pos > 1); pos--);
                if (val == node->val[pos]) {
                        printf("Duplicates not allowed\n");
                        return 0;
                }
        }
        if(setValueInNode(val, pval, node->link[pos], child)) 
	{
                if (node->count < MAX) {
                        addValToNode(*pval, pos, node, *child);
                } else {
                        splitNode(*pval, pval, pos, node, *child, child);
                        return 1;
                }
        }
        return 0;
}

void addValToNode(int val, int pos, B *node,B *child) 
{
        int j = node->count;
        while (j > pos) 
	{
                node->val[j + 1] = node->val[j];
                node->link[j + 1] = node->link[j];
                j--;
        }

        node->val[j + 1] = val;
        node->link[j + 1] = child;
        node->count++;
}

void splitNode (int val, int *pval, int pos, B *node,B *child, B **newNode) 
{
        int median, j;

        if (pos > MIN)
                median = MIN + 1;
        else
                median = MIN;

        *newNode = (B *)malloc(sizeof(B));
        j = median + 1;
        while (j <= MAX) {
                (*newNode)->val[j - median] = node->val[j];
                (*newNode)->link[j - median] = node->link[j];
                j++;
        }
        node->count = median;
        (*newNode)->count = MAX - median;

        if (pos <= MIN) {
                addValToNode(val, pos, node, child);
        } else {
                addValToNode(val, pos - median, *newNode, child);
        }
        *pval = node->val[node->count];
        (*newNode)->link[0] = node->link[node->count];
        node->count--;
}

