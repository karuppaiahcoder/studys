#include<stdio.h>
#include<stdlib.h>
#define MAX 3
#define MIN 1
typedef struct node
{
	int count;
	int block[MAX+1];	  //	------------------------
	struct node *child[MAX+1];//	|         root		|block 0  block 1 block 2 block 3
}B_TREE;			  //	------------------------	
				//	|	|	|	|
				//	child  child  child  child
				//	  0     1       2	3
B_TREE *root=NULL,*child_node=NULL;
int val;

int split(int key,int pos,B_TREE *root,B_TREE *child);
int Add_key(int key,int pos,B_TREE *root,B_TREE *child);
int Available(int key,B_TREE *root);
B_TREE *Create_node(int key);
int insertion(int key);
int print(B_TREE *node);

int main()
{
        int val, ch;
        while (1)
        {
                printf("1. Insertion\t2.Print\t3.Exit\n");
                printf("Enter your choice:");
                scanf("%d", &ch);
                switch (ch)
                {
                        case 1:
                                printf("Enter your input:");
                                scanf("%d", &val);
                                insertion(val);
                                break;
                        case 2:
                                print(root);
				printf("\n");
                                break;
                        case 3:
                                exit(0);
                        default:
                                printf("U have entered wrong option!!\n");
                                break;
                }
                printf("\n");
        }
}

int print(B_TREE *node)
{
	int i;
	if(node)
	{
		for(i = 0; node->count > i; i++)
		{
			print(node->child[i]);
	//		printf("(%p) ",node);
			printf("%d ",node->block[i+1]);
		}
		print(node->child[i]);
	}
}

int split(int key,int pos,B_TREE *root,B_TREE *child)
{
	int mid,j;
	if(pos > MIN)
		mid=MIN+1;
	else
		mid=MIN;
	child_node=(B_TREE *)malloc(sizeof(B_TREE));
	j=mid+1;
	while(j <= MAX)
	{
		child_node->block[j-mid]=root->block[j];
		child_node->child[j-mid]=root->child[j];
		j++;
	}
	root->count=mid;
	child_node->count=MAX-mid;
	if(pos <= MIN)
		Add_key(key,pos,root,child);
	else
		Add_key(key,pos - mid,child_node,child);
	val=root->block[root->count];
	child_node->child[0]=root->child[root->count];
	root->count--;
}

int Add_key(int key,int pos,B_TREE *root,B_TREE *child)
{
	int j=root->count;
	while(j > pos)
	{
		root->block[j+1]=root->block[j];
		root->child[j+1]=root->child[j];
		j--;
	}
	root->block[j+1]=key;
	root->child[j+1]=child;
	root->count++;
}

int Available(int key,B_TREE *root)
{
	int pos;
	if(!root)
	{
		val=key;
		child_node=NULL;
		return 1;
	}
	if(key < root->block[1])
		pos=0;
	else{
		for(pos=root->count;(key<root->block[pos] &&pos>1);pos--);//Find position
		if(root->block[pos]==key)
		{
			printf("\nDuplicates Not allowed .....!\n\n");
			return 0;
		}
	}
	if(Available(key,root->child[pos]))
	{
		if(root->count < MAX)
			Add_key(val,pos,root,child_node);
		else{
			split(val,pos,root,child_node);
			return 1;
		}
	}
	return 0;
}

B_TREE *Create_node(int key)
{
	B_TREE *node;
	node=(B_TREE *)malloc(sizeof(B_TREE));
	node->block[1]=key;
	node->count=1;
	node->child[0]=root;
	node->child[1]=child_node;
	return node;
}

int insertion(int key)
{
	int flag=0;
	flag=Available(key,root);
	if(flag)
		root=Create_node(val);
}
