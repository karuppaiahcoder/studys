#include<stdio.h>
#include<stdlib.h>

typedef struct tree
{
	int data;
	struct tree *left;
	struct tree *right; 
}TREE;

TREE *ROOT=NULL;

TREE *Binary_tree(TREE *root,int data);
void print(TREE *root);
TREE *Splay(TREE *root,int data);
TREE *find(TREE *root,int data);
TREE *Rotate(TREE *root,int data);
int stop=0;
TREE *tmp=NULL;
int main()
{
	int data,opt=1;

	while(opt<=3)
	{
		if(opt==1)				//Insert
		{
			printf("Enter the value : ");
			scanf("%d",&data);
			ROOT=Binary_tree(ROOT,data);
			ROOT=Splay(ROOT,data);			//Change Root		
		}else if(opt==2)			//delete
		{
//			root=Splay(root,data);			//Change Root		
//			i=delete();
		}else if(opt==3)			//Print
		{
			printf("print [%p]",ROOT);
			print(ROOT);
		}
		printf("1.Add\t2.Delete\t3.Print\t(Any)Exit\n");
		scanf("%d",&opt);	
		if(opt>3||0>opt)
			break;		//Exit
		stop=0;tmp=NULL;
	}
}


TREE *Splay(TREE *root,int data)
{
        if(root->left!=NULL||root->right!=NULL)
        {
                if((root->left!=NULL) && root->left->data==data)	//Left process	
		{
                        tmp=root;stop=1;
		}
                else if((root->right!=NULL) && root->right->data==data)	//Right process
                {
		       tmp=root;stop=1;
		}

                if(root->left!=NULL && stop==0)
                        Splay(root->left,data);
                if(root->right!=NULL && stop==0)
                        Splay(root->right,data);
//		printf("M-root[%p]->%d	root[%p]->%d\n",root,root->data,tmp,tmp->data);
//		print(ROOT);
                if(tmp!=NULL && ROOT!=tmp && stop==0)
                {
                        if(root->left==tmp)
                        {
                                printf("[left]\n");
                                tmp=root->left=Rotate(tmp,data);
                                printf("root->left->data=%d\n",root->left->data);
                                tmp=root;
                        }else{
                                printf("[right]\n");
                                tmp=root->right=Rotate(tmp,data);
                                printf("root->right->data=%d\n",root->right->data);
                                tmp=root;
                        }

                }else if(tmp!=NULL && ROOT!=tmp && stop==1){
			printf("Stop\n");
			stop=0;
			return;
		}

                if(ROOT==tmp)
                {
                        printf("root\n");
                        printf("%p      [%d]\n",ROOT,ROOT->data);
                        print(ROOT);
                        tmp=Rotate(tmp,data);
                }
        }
        if(tmp==NULL)
                return root;
        else
                return tmp;

}

TREE *Rotate(TREE *root,int data)
{
	TREE *left,*right,*parent;
	left=right=parent=NULL;
	left=root->left;		//left
	right=root->right;		//right
	
	if((left!=NULL) && left->data==data)
	{
		right=left->right;
		left->right=root;
		root->left=right;
		parent=left;	
		printf("Left [%p]\n",parent);
	}else{
		left=right->left;
		right->left=root;
		root->right=left;
		parent=right;
		printf("Right [%p]\n",parent);
	}
	return parent;
}

TREE *Binary_tree(TREE *root,int data)
{
	if(root==NULL)
	{
		root=(TREE *)malloc(sizeof(TREE));
		root->data=data;
		root->left=root->right=NULL;
		printf("Tree   [%p]\n",root);
	}else if(root->data>data)
		root->left=Binary_tree(root->left,data);
	else
		root->right=Binary_tree(root->right,data);

	return root;
}

void print(TREE *root)
{
	if(root!=NULL)
	{
	printf("\n\nparent	[%p]	left[%p]	right[%p]\n\n",root,root->left,root->right);
		print(root->left);
		printf("%d ",root->data);
		print(root->right);
	}
}
