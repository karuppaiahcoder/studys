#include <stdio.h>
#include <stdlib.h>
#define MAX 3
#define MIN 1
typedef struct node 
{
	int val[MAX + 1], count;
	struct node *link[MAX + 1];
}B_TREE;

B_TREE  *root;

void print(B_TREE *node); 
/* creating new node */
B_TREE * createNode(int val, B_TREE *child) 
{
	B_TREE *node;
	node = (B_TREE *)malloc(sizeof(B_TREE));
	node->val[1] = val;
	node->count = 1;
	node->link[0] = root;
	node->link[1] = child;
	return node;
}

/* Places the value in appropriate position */
void Add_key(int val, int pos, B_TREE *node,B_TREE *child) 
{
	int j = node->count;
	while (j > pos) 
	{
		node->val[j + 1] = node->val[j];
		node->link[j + 1] = node->link[j];
		j--;
	}
	node->val[j + 1] = val;
	node->link[j + 1] = child;
	node->count++;
}

/* split the node */
void split (int val,int *pval,int pos,B_TREE *node,B_TREE *child,B_TREE **new) 
{
	int median, j;
	
	printf("%d %d %d\n",node->val[1],node->val[2],node->val[3]);

	if (pos > MIN)
		median = MIN + 1;
	else
		median = MIN;

	*new = (B_TREE *)malloc(sizeof(B_TREE));
	j = median + 1;
	while (j <= MAX) 
	{
		(*new)->val[j - median] = node->val[j];
		(*new)->link[j - median] = node->link[j];
		j++;
	}
	node->count = median;
	(*new)->count = MAX - median;

	if (pos <= MIN) 
		Add_key(val, pos, node, child);
	else 
		Add_key(val, pos - median, *new, child);

	*pval = node->val[node->count];
	//printf("[%d] %p\n",*pval,pval);
	(*new)->link[0] = node->link[node->count];
	node->count--;
	printf("{%p}\n",(*new)->link[0]);	
	print(node);
	printf("------------\n");
}

/* sets the value val in the node */
int Available(int val, int *pval,B_TREE *node, B_TREE **child) 
{
	int pos;
	if (!node) 
	{
		*pval = val;
		*child = NULL;
		return 1;
	}

	if (val < node->val[1]) 
		pos = 0;
	else 
	{
		for (pos = node->count;(val < node->val[pos] && pos > 1); pos--);

		if (val == node->val[pos]) 
		{
			printf("Duplicates not allowed\n");
			return 0;
		}
	}
	if (Available(val, pval, node->link[pos], child)) 
	{
		if (node->count < MAX) 
			Add_key(*pval, pos, node, *child);
		else 
		{
			split(*pval, pval, pos, node, *child, child);
			return 1;
		}
	}
	return 0;
}

/* insert val in B-Tree */
void insertion(int val) 
{
	int flag, i;
	B_TREE *child;

	printf("\n[%p]p\n",&child);
	flag = Available(val, &i, root, &child);
	if (flag)
		root = createNode(i, child);
}

void print(B_TREE *node) 
{
	int i;
	if (node) 
	{
		for (i = 0; i < node->count; i++) 
		{
			printf("root->[%p] %d child[%p][%d] child[%p][%d]\n",node,i,node->link[i],i,node->link[i+1],i+1);
			print(node->link[i]);
			printf("\n[%d]\n ", node->val[i + 1]);
		}
//		printf("I->[%d] %p\n",i,node);
		print(node->link[i]);
	}
}

int main() 
{
	int val, ch;
	while (1) 
	{
		printf("1. Insertion\t2.Print\t3.Exit\n");
		printf("Enter your choice:");
		scanf("%d", &ch);
		switch (ch) 
		{
			case 1:
				printf("Enter your input:");
				scanf("%d", &val);
				insertion(val);
				break;
			case 2:
				print(root);
				break;
			case 3:
				exit(0);
			default:
				printf("U have entered wrong option!!\n");
				break;
		}
		printf("\n");
	}
}

