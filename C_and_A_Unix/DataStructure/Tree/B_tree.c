#include<stdio.h>
#include<stdlib.h>
#define MAX 3
#define MIN 1

typedef struct node
{
	int count;
	int block[MAX+1];	  //	------------------------
	struct node *child[MAX+1];//	|         root		|block 0  block 1 block 2 block 3
}B_TREE;			  //	------------------------	
				//	|	|	|	|
				//	child  child  child  child
				//	  0     1       2	3
B_TREE *root;
int k=0;

int print(B_TREE *node)
{
	int i;
	if(node)
	{
		for(i = 0; node->count > i; i++)
		{
			print(node->child[i]);
			printf("%d [%d]",node->block[i+1],++k);
		}
		print(node->child[i]);
	}
}

int split(int key,int *pval,int pos,B_TREE *root,B_TREE *child,B_TREE **nchild)
{
	int mid,j;

	if(pos > MIN)
		mid=MIN+1;
	else
		mid=MIN;

	*nchild=(B_TREE *)malloc(sizeof(B_TREE));
	j=mid+1;
	while(j <= MAX)
	{
		(*nchild)->block[j-mid]=root->block[j];
		(*nchild)->child[j-mid]=root->child[j];
		j++;
	}
	root->count=mid;
	(*nchild)->count=MAX-mid;

	if(pos <= MIN)
		Add_key(key,pos,root,child);
	else
		Add_key(key,pos - mid,*nchild,child);

	*pval=root->block[root->count];
	(*nchild)->child[0]=root->child[root->count];
	root->count--;
}

int Add_key(int key,int pos,B_TREE *root,B_TREE *child)
{
	int j=root->count;
	while(j > pos)
	{
		root->block[j+1]=root->block[j];
		root->child[j+1]=root->child[j];
		j--;
	}

	root->block[j+1]=key;
	root->child[j+1]=child;
	root->count++;
}

int Available(int key,int *pval,B_TREE *root,B_TREE **child)
{
	int pos;
	if(!root)
	{
		*pval=key;
		*child=NULL;
		return 1;
	}
	if(key < root->block[1])
		pos=0;
	else{
		for(pos=root->count;(key<root->block[pos] &&pos>1);pos--);//Find position
		if(root->block[pos]==key)
		{
			printf("\nDuplicates Not allowed .....!\n\n");
			return 0;
		}
	}
	if(Available(key,pval,root->child[pos],child))
	{
		if(root->count < MAX)
			Add_key(*pval,pos,root,*child);
		else{
			split(*pval,pval,pos,root,*child,child);
			return 1;
		}
	}
	return 0;
}

B_TREE *Create_node(int key,B_TREE *child)
{
	B_TREE *node;
	node=(B_TREE *)malloc(sizeof(B_TREE));
	node->block[1]=key;
	node->count=1;
	node->child[0]=root;
	node->child[1]=child;
	return node;
}

int insertion(int key)
{
	int flag=0,val;
	B_TREE *child;
	
	flag=Available(key,&val,root,&child);

	if(flag)
		root=Create_node(val,child);
}

int main()
{
        int val, ch;
        while (1)
        {
                printf("1. Insertion\t2.Print\t3.Exit\n");
                printf("Enter your choice:");
                scanf("%d", &ch);
                switch (ch)
                {
                        case 1:
                                printf("Enter your input:");
                                scanf("%d", &val);
                                insertion(val);
                                break;
                        case 2:
				k=0;
                                print(root);
				printf("\n");
                                break;
                        case 3:
                                exit(0);
                        default:
                                printf("U have entered wrong option!!\n");
                                break;
                }
                printf("\n");
        }
}
