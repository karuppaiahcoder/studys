#include<stdio.h>
#include<stdlib.h>

typedef struct tree
{
	int data;
	struct tree *left;
	struct tree *right; 
}TREE;

TREE *ROOT=NULL;

TREE *Binary_tree(TREE *root,int data);
void print(TREE *root);
TREE *Splay(TREE *root,int data);
TREE *find(TREE *root,int data);
TREE *Rotate(TREE *root,int data);
int delete(TREE *root,int data);
int stop=0;
TREE *tmp=NULL;
int main()
{
	int data,opt=1,i=0;

	while(opt<=3)
	{
		if(opt==1)				//Insert
		{
			printf("Enter the value : ");
			scanf("%d",&data);
			ROOT=Binary_tree(ROOT,data);
			ROOT=Splay(ROOT,data);			//Change Root		
		}else if(opt==2)			//delete
		{
			printf("Enter the value : ");
			scanf("%d",&data);
			ROOT=Splay(ROOT,data);			//Change Root		
//			print(ROOT);
			if((i=delete(ROOT,data))==1)
				printf("Delete Success\n");
			else
				printf("Not Exist this number [ %d ] in Your tree\n",data);
		}else if(opt==3)			//Print
		{
			printf("print [%p]",ROOT);
			print(ROOT);
		}
		printf("1.Add\t2.Delete\t3.Print\t(Any)Exit\n");
		scanf("%d",&opt);	
		if(opt>3||0>opt)
			break;		//Exit
		stop=0;tmp=NULL;
	}
}

int delete(TREE *root,int data)
{
	TREE *left,*right;
	left=right=NULL;

	if(root->data==data)
	{
		left=root->left;	
		right=root->right;
		if(left!=NULL)
		{
			ROOT=left;
			while(left->right!=NULL)
				left=left->right;
			left->right=right;
		}else{
			ROOT=right;
		}
		return 1;
	}else
		return 0;
}


TREE *Splay(TREE *root,int data)
{
        if(root->left!=NULL||root->right!=NULL)
        {
                if((root->left!=NULL) && root->left->data==data)	//Left process	
		{
			printf("root->left->data=%d\n",root->left->data);
                        tmp=root;stop=1;
		}
                else if((root->right!=NULL) && root->right->data==data)	//Right process
                {
			printf("root->right->data=%d\n",root->right->data);
			tmp=root;stop=1;
		}

                if(root->left!=NULL && stop==0)
                        Splay(root->left,data);
                if(root->right!=NULL && stop==0)
                        Splay(root->right,data);

                if(tmp!=NULL && ROOT!=tmp && stop==0)			//Change the root in current value
                {
                        if(root->left==tmp)				//left to right Rotation
                                tmp=root->left=Rotate(tmp,data);
                        else						//Right to left rotation
                                tmp=root->right=Rotate(tmp,data);
			tmp=root;
                }else if(tmp!=NULL && ROOT!=tmp && stop==1){		//Find the master parent node.
			stop=0;
			return;
		}
                if(ROOT==tmp)						//Final Change the Master parent node.
                {
                        print(ROOT);
                        tmp=Rotate(tmp,data);
                }
        }
        if(tmp==NULL)
                return root;						//The tree only have one parent.
        else
                return tmp;						//Multi parent.
}

TREE *Rotate(TREE *root,int data)
{
	TREE *left,*right,*parent;
	left=right=parent=NULL;
	left=root->left;		//left
	right=root->right;		//right
	
	if((left!=NULL) && left->data==data)	//Left to Right Rotation
	{
		right=left->right;
		left->right=root;
		root->left=right;
		parent=left;	
	}else{					//Right to Left Rotation
		left=right->left;
		right->left=root;
		root->right=left;
		parent=right;
	}
	return parent;
}

TREE *Binary_tree(TREE *root,int data)
{
	if(root==NULL)
	{
		root=(TREE *)malloc(sizeof(TREE));
		root->data=data;
		root->left=root->right=NULL;
	}else if(root->data>data)
		root->left=Binary_tree(root->left,data);
	else
		root->right=Binary_tree(root->right,data);

	return root;
}

void print(TREE *root)
{
	if(root!=NULL)
	{
//	printf("\n\nparent	[%p]	left[%p]	right[%p]\n\n",root,root->left,root->right);
		print(root->left);
		printf("%d ",root->data);
		print(root->right);
	}
}
