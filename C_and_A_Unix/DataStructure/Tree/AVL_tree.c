#include<stdio.h>
#include<stdlib.h>

typedef struct tree
{
        int data;
	int hight;
        struct tree *left;
        struct tree *right;
}TREE;

TREE *Binary_tree(TREE *root,int data);
void print(TREE *root);
TREE *Left_to_right_rotation(TREE *root);
TREE *Right_to_left_rotation(TREE *root);
TREE *delete(TREE *root,int data);
TREE *Remove_node(TREE *root);

TREE *ROOT=NULL;

int main()
{
	int data=0,opt=1;

	while(opt<=3)
	{
		if(opt==1)
		{
			printf("Enter the Number: ");
			scanf("%d",&data);
			ROOT=Binary_tree(ROOT,data);
			//print(ROOT);
		}else if(opt==2)
		{
			printf("Enter the Number: ");
			scanf("%d",&data);
			ROOT=delete(ROOT,data);
		}else
			print(ROOT);
		printf("1.Add\t2.Delete\t3.Print\t(any) Exit\nOption: ");
		scanf("%d",&opt);
	}
}

TREE *delete(TREE *root,int data)
{
	static int stop=1;
	if(root==NULL)
		return;
	if(root!=NULL && stop==1)
	{
		if(root->data==data)
			stop=0;
		else if(root->data > data)
			root->left=delete(root->left,data);
		else 
			root->right=delete(root->right,data);
	}
	stop=1;
	if(root->data==data)
		root=Remove_node(root);
	if(root!=NULL)
		root->hight=max(root->left,root->right);
	return root;
}

TREE *Remove_node(TREE *root)
{
	TREE *parent,*left,*right,*temp;

	parent=left=right=temp=NULL;
	
	if(root->left==NULL && root->right==NULL)
			return NULL;

	if(root->left!=NULL && root->right!=NULL)
	{
		temp=left=root->left;
		right=root->right;
		while(temp->right!=NULL)
			temp=temp->right;
		temp->right=right;
		parent=left;
	}else if(root->left!=NULL)
		parent=root->left;
	else
		parent=root->right;

	return parent;
}


TREE *Binary_tree(TREE *root,int data)
{
        if(root==NULL)
        {
                root=(TREE *)malloc(sizeof(TREE));
                root->data=data;
                root->left=root->right=NULL;
		root->hight=1;
		printf("tree [%p]->%d\n",root,root->data);
        }else if(root->data > data)
                root->left=Binary_tree(root->left,data);
        else
                root->right=Binary_tree(root->right,data);
	
	if(root->left!=NULL || root->right!=NULL)		//Balance the node hight
	{
		int lef=0,rig=0,bal=0;
		root->hight=max(root->left,root->right);
		if(root->left!=NULL)
			lef=root->left->hight;
		if(root->right!=NULL)
			rig=root->right->hight;
		bal=lef-rig;
		if(bal>1)						
		{
			if((root->left!=NULL) && root->left->data > data)	
				root=Left_to_right_rotation(root);
			else
			{				
				root->left=Right_to_left_rotation(root->left);
				root=Left_to_right_rotation(root);
			}
		}
		else if(bal<-1)
		{					

			if((root->right!=NULL) && root->right->data < data)	
				root=Right_to_left_rotation(root);
			else
			{							
				root->right=Left_to_right_rotation(root->right);
				root=Right_to_left_rotation(root);
			}
		}
	}

        return root;
}
int max(TREE *left ,TREE *right)
{
	if(left!=NULL && right!=NULL)
	{
		if(left->hight <=right->hight)
			return right->hight+1;
		else 
			return left->hight+1;	
	}
	else if(left==NULL&&right!=NULL)
		return right->hight+1;
	else if(left!=NULL&&right==NULL)
		return left->hight+1;
	else
		return 1;
}
TREE *Left_to_right_rotation(TREE *root)
{
	TREE *parent,*left,*right,*temp;
	parent=left=right=temp=NULL;

	left=root->left;
	right=left->right;
	left->right=root;
	root->left=right;
	parent=left;
	
	left->right->hight=max(left->right->left,left->right->right);
	parent->hight=max(parent->left,parent->right);
	return parent;
}

TREE *Right_to_left_rotation(TREE *root)
{
	TREE *parent,*left,*right,*temp;
	parent=left=right=temp=NULL;

	right=root->right;
	left=right->left;
	right->left=root;
	root->right=left;
	parent=right;	
	right->left->hight=max(right->left->left,right->left->right);
	parent->hight=max(parent->left,parent->right);
	return parent;
}

void print(TREE *root)
{
        if(root!=NULL)
        {
    printf("\n\nparent      [%p]    left[%p]        right[%p]\n\n",root,root->left,root->right);
                print(root->left);
                printf("\n%d (node) ->%d(hight)\n",root->data,root->hight);
                print(root->right);
        }
}
