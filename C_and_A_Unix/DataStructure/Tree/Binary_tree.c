#include<stdio.h>
#include<stdlib.h>
typedef struct n
{
	int data;
	struct n *left;
	struct n *right;
}TREE;

TREE *Binary_tree(TREE *root,int data);
void print(TREE *root);
int delete(int data);
TREE *find_parent(TREE *root,int data);
TREE *search(TREE *root,int data);

TREE *root=NULL;

int main()
{
	int opt=1,data;
	while(opt<=3)
	{
		if(opt==1)
		{
			printf("Enter the data:");
			scanf("%d",&data);
			root=Binary_tree(root,data);
		}else if(opt==2)
		{
			print(root);
			printf("\n\nEnter the data:");
			scanf("%d",&data);
			delete(data);
		}else if(opt==3)
			print(root);

		printf("1.add\t2.Delete\t3.Print\t(Any)Exit....\n");
		scanf("%d",&opt);
	}
}


int delete(int data)
{
	TREE *parent=find_parent(root,data);
	TREE *left,*right,*temp,*dnode;
	left=right=temp=dnode=NULL;

	if(parent==NULL)
	{
		temp=left=root->left;
		right=root->right;
		if(root->left!=NULL)
		{
			while(temp->right!=NULL)
				temp=temp->right;
			temp->right=root->right;
			root=left;
		}else
			root=root->right;
	}else if(parent!=NULL){

		if((parent->left!=NULL)&&parent->left->data==data)
			dnode=parent->left;
		else
			dnode=parent->right;

			temp=left=dnode->left;
			right=dnode->right;
			if(left!=NULL)
			{
				while(temp->right!=NULL)
					temp=temp->right;
				temp->right=right;
			}else
				left=right;

                if((parent->left!=NULL)&&parent->left->data==data)
                        parent->left=left;
                else
                        parent->right=left;
	}
}

TREE *tmp=NULL;

TREE *find_parent(TREE *root,int data)
{
	tmp=NULL;
	if(root->data==data)
		return NULL;	
	search(root,data);
	root=tmp;
	return root;
}

TREE *search(TREE *root,int data)
{
	if(root->left!=NULL||root->right!=NULL)
	{
		if((root->left!=NULL) && root->left->data==data)
			tmp=root;
		else if((root->right!=NULL) && root->right->data==data)
			tmp=root;
	
		if(root->left!=NULL)
			search(root->left,data);
		if(root->right!=NULL)
			search(root->right,data);
	}
}

TREE *Binary_tree(TREE *root,int data)
{
	if(root==NULL)
	{
		root=(TREE*)malloc(sizeof(TREE));
		root->data=data;
		root->left=root->right=NULL;
	}else if(root->data>data)	
		root->left=Binary_tree(root->left,data);
	else
		root->right=Binary_tree(root->right,data);

	return root;
}

void print(TREE *root)
{
	if(root!=NULL)
	{
		print(root->left);
		printf("%d ",root->data);
		print(root->right);
	}
}
