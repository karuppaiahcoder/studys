/*
Program name: heartbeat monitor server
Writer name : Karuappaiah
purpose     : Create server using UDP concept
*/
/*
Algorithm:
	+ First we create daemon process with follow particular rules.

	+ Daemon process:
	  `````` ````````
		umask():
			-Set the umask value zero, because umask can be affect the daemon programs work.
			-So set the umask value zero umask(0);

		fork():
			-Then create one fork at that time kill or exit the parent process. Because daemon process run without controling terminal.
			-The parent can be provide the stdin,stdout and stderr.

		setsid():
			-The setsid function create new session so that session doesn't have controling terminal. But can be possible for control terminal come to daemon process.
			-So create another one fork then kill that fork parent process. Now we can get the perfact daemon process.
	
		chdir():
			-Then each daemon process should be run the home directory, so you change the directory, this not must we can choose. So I create the daemon current directry.

	+ Each daemon process must be run one time, so we should be chech, that daemon process already run or not.

	+ Already running:
	  ``````` ````````
		fcntl():
			fcntl function block the ".haert.pid" file, because this log enable unlock, when this program terminate at that time only unlock the file.

		open(): open the .haert.pid file with write mode.

		write pid: Write the process id in the .haert.pid file.

	+ Read conficuration file:
	  ```` ````````````` `````

		+ When program is start imediatly read the .haert.conf file, because that file have the client process information.

		+ Then whennever SIGHUP signal come to the process that time re-read the conficuration file.
		+ We can send the SIGHUP signal to daemon process like that format.
	
				kill -HUP <process id>
		signal():
			+ The signal function handle the signal , this function call the read_conf function, when SIGHUP signal come to re-read the conficuration file.
		
*/
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<fcntl.h>
#include<unistd.h>
#include<signal.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
int e_fd,l_fd;

int error_log(char *ptr)
{
	write(e_fd,ptr,strlen(ptr)); //log the error then exit
	exit(1);
}

int log_info(char *inf)
{
	if(write(l_fd,inf,strlen(inf))!=strlen(inf))  //get log to .haert.log file
		error_log("Write function error [71]\n");
}

int daemon_init(void)
{
	umask(0);	//change umask zero
	pid_t pid;
	if((pid=fork())<0)
	{
		printf("Fork error [80]\n");
		exit(1);
	}else if(pid>0)	//close the parent 
		exit(1);
	if(setsid()==-1)
	{
		printf("setsid function error [86]\n");
		exit(1);
	}
	if((pid=fork())<0)
	{
		perror("fork error [91]\n");
		exit(1);
	}else if(pid>0)
		exit(1);
	int i=0,fd;
	for(i=0;i<1024;i++)	//close the all descriptor with stdin,stdout and stderr
		if(close(i)==-1)
			perror("close function error [98]\n");
	if((fd=open("/dev/null",O_RDWR))<0)//open /dev/null file to give atdin ,stdout and stderr
	{
		perror("open function error [101]\n");
		exit(1);
	}
	dup2(fd,1);
	dup2(fd,2);
}

int already_run(void)
{
	int pid_fd;
	char pid[10]={};
	struct flock lk;
	lk.l_type=F_WRLCK;
	lk.l_whence=SEEK_SET;
	lk.l_start=0;
	lk.l_len=0;
	lk.l_pid=getpid();

	if((pid_fd=open(".haert.pid",O_WRONLY|O_CREAT,0666))<0)
		error_log("open error [120]\n");

	if(fcntl(pid_fd,F_SETLK,&lk)==-1) //lock the file to check already running
		error_log("haertbeat program already running [123]\n");

	sprintf(pid,"%d",getpid());
	if(write(pid_fd,pid,sizeof(pid))!=sizeof(pid))	//store the process id
		error_log("write function error [127]\n");
}

typedef struct {	//conf file storage structure
	int pg_id;
	char *pg_name;
	int int_vl;
	int int_vlc;
	int mis_msg;
	char *cmd;
	char cl_ip[50];
	int cl_port;
	char *sr_ip;
	int sr_port;
}client_st;

client_st data_lock[1024];
int maxfd=1024,dl_loc;

char *alloc(char buf[])	//memory allocation function with store string
{
	char *ptr;
	if((ptr=(char *)malloc(strlen(buf)))==NULL)
		error_log("malloc function error [150]\n");
	buf[strlen(buf)]='\0';
	strcpy(ptr,buf);
	return ptr;
}

void read_conf()	//read conficuration file
{
	FILE *fp;
	char buf[1024]={};
	if((fp=fopen(".haert.conf","r"))==NULL)
		error_log("conficuration file open error [161]\n");
	dl_loc=0;
	while((fgets(buf,1024,fp))!=NULL) 
	{
		/*Here store the each client information*/
		data_lock[dl_loc].pg_id=atoi(strtok(buf,"|")); //program id
		data_lock[dl_loc].pg_name=alloc(strtok(NULL,"|"));//program name
		data_lock[dl_loc].int_vl=atoi(strtok(NULL,"|"));//interval
		data_lock[dl_loc].int_vlc=0;//interval checking count variable default value 0
		data_lock[dl_loc].mis_msg=atoi(strtok(NULL,"|"));//missing msg
		data_lock[dl_loc].cmd=alloc(strtok(NULL,"|"));//command
		strcpy(data_lock[dl_loc].cl_ip,strtok(NULL,"|"));//client ip address
		data_lock[dl_loc].cl_port=atoi(strtok(NULL,"|"));//client port number
		data_lock[dl_loc].sr_ip=alloc(strtok(NULL,"|"));//server ip address
		data_lock[dl_loc].sr_port=atoi(strtok(NULL,"|"));//server port number
		dl_loc++;
	}
	if(dl_loc==0) 
		error_log("conficuration file empty [179]\n");
	fclose(fp);
}

int check_beat(void)
{
	int i=0,max=dl_loc;
	char date_c[50]={},buf[1500]={};
	FILE *fp;
	while(max>i)
	{
		data_lock[i].int_vlc++;
		/*check the interval time*/
		if((data_lock[i].int_vl*data_lock[i].mis_msg)<=data_lock[i].int_vlc)
		{
			if((fp=popen("date","r"))==NULL)
				error_log("popen function error [194]\n");
		        fgets(date_c,50,fp); //get date and current time
			date_c[strlen(date_c)-1]='\0';
			sprintf(buf,"%s is not alive at %s restarting the %s\n",data_lock[i].pg_name,date_c,data_lock[i].pg_name);
			log_info(buf);
			system(data_lock[i].cmd);
			data_lock[i].int_vlc=0;
		}
		i++;
	}
}

int recv_beat(char *buf,struct sockaddr_in *c_inf)
{
	char prg_id[10]={},prc_id[10]={},*msg,date_c[50]={},*c_ip;
	char tmp[1500]={};
	int i=0,max=dl_loc;
	strcpy(prg_id,strtok(buf,"$=$"));
	strcpy(prc_id,strtok(NULL,"$=$"));
	msg=alloc(strtok(NULL,"$=$"));
	FILE *fp=NULL;
	c_ip=alloc((char *)inet_ntoa(c_inf->sin_addr.s_addr)); //get client IP
	while(max>i)
	{
		if(data_lock[i].pg_id==atoi(prg_id)) //check program id
		{
		/*Check IP address and port number*/
			if(ntohs(c_inf->sin_port)==data_lock[i].cl_port&&!strcmp(c_ip,data_lock[i].cl_ip))
			{
				if((fp=popen("date","r"))==NULL)
					error_log("popen function error [224]\n");
				fgets(date_c,50,fp);
				sprintf(tmp,"%s is alive at %s",data_lock[i].pg_name,date_c);
				log_info(tmp);
				data_lock[i].int_vlc=0;
				return;
			}
			sprintf(tmp,"Warning fake IP and port number\n");
			log_info(tmp);
			return;
		}
		i++;
	}
	sprintf(tmp,"%s this program id not exist in conficuration file\n",prg_id);
	log_info(tmp);
}

int main(void)
{
	daemon_init();
	if((e_fd=open(".err_heart.log",O_WRONLY|O_APPEND|O_CREAT,0666))<0) //log the all error
	{
		perror("open function error [246]\n");
		exit(1);
	}
	already_run();
	read_conf();
	signal(SIGHUP,read_conf);
	int sfd;
	struct sockaddr_in s_inf,c_inf;
	socklen_t size;
	char buf[1024]={};

	if((l_fd=open(".haert.log",O_WRONLY|O_APPEND|O_CREAT,0666))<0) //log the all information
		error_log("haert.log file open error [258]\n");

	s_inf.sin_family=AF_INET;
	s_inf.sin_port=htons(data_lock[0].sr_port);
	s_inf.sin_addr.s_addr=inet_addr(data_lock[0].sr_ip);
	if((sfd=socket(AF_INET,SOCK_DGRAM,0))<0)
		error_log("socket function error [264]\n");
	if(bind(sfd,(struct sockaddr *)&s_inf,sizeof(s_inf))==-1)
		error_log("Bind function error [266]\n");
	size=sizeof(s_inf);
	if(fcntl(sfd,F_SETFL,O_NONBLOCK,NULL)<0) //Set nonflock mode t server socket
		error_log("fcntl function error [269]\n");
	while(1)
	{
		if(recvfrom(sfd,buf,30,0,(struct sockaddr *)&c_inf,&size)>0)
			recv_beat(buf,&c_inf);
		check_beat();
		sleep(1);
	}
}
