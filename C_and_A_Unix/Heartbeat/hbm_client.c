/*
	program name :haertbeat monitor client
	writer by    :karuppaiah
	purpose      :Communicate to server with UDP concept.
*/
/*
	Algorithm:

		+ Get program number to user by command line argument.

		+ Read configuration file:

			* Then check the configuration file entry have user given program number.
			* If program id match, then read that full line and send the ALIVE MESSAGE message to server.
		+ But program ID doesn't match means print the error message and exit.

*/
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
typedef struct {        //conf file storage structure
        int pg_id;
        char *pg_name;
        int int_vl;
        int mis_msg;
        char *cmd;
        char cl_ip[50];
        int cl_port;
        char *sr_ip;
        int sr_port;
}client_st;
client_st data_lock;
char *alloc(char buf[])
{
        char *ptr;
        if((ptr=(char *)malloc(strlen(buf)))==NULL) //allocate the memory by malloc function
        {
		printf("malloc function error [41]\n");
		exit(1);
	}
        buf[strlen(buf)]='\0';
        strcpy(ptr,buf);
        return ptr;
}

/*Read the conficuration file ".haert.conf" to get the client and server information*/
int read_conf(char *p_id)
{
	FILE *fp;
	char buf[1024]={};
	if((fp=fopen(".haert.conf","r"))==NULL)
	{
		printf("Conficuration file open error[56]\n");
		exit(1);
	}
	while((fgets(buf,1024,fp))!=NULL)
	{
		if(strstr(buf,p_id)!=NULL)
		{
	                data_lock.pg_id=atoi(strtok(buf,"|")); //program id
	                data_lock.pg_name=alloc(strtok(NULL,"|"));//program name
	                data_lock.int_vl=atoi(strtok(NULL,"|"));//interval
        	        data_lock.mis_msg=atoi(strtok(NULL,"|"));//missing msg
	                data_lock.cmd=alloc(strtok(NULL,"|"));//command
	                strcpy(data_lock.cl_ip,strtok(NULL,"|"));//client ip address
	                data_lock.cl_port=atoi(strtok(NULL,"|"));//client port number
	                data_lock.sr_ip=alloc(strtok(NULL,"|"));//server ip address
	                data_lock.sr_port=atoi(strtok(NULL,"|"));//server port number
			return 1;
		}
	}
	return 0;
}

int main(int argc,char *argv[])
{
	char p_id[20]={};
	int i;
	if(argc!=2)
	{
		printf("Usage:./a.out <program id [from .haert.conf] >\n");
		exit(1);
	}
	strcpy(p_id,argv[1]);
	for(i=0;p_id[i]!='\0';i++)
		if(!isdigit(p_id[i]))
		{
			printf("%s:Invalid program id\n",p_id);
			exit(1);
		}
	if(read_conf(p_id)==0)
	{
		printf("%s :this program id not exits from .haert.conf file [96]\n",p_id);
		exit(1);
	}
	int fd;
	struct sockaddr_in s_inf,c_inf;
	socklen_t size;
	char buf[30]={};

	/*store the server information*/
	s_inf.sin_family=AF_INET;
	s_inf.sin_port=htons(data_lock.sr_port);
	s_inf.sin_addr.s_addr=inet_addr(data_lock.sr_ip);

	/*store the client information*/
	c_inf.sin_family=AF_INET;
	c_inf.sin_port=htons(data_lock.cl_port);
	c_inf.sin_addr.s_addr=inet_addr(data_lock.cl_ip);

	if((fd=socket(AF_INET,SOCK_DGRAM,0))<0)
	{
		printf("socket function errir [116]\n");
		exit(1);
	}
	if(bind(fd,(struct sockaddr *)&c_inf,sizeof(c_inf))==-1) //bind the client information
	{
		printf("Bind function error [121]\n");
		exit(1);
	}
	size=sizeof(c_inf);
	/*Store the message like this format "PROGRAM_ID$=$PROCESS-ID$=$ALIVE"*/
	sprintf(buf,"%s$=$%d$=$ALIVE",p_id,getpid());
	while(1)
	{
		/*send the alive message to server*/
		if(sendto(fd,buf,strlen(buf),0,(struct sockaddr *)&s_inf,size)<0)
		{
			printf("sendto function error [132]\n");
			exit(1);
		}
		sleep(data_lock.int_vl);
	}
}
