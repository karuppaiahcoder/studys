//Write a program to Print the number of zeroes you encounter between the numbers 0 to 1000.

#include<stdio.h>
main()
{
	int i,j,count=0,zero=0;
	for(i=0;i<=1000;i++)
	{
		j=i;
		while(1)
		{
			if(j%10==0)
				count++;
			j/=10;	
			if(j==0)
				break;
		}
	}
	printf("number of zeroes in number 0 to 1000=%d\n",count);
}
