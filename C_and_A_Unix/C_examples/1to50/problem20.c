//Write a program to print the total number of single digit Prime numbers

#include<stdio.h>
main()
{
	int i,j,prime=0,count=0;
	for(i=1;i<=9;i++)
	{
		for(j=1;j<=9;j++)
		{
			if(i%j==0)
				prime++;
		}
		if(prime==2)
			count++;
		prime=0;
	}
	printf("Single digit PRIME number total=%d\n",count);
}

