//Write a program to print the sum of all single digit odd numbers.

#include<stdio.h>
main()
{
        int num,sum=0;
        for(num=0;num<=9;num++)
        {
                if(num%2==1)
                        sum+=num;
        }
        printf("Single digit odd number SUM= %d\n",sum);
}
