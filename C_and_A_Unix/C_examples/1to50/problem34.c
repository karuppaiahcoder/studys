//Write a program to Print total number non-decreasing numbers from 1000 to 9999.Non decreasing numbers have individual digits that do not have a decreasing order from left to right. A digit can be used only once.

#include<stdio.h>
main()
{
	int i,num=0,valid=0,l=0,count=0,check=0;
	for(i=1000;i<=9999;i++)
	{
		num=i;
		l=i%10;
		num/=10;
		while(num)
		{
			check=num%10;
			if(check<l)
				valid=1;
			else
			{
				valid=0;
				break;
			}
			l=check;
			num/=10;
		}
		if(valid)
		{
			count++;
			valid=0;
		}
	}
	printf("non-decreasing Total numbers=%d\n",count);
}
