//Write a program to get a number up to 50 digits from user and check whether the number is a valid number. Valid number contains digits 0 -9.

#include<stdio.h>
main()
{
	char array[51]={};
	int i,count=0;
	printf("\n\t--------Check the number valid or non valid------\n\n");
	printf("Enter the 50 digit number:");
	scanf("%s",array);
	for(i=0;array[i]!='\0';i++)
	{
		if(array[i]>='0'&&'9'>=array[i])
			count=1;
		else
		{
			count=0;
			break;
		}
	}	
	if(count==1)
		printf("valid number:%s\n",array);
	else
		printf("Non-valid number:%s\n",array);
}
