//Write a program to print the sum of single digit Prime numbers

#include<stdio.h>
main()
{
        int i,j,prime=0,sum=0;
        for(i=1;i<9;i++)
        {
                for(j=2;j<i;j++)
                {
                        if(i%j==0)
                                prime++;
                }
                if(prime==0)
                        sum+=i;
                prime=0;
        }
        printf("SUM of Single digit PRIME number total=%d\n",sum);
}

