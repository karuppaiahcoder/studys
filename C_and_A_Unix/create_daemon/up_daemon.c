/*
File name : up_daemon.c
Usage     : create daemon process to get log about particular directory details.
Writer by : Karuppaiah K
*/
/*
Algorithm:
``````````
	+ Create one daemon process with coding rules.
	+ I use the fork function to create daemon process.
	+ Then create one restriction file like ".daemon.pid" file to maintain the single intences, which means dont run the same program many time.
	+ Use the fcntl function to lock particular files.
	+ That process will be maintain two things. That first we mension particular directory, so we need conficuration file like ".daemon.conf". We mentioned particlar directory to get the log about that directory.
	+ These files will be placed in a "~/AdvancedUnix" path.
	+ I write the single intent function like already_run() function.
	+ When user give the signal SIGHUN in manualy that time program goto re-read the conficuration file path.
	+ Every action will be loged in the ".daemon.log" file.
*/

#include<stdio.h>
#include<signal.h>
#include<string.h>
#include<stdlib.h>
#include<fcntl.h>
#include<unistd.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<dirent.h>

#define _PID "/home/karuppaiah/UNIX/A_Unix/02282017/.daemon.pid" //_PID is a Path of .daemon.pid files.
#define _CONF "/home/karuppaiah/UNIX/A_Unix/02282017/.daemon.conf" //_CONF is a Path of .daemon.conf files.
#define _LOG "/home/karuppaiah/UNIX/A_Unix/02282017/.daemon.log" //_LOG is a Path of .daemon.log files.
#define _WORK_DIR "/home/karuppaiah/UNIX/A_Unix/02282017" //daemon process running path

int log_fd,vir_log;
FILE *conf_fp;
char dir_path[500];
time_t old;

int file_lock(int fd)
{
	struct flock lk;
	lk.l_type=F_WRLCK;
	lk.l_whence=SEEK_SET;
	lk.l_start=0;
	lk.l_len=10;
	return (fcntl(fd,F_SETLK,&lk,NULL));
}

int already_run(void)
{
	int fd;
	char buf[10]={};
	if((fd=open(_PID,O_WRONLY|O_CREAT,0666))<0)
	{
		write(vir_log,"File open error [56]\n",20);
		exit(1);
	}
	if(file_lock(fd)<0)
	{
		return 1;
	}
	ftruncate(fd,0);
	sprintf(buf,"%d",getpid());
	write(fd,buf,sizeof(buf));
	return 0;
}

int daemon_init(void)
{
	pid_t pid;
	int i,fd;
	umask(0);		//first coding rull set umask zero.
	if((pid=fork())==-1)
	{
		printf("Fork error\n");
		exit(1);
	}else if(pid>0)		//parent
		exit(0);
	setsid();		//Create new session to avoid the controlling terminal
	if((pid=fork())==-1)
	{
		printf("Fork error\n");
		exit(1);
	}else if(pid>0) 	//parent
		exit(0);
	if(chdir(_WORK_DIR)==-1)
	{
		printf("Chdir function error\n");
		exit(1);
	}
	for(i=0;i<1024;i++)	//close all descriptors.
		close(i);
	if((fd=open("/dev/null",O_RDWR))==-1)//set stdin,stdout and stderr to the /dev/null file
	{
		printf("open error\n");
		exit(1);
	}
	dup2(fd,1);
	dup2(fd,2);
}

void read_conf()
{
	printf("Signal received\n");
	char buf[255]={},cmd[200]={};
	struct stat ds;
	if((conf_fp=fopen(_CONF,"r"))==NULL)
	{
		write(vir_log,"conf file open  error [109]\n",29);
		exit(1);
	}
	fgets(buf,255,conf_fp);
	printf("%d\n",write(vir_log,buf,strlen(buf)));
	buf[strlen(buf)-1]='\0';
	fclose(conf_fp);
	strcpy(dir_path,buf);
	printf("%s\n",dir_path);
	sprintf(cmd,"ls -a %s>/tmp/.dir_list.txt",dir_path);
	system(cmd);		//ls w_dir > /tmp/.dir_list.txt
	stat(dir_path,&ds);
	old=ds.st_mtime;
}

char *rem(char dir_path[])
{
	DIR *dir;
	char buf[255]={},*ptr;
	struct dirent *con;
	int i;
	FILE *fp=fopen("/tmp/.dir_list.txt","r");
	while(fgets(buf,255,fp)!=NULL)
	{
		i=0;
		buf[strlen(buf)-1]='\0';
		if(!strcmp(buf,".") || !strcmp(buf,".."))
			continue;
		dir=opendir(dir_path);
	        while((con=readdir(dir))!=NULL)
	        {
		        if(!strcmp(con->d_name,".") || !strcmp(con->d_name,".."))
				continue;
			if(!strcmp(buf,con->d_name))
			{
				i=1;
				break;
			}
	 	}
		if(i==0)
		{
			ptr=(char *)malloc(strlen(buf));
			strcpy(ptr,buf);
			fclose(fp);
			return ptr;
		}
		closedir(dir);
	}
	fclose(fp);
	return NULL;
}

int main(int argc,char *argv[])
{
//	daemon_init();	//create daemon process
	vir_log=open("/home/karuppaiah/UNIX/A_Unix/02282017/tmp.log",O_WRONLY|O_CREAT|O_APPEND,0666);
	if(already_run())	//check single instence.
	{
		write(vir_log,"Daemon already running [164]\n",30);
		exit(1);
	}
	if((log_fd=open(_LOG,O_WRONLY|O_CREAT|O_APPEND,0666))<0)
	{
		write(vir_log,"log file open error [169]\n",30);
		exit(1);
	}
	struct stat ds,fs;
	struct dirent *con;
	DIR *dir;
	FILE *fp;
	char buf[255]={},path[1024]={},*ptr,date_c[30]={},data[2000]={},cmd[200]={};
	int i=0;
	signal(SIGHUP,read_conf);
	read_conf();
	while(1)
	{
		stat(dir_path,&ds);
		printf("%s\n",dir_path);
		if(ds.st_mtime!=old)
		{
			i=0;
			dir=opendir(dir_path);
			bzero(path,1024);
			bzero(data,1000);
			while((con=readdir(dir))!=NULL)
			{
				if(!strcmp(con->d_name,".") || !strcmp(con->d_name,".."))
					continue;
				sprintf(path,"%s%s",dir_path,con->d_name);
				stat(path,&fs);
				if(ds.st_mtime<=fs.st_mtime)
				{
					fp=popen("date","r");
					fgets(date_c,30,fp);
					date_c[strlen(date_c)-1]='\0';
					sprintf(data,"[%s:%s:create the file%s%s]\n",date_c,argv[0],dir_path,con->d_name);
					write(log_fd,data,strlen(data));
					i=1;
					break;
				}
			}
			closedir(dir);
			if(i==0)
			{
				if((ptr=rem(dir_path))!=NULL)
				{
					fp=popen("date","r");
					fgets(date_c,30,fp);
					date_c[strlen(date_c)-1]='\0';
					sprintf(data,"[%s:%s:delete the file%s%s]\n",date_c,argv[0],dir_path,ptr);
					write(log_fd,data,strlen(data));
				}
			}
			sprintf(cmd,"ls -a %s>/tmp/.dir_list.txt",dir_path);
			system(cmd);		//ls w_dir > /tmp/.dir_list.txt
			old=ds.st_mtime;
		}else 
			sleep(2);
	}
}
