/*
 * Copyright (c) 1988, 1989, 1991, 1994, 1995, 1996, 1997, 1998, 1999, 2000
 *	The Regents of the University of California.  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that: (1) source code distributions
 * retain the above copyright notice and this paragraph in its entirety, (2)
 * distributions including binary code include the above copyright notice and
 * this paragraph in its entirety in the documentation or other materials
 * provided with the distribution, and (3) all advertising materials mentioning
 * features or use of this software display the following acknowledgement:
 * ``This product includes software developed by the University of California,
 * Lawrence Berkeley Laboratory and its contributors.'' Neither the name of
 * the University nor the names of its contributors may be used to endorse
 * or promote products derived from this software without specific prior
 * written permission.
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
 */
/*
	lint
		Lint is a c program verfier.
		It will detects feauture of c program files which is likely to be bugs, non portable, or wasteful.
		Lint run the c preprocessor with defining 'lint'at its first phase.
		As for the code the below statement should be skipped while linting.			

*/
#ifndef lint
static const char copyright[] =
    "@(#) Copyright (c) 1988, 1989, 1991, 1994, 1995, 1996, 1997, 1998, 1999, 2000\n\
The Regents of the University of California.  All rights reserved.\n";
	/*
		rcs
			rcs stands for revision control system.
			rcs manges multiple revisions of files.
				
	*/
static const char rcsid[] =
    "@(#)$Id: traceroute.c,v 1.68 2000/12/14 08:04:33 leres Exp $ (LBL)";
#endif

/*
 * traceroute host  - trace the route ip packets follow going to "host".
 *
 * Attempt to trace the route an ip packet would follow to some
 * internet host.  We find out intermediate hops by launching probe
 * packets with a small ttl (time to live) then listening for an
 * icmp "time exceeded" reply from a gateway.  We start our probes
 * with a ttl of one and increase by one until we get an icmp "port
 * unreachable" (which means we got to "host") or hit a max (which
 * defaults to 30 hops & can be changed with the -m flag).  Three
 * probes (change with -q flag) are sent at each ttl setting and a
 * line is printed showing the ttl, address of the gateway and
 * round trip time of each probe.  If the probe answers come from
 * different gateways, the address of each responding system will
 * be printed.  If there is no response within a 5 sec. timeout
 * interval (changed with the -w flag), a "*" is printed for that
 * probe.
 *
 * Probe packets are UDP format.  We don't want the destination
 * host to process them so the destination port is set to an
 * unlikely value (if some clod on the destination is using that
 * value, it can be changed with the -p flag).
 *
 * A sample use might be:
 *
 *     [yak 71]% traceroute nis.nsf.net.
 *     traceroute to nis.nsf.net (35.1.1.48), 30 hops max, 56 byte packet
 *      1  helios.ee.lbl.gov (128.3.112.1)  19 ms  19 ms  0 ms
 *      2  lilac-dmc.Berkeley.EDU (128.32.216.1)  39 ms  39 ms  19 ms
 *      3  lilac-dmc.Berkeley.EDU (128.32.216.1)  39 ms  39 ms  19 ms
 *      4  ccngw-ner-cc.Berkeley.EDU (128.32.136.23)  39 ms  40 ms  39 ms
 *      5  ccn-nerif22.Berkeley.EDU (128.32.168.22)  39 ms  39 ms  39 ms
 *      6  128.32.197.4 (128.32.197.4)  40 ms  59 ms  59 ms
 *      7  131.119.2.5 (131.119.2.5)  59 ms  59 ms  59 ms
 *      8  129.140.70.13 (129.140.70.13)  99 ms  99 ms  80 ms
 *      9  129.140.71.6 (129.140.71.6)  139 ms  239 ms  319 ms
 *     10  129.140.81.7 (129.140.81.7)  220 ms  199 ms  199 ms
 *     11  nic.merit.edu (35.1.1.48)  239 ms  239 ms  239 ms
 *
 * Note that lines 2 & 3 are the same.  This is due to a buggy
 * kernel on the 2nd hop system -- lbl-csam.arpa -- that forwards
 * packets with a zero ttl.
 *
 * A more interesting example is:
 *
 *     [yak 72]% traceroute allspice.lcs.mit.edu.
 *     traceroute to allspice.lcs.mit.edu (18.26.0.115), 30 hops max
 *      1  helios.ee.lbl.gov (128.3.112.1)  0 ms  0 ms  0 ms
 *      2  lilac-dmc.Berkeley.EDU (128.32.216.1)  19 ms  19 ms  19 ms
 *      3  lilac-dmc.Berkeley.EDU (128.32.216.1)  39 ms  19 ms  19 ms
 *      4  ccngw-ner-cc.Berkeley.EDU (128.32.136.23)  19 ms  39 ms  39 ms
 *      5  ccn-nerif22.Berkeley.EDU (128.32.168.22)  20 ms  39 ms  39 ms
 *      6  128.32.197.4 (128.32.197.4)  59 ms  119 ms  39 ms
 *      7  131.119.2.5 (131.119.2.5)  59 ms  59 ms  39 ms
 *      8  129.140.70.13 (129.140.70.13)  80 ms  79 ms  99 ms
 *      9  129.140.71.6 (129.140.71.6)  139 ms  139 ms  159 ms
 *     10  129.140.81.7 (129.140.81.7)  199 ms  180 ms  300 ms
 *     11  129.140.72.17 (129.140.72.17)  300 ms  239 ms  239 ms
 *     12  * * *
 *     13  128.121.54.72 (128.121.54.72)  259 ms  499 ms  279 ms
 *     14  * * *
 *     15  * * *
 *     16  * * *
 *     17  * * *
 *     18  ALLSPICE.LCS.MIT.EDU (18.26.0.115)  339 ms  279 ms  279 ms
 *
 * (I start to see why I'm having so much trouble with mail to
 * MIT.)  Note that the gateways 12, 14, 15, 16 & 17 hops away
 * either don't send ICMP "time exceeded" messages or send them
 * with a ttl too small to reach us.  14 - 17 are running the
 * MIT C Gateway code that doesn't send "time exceeded"s.  God
 * only knows what's going on with 12.
 *
 * The silent gateway 12 in the above may be the result of a bug in
 * the 4.[23]BSD network code (and its derivatives):  4.x (x <= 3)
 * sends an unreachable message using whatever ttl remains in the
 * original datagram.  Since, for gateways, the remaining ttl is
 * zero, the icmp "time exceeded" is guaranteed to not make it back
 * to us.  The behavior of this bug is slightly more interesting
 * when it appears on the destination system:
 *
 *      1  helios.ee.lbl.gov (128.3.112.1)  0 ms  0 ms  0 ms
 *      2  lilac-dmc.Berkeley.EDU (128.32.216.1)  39 ms  19 ms  39 ms
 *      3  lilac-dmc.Berkeley.EDU (128.32.216.1)  19 ms  39 ms  19 ms
 *      4  ccngw-ner-cc.Berkeley.EDU (128.32.136.23)  39 ms  40 ms  19 ms
 *      5  ccn-nerif35.Berkeley.EDU (128.32.168.35)  39 ms  39 ms  39 ms
 *      6  csgw.Berkeley.EDU (128.32.133.254)  39 ms  59 ms  39 ms
 *      7  * * *
 *      8  * * *
 *      9  * * *
 *     10  * * *
 *     11  * * *
 *     12  * * *
 *     13  rip.Berkeley.EDU (128.32.131.22)  59 ms !  39 ms !  39 ms !
 *
 * Notice that there are 12 "gateways" (13 is the final
 * destination) and exactly the last half of them are "missing".
 * What's really happening is that rip (a Sun-3 running Sun OS3.5)
 * is using the ttl from our arriving datagram as the ttl in its
 * icmp reply.  So, the reply will time out on the return path
 * (with no notice sent to anyone since icmp's aren't sent for
 * icmp's) until we probe with a ttl that's at least twice the path
 * length.  I.e., rip is really only 7 hops away.  A reply that
 * returns with a ttl of 1 is a clue this problem exists.
 * Traceroute prints a "!" after the time if the ttl is <= 1.
 * Since vendors ship a lot of obsolete (DEC's Ultrix, Sun 3.x) or
 * non-standard (HPUX) software, expect to see this problem
 * frequently and/or take care picking the target host of your
 * probes.
 *
 * Other possible annotations after the time are !H, !N, !P (got a host,
 * network or protocol unreachable, respectively), !S or !F (source
 * route failed or fragmentation needed -- neither of these should
 * ever occur and the associated gateway is busted if you see one).  If
 * almost all the probes result in some kind of unreachable, traceroute
 * will give up and exit.
 *
 * Notes
 * -----
 * This program must be run by root or be setuid.  (I suggest that
 * you *don't* make it setuid -- casual use could result in a lot
 * of unnecessary traffic on our poor, congested nets.)
 *
 * This program requires a kernel mod that does not appear in any
 * system available from Berkeley:  A raw ip socket using proto
 * IPPROTO_RAW must interpret the data sent as an ip datagram (as
 * opposed to data to be wrapped in a ip datagram).  See the README
 * file that came with the source to this program for a description
 * of the mods I made to /sys/netinet/raw_ip.c.  Your mileage may
 * vary.  But, again, ANY 4.x (x < 4) BSD KERNEL WILL HAVE TO BE
 * MODIFIED TO RUN THIS PROGRAM.
 *
 * The udp port usage may appear bizarre (well, ok, it is bizarre).
 * The problem is that an icmp message only contains 8 bytes of
 * data from the original datagram.  8 bytes is the size of a udp
 * header so, if we want to associate replies with the original
 * datagram, the necessary information must be encoded into the
 * udp header (the ip id could be used but there's no way to
 * interlock with the kernel's assignment of ip id's and, anyway,
 * it would have taken a lot more kernel hacking to allow this
 * code to set the ip id).  So, to allow two or more users to
 * use traceroute simultaneously, we use this task's pid as the
 * source port (the high bit is set to move the port number out
 * of the "likely" range).  To keep track of which probe is being
 * replied to (so times and/or hop counts don't get confused by a
 * reply that was delayed in transit), we increment the destination
 * port number before each probe.
 *
 * Don't use this as a coding example.  I was trying to find a
 * routing problem and this code sort-of popped out after 48 hours
 * without sleep.  I was amazed it ever compiled, much less ran.
 *
 * I stole the idea for this program from Steve Deering.  Since
 * the first release, I've learned that had I attended the right
 * IETF working group meetings, I also could have stolen it from Guy
 * Almes or Matt Mathis.  I don't know (or care) who came up with
 * the idea first.  I envy the originators' perspicacity and I'm
 * glad they didn't keep the idea a secret.
 *
 * Tim Seaver, Ken Adelman and C. Philip Wood provided bug fixes and/or
 * enhancements to the original distribution.
 *
 * I've hacked up a round-trip-route version of this that works by
 * sending a loose-source-routed udp datagram through the destination
 * back to yourself.  Unfortunately, SO many gateways botch source
 * routing, the thing is almost worthless.  Maybe one day...
 *
 *  -- Van Jacobson (van@ee.lbl.gov)
 *     Tue Dec 20 03:50:13 PST 1988
 */

#include <sys/param.h>		/* + param.h defines  machine-specific parameters.  
			 	   + These  parameters set limits on the operation of the COHERENT system; 
				   + e.g., the number of files that can be open at any one time.
				*/
#include <sys/file.h>
#include <sys/ioctl.h>		// This file contains the ioctl() function.
#ifdef HAVE_SYS_SELECT_H
#include <sys/select.h>		/* + This file contains the select() function ,We using this function to 
			   	   wait for the response.
				*/
#endif
#include <sys/socket.h>		/* + This file contains the socket related functions and definitions.
				   eg: socket()
				*/
#include <sys/time.h>		/* + This file contains the time related functions and structures. 
				   eg: gettimeofday()
				*/
#include <netinet/in_systm.h>	// This header file contains the u_int32_t implementation dependant data type.
#include <netinet/in.h>		/* + This file contains the structure for IP address family and macros for 
				   protocols.(IPPROTO_IP)
				*/
#include <netinet/ip.h>		// This file contains the definitions and structure for IP header.
#include <netinet/ip_var.h>	// it's contains overlay IP structures for the other protocols.
#include <netinet/ip_icmp.h>	// This header file contains the macro definitions and structure for the ICMP.
#include <netinet/udp.h>	// This header file contains the UDP structure.
#include <netinet/udp_var.h>	// We are accessing pseudo header structure from this header file.

#include <arpa/inet.h>		/* + This file contains the functions such as inet_ntoa().
				   + (convert the doted decimal notation IP address to string)
				*/


#include <ctype.h>		/* + This file  contains the functions that are useful for testing and mapping 
				  characters .
				*/
#include <errno.h>		
#include <fcntl.h>		/* + This file contains the functions and structure and macros for handle 
				   the files .
				*/
#ifdef HAVE_MALLOC_H
#include <malloc.h>		// This file contains the  Prototypes and definition for malloc implementation.
#endif
#include <memory.h>	
#include <netdb.h>		// definitions for network database operations .( gethostbyname() )	
#include <stdio.h>		/* + its contains the definitions to handle the standard input and output .
				   (printf())
				*/
#include <stdlib.h>		/* + This is the header of the general purpose standard library ,
				  it's contains functions involving memory allocation, process control, 
				  conversions and others .
				*/
#include <string.h>		/* +  This header file contains the definitions and functions used to 
				   handle the strings .
				*/
#include <unistd.h>		// This header file contains the definitions and functions for Unix (getopt())

#include "gnuc.h"		/* + This header file contains the  __dead macro definition 
				   (#define __dead volatile)
				*/
#ifdef HAVE_OS_PROTO_H
#include "os-proto.h"
#endif

/* rfc1716 */
#ifndef ICMP_UNREACH_FILTER_PROHIB
/* 
         ICMP_UNREACH_FILTER_PROHIB:
		"admin prohibited filter" means that there's a firewall that blocks a packets to get through it.
*/
#define ICMP_UNREACH_FILTER_PROHIB	13	/* admin prohibited filter */
#endif

#ifndef ICMP_UNREACH_HOST_PRECEDENCE
/*
	ICMP_UNREACH_HOST_PRECEDENCE           
                'host precedence violation' is nothing but It is send by the first hop router to the host to indicate that requested precedence is not permitted for the packet.
*/
#define ICMP_UNREACH_HOST_PRECEDENCE	14	/* host precedence violation */
#endif

#ifndef ICMP_UNREACH_PRECEDENCE_CUTOFF
/* 
        ICMP_UNREACH_PRECEDENCE_CUTOFF
		⇒ The network operator requires the minimum level of precedence for operation.
		⇒ if the datagram has the minimum level of precedence then the cutoff function will drop the packet and send this icmp response to the sender .
*/
#define ICMP_UNREACH_PRECEDENCE_CUTOFF	15	/* precedence cutoff */
#endif

#include "findsaddr.h"	// This header file contains the ifaddrlist() function.
#include "ifaddrlist.h" // This header file contains the findsaddr() function.
#include "traceroute.h" // The setsin() function declaration contains.

/* Maximum number of gateways (include room for one noop) */
/* 
    What is the calculation?
        * MAX_IPOPTLEN  and IPOPT_MINOFF macro define in the ip.h header file path(/usr/include/linux/ip.h).

                MAX_IPOPTLEN    40      ->Because the IP header option field only store maximum value 40 bytes.
                POPT_MINOFF     4       ->The option field have some additional information like,
                                          type,length and pointer.
                                  8                   16               24                 32
                +-----------------+-------------------+----------------+------------------+
                |option type(8bit)|option length(8bit)|option pointer  |option data       |
                +-----------------+-------------------+----------------+  remaining bytes |
                |                                                                         |
                +-------------------------------------------------------------------------+
                (But I don't know why they were subtract 1?)
                        + here first four bytes used for option standard field, then next 4bytes used for destination IP, that reason for subtract 1.

                *Calculation:
                ((40-4-1)/4)
                (40-4-1)=35     -here reserved 5 bytes
                35/4=8          -Here reserved 3 bytes 5+3=8 bytes
                                 4 for option standards, next 4 for destination.
        So here maximum number of gateway value is 8.
*/
#define NGATEWAYS ((int)((MAX_IPOPTLEN - IPOPT_MINOFF - 1) / sizeof(u_int32_t)))

#ifndef MAXHOSTNAMELEN
/*
Why MAXHOSYNAMELEN need?
	+ Because, this variable indicate the maximum length of host name (64). 
	+ It is standard for network. 
	+ Each system have this rang, if we want check it,use following command.
        Note:
                This macro already define in our system
                        Path:
                                'asm-generic/param.h'
                But they doesn't use this header file in this program.
                                Example command:
                                $ getconf HOST_NAME_MAX
                                64
*/
#define MAXHOSTNAMELEN	64
#endif

#define Fprintf (void)fprintf
#define Printf (void)printf

/* Host name and address list */
/*
        This is 12byte size structure,To store a host name information.

                name    ->      Name of the host

                n       ->      IP address count,because single may be have multiple IP address. 
                                Here they were use integer, maximum value is (2^31)=2147483648.

                addrs   ->      This is pointer variable,So here we can store the number of IP address.
                                Within 'n' variable range.
                                Usually per IP address size is 32 bits, so they were use u_int32_t variable.
        Example:
                given host name is (192.168.2.51).
                name=192.168.2.51       (this host have only one IP)
                n=1;
                addrs=192.168.2.51
*/
struct hostinfo {
	char *name;
	int n;
	u_int32_t *addrs;
};

/* Data section of the probe packet */
/*
	Why we need outdata structure?
	        + we stored some information to the payload, In this case the receiver send responce packet with our original packet information. 
		+ So, we have to verify the packets mine or not.
	
	* This is the 12byte structure (32bits architecture) or 24byte structure (64 bits architecture),
          So, every structure size may change the  different architecture machine.
                Example:
                        our root machine is 64bit and local machine is 32bits.
                        # lscpu
                        Architecture:          x86_64
                        $ lscpu
                        Architecture:          i686
        If we have 64bits architecture,
                seq     -> 1 byte
                ttl     -> 1 byte but padding with 6 bytes 
                tv      -> 16 bytes
        If we have 32 bit architecture,
                seq     -> 1 byte 
                ttl     -> 1 byte but padding with 2 bytes 
                tv      -> 8 bytes
*/
struct outdata {
	u_char seq;		/* sequence number of this packet */
	u_char ttl;		/* ttl packet left with */
	struct timeval tv;	/* time packet left */
};

#ifndef HAVE_ICMP_NEXTMTU
/* Path MTU Discovery (RFC1191) */
/*
	path MTU is nothing but the MTU of the next hop in the path .
*/
struct my_pmtu {
	u_short ipm_void;
	u_short ipm_nextmtu;
};
#endif

u_char	packet[512];		/* last inbound (icmp) packet */

/*
        1          4             8                      16                          32
        +----------+-------------+----------------------+---------------------------+
        |Version   |header length|   type of service    |               total length|
        |(4 bit)   |   (4 bit)   |      (8 bit)         |               (16 bit)    |
        +----------+-------------+----------------------+---------------------------+
        |               identification  (16 bit)        | flag   |fragment offset   |
        |                                               |(3  bit)|(13 bit)          |
        +------------------------+----------------------+--------+------------------+
        |    Time to lives       |      protocol        | Header checksum (16 bit ) |
        |       (8 bit)          |      (8 bit)         |                           |
        +------------------------+----------------------+---------------------------+
        |               Source  IP address      (32 bit)                            |
        +---------------------------------------------------------------------------+
        |               Destination IP address  (32 bit)                            |
        +---------------------------------------------------------------------------+
        |               Optionals       0-40 byte                                   |
        +---------------------------------------------------------------------------+
        |                               Data                                        |
        +---------------------------------------------------------------------------+

	struct ip
	{
		#if __BYTE_ORDER == __LITTLE_ENDIAN
			unsigned int        ip_hl:4;    ->    header length 
			unsigned int        ip_v:4;     ->    version 
		#endif
		#if __BYTE_ORDER == __BIG_ENDIAN
			unsigned int        ip_v:4;     ->    version 
			unsigned int        ip_hl:4;    ->    header length
		#endif
		u_int8_t    ip_tos;             ->    type of service                       
		u_short     ip_len;             ->    total length                          
		u_short     ip_id;              ->    identification 
		u_short     ip_off;             ->    fragment offset field
		#define         IP_RF 0x8000        ->    reserved fragment flag        
		#define         IP_DF 0x4000        ->    don't fragment flag
		#define         IP_MF 0x2000        ->    more fragments flag           
		#define         IP_OFFMASK 0x1fff   ->    mask for fragmenting bits 
		u_int8_t    ip_ttl;             ->    time to live 
		u_int8_t    ip_p;               ->    protocol 
		u_short     ip_sum;             ->    checksum 
		struct in_addr ip_src, ip_dst;  ->    source and destination address 
	};
        + Totally 20 bytes for IP structure size. 
	+ because the IP header size minimum 20 bytes.
        + It is standard for network.
*/
struct ip *outip;		/* last output (udp) packet */
/*
	'outudp' used to store the udp information. Usually the udp header size is 8 byte.

                1               16              32
                +---------------+----------------+
                |source port    |destination port|
                |--------------------------------|
                |    length     |  check sum     |
                +---------------+----------------+

        struct udphdr 
        {
                u_short uh_sport;                source port 
                u_short uh_dport;                destination port 
                short   uh_ulen;                 udp length 
                u_short uh_sum;                  udp checksum 
        };
*/
struct udphdr *outudp;		/* last output (udp) packet.*/

struct outdata *outdata;	/* last output (udp) packet.To store the Payload information */
struct icmp *outicmp;		/* last output (icmp) packet. To store the icmp packet information.*/

/* loose source route gateway list (including room for final destination) */
/* 
	why ngateway is 8+1?
		Then maximum gateways 8 and then we should be store the destination. That is the reason add 1.
*/
u_int32_t gwlist[NGATEWAYS + 1];

int frag_done;	// This variable used to check whether the fragmentation done or not.
int tnoframe; // It is used for temporary purpose,it contains the no of fragmentation done to a packet.


int s;				/* + receive (icmp) socket file descriptor. 
				   + To store the receive socket file descriptor. */
int sndsock;			/* + send (udp/icmp) socket file descriptor.
				   + To store the send socket file descriptor. */
struct sockaddr whereto;	/* Who to try to reach */
struct sockaddr wherefrom;	/* Who we are */
int packlen;			/* total length of packet */
int minpacket;			/* min ip packet size */

/* why the maxpacket length is 32768 ?
        
        * this is because the size of ip_header_length field in ip header is 2 bytes (short)
          so, at max we can send 2 ^ 15 => 32768 bytes.
        * you can ask one more question like,

   	  Why they were directly doesn't store that value like, maxpacket=32768?
                + It just human understanding. 
		+ Which means they were give one clue for us, that we can sent the 32 KB data per packet.
*/
int maxpacket = 32 * 1024;	/* max ip packet size */
/*
   What is RFC ?
         A Request for Comments (RFC) is a formal document from the Internet Engineering Task Force ( IETF ).
*/
int pmtu;			/* Path MTU Discovery (RFC1191) */
u_int pausemsecs;

char *prog;	//To Store the process name like 'traceroute'
char *source;	//If we use a -s option, that time we want to store the source name in this variable.
char *hostname;	//To store the destination host name (e:g www.google.com)
char *device;	//If we use a -i option, that time we want to store the interface name in this variable.
static const char devnull[] = "/dev/null";
/*
        Why nprobes initialize 3?
                * Because the trace route command send the three probe per ttl values. 
                * But we can change a value by using -q option.
                * They were mentioned minimum value is 1, but maximum value is -1 in a str2val() function.
                * But we know maximum and minimum value in integer.
                        max     (2^31-1)=2147483647.
                        min     (2^31)=-2147483648.
        maximum value -1 reason?
                * Because they give maximum range is our machine range. e:g (32 machine) 2147483647.
*/
int nprobes = 3;
/*
        Why max_ttl initialize 30?

                * Because they were just assume we can reach the destination within this range (30).
                * But we change this range,using -m option.
                * Note the maximum ttl value is 255, because the TTL field in IP header is 1byte(u_char).
                  so, at maximum value we can send 2^8-1=255. Otherwise give error message.

                Example:
                        ./traceroute -f 257 192.168.2.55  2
                        traceroute: first ttl must be <= 255
                * Here u_char is enough,(But I don't know why they were use integer variable)?
*/
int max_ttl = 30;
/*
        Why first_ttl  is initialised by 1?
        
                * The first_ttl variable used to store the starting value of time to live.
                * Because of we have travel at least 1 gateway.That's why we give 1. 
                * If we already known, our packet travel some number of gateway, in that case we can use -f with number.
                * Note, If first_ttl(35) value grater than max_ttl(30),it should be give error message.
                        Example:
                                ./traceroute -f 35 192.168.2.55  
                                traceroute: first ttl (35) may not be greater than max ttl (30)
                * Here u_char is enough,(But I don't know why they were use integer variable)?
*/
int first_ttl = 1;
/*
        why ident variable declare by u_short?
        
                * Because the identification field in IP header only have 2bytes. 
                  That's why they were use the u_short data type variable.
                        2^(u_short)
                        2^16=65535.
                * So, we can use maximum value of identification is 65535.
*/
u_short ident;

u_short port = 32768 + 666;	/* start udp dest port # for probe packets */

int options;			/* socket options,like SO_DONTROUTE and SO_DEBUG*/
int verbose;
int aspath;
int waittime = 5;		/* time to wait for response (in seconds) */
int nflag;			/* print addresses numerically */
int useicmp;			/* use icmp echo instead of udp packets */
#ifdef CANT_HACK_IPCKSUM
int doipcksum = 0;		/* + don't calculate ip checksums by default 
				   + CANT_HACK_IPCKSUM is a Toggle variable.
				*/
#else
int doipcksum = 1;		/* calculate ip checksums by default */
#endif
int optlen;			/* length of ip options */

/*
        * The optind,opterr and optarg variable are extern from <unistd.h>,because getopt() function use this variables to following purpose:
        * If we give command line argument like,
                
                ./traceroute -f 5 192.168.2.55  56

        here the argc value is 5, which means number of arguments. This time getopt function first get '-f' option and store that option argument to 'optarg' pointer variable,then 'optind' variable point the next argument index.
                        -f      optarg="5"
                                optind=3
				
                        then getopt() function exit, now optind variable point the 3rd index which means host name. In this case we easy to manage the command line arguments values.
*/
extern int optind;	//argv[] array index
extern int opterr;	//store getopt() function given error number
extern char *optarg;	//argument of given option.

/* Forwards */

double	deltaT(struct timeval *, struct timeval *);	//To calculate time between send and receive (RTT).
void	freehostinfo(struct hostinfo *);		// To free the used memory
void	getaddr(u_int32_t *, char *);			// To get the IP address of the host name
struct	hostinfo *gethostinfo(char *);			// To get the information of given host name
	                                                /*
	                                                struct hostinfo {
	                                                        char *name;             // host name
	                                                        int n;                  //IP address count
	                                                        u_int32_t *addrs;       //list of IP
	                                                        };
	                                                */
u_short	in_cksum(u_short *, int);		// To do the checksum calculation to IP head and UDP header
char	*inetname(struct in_addr);		// To Get host name
int	main(int, char **);			//main function of the program
int	packet_ok(u_char *, int, struct sockaddr_in *, int,int *);//To check the response packet mine or not
char	*pr_type(u_char);				//To get the  error message it is depend upon the type
void	print(u_char *,int, struct sockaddr_in *);	//To print the information.
void	send_probe(int, int,struct timeval *,int);		//Send the probe packet to hops
int	str2val(const char *, const char *, int, int);	// to convert string into hex or dec
void	tvsub(struct timeval *, struct timeval *);	
__dead	void usage(void);				// To display the usage information 
int	wait_for_reply(int, struct sockaddr_in *, const struct timeval *);//wait for reply message
#ifndef HAVE_USLEEP
int	usleep(u_int);
#endif

// get_as() function used to get the Autonomous system number of a gateway.    
const char *get_as(const char *query);

//fragment_func() is used to send the framgmented packet with help of sendto() function.
int fragement_func(int,struct ip *);

/*
	Before here and the main function, We typed the comments together.
	kannan,karuppaiah,sathya 
	
*/
int main(int argc, char **argv)
{
	register int op, code, n;// op ->To store the getopt() function return value
				 // code->To store the ICMP error code message
				 // n  ->To store the ifaddrlist() function return value(interface count).
	register char *cp;	 // Temporary usage  like store the (path,"icmp","ip").
	register const char *err;// To store the error message.
	register u_char *outp;	 // temporary usage
	register u_int32_t *ap;	 // temporary usage
	register struct sockaddr_in *from = (struct sockaddr_in *)&wherefrom; //To store the source IP info
	register struct sockaddr_in *to = (struct sockaddr_in *)&whereto;     //To store the destination IP info
	register struct hostinfo *hi;
	int on = 1;			//To use the setsockopt() function to third field to (true or false)
	register struct protoent *pe;	//To store the getprotobyname() function return value.
	register int ttl, probe, i;	//ttl   ->To maintain a ttl count
					//probe ->To maintain a probe count
					//i     ->Temporary usage
	register int seq = 0;		//To maintain a squints count.
	int tos = 0, settos = 0;	//tos   ->If we enable the type of service  by using -t option
					//        At that time we store that value.
					//settos ->To indicate the type of service enable or not.
	register int lsrr = 0;		//To maintain a gateway count.
	register u_short off = 0;	//enable the Don't fragment filed
	struct ifaddrlist *al;		//To get the interface information
	char errbuf[132];		//To get the error message
	int mtu=0;	// It is the mtu of user's network interface. This variable used while doing fragmentation.

	/*
                * We can execute the executable file using exec family functions and also,
		  we can call the exec functions with program name as NULL( namely argv[0]).

		 example:
			 execl("./traceroute",NULL);
	*/
	if (argv[0] == NULL)
		prog = "traceroute";
	else if ((cp = strrchr(argv[0], '/')) != NULL) // To handle the absolute path and relative path.
		prog = cp + 1;			       // To ignore the '/' in the program name
	else
		prog = argv[0];	/* + Get the program name from the command line. 
				   + Which means 'prog' variable point the argv[0] index.*/

	opterr = 0;	/*
			+ If getopt doesn't recognize an option character, it will print the error message using stderr. If we initialized zero to opterr, we can prevent the error message.
			*/
	 /* 
        	+ To parse the command line arguments. 
		+ Here argv[] values must be start with '-', It is only indicate the  options.
        	+ The getopt() function check all options with arguments, then any option have arguments 
	 	we must mention ':' after option.
                Example:
                        ./traceroute -f 5 192.168.2.55  56

                        -Here -f have arguments, So we mention like 'f:',which means '-f' option have argument.
        	+ getopt function return the ASCII value of option.
        */
	while ((op = getopt(argc, argv, "dFAInrvxf:g:i:m:p:q:s:t:w:z:")) != EOF)
		switch (op) {

		case 'd':	/* 
				What is the use of SO_DEBUG flag?
					+ To enable socket level debugging ,but only allow if the effective 
					user id is 0 (root).

				Socket level debugging ?
				   -d   ⇒ option is used to enable the socket level debugging for trace route.

				 	⇒ The followings will happen while we gave this option.
						+ The kernel will track the details of all the send and 
						received packets in ring buffer .
						+ We can see that details by using the command 'dmesg'.
				SO_DEBUG  1
				*/
			options |= SO_DEBUG;
			break;
		case 'A':
			aspath=1;
			break;
		case 'f':
				/*
				* To set the first ttl to start ( Default 1 ) .
				* The str2val function used to change the character to decimal or 
				hex value, with check the minimum and max. 
				* Which means user given value must have between (1<=value) and (255>=value)
	                        */
			first_ttl = str2val(optarg, "first ttl", 1, 255);
			break;

		case 'F':	
				/*
				* To say to Do not fragment the probe packets on intermediate router.
				IP_DF   16384
					⇒ Because the ip_off is a u_short data type variable, 
					here first three bits reserved for flag.

					  flag		offset
					0  0  0       0 0000 0000 0000
					R  D  M		
					0  1  0	      0 0000 0000 0000
					   |
					   16384
				*/
			off = IP_DF;
			break;

		case 'g':
				// To send the probe packets through the specified gateway.
			if (lsrr >= NGATEWAYS) {
				Fprintf(stderr,
				    "%s: No more than %d gateways\n",
				    prog, NGATEWAYS);
				exit(1);
			}
			getaddr(gwlist + lsrr, optarg);
			++lsrr;
			break;

		case 'i':	/*
				* To specify the interface through which traceroute should sends packets .
				* (default the interface will be chosen by the routing table ) 
				*/
			device = optarg;
			break;

		case 'I':
				// To say to use the ICMP packets for probe instead of  UDP.
			++useicmp;
			break;

		case 'm':
				// To set the maximum hop (default 30).
			max_ttl = str2val(optarg, "max ttl", 1, 255);
			break;

		case 'n':
				// To avoid the host name in the traceroute output .
			++nflag;
			break;

	

		case 'p':
				/*
				* To specify the port number to the  destination 
				  which the trace route will use.
	                        * port number will be incremented for each probe.
				*/

			port = (u_short)str2val(optarg, "port",1, (1 << 16) - 1);
			break;

		// -------- 04/04/2017 ---

		case 'q':
				//  To specify the Maximum query (default 3).
			nprobes = str2val(optarg, "nprobes", 1, -1);
			break;

		case 'r':
				//  Don't  send  via  a  gateway,  only  send  to directly connected hosts.
			options |= SO_DONTROUTE;
			break;

		case 's':
				/*
				 * set the ip source address of the outbound
				 * probe (e.g., on a multi-homed host).
				 */
			source = optarg;
			break;

		case 't':
				/*
				* To set the type of service . 
				* Useful values are 16 (low  delay) and  8 (high throughput)
				*/
				/* 
				* If we give -t option from the command line, that time only this statement 
				  will execute.
				* IP_TOS:
				  ```````
					+ It set the precedence.
					+ like,
					        delay		->low priority of packet
					        throughput      ->Straight
					        reliability     ->actual data
					        monetary        ->cast of data (satellite and tower)

					+ reserved precedence like,
					000     ⇒  Routine              ->Reserved for default routing protocol
					001     ⇒  Priority             ->Medium priority data
					010     ⇒  Immediate            ->hight priority data
					011     ⇒  Flash                ->call control and signaling
					100     ⇒  Flash override       ->recommended for video
					101     ⇒  CRITIC/ECP           ->recommended for audio (voice)
					110     ⇒  internetworking      ->routing
					111     ⇒  Network              ->traffic control
				*/
			tos = str2val(optarg, "tos", 0, 255);
			++settos;
			break;

		case 'v':
				/*
	                        *Received ICMP packets other than time exceeded message and unreachable message.
	                        */
			++verbose;
			break;

		case 'x':
				//To enable the kernel to do the check sum calculation.
			doipcksum = (doipcksum == 0);
			break;

		case 'w':
				 // To change the waiting time for response .Default 5 sec).
			waittime = str2val(optarg, "wait time",2, 24 * 60 * 60);
			break;

		case 'z':
				/*
				* To set the time to pause between probes (default 0 ).
	                        */
			pausemsecs = str2val(optarg, "pause msecs",0, 60 * 60 * 1000);
			break;

		default:
			usage();
		}


	if (first_ttl > max_ttl) // To check the user given ttl value is exceed or not over the maximum ttl.
	{
		Fprintf(stderr,
		    "%s: first ttl (%d) may not be greater than max ttl (%d)\n",prog, first_ttl, max_ttl);
		exit(1);
	}
	if (!doipcksum)		// To check whether the checksum calculation is enabled or not.
	{
		Fprintf(stderr, "%s: Warning: ip checksums disabled\n", prog);
	}

	if (lsrr > 0)		
	{
			/*
                        * "optlen is IP filed"
			* Why we need to add 1 to the lsrr variable ?
				+ May be it will take the destination into the routing path.              
                        	+ Because, here 1 is instead of 4 bytes. 
	                        + Here get the option legth including destination IP also. 
				+ That is the reason for they were add 1.
			*/
		optlen = (lsrr + 1) * sizeof(gwlist[0]);
	}

		/*
		* Why we need minpacket size?
		* To ensure that the user given packet size from the command line is should not be less than 
		  the minimum size of packet .
		* If we give 20 It will throw an error.

	                outip size is 20
	                outdata sizeis 24 
	                optlen          0  => if we not give any option .
		* When the 'optlen' variable have value?
                        ./traceroute -g 192.168.2.10 www.google.com 
                                        
                        outip size is  20
                        outdata size is 24 
                        optlen          8  
		*/
	minpacket = sizeof(*outip) + sizeof(*outdata) + optlen;
	if (useicmp)
	{
		/*
		* What is the use of this statement  minpacket+=8?
			+ Usually magic number has some meaning in computer programming .
	                + But ,in this case the 8 is just a constant value and this is the size of ICMP 
			packet Message header for the 'ECHO REQUEST' query.
                        + ICMP Message header 
                                        1 byte      1 byte              2 byte
                                ------------------------------------------------------------
                                |     Type      |       code     |      Checksum           |  4 byte
                                ------------------------------------------------------------
                                |   Identifier                   | Sequence number         |  4 byte
	                        ------------------------------------------------------------
		*/
		minpacket += 8;			/* XXX magic number */
	}
	else
		minpacket += sizeof(*outudp);
	packlen = minpacket;			/* minimum sized packet */

	/* Process destination and optional packet size */
	switch (argc - optind) 
	{

	case 2:			// If we give the packet length from command line.
		packlen = str2val(argv[optind + 1],"packet length", minpacket, maxpacket);
		/* Fall through */
	case 1:			//If we give the only host name. But, always execute when 'case 2' is true.

		hostname = argv[optind];
		/*
                        To get the host name information
                        struct hostinfo 
                        {
                                char *name;             // host name
                                int n;                  //IP address count
                                u_int32_t *addrs;       //list of IP
                        };
                */
		hi = gethostinfo(hostname);
			/* 
			* To get the destination address. 
			* It will fill the following  structure information.
				struct sockaddr_in 
				{
					sa_family_t    sin_family; 
					struct in_addr sin_addr;
				};
			*/
		setsin(to, hi->addrs[0]);	

		if (hi->n > 1)
			Fprintf(stderr,"%s: Warning: %s has multiple addresses; using %s\n",
			prog, hostname, inet_ntoa(to->sin_addr));

		hostname = hi->name;
		hi->name = NULL;
		freehostinfo(hi);
		break;

	default:
		usage();
	}

	/*
	* what is the use of the setlinebuf() and setvbuf()?
		HAVE_SETLINEBUF         =>If we have setlinebuf function.
		setlinebuf()
			⇒ setlinebuf is equaled to, setvbuf(stream, (char *) NULL, _IOLBF, 0);
		setvbuf()
			⇒ To change the buffer stream.
			_IONBF unbuffered
			_IOLBF line buffered
			_IOFBF fully buffered
	*/

#ifdef HAVE_SETLINEBUF
	setlinebuf (stdout);
#else
	setvbuf(stdout, NULL, _IOLBF, 0);
#endif
//-------------05-04-2017

			/*
			        0                               packlen (minimum 52)
			        +-------------------------------+
			        |                               |
			        +-------------------------------+
			        outip
		        * Minimum length for  each members in outip(20 bytes) .
			*/
	outip = (struct ip *)malloc((unsigned)packlen);

	if (outip == NULL) 
	{
		Fprintf(stderr, "%s: malloc: %s\n", prog, strerror(errno));
		exit(1);
	}
		/* 
		* why we need to full zero to outip ?
	                + Because,the malloc function doesn't fill with null, only allocate the memory.
			+ So  garbage value may be affect our checksum calculation.
	        */
	memset((char *)outip, 0, packlen);
	outip->ip_v = IPVERSION; 	//Ip version will be 4 (IPv4 ) in default .
	if (settos)
	{
			/*      0    4    8     16                      32
				+----+----+-----+-----------------------+
				| 4  |    | tos |                       |
				+----+----+-----+-----------------------+
			* type of service is 1 byte
			*/
		outip->ip_tos = tos;
	}
#ifdef BYTESWAP_IP_HDR
	outip->ip_len = htons(packlen);
	outip->ip_off = htons(off);
#else
	outip->ip_len = packlen;
	outip->ip_off = off;
#endif
		/* 
		*outp is a u_char pointer .
			 1                       21                              packlen(minimum 52)
		         +-----------------------+-------------------------------+
		         | Ip header (20 byte)   |                               |       
		         +-----------------------+-------------------------------+
		         outip                   outp
		 */
	outp = (u_char *)(outip + 1);

		/*
		* Usage of HAVE_RAW_OPTIONS and where is defined?
			⇒  It is defiende in the 'configure' file.
		*/
#ifdef HAVE_RAW_OPTIONS
	if (lsrr > 0) 
	{
		register u_char *optlist;

			/*                                         (minimum 8)
				1                       21              29              packlen(minimum 52)
				+-----------------------+---------------+---------------+
				| Ip header (20 byte)   |option(8byte)  |               |       
				+-----------------------+---------------+---------------+
				outip                   optlist         outp
			*/
		optlist = outp;
		outp += optlen;

		/* final hop */
		gwlist[lsrr] = to->sin_addr.s_addr; 	// To store destination IP to gwlist.

		outip->ip_dst.s_addr = gwlist[0];	/*
							* To store the user given gatway address to 
	  						  IP destination IP fields.
							*/
		/* force 4 byte alignment */
		optlist[0] = IPOPT_NOP;
		/* loose source route option */
		optlist[1] = IPOPT_LSRR;
		i = lsrr * sizeof(gwlist[0]);
		optlist[2] = i + 3;
		/* Pointer to LSRR addresses */
		optlist[3] = IPOPT_MINOFF;
		memcpy(optlist + 4, gwlist + 1, i);
	} else
#endif
		outip->ip_dst = to->sin_addr;

			/*
			* Why he right shift the result ?
				+ The header length field only have 4 bits in IP header, 
				  so we can store maximum 15, that's why they did right shift 
				  (outp - (u_char *)outip) >> 2).
				+ The receiver should be do the ((outp - (u_char *)outip) << 2)left shif.

			* outp is a char  pointer and the outip is structure pointer of ip header that's why, 
			  we type casting that before subtract .
			*/
	outip->ip_hl = (outp - (u_char *)outip) >> 2;  
			/*
			* UDP Tracing
				+ Using the identification (ident) as a source port for the UDP packet.
			* ICMP Tracing
				+ Using the identification (ident) as a ICMP ID for the Icmp packet.
			* Why we need to do getpid() & 0xffff ?
				+ Because each process must have unique process id in our system.
				+ That is the reason for they are add the process id.
			* why we need to do | 0x8000 ?
				+ Because, they were enable the 15th bit in the identification. 
				+ which means the port number start from 32768.
				+ We know the maximum identification range is 65535.
			0xffff => 65535
			0x8000 => 32768
			*/
	ident = (getpid() & 0xffff) | 0x8000;
	if (useicmp) 		//If we enable ICMP packet.
	{
				/*
				* Why need protocol in ip header?
					+ The network layer should know to which service in the transport 
					layer to delivery the packet.
				*/
		outip->ip_p = IPPROTO_ICMP;

				/*
				1                       21             Packlen(minimum 52)
				+-----------------------+-------------------------------+
				| Ip header (20 byte)   |                               |       
				+-----------------------+-------------------------------+
				outip                   outicmp
				*/
		outicmp = (struct icmp *)outp;
		outicmp->icmp_type = ICMP_ECHO;
		outicmp->icmp_id = htons(ident);

				/*
				1               21              29                      52
				+-------------------------------------------------------+
				|IP header(20)  | ICMP header(8)|       payload         |       
				+-------------------------------------------------------+
				outip           outicmp         outdata
				*/
		outdata = (struct outdata *)(outp + 8);	/* XXX magic number */
	} else {
		outip->ip_p = IPPROTO_UDP;

				/*
				1                       21             packlen(minimum 52)
				+-----------------------+-------------------------------+
				| Ip header (20 byte)   |                               |       
				+-----------------------+-------------------------------+
				outip                   outudp
				*/
		outudp = (struct udphdr *)outp;

				/*
				* What is the use of having source port ?
					+ To verify the each packets, its mine or not. 
					+ Here source port doesn't change every packets.
				1               16              32
				+---------------+----------------+
				|source port    |       -        |
				|--------------------------------|
				|    length     |       -        |
				+---------------+----------------+
				*/
		outudp->uh_sport = htons(ident);

				//To store the UDP header length including payload.
		outudp->uh_ulen = htons((u_short)(packlen - (sizeof(*outip) + optlen)));

				/*
				1               21              29                      52
				+-------------------------------------------------------+
				|IP header(20)  | UDP  header(8)|       payload         |       
				+-------------------------------------------------------+
				outip           outudp          outdata
				*/
		outdata = (struct outdata *)(outudp + 1);
	}
	cp = "icmp";

				/*
				* Get protocol information by calling getprotobyname().
					struct protoent
					{
						char  *p_name;       // official protocol name 
						char **p_aliases;    // alias list 
						int    p_proto;      // protocol number 
					}                    
				*/
//-----------07-04-2017
	if ((pe = getprotobyname(cp)) == NULL) 
	{
		Fprintf(stderr, "%s: unknown protocol %s\n", prog, cp);
		exit(1);
	}
				/*
				* Here open the three standard files such as stdin,stdout and stderr.
				* When it will affect ?
					+ If we run the traceroute command with daemon process. 
				*/
				/* ensure the socket fds won't be 0, 1 or 2 */
	if (open(devnull, O_RDONLY) < 0 ||open(devnull, O_RDONLY) < 0 ||open(devnull, O_RDONLY) < 0) 
	{
		Fprintf(stderr, "%s: open \"%s\": %s\n",
		    prog, devnull, strerror(errno));
		exit(1);
	}
				/*
				Why we need raw-socket?
					⇒ If we want to receive all icmp reply packets in the network.
					  we can use the raw-socket.
					⇒ Because we were build the own protocol header. 
					⇒ which means we can established the security mechanisms.
				*/
	if ((s = socket(AF_INET, SOCK_RAW, pe->p_proto)) < 0) 
	{
		Fprintf(stderr, "%s: icmp socket: %s\n", prog, strerror(errno));
		exit(1);
	}
			/*
			* If the -d option has given ,We need to set that option to the socket .
			* setsockopt()
				SO_DEBUG flag is used to enable the socket level debugging .
			 
			* int setsockopt(int sockfd, int level, int optname,const void *optval, socklen_t optlen);
			* This function is used to set the options for given socket. 
			* sockfd          -> which socket you need to set the options. 
			* SOL_SOCKET      -> Level should be this flags.
        		* SO_DEBUG        -> For Socket level debugging.
			* SO_DONTROUTE    -> Used to don't send the packet apart from our network.
                          		     Even destination is ISP also.
			*/
	if (options & SO_DEBUG)
	{
				/*
				* what is the use SOL_SOCKET ?
					+ To manipulate options at the sockets API level, 
					level is specified as  SOL_SOCKET.
				* what will happen if the value of variable 'on' is 0 ?
					1	-> Enable
					0	-> disable
				*/
		(void)setsockopt(s, SOL_SOCKET, SO_DEBUG, (char *)&on,sizeof(on));
	}
	if (options & SO_DONTROUTE)
	{
				/*
				* If the -r option has given ,We need to set that option to the socket .
				* setsockopt()
					⇒ SO_DONTROUTE flag is used to enable the Don't route  .
				* What is the use of SO_DONTROUTE flag ?
					⇒ Which means we want trace the hops within our network.
				*/
		(void)setsockopt(s, SOL_SOCKET, SO_DONTROUTE, (char *)&on,sizeof(on));
	}


/*
* __hpux stands for havelet packard unix operating system .
* Check the system version HP-UX or not.
* What is HP-UX (Hewlett Packard Unix)?
	+ It is a Unix operating system and based on UNIX system V.
* HP-UX   -       BigEndian
* So we want to check our current version.
*/
#ifndef __hpux

		/*
		* IPPROTO_RAW	255 
		* What is the use of IPPROTO_RAW ?
			⇒ We told kernel to send the packet using  any IP protocol that is specified in 
			the passed header.
			⇒ Because we used the UDP and ICMP protocols.
		*/
	sndsock = socket(AF_INET, SOCK_RAW, IPPROTO_RAW);
#else
	sndsock = socket(AF_INET, SOCK_RAW,useicmp ? IPPROTO_ICMP : IPPROTO_UDP);
#endif
	if (sndsock < 0) 
	{
		Fprintf(stderr, "%s: raw socket: %s\n", prog, strerror(errno));
		exit(1);
	}

/*
* What is the use of IP_OPTIONS ?
	+ Set the IP  options for every packet from this socket. 
	+ This argument are a pointer to a memory buffer, It containing the options and its length.
	+ The maximum size of options for IPV4 : 40 bytes. 
	+ In coming packets are not allowed to change options after connection is established.

* IP_OPTIONS      4        ip_opts; IP per-packet options.  (netinet/in.h)
*/
#if defined(IP_OPTIONS) && !defined(HAVE_RAW_OPTIONS)
	if (lsrr > 0) 
	{
		u_char optlist[MAX_IPOPTLEN];

		cp = "ip";
		if ((pe = getprotobyname(cp)) == NULL) 
		{
			Fprintf(stderr, "%s: unknown protocol %s\n", prog, cp);
			exit(1);
		}

		/* final hop */
		gwlist[lsrr] = to->sin_addr.s_addr;
		++lsrr;

		/*
		option:
		``````` 
		0                                                                               40
		+-------------------------------------------------------------------------------+
		|IPOPT_NOP  |IPOPT_LSRR |length  |IPOPT_MINOFF|user specify gatway IP   |dst_IP |
		+-------------------------------------------------------------------------------+
		optlist

		/usr/include/netinet/ip.h
		#define IPOPT_NOP      1         no operation 
		#define IPOPT_LSRR   131         loose source route 
			⇒ Which means send the packet to destination using given gateway list.
		#define IPOPT_MINOFF   4         min value of above
		*/
		/* force 4 byte alignment */
		optlist[0] = IPOPT_NOP;
		/* loose source route option */
		optlist[1] = IPOPT_LSRR;
		i = lsrr * sizeof(gwlist[0]);
			/*
			* why add 3?
				+ Because, here three additional information stored in the optlist[] array. 
				+ So option length is i+3 (gateway size + 3).
			*/
		optlist[2] = i + 3;
		/* Pointer to LSRR addresses */
		optlist[3] = IPOPT_MINOFF;
		memcpy(optlist + 4, gwlist, i);

		if ((setsockopt(sndsock, pe->p_proto, IP_OPTIONS,(char *)optlist, i + sizeof(gwlist[0]))) < 0) 
		{
			Fprintf(stderr, "%s: IP_OPTIONS: %s\n",
			    prog, strerror(errno));
			exit(1);
		}
	}
#endif

/*
* What is the use of SNDBUF ?
	+ The effect of setting SO_SNDBUF to the socket.
	+ For UDP this sets the limit on the size of the datagram, 
	i.e. anything larger will be discarded .
	+ Note:
		⇒  The default value is set by the /proc/sys/net/core/wmem_default file and 
		the maximum allowed value is set by the /proc/sys/net/core/wmem_max file.
		⇒ The  minimum (doubled) value for this option is 2048.
*/
#ifdef SO_SNDBUF
	if (setsockopt(sndsock, SOL_SOCKET, SO_SNDBUF, (char *)&packlen,sizeof(packlen)) < 0) 
	{
		Fprintf(stderr, "%s: SO_SNDBUF: %s\n", prog, strerror(errno));
		exit(1);
	}
#endif

/*
/usr/include/linux/in.h
#define        IP_HDRINCL      3        int; Header is included with data.  

IP_HDRINCL :
	+ We have to say the kernel, I filled the IP header information you don't care about them. 
	+ It is specified the IP header have non zero destination address to the kernel.
	+ Then the destination address of the socket is used to route the packet. 
*/

#ifdef IP_HDRINCL
	if (setsockopt(sndsock, IPPROTO_IP, IP_HDRINCL, (char *)&on,sizeof(on)) < 0) 
	{
		Fprintf(stderr, "%s: IP_HDRINCL: %s\n", prog, strerror(errno));
		exit(1);
	}
#else

/*
/usr/include/linux/in.h
#define        IP_TOS          1        int; IP type of service and precedence.
*/
#ifdef IP_TOS
	if (settos && setsockopt(sndsock, IPPROTO_IP, IP_TOS,(char *)&tos, sizeof(tos)) < 0) 
	{
		Fprintf(stderr, "%s: setsockopt tos %d: %s\n",prog, tos, strerror(errno));
		exit(1);
	}
#endif
#endif
			/*
			* Socket level debugging ?
				-d 
			* option is used to enable the socket level debugging for trace route .
			* The followings will happen while we gave this option .
				+ The kernel will track the details of all the send and received packets in 
				ring buffer .
				+ We can see that details by using the command 'dmesg'.
			* Note:
				+This only track the details of tcp packets .
			*/
	if (options & SO_DEBUG)
		(void)setsockopt(sndsock, SOL_SOCKET, SO_DEBUG, (char *)&on,sizeof(on));

			/*
			* SO_DONTROUTE
				+ The S_DONTROUTE option will be enable on the socket if the  user has 
				  given the -r option .

				+ This option specifies that outgoing packets are bypass the normal routing 
				  mechanisms of the underlying protocol.

				+ For example ,with ipv4 ,the packet is directed to the appropriate local 
				  interface as specifies by the network and subnet protions of the destination 
				  address.

				+ if the local interface cannot determined  the destination address the 
				  ENETUNREACH is returned.
			*/
	if (options & SO_DONTROUTE)
		(void)setsockopt(sndsock, SOL_SOCKET, SO_DONTROUTE, (char *)&on,sizeof(on));

	/* Get the interface address list */
			/*
			* 'al' is a (struct ifaddrlist) ifaddrlist structure variable.
					struct ifaddrlist 
					{
						u_int32_t addr;
						char *device;
					};
			* 'errbuf' is a (char errbuf[132]) character array variable. 
			*/
	n = ifaddrlist(&al, errbuf);
			// ifaddrlist function failed .
	if (n < 0) 
	{
		Fprintf(stderr, "%s: ifaddrlist: %s\n", prog, errbuf);
		exit(1);
	}
			//  There are no interfaces .
	if (n == 0) 
	{
		Fprintf(stderr,
		    "%s: Can't find any network interfaces\n", prog);
		exit(1);
	}

	/* Look for a specific device */
			//if user specified the device name by using -i option.
	if (device != NULL) 
	{
			//To compare the  user given device match or not with the fetched interface list.
		for (i = n; i > 0; --i, ++al)
			if (strcmp(device, al->device) == 0)
			{
				mtu=al->mtu;
				break;
			}
		if (i <= 0) 
		{
			Fprintf(stderr, "%s: Can't find interface %.32s\n",
			    prog, device);
			exit(1);
		}
	}

	/* Determine our source address */
	if (source == NULL) 
	{
		/*
		 * If a device was specified, use the interface address.
		 * Otherwise, try to determine our source address.
		 */
		if (device != NULL)
			setsin(from, al->addr);// To fill the interface address into the 'from' structure.

			/*
			* if the user didn't give the ip address of source and didn't specified the device 
			  then we have to find the device .
			* findsaddr() function is used to find the interface according to the destination ip 
			  address.
			*/
		else if ((err = findsaddr(to, from,&mtu)) != NULL) 
		{
			Fprintf(stderr, "%s: findsaddr: %s\n",prog, err);
			exit(1);
		}
	} 	
	else 
	{

		
		// To get the information about the given source host.
		hi = gethostinfo(source);
                source = hi->name; 
                hi->name = NULL;
                /*
                 * If the device was specified make sure it  corresponds to the source address specified.
            	  Otherwise, use the first address (and warn if there are more than one).
	      	*/
        	 if (device != NULL) // user specified interface name by using '-i' option.
                {
			
			// To get the ip address which is belongs to given network interface.( because a host can have multiple ip address)
                        for (i = hi->n, ap = hi->addrs; i > 0; --i, ++ap) 
                                if (*ap == al->addr)
                               		break;

                        if (i <= 0) // if the source ip which is given by the user doesn't match with any interface.
                        {
                                Fprintf(stderr,
                                    "%s: %s is not on interface %.32s\n",
                                    prog, source, device);
                                exit(1);
                        }
                        setsin(from, *ap); // To fill from structure with source ip.
                }
		else
                {
			// To get the interface which has the user given ip address.
                        for (i = n; i > 0; --i, ++al)
                        {
                                if (hi->addrs[0]==al->addr)
                                {
                                        mtu=al->mtu; // To get the mtu of choosed network interface.
                                        break;
                                }
                        }
                        if (i <= 0)
                        {
                                Fprintf(stderr, "%s: Source Can't match with Any interface  %.32s\n",prog,source);
                                exit(1);
                        }
			
			// Get the first ip address ,if the host has many ip addresses .
                        setsin(from, hi->addrs[0]);
                        if (hi->n > 1)
                                Fprintf(stderr,
                        "%s: Warning: %s has multiple addresses; using %s\n",
                                    prog, source, inet_ntoa(from->sin_addr));

                }
	}

	/* Revert to non-privileged user after opening sockets */
	setgid(getgid());
	setuid(getuid());

	outip->ip_src = from->sin_addr;
#ifndef IP_HDRINCL
	if (bind(sndsock, (struct sockaddr *)from, sizeof(*from)) < 0) {
		Fprintf(stderr, "%s: bind: %s\n",
		    prog, strerror(errno));
		exit (1);
	}
#endif
			//First line of the output 
	Fprintf(stderr, "%s to %s (%s)",prog, hostname, inet_ntoa(to->sin_addr));
			//If i give source address ,this will be print in a trace route output .
	if (source)
		Fprintf(stderr, " from %s", source);
			//First line of the output 
	Fprintf(stderr, ", %d hops max, %d byte packets\n", max_ttl, packlen);
	(void)fflush(stderr);

			/* 
			* The below looping is the main process of the traceroute program .
			* The looping process will end either the packets reached the destination host or 
			  reached maximum ttl.          
			*/
	for (ttl = first_ttl; ttl <= max_ttl; ++ttl) 
	{
		u_int32_t lastaddr = 0;	// This 32 bit variable contains the ip address of the lastly hop.
		int gotlastaddr = 0;	// Indicates we got the same hop which we got in previous.
		int got_there = 0;	// Indicates the specified destination reached.
		int unreachable = 0;	// To have the Unreachable status.
		int sentfirst = 0;	/* To ensure the first packet should not be affect because of the time 
					   delay between probe packets sending.
					*/
		int noframe=1;  // It's contains the no fragment done for a probe packet.
		static int ck=0;      // To get the total length of fragmented packet from the icmp response.


		Printf("%2d ", ttl);	// To print the ttl value.

					/*      
					* The below for loop is used to send the probe packets until reached 
					  the maximum no of probes.
					*/
		for (probe = 0; probe < nprobes; ++probe) 
		{
			register int cc;	/* 
						*  To keep that the no of bytes returned from the function 
						*  wait_for_reply().
						*/
			struct timeval t1, t2;	// To keep the time.
			struct timezone tz;	// Its contains the time zone
			register struct ip *ip;	// Ip header structure.
			int nf=1;		// To keep track icmp reponse of the fragments.
					//To ensure the first packet should not be wait.
			if (sentfirst && pausemsecs > 0)
			{
				/*
				* The  usleep() function suspends execution of the calling thread for 
				  (at least) usec microseconds.      
					1 millisecond == 1000 micro seconds
				* Why we need to pasuemsec() ?
					+ If we send a probe packets simultaneously may the destination router 
					  will discard the packets silently due to icmp rate limit.
					+ The default value is one ICMP destination unreachable message 
					  per 500 milliseconds( 1/2 second).
                                */
				usleep(pausemsecs * 1000);
			}
			(void)gettimeofday(&t1, &tz);	//It will give us current second and micro second.

			//To send the probe packets to the target.
			send_probe(++seq, ttl, &t1,mtu);

			if(frag_done)
			{
				noframe=tnoframe;	// To get the no fragment done for a probe packet.
			}
			++sentfirst;

							
			while ((cc = wait_for_reply(s, from, &t1)) != 0) //To wait for the response.
			{
				(void)gettimeofday(&t2, &tz);
							// To check the received packets belongs to us.
					/*
					* packet_ok ():
					* "packet"-> It is a character array initialised with 512.
					* Why it is 512 ? 
						+ It is the maximum size of ICMP reply message.
					* "cc"    -> Number of bytes received from wait_for_reply function.
					* "from"  -> struct sockaddr_in variable. (source)
					* "seq"   -> sequence number. First it is 1 then every time it incremented.
					* "ck"    -> To get the total length of a packet from icmp response message(payload).
					*/

				i = packet_ok(packet, cc, from, seq,&ck);
					
				/* Skip short packet */
				if(i == 0)
				{
					/*
					* If received packet but doesn't match anywhere or size short, so that 
					  time skip that packet and then goto wait for replay until wait 
					  for second (default 5).
					*/
					continue;
				}
							/* 
							* To check whether the same hop response for all the 
							  probe packets or not.
							* If same address repeated ,we don't need to print the 
							  address.
							*/
				if(!gotlastaddr || from->sin_addr.s_addr != lastaddr) 
				{
					print(packet, cc, from);	 //To print the address of that hop.
					lastaddr = from->sin_addr.s_addr;//To change the last hop address.
					++gotlastaddr;	// indicates that lastaddr contains the ip address.	
					 if(frag_done) // indicates our program did fragment .
                                        {
                                               if(ck==packlen)  // To check whether the packets are reassmeble .
                                                {
                                                        printf("(R:%d)",ck);
                                                        tnoframe=1;     // To ensure the packets are reassembled, so we don't need to wait for the respose.
                                                }
                                                else
                                                {
                                                        printf("(!NR)"); // indicates that the  packets are not reassembled 
                                                }
                                        }

				}
				/*
				else
				{
					printf("\t\t");
				}
				*/
				 // if the packets are reassembled we don't need to wait for the reply .         
                                if(frag_done && ck==packlen)
                                {
                                        tnoframe=1;
                                }

                                if(frag_done)
                                {
                                        Printf("\tF%d[ %.3f ms]",nf,deltaT(&t1, &t2)); // To print the round trip time 
                                        --tnoframe;     // indicates the  remaining fragments that we have to wait for reply.
                                        if(tnoframe!=0) // To check whether, we got the response for all the fragments . 
                                        {
                                                nf++;
                                                if(i==-1)       // continue, if the icmp message code  is ttl time exceed .     
                                                        continue;
                                        }
                                }
                                else
                                {

                                        Printf(" %.3f ms  ", deltaT(&t1, &t2));
                                }

				if (i == -2) 		// For icmp response
				{
#ifndef ARCHAIC
					ip = (struct ip *)packet;
					if (ip->ip_ttl <= 1)
						Printf(" !");
#endif
					++got_there;
					break;
				}
				/* time exceeded in transit */
				if (i == -1)
					break;

				code = i - 1;
				switch (code) 
				{

				case ICMP_UNREACH_PORT:		//port unreachable.
#ifndef ARCHAIC
					ip = (struct ip *)packet;
					if (ip->ip_ttl <= 1)
						Printf(" !");
#endif
					++got_there;
					break;

				case ICMP_UNREACH_NET:		// Network unreachable
					++unreachable;
					Printf(" !N");
					break;

				case ICMP_UNREACH_HOST:		// Host Unreachable
					++unreachable;
					Printf(" !H");
					break;

				case ICMP_UNREACH_PROTOCOL:	// Protocol unreachable
					++got_there;
					Printf(" !P");
					break;

				case ICMP_UNREACH_NEEDFRAG:	// Packet is too large need fragmentation.
					++unreachable;
					Printf(" !F-%d", pmtu);
					break;

				case ICMP_UNREACH_SRCFAIL:	// Source route failed
					++unreachable;
					Printf(" !S");
					break;

				case ICMP_UNREACH_FILTER_PROHIB:// admin prohibited
					++unreachable;
					Printf(" !X");
					break;

				case ICMP_UNREACH_HOST_PRECEDENCE: // precedence violation
					++unreachable;
					Printf(" !V");
					break;

				case ICMP_UNREACH_PRECEDENCE_CUTOFF:// precedence cutoff
					++unreachable;
					Printf(" !C");
					break;

				default:
					++unreachable;		
					Printf(" !<%d>", code);
					break;
				}
				if(frag_done)  // To ensure the fragmentation happened or not.
                                {
                                        if(tnoframe!=0) // To check whether we got the response for all the fragmentation or not.
                                        {
                                                continue; // if tnoframe not equal to zero which means we  have to wait for the ICMP response for the remaining fragments. 
                                        }
                                }
				break;
			}
		
			// if The destination didn't response within the Specified 'wait secs'.
			  if (cc == 0)
                        {
                                if(frag_done && nf!=1) // Indicates there is no response arrived for the fragments.
                                        Printf(" ~");
                                else
                                        Printf(" *");

                        }

			if(frag_done)
				printf("\n   ");
			
			(void)fflush(stdout);
		}
		putchar('\n');

			/*
			* got_there
				+ To check whether we reached the destination or not .
				+ The process will end if we reached the destination .
			* unreachable
				+ To check that the destination is unreachable .
				+ The process will be end if we got the unreachable response for all the probe packets.
				
				Note:
					if the fragmentation happened then we should terminate the trace route once we got the  unreachable
				ICMP for all the fragments of all the probes.
					
				
			*/
		if (got_there ||(unreachable > 0 && unreachable >= (nprobes*noframe) - 1))
			break;
	}
	exit(0);
}
// kannan
/*
* wait_for_reply
* This function is used to wait for the response from the destination.
* sock => The socket descriptor Which is from, we get the ICMP response packet .
* fromp => It's contains the port number and IP Address of the Source.
* tp => It's contains the current seconds and micro seconds .
*/

int wait_for_reply(register int sock, register struct sockaddr_in *fromp,register const struct timeval *tp)
{
	fd_set fds;			// Set of file descriptors 
	struct timeval now, wait;	
	struct timezone tz;		// time zone
	register int cc = 0;		// Return value of recvfrom function
	int fromlen = sizeof(*fromp);

	FD_ZERO(&fds); // To clear all the bits in fdset.
	FD_SET(sock, &fds); // To add the socked fd into the fdset.

	wait.tv_sec = tp->tv_sec + waittime;	// Adding the current second with the seconds we have to wait.(By default 'waittime' is 5)
	wait.tv_usec = tp->tv_usec;

	(void)gettimeofday(&now, &tz); // To get the current seconds and microseconds.
	// To get the exact time to wait.
	tvsub(&wait, &now);
	/* Wait for the ICMP response of send probe for Given seconds. */
	if (select(sock + 1, &fds, NULL, NULL, &wait) > 0)
		cc = recvfrom(sock, (char *)packet, sizeof(packet), 0,(struct sockaddr *)fromp, &fromlen);
	return(cc);
}
// kannan
/*
* fragement_func():-
	+ This function do the followings.
		+ Do the ip checksum calculation by using in_cksum() function .
		+ send out the packets with help of sendto function .
*/
int fragement_func(int len,struct ip *fip)
{
        register int cc;        	// To keep track of How many bytes the sendto() function returned.

        fip->ip_len = htons(len); 	// Fill the Total length of IP packet.

        if (doipcksum) 			// To check whether the checksum calculation enabled.
        {
		/* Fill the check sum value into the header checksum field.*/
                fip->ip_sum =in_cksum((u_short *)fip, sizeof(*fip) + optlen);
	
	                /*
	                * if the result of checksum calculation is 0 then the value should be 65535 .
	                * Because ,if We kept the checksum value as zero then it's means there is no 
			  checksum calculation happened .
	                */
                if (fip->ip_sum == 0)
                        fip->ip_sum = 0xffff; // 0xffff decimal value is 65535. 
        }

        if (verbose > 1)
        {
                register const u_short *sp;
                register int nshorts, i;

                sp = (u_short *)fip;
                nshorts = (u_int)len / sizeof(u_short);
                i = 0;
                Printf("[ %d bytes", len);
                while (--nshorts >= 0) {
                        if ((i++ % 8) == 0)
                                Printf("\n\t");
                        Printf(" %04x", ntohs(*sp++));
                }
                if (len & 1) {
                        if ((i % 8) == 0)
                                Printf("\n\t");
                        Printf(" %02x", *(u_char *)sp);
                }
                Printf("]\n");
        }

#if !defined(IP_HDRINCL) && defined(IP_TTL)
        if (setsockopt(sndsock, IPPROTO_IP, IP_TTL,(char *)&ttl, sizeof(ttl)) < 0)
        {
                Fprintf(stderr, "%s: setsockopt ttl %d: %s\n",
                    prog, ttl, strerror(errno));
                exit(1);
        }
#endif
#ifdef __hpux
        cc = sendto(sndsock, useicmp ? (char *)ficmp : (char *)fudp,len, 0, &whereto, sizeof(whereto));
        if (cc > 0)
                cc += sizeof(*fip) + optlen;
#else

        cc = sendto(sndsock, (char *)fip,len, 0, &whereto, sizeof(whereto));
#endif
        if (cc < 0 || cc != len)
        {
                if (cc < 0)
                        Fprintf(stderr, "%s: sendto(MY test): %s\n",
                            prog, strerror(errno));
                Printf("%s: wrote(MY test) %s %d chars, ret=%d\n",
                    prog, hostname, len, cc);
                (void)fflush(stdout);
            }
}
// karuppaiah
/*
* send_probe()
	+ This function is used to send the probe packets.
*/

void send_probe(register int seq, int ttl, register struct timeval *tp,int mtu)
{
	

				/*
			                struct  udpiphdr 
			                {
			                        struct  ipovly ui_i;             overlaid ip structure 
			                        struct  udphdr ui_u;             udp header 
			                };
				* To split the IP header (ipovly) and udp (udphdr) information from packet.
                			struct ipovly 
			                {
			                        u_char  ih_x1[9];               (unused) 
			                        u_char  ih_pr;                  protocol 
			                        short   ih_len;                 IP Header length 
			                        struct  in_addr ih_src;         source internet address 
			                        struct  in_addr ih_dst;         destination internet address 
			                };
			                typedef uint32_t in_addr_t;
			                struct in_addr
			                {
			                    in_addr_t s_addr;
			                };
			                struct udphdr 
			                {
			                        u_short uh_sport;               source port 
			                        u_short uh_dport;               destination port 
			                        short   uh_ulen;                udp length 
			                        u_short uh_sum;                 udp checksum 
			                };
				*/
	register struct udpiphdr *ui, *oui;
	struct ip tip;

	outip->ip_ttl = ttl;	// set the ttl value 

/*
* Check the system version HP-UX or not.
* What is HP-UX (Hewlett Packard Unix)?
        It is a Unix operating system and based on UNIX system V.
* HP-UX   -       BigEndian
* So we want to check our current version.
*/

#ifndef __hpux
		/*
		* Each packet identification numbers will be have unique, that is the reason for add ident+seq.
		*/
	outip->ip_id = htons(ident + seq);
#endif

	/*
	 * In most cases, the kernel will recalculate the ip checksum.
	 * But we must do it anyway so that the udp checksum comes out
	 * right.
	 */

	/*To check the doipcksum, It indicate, that we want to calculate the checksum or not.*/

	/* Payload */
	outdata->seq = seq;
	outdata->ttl = ttl;
	outdata->tv = *tp;

	if (useicmp)
		outicmp->icmp_seq = htons(seq);
	else
		outudp->uh_dport = htons(port + seq);

		/*
		* This if condition will be true, when we enable the ICMP packet. 
		* Otherwise false which means udp packet.
		*/
	if (useicmp)				// Do the icmp checksum calculation if the probe packets are ICMP . 
	{
		/* Always calculate checksum for icmp packets */
		outicmp->icmp_cksum = 0;    	/*
						* To make sure the checksum field will not be involved in 
						  the check sum calculation.
						*/
		
		outicmp->icmp_cksum = in_cksum((u_short *)outicmp,packlen - (sizeof(*outip) + optlen));

		/* if the result of checksum is zero then the value should be 65535 */
		if (outicmp->icmp_cksum == 0)
			outicmp->icmp_cksum = 0xffff;

	} else if (doipcksum) 
	{
		/* Checksum (we must save and restore ip header) */
		tip = *outip;
		ui = (struct udpiphdr *)outip;
		oui = (struct udpiphdr *)&tip;
		/* Easier to zero and put back things that are ok */
		memset((char *)ui, 0, sizeof(ui->ui_i)); //To set the zero first 20 bytes IP header.
			/*
			* why we need pseudo header? what are the field?
				⇒ The main purpose of UDP checksum is to detect errors in transmitted segment.
				⇒ To calculate UDP checksum a "pseudo header" is added to the UDP header.
				⇒ This used on receiving system to make sure that IP datagram is being 
				  received by proper computer.
				⇒ Note:
					+ UDP checksum test is end to end test. Which means test performed only 
					  at the sender and receiver. 
					+ But IP header checksum test performed at every intermediat routers.
	                        The pseudo header field:
	                +---------------------------------------------------------------+
	                |                       Source IP address                       |
	                |---------------------------------------------------------------|
	                |                       Destination IP address                  |
	                |---------------------------------------------------------------|
	                |padding (0)    |protocol       |       UDP length              |
	                |---------------------------------------------------------------|
	                |       source port             |       destination port        |
	                |---------------------------------------------------------------|
	                |       UDP length              |       UDP checksum            |
	                |---------------------------------------------------------------|
	                |                       Payload (data)                          |
	                +---------------------------------------------------------------+

			* You asked one more questions like, Why you get specify field in IP?
				+ It is also have reason. You assume that, you had one post now, 
				  What you want?", 
					Who is send this post to me?
						Source IP and destination IP.
	  			        What think he or she post me?
						It is indicate protocol.
					Actually how many thing he or she post me?
						It is indicate length.
				+ That is why were get specify field in the IP header, otherwish your content.
			*/
		ui->ui_src = oui->ui_src; 	//ui_i.ih_src   source IP from IP header
		ui->ui_dst = oui->ui_dst;	//ui_i.ih_dst   destination IP from IP header
		ui->ui_pr = oui->ui_pr;		//ui_i.ih_pr    protocol from IP header
		ui->ui_len = outudp->uh_ulen;	//ui_i.ih_len   length from UDP header.
		outudp->uh_sum = 0;

			/*
			* To calculate the checksum value including (IP header+UDP header+payload).
			* default value is 52. (20+8+24)
			*/
		outudp->uh_sum = in_cksum((u_short *)ui, packlen);

			/*
			* If you assume that, the checksum calculation return zero, In this case receiver 
			  check the checksum value, that zero means checksum disabled, so at that  time we 
			  want set the default value 65535.

                        * Example:
				answer=~sum     =>sum value is 65535
				Here answer is zero.
			*/
		if (outudp->uh_sum == 0)
			outudp->uh_sum = 0xffff;
		*outip = tip;	//restore IP header values.
	}

	/*
         * In most cases, the kernel will recalculate the ip checksum.
         * But we must do it anyway so that the udp checksum comes out
         * right.
         */

        /*
	* noframe 	=> This variable holds the no of fragments will occur in the original packet.
	* i 		=> it's for looping (used in for loop).
	* remain	=> remain contains the remaining data to fragment from the original payload.
	* roffset 	=> offset of the each fragmented packet.
	* len     	=> total length of each fragmented packet.
	* off     	=> flags of each fragmented packet.
        */
        int noframe,i,remain=0,roffset=0,len,off=0;
        /*
	* hsize => Header size of each fragmented packet.
	* psize => payload size which is used to split the original packet accordingly.
        */
        int hsize=0,psize=0;
	        /*
	        * To check whether the packet size greater than the MTU(maximum transmission unit) and check 
		  whether the User gave the -F option (Don't fragment).
		* The 'if' Block will execute if the following condition is satisfied.
			+ Size of the packet should be greater than MTU whereas the -F option doesn't give.
	        */
      if(mtu < packlen && (ntohs(outip->ip_off) & IP_DF)!=IP_DF)  
      {
		frag_done=1;  // fragmentation done.

                struct ip *fip; // IP structure for fragment .
                u_char *fcur; // Pointer to the payload .

		/* Allocate the memory according to the original packet size.*/
                fip = (struct ip *)malloc((unsigned)packlen); 
                if (fip == NULL) {
                        Fprintf(stderr, "%s: malloc: %s\n", prog, strerror(errno));
                        exit(1);
                }
                memset(fip,0,packlen); 		// Fill the fragment packet with zero.
                memcpy(fip,outip,packlen); 	// take a copy of orginal packet.
                fcur=(u_char *)fip; 		/* store the base address of packet into the 
						   fcur( current pointer of packet) pointer.
						*/

                hsize=sizeof(*fip); 		// Get the header size.
                fcur+=hsize;  			// Skip the header to point the payload.


						 /*
						* For example
                                			packlen => 1501
			                                hsize => 20
			                                mtu   => 1500
                                                
			                                1501-20 => 1481 
			                                1500-20 => 1480
                                
	                                	* So,No frame is 1481/1480 => 1 => 1+1 => 2
                                
						* Why we need to Add 1 with quotient?
		                                        Because the remainder is not zero.
                				*/
                noframe=(packlen-hsize)/(mtu-hsize); 	// Find no of frame we need.
                if((packlen-hsize)%(mtu-hsize)!=0) 	// If the remainder is not zero add 1 with noframe.
                {
                        ++noframe;
                }

		tnoframe=noframe;  // To take the copy of "noframe" value for further use.
		
		/*processing for fragmentation to split the original packet into no of frame.*/
                for(i=0,remain=packlen-hsize;i<noframe;i++)
                {
                        off=0;  		/* 
						* To make sure each fragment  will not contain the flags 
						  of previous fragment.
						*/
                        if(i!=noframe-1) 	// Check whether the last fragment .
                        {
                                off|=IP_MF; 	// Add more fragment flag if the fragment is not last one.
                        }

                        off|=roffset; 		// Add the fragment offset of the fragment packet .
                        fip->ip_off=htons(off); // To change host byte to network byte order .

			/* Check whether the remaining payload is greater than (mtu-hsize).*/
                        if(remain>(mtu-hsize)) 	
                        {
                                remain-=(mtu-hsize); 	/*
							* To find the remaining payload to be send with next 
							  fragment.
							*/
                                len=mtu; 		// keep the length of the packet as a MTU.
                                roffset+=(mtu-hsize)/8; // Calculate the fragment offset.
                                psize=mtu-hsize; 	// Calculate the exact payload size of fragment.       
                        }
                        else 				// if the remaining payload less than the (mtu-hsize).
                        {
                                len=remain+hsize;	/*
							* Calculate the length ,Just adding the header size 
							  with remaining payload.
							*/
                                psize=remain; 		// take the remain value as size of payload.
                        }
                        /*
			* cut the payload from the original packet and merge with fragment packet.

			* Depicts of concepts:

			        + Original packet size => 1501 .
		
		             20                         1481
		        -------------------------------------------------------------
		        | IP header     |       payload                             |
		        -------------------------------------------------------------

		        MTU is 1500 ,so the no of frame should be 2 .   
                
		        frame-1
		                original_payload  = 1481
		                new_payload_size  = 1500-20
		                remaining_payload = original_payload - new_payload_size => 1481 - 1480 => 1

		             20                         1480
		        -------------------------------------------------------------
		        | IP header     |       payload                             |
		        -------------------------------------------------------------
        
		        frame-2
		                + Here the remaining payload(1) is less than the maximum payload we can send 
				  via network interface (1480) .
                	* So ,We can cut that 1 byte payload from original packet payload and can directly add
	  	          with fragment packet as like below .
                	* Of course this is the last one so we don't need to find remaining payload.
		             20                         1
		        ------------------------------------------
		        | IP header     |       payload          |
		        ------------------------------------------
                        */
                        memcpy(((u_char *)fip)+hsize,fcur,psize);
                        fragement_func(len,fip); 	/*
							* This function will do the ip check sum calculation 
							  and send out the packet.
							*/
                       /*                      
			* fcur => indicates the base address of next fragment payload in the original 
			  packet payload.
       
			* For example
			        + size of Original packet payload is 1481 .
		 	        + We need two fragment here one has the payload size 1480 and another one 
				  has the payload size 1.
        		* So ,for first fragment fcur pointing at the original payload like below.
		            20          fcur                                                            
		        -----------------------------------------
		        | IP header     |       payload  1481   |
		        -----------------------------------------
        
	        	* For second fragment fcur pointing at the original payload like below .
                                                
		            20                                              fcur         
		        -----------------------------------------------------------------
		        | IP header     |       1480                        |payload  1 |
		        -----------------------------------------------------------------
			*/

	                fcur+=psize;  	/* 
					* To move the fcur pointer to skip the send out payload of the 
					  original packet.
					*/

                }
        }
        else
        {

		/*
		* This function will do the ip check sum calculation and send 
		  out the packet.
		*/
		
                fragement_func(packlen,outip); 
        }
}

/*
* deltaT()
	+ This function will do the following things .
        + Calculate the round trip time by using the Time before send the probe packet and after 
	  response for that probe packet.
*/
// kannan
double deltaT(struct timeval *t1p, struct timeval *t2p)
{
	register double dt;
		/*
		* 1 second is equal to 1000 millisecond.
		* 1 millisecond equal to 1000 microsecond.

			t2p->tv_sec = 1490775063 
			t1p->tv_sec = 1490775060 
			t2p->tv_usec = 639941 
			t1p->tv_usec = 635086 
	
		* 1490775063-1490775060 => 3 => 3 * 1000.0 => 3000.0
	
		* 639941-635086 => 4855 => 4855/1000.0 =>4.8550 

		* So, the Result is 3000.0 + 4.8550 = 3004.855 .
		*/
	dt = (double)(t2p->tv_sec - t1p->tv_sec) * 1000.0 +
	     (double)(t2p->tv_usec - t1p->tv_usec) / 1000.0;
	return (dt);
}

/*
 * Convert an ICMP "type" field to a printable string.
 */
char * pr_type(register u_char t)
{
	static char *ttab[] = {
	"Echo Reply",	"ICMP 1",	"ICMP 2",	"Dest Unreachable",
	"Source Quench", "Redirect",	"ICMP 6",	"ICMP 7",
	"Echo",		"ICMP 9",	"ICMP 10",	"Time Exceeded",
	"Param Problem", "Timestamp",	"Timestamp Reply", "Info Request",
	"Info Reply"
	};

	if (t > 16)
		return("OUT-OF-RANGE");

	return(ttab[t]);
}
// Sathiya
int packet_ok(register u_char *buf, int cc, register struct sockaddr_in *from,register int seq,int *ck)
{
	register struct icmp *icp;	// Size of of this structure : 28 bytes.
	register u_char type, code;	// "type,code" ->Are unsigned char. It taking 1byte.
	register int hlen;

/*
* in our system don't have ARCHAIC so the first black is executed.
* ARCHAIC is used to strip the IP header from the packet. 
* If this flags is disabled than, We can tack care for the  IP header of the receiving packet.
*/
#ifndef ARCHAIC
	register struct ip *ip;

		/*
		* Here we have 512 bytes for "buf".
		* Then we typecast the buf into IP header size.
		* 512/20 =25 .we can access 25 indices only.
		*/
	ip = (struct ip *) buf;
		/*
		* We already discussed about this shift option.
		* The receiver should be do the left sift (ip->ip_hl << 2). 
		* Because the sender will be did right shift (ip->ip_hl >> 2). 
		* It is standard for world network. 
		* It's reason for the header length field only have 4bit in the IP header.
		*/
	hlen = ip->ip_hl << 2;

		/*
		* cc -number of byte received from socket fd.
		* ICMP_MINLEN -> 8 minimum length of ICMP header.
		* heln-> 20 minimum length of IP header.
		* So the received packet should have minimum 28 bytes. Otherwise it will print error message.
		*/
	if (cc < hlen + ICMP_MINLEN) 
	{
			/*
			* If -v option enabled, they print the information like packet is too short.
			* Other wise return 0.
			*/
		if (verbose)
			Printf("packet too short (%d bytes) from %s\n", cc,inet_ntoa(from->sin_addr));
		return (0);
	}

		/*
		* "cc" have header length also thats why we need to subtract the header length.
		* Now number of byte received from the socket fd. 
		*/
	cc -= hlen;

		// Here,They just ignore the IP header information.
	icp = (struct icmp *)(buf + hlen);
#else
	icp = (struct icmp *)buf;
#endif

		/*
		* type : ICMP's type.
			+ This field indicates the type of ICMP message.
		* Code : ICMP's code.
			+ This field indicates the reason for the error.  
        	* Both taking two 1byte.
                
		* ICMP Message     Type
	---------------------------------------------------
		        0       Echo reply 
		        1       Host is unreachable
		        2       Protocol is unreachable
		        3       Port is unreachable
		        4       Fragmentation is needed and Don't Fragment was set
		        5       Redirect (Source route failed)
		        6       Destination network is unknown
		        7       Destination host is unknown
		        8       Source host is isolated
		        9       Router advertisement
		        10      Router selection
		        11      Time exceeded
		        12      Parameter problem
		        13      Time stamp
		        14      Time stamp reply
		        15      Information request
		        16      Information reply
		        17      Address mask request
		        18      Address mask reply
		        30      Trace route
		*/
	type = icp->icmp_type;
	code = icp->icmp_code;
	/* Path MTU Discovery (RFC1191) */
	/* 
	* ICMP_UNREACH_NEEDFRAG =4.
	* Because of code: 4 is indicate Fragmentation is needed and Don't Fragment was set.
	* So, they check the code value as 4 or not.
	* If it is 4 then else block is execute. 
	* Other wise set pmtu  is zero.
	* Because of they indicate the error message.
	* MTU is not in range & enable the Don't fragment offset. 
		> if(3!=4) pmtu=0;
	*/
	if (code != ICMP_UNREACH_NEEDFRAG)
		pmtu = 0;
	else 
	{
			/*
			* If we have a icmp_nextmtu in structure then directly we calculate the pmtu value.
			* Else we have to refer the my_mtu structure.
			* Finally we get the pmtu value.
			* This value is used to print the message for error.
			*/
#ifdef HAVE_ICMP_NEXTMTU
		pmtu = ntohs(icp->icmp_nextmtu);
#else
		pmtu = ntohs(((struct my_pmtu *)&icp->icmp_void)->ipm_nextmtu);
#endif
	}

			/*
			* type == ICMP_TIMEXCEED  -> 4
			* code == ICMP_TIMXCEED_INTRANS -> 0
			* type == ICMP_UNREACH    -> 3
			* type == ICMP_ECHOREPLY  -> 0
			*/
	if ((type == ICMP_TIMXCEED && code == ICMP_TIMXCEED_INTRANS) ||
	type == ICMP_UNREACH || type == ICMP_ECHOREPLY) 
	{
		register struct ip *hip;
		register struct udphdr *up;
		register struct icmp *hicmp;
			/*
			* #define icmp_ip icmp_dun.id_ip.idi_ip.
				+ Here idi_ip is a IP structure.
			*/
		hip = &icp->icmp_ip;
		hlen = hip->ip_hl << 2;
		if (useicmp) 
		{
			/*
			* ICMP_ECHOREPLY ->0
			* The following condition is true then it will return -2. 
			* Then in main function their check whether the reason is TIME exceeded or 
			  anything else.
			*/
	
			 *ck=ntohs(hip->ip_len);
                        if (type == ICMP_ECHOREPLY && icp->icmp_id == htons(ident) && icp->icmp_seq == htons(seq))
                        {

                                return (-2);
                        }
                        hicmp = (struct icmp *)((u_char *)hip + hlen);
                        /* XXX 8 is a magic number */
                        if (hlen + 8 <= cc && hip->ip_p == IPPROTO_ICMP && hicmp->icmp_id == htons(ident) && hicmp->icmp_seq == htons(seq))
                        {
                                return (type == ICMP_TIMXCEED ? -1 : code + 1);
                        }
			 // if the total length of a packet isn't equal to the actual length of the packet and the packet is response of our send probe fragment. 	
                        if(*ck!=packlen && htons(ident+seq)==hip->ip_id)
                        {
				// Here, We need to send the appropriate code of icmp message type  instead of zero.
                                return (type == ICMP_TIMXCEED ? -1 : code + 1);
                        }

		} 
		else
		{
			up = (struct udphdr *)((u_char *)hip + hlen);
                        /* XXX 8 is a magic number */
                        *ck=ntohs(hip->ip_len);
//			printf("ident %d ip ident %d \n",ident+seq,ntohs(hip->ip_id));
                        if(hlen + 12 <= cc && hip->ip_p == IPPROTO_UDP && up->uh_sport == htons(ident) && up->uh_dport == htons(port + seq))
                        {

        //                      printf("Len %d offset %d dent=%d\n",*ck,ntohs(hip->ip_off),ntohs(hip->ip_id));  
                                return (type == ICMP_TIMXCEED ? -1 : code + 1);
                        }
                        if(*ck!=packlen && htons(ident+seq)==hip->ip_id)  // if the total length of a packet isn't equal to the actual length of the packet and the packet is response of our send probe fragment.
                        {
					
				// Here, We need to send the appropriate code of icmp message type  instead of zero.
                                return (type == ICMP_TIMXCEED ? -1 : code + 1);
                        }


		}
	}
#ifndef ARCHAIC
		/* 
		* If verbose is enabled It print the some what information.
		* Which is obtain from pr_type function. 
		*/
	if (verbose)
	{
		register int i;
		u_int32_t *lp = (u_int32_t *)&icp->icmp_ip;

		Printf("\n%d bytes from %s to ", cc, inet_ntoa(from->sin_addr));
		Printf("%s: icmp type %d (%s) code %d\n",
		    inet_ntoa(ip->ip_dst), type, pr_type(type), icp->icmp_code);
		for (i = 4; i < cc ; i += sizeof(*lp))
			Printf("%2d: x%8.8x\n", i, *lp++);
	}
#endif
	return(0);
}

// sathiya
/*
	print() function used to print the gateway address, host name of the gateway and the AS information of the gateway.
	
*/
void print(register u_char *buf, register int cc, register struct sockaddr_in *from)
{
	register struct ip *ip;// IP structure variable.
	register int hlen; // header length of the packet.
	char str[256]={}; // its contains the ip address which is passed to the  get_as() function.

	ip = (struct ip *) buf; 
	hlen = ip->ip_hl << 2; // To fetch the length of the packet.
	cc -= hlen; // To get the size of the payload.

	if (nflag) // To check whether the user have the -n option or not.
	{
		Printf(" %s", inet_ntoa(from->sin_addr)); // To print the address of intermediate router only.
	}
	else
	{		
		// To print the address of the gateway and the hostname of the gateway.
			
		Printf(" %s (%s)", inetname(from->sin_addr),inet_ntoa(from->sin_addr));
	}
	
	if(aspath) // to check whether the user gave the -A option or not.
	{
		
		sprintf(str,"%s",inet_ntoa(from->sin_addr));
		Printf("[%s]",get_as(str));     //To get the AS information of the specified IP.
	}
	if (verbose)
		Printf(" %d bytes to %s", cc, inet_ntoa (ip->ip_dst));
}
// Karuppaiah
/*
 * Checksum routine for Internet Protocol family headers (C Version)
 * cheksum algorithm:
	+ We want IP header information and total length IP header with option length.
	+ 16 bit one’s complement of the sum of all 16 bit words in the header. 
	+ So we want to sum the all header information until IP header length.
        + Note:   
		⇒  Per time add the 16 bit words. 
		⇒  But we have to sum 1 byte, when IP header length is odd number.
 */
u_short in_cksum(register u_short *addr, register int len)
{
	register int nleft = len;	//length of the packet.
	register u_short *w = addr; // Base address of the packet.
	register u_short answer; // It has the final result of checksum calculation.
	register int sum = 0; 

	/*
	 *  Our algorithm is simple, using a 32 bit accumulator (sum),
	 *  we add sequential 16 bit words to it, and at the end, fold
	 *  back all the carry bits from the top 16 bits into the lower
	 *  16 bits.
	 */
	while (nleft > 1)  {
		sum += *w++;
		nleft -= 2;
	}

	/* mop up an odd byte, if necessary */
	if (nleft == 1)
		sum += *(u_char *)w;

	/*
	 * add back carry outs from top 16 bits to low 16 bits
	 */
		//Add high 16 bit to low 16 bits, because the field of checksum have only 16 bits in IP header.
	sum = (sum >> 16) + (sum & 0xffff);	/* add hi 16 to low 16 */
	sum += (sum >> 16);			/* add carry */
	answer = ~sum;				/* truncate to 16 bits */
	return (answer);
}
// kannan
/*
  Subtract 2 timeval structs:  out = out - in.
  Out is assumed to be >= in.

	tvsub
		This function used to determine the exact seconds and microseconds to wait for the ICMP response for a probe packet.
			 
 */
void tvsub(register struct timeval *out, register struct timeval *in)
{
		/*
		* 1000000 microseconds == 1 second .
		* For example
			out.sec  => 1490585601 
			out.usec => 840360 
			in.sec   => 1490585596  
			in.usec =>  840892  

		 if(out.usec-=in.usec )  => 840360-=840892 => -532
		 {
			--out.sec;  => 1490585600
			out.usec += 1000000;  -532+=1000000 => 999468
		 }

		 out.sec -= in.sec ; => 1490585600 - 1490585596 => 4 
		* So ,Now .
			out.sec = 4
			out.usec = 999468
		*/
	
	
	if ((out->tv_usec -= in->tv_usec) < 0)   
	{
		--out->tv_sec;
		out->tv_usec += 1000000;
	}
	out->tv_sec -= in->tv_sec;
}
// kannan
/*
 * Construct an Internet address representation.
 * If the nflag has been supplied, give
 * numeric value, otherwise try for symbolic name.
 */
char *inetname(struct in_addr in)
{
	register char *cp;	//It's contains the domain name.
	register struct hostent *hp; //structure which is contains the information about the given host.
	static int first = 1; //To ensure to get the domain name of the user's host only once.
	/*
		domain => it's contains the domain name of the user.
		line   => it's contains the hostname and domain name together if the destination host is not belogns to the user's network.Otherwise it's contains only the host name.
	 
	*/
	static char domain[MAXHOSTNAMELEN + 1], line[MAXHOSTNAMELEN + 1];

	if (first && !nflag) // This flag will excute if the user didn't give the '-n' option and it's execute only once.
	{
		first = 0; // To ensure next time that don't need get the domain name of the user's host.
				
		if (gethostname(domain, sizeof(domain) - 1) < 0) // To  get the host name of the user's host.
			domain[0] = '\0'; // if the gethostname function failed.
		else // if the gethostname function is succeed.
		 {
			cp = strchr(domain, '.'); // To split the domain name itself from the host name.
//			printf("CP : %s \n",cp);
			if (cp == NULL) // if there is no domain name attached with host name.
			{
				hp = gethostbyname(domain); // To get the host info.
				if (hp != NULL)
					cp = strchr(hp->h_name, '.'); // To get the domain name. 
			}
			if (cp == NULL)
				domain[0] = '\0'; // indicates no domain name.
			else 
			{
				++cp; // To skip the '.'.
				(void)strncpy(domain, cp, sizeof(domain) - 1); //To get the domain name from the host name.
				domain[sizeof(domain) - 1] = '\0'; 
			}
		}
	}
	/*
		To ensure the user gave the -n option and the address in the in structure should not be INADDR_ANY(0.0.0.0).
		
	*/
	if (!nflag && in.s_addr != INADDR_ANY) 
	{

		// To get the host info by passing the IP address.
		hp = gethostbyaddr((char *)&in, sizeof(in), AF_INET);
		if (hp != NULL) // To check whether the gethostbyaddr() function failed or not.
		{
			/*
				The given ip domain and the user's domain  is same then we don't need to print the domain name along with host name.
				So, Here we only get the host name.
 				
			*/
			if ((cp = strchr(hp->h_name, '.')) != NULL && strcmp(cp + 1, domain) == 0)
				*cp = '\0';

			(void)strncpy(line, hp->h_name, sizeof(line) - 1);
			line[sizeof(line) - 1] = '\0'; 
			return (line);  // return the host name.
		}
	}
	return (inet_ntoa(in));
}
// kannan
/*
* gethostinfo()
	+  This function is used to get the host information by calling the function gethostbyname().
	+ Possible of parameter this function can accept 
		1.host name such as www.google.com .    
		2.IP address of the destination such as (192.168.2.10) .
	+ if the parameter is invalid it will throw an error .
*/
struct hostinfo *gethostinfo(register char *hostname)
{
	register int n;
	register struct hostent *hp;
	register struct hostinfo *hi;
	register char **p;
	register u_int32_t addr, *ap;
	/*       
                To ensure that the length of the host name should be less than 64. 
                When I entered below command ,I will get the result 64 ,so its define in system .
                        
                                 $ getconf HOST_NAME_MAX
                                
        */       
	if (strlen(hostname) > 64) 
	{
		Fprintf(stderr, "%s: hostname \"%.32s...\" is too long\n",
		    prog, hostname);
		exit(1);
	}
	hi = calloc(1, sizeof(*hi));	 // To allocate and initialize  with null .
	if (hi == NULL) 
	{
		Fprintf(stderr, "%s: calloc %s\n", prog, strerror(errno));
		exit(1);
	}

	/*
	* inet_addr
		+ The inet_addr() function converts the Internet host address cp  from  IPv4  
		  numbers-and-dots  notation  into  binary  data  in  network  byte  order. 
	*/

	addr = inet_addr(hostname);
	if ((int32_t)addr != -1)	// if we give the IP address, this block will execute. 
	{
		hi->name = strdup(hostname);
		hi->n = 1;		// Because we give the ip address directly.
		hi->addrs = calloc(1, sizeof(hi->addrs[0]));
		if (hi->addrs == NULL) {
			Fprintf(stderr, "%s: calloc %s\n",
			    prog, strerror(errno));
			exit(1);
		}
		hi->addrs[0] = addr;
		return (hi);	// Finally return the host info structure address to the called function.
	}
	/*
	* gethostbyname ()
		+ This function is used to get the information of the given host name .
	*/
	hp = gethostbyname(hostname);

	if (hp == NULL) {
		Fprintf(stderr, "%s: unknown host %s\n", prog, hostname);
		exit(1);
	}
	// here We ensuring the IP adddress of the host is belongs to the ipv4 family.
	if (hp->h_addrtype != AF_INET || hp->h_length != 4)
	 {
		Fprintf(stderr, "%s: bad host %s\n", prog, hostname);
		exit(1);
	}
	/*
		strdup()		
			This function allocates new memory and take the copy of the host name into that memory.
			
	*/
	hi->name = strdup(hp->h_name);
		
	// To find the number of ip adddress the host name has.
	for (n = 0, p = hp->h_addr_list; *p != NULL; ++n, ++p)
		continue;

	hi->n = n; // To assign the Number of ip address has the given host name.

	// Allocating the memory for all the ip address and initializing all the blocks with null.
	hi->addrs = calloc(n, sizeof(hi->addrs[0]));
	if (hi->addrs == NULL) 
	{
		Fprintf(stderr, "%s: calloc %s\n", prog, strerror(errno));
		exit(1);
	}
	/* To get all the ip address of the host.  */
	for (ap = hi->addrs, p = hp->h_addr_list;*p != NULL; ++ap, ++p)
		memcpy(ap, *p, sizeof(*ap));

return (hi);
}
// kannan
/*
   freehostinfo()
	+ This function is used to to free up the host info structure memory which is used by the gethostinfo 
	  function.
*/

void freehostinfo(register struct hostinfo *hi)
{
		/*
		* If we gave the IP address only then we don't store the name into the host info structure.
		* So,to ensure that the below if statement is used.
		*/
	if (hi->name != NULL) 
	{
		free(hi->name);
		hi->name = NULL;
	}

		/*
		* If I free the memory of the structure 'hi' only, then the memory which is used by the 
		  'hi->addrs' will not be free .
		* For that reason only we free the 'hi->addrs' memory before free the whole structure memory.
		*/
	free((char *)hi->addrs);
	free((char *)hi);
}
// sathiya
/* To get the IP address of the given host name.*/
void getaddr(register u_int32_t *ap, register char *hostname)
{
	register struct hostinfo *hi;

	hi = gethostinfo(hostname); // To get the host info.
	*ap = hi->addrs[0];	// Assign the first ip address from the list.
	freehostinfo(hi); // To free the used memory.
}
// sathiya
void setsin(register struct sockaddr_in *sin, register u_int32_t addr)
{

	memset(sin, 0, sizeof(*sin));	// fill the null values to the given memory for given size.
#ifdef HAVE_SOCKADDR_SA_LEN
	sin->sin_len = sizeof(*sin);	// if the system is FREEBSD then the sockaddr_in structure also has the length as a member.
#endif
	sin->sin_family = AF_INET; // The address family. (here the family is IPV4)
	sin->sin_addr.s_addr = addr; // Filling the IP address.
}
// Karuppaiah
/* String to value with optional min and max. Handles decimal and hex. */
int str2val(register const char *str, register const char *what,register int mi, register int ma)
{
	register const char *cp; // To point the actual value in the string 'str'(point a.
	register int val; // it is contains the result of converting string ('str') to integer.
	char *ep; // it has the unwanted characters in given str.

	// To check whether the user gave the hexa decimal value or not.
	if (str[0] == '0' && (str[1] == 'x' || str[1] == 'X'))
	{
		cp = str + 2; // To skip the '0x' or '0X'. 
		val = (int)strtol(cp, &ep, 16); // To convert the string of integer into the hexa decimal value.
	}
	else 
	{
		// To convert the string of integer into the decimal value.
		val = (int)strtol(str, &ep, 10);
	}
		
	if (*ep != '\0') // To check whether the user given value is valid or not.
	 {
		Fprintf(stderr, "%s: \"%s\" bad value for %s \n",
		    prog, str, what);
		exit(1);
	}
	// To ensure the value should not be less than the minimum value and the minimum value should be greater than or equal to zero.
	if (val < mi && mi >= 0) 
	{
		if (mi == 0)
			Fprintf(stderr, "%s: %s must be >= %d\n",
			    prog, what, mi);
		else
			Fprintf(stderr, "%s: %s must be > %d\n",
			    prog, what, mi - 1);
		exit(1);
	}
	// To ensure the value must be less than the maximum value and the maximum value should be greater than or equal to zero.
	if (val > ma && ma >= 0)
	 {
		Fprintf(stderr, "%s: %s must be <= %d\n", prog, what, ma);
		exit(1);
	}
	return (val); // Return the value.
}

__dead void
usage(void)
{
	extern char version[];

	Fprintf(stderr, "Version %s\n", version);
	Fprintf(stderr,
	    "Usage: %s [-dFInrvx] [-g gateway] [-i iface] [-f first_ttl]\n"
	    "\t[-m max_ttl] [ -p port] [-q nqueries] [-s src_addr] [-t tos]\n"
	    "\t[-w waittime] [-z pausemsecs] host [packetlen]\n", prog);
	exit(1);
}
// kannan
/*      
* RAdb
	+ Routing Assets Database .
	+ The Radb is a public registry of routing information for network in the internet .
	+ ISP,Universities ,organizations publicy publish or register their routing policy and router 
	  announcements in the RADb.
	+ So,We can get the Autonomous System information of given IP address with help of following Domain.

				 "whois.radb.net" 
				IP address => 192.108.0.18 
				Port number => 43      
*/

const char *get_as(const char *query)
{
        int sfd,n;			// sfd	-> TCP socket descriptor.
					// n	-> snprintf return value.
        FILE *fp;       		// File pointer
        char buf[1024];			// temporary usage
        static char res_buf[256]={};  	// Result buffer to kept the Autonomous system number .
        struct sockaddr_in serv;

        serv.sin_family=AF_INET;                	// IPv4 family .
        serv.sin_port=htons(43);                	// RADb server port number.
        serv.sin_addr.s_addr=inet_addr("198.108.0.18"); // RADb server IP address.
        sfd= socket(AF_INET,SOCK_STREAM,0);  		// Create a Socket to communicate with RADb server.

        if (sfd< 0)
                perror("socket");

	/* Make connection with RADb server .*/
        if (connect(sfd,(struct sockaddr *)&serv,sizeof(serv))<0)
        {
                perror("connect");
        }

        n=snprintf(buf,sizeof(buf),"%s\n",query); 	/*
							* Store the IP Address into the buf to req uest to the 
							  RADb server.
							*/
 	/* Request to the RADb server for asking the Autonomous system for given IP address.*/
        if (write(sfd,buf,n) < n)
        {
                perror("write");
        }
        fp=fdopen(sfd,"r");
        if (fp==NULL)   		// Check whether the fdopen function failed .
        {
                perror("fdopen");
        }
        strcpy(res_buf,"*"); 		// Print if there is no Autonomous System for given IP address .

	/* Parse the lines of given file.*/
        while (fgets(buf,sizeof(buf),fp)!=NULL)
        {
		/* To Get the line which contains origin .(AS number)*/
                if (strncmp(buf,"origin:",sizeof("origin:")-1)==0) 
                {
                        char *p,*as;
                        // sizeof returns the size including null .
                        p=buf+(sizeof("origin:")-1);

                        while (isspace(*p)) 	// To skip the space . 
                                p++;
                        as=p; 			// Store the Autonomous system number .
                        /* To skip the spaces.*/
                        while(*p!=NULL && !isspace(*p))
                                p++;
                        *p='\0';        	// Terminate with '\0' null terminator .
                        strcpy(res_buf,as); 	// Store the Autonomous system number to the result buffer.
                        break;
                }
        }	
        fclose (fp);  	//close the file stream 
        close (sfd); 	// Close the Socket file descriptor 
        return res_buf; // Return the Autonomous System of given ip address .
}

