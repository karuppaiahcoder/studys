/*
 * Copyright (c) 1997, 1998, 1999, 2000
 *	The Regents of the University of California.  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *	This product includes software developed by the Computer Systems
 *	Engineering Group at Lawrence Berkeley Laboratory.
 * 4. Neither the name of the University nor of the Laboratory may be used
 *    to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#ifndef lint
static const char rcsid[] =
    "@(#) $Id: ifaddrlist.c,v 1.9 2000/11/23 20:01:55 leres Exp $ (LBL)";
#endif

#include <sys/param.h>
#include <sys/file.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#ifdef HAVE_SYS_SOCKIO_H
#include <sys/sockio.h>
#endif
#include <sys/time.h>				/* concession to AIX */

#if __STDC__
struct mbuf;
struct rtentry;
#endif

#include <net/if.h>
#include <netinet/in.h>

#include <ctype.h>
#include <errno.h>
#include <memory.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "gnuc.h"
#ifdef HAVE_OS_PROTO_H
#include "os-proto.h"
#endif

#include "ifaddrlist.h"

// kannan

/*
		
	Flow of this function
		
		+ Create a socket to access the Ethernet interfaces .

		+ Call the ioctl function like below to determine the amount of memory we need to get the interface list .
				
			ioctl(fd, SIOCGIFCONF, (char *)&ifc)
			
			fd => descriptor of the created socket .
		
			SIOCGIFCONF
				Request code to get the interface list .
			ifc
				ifconf structure pointer .
				It has following information.
					ifc_len => Amount of memory (in bytes ) we need .

					ifc_buf => character buffer which is hold the name of all the interfaces  .

					ifc_req => Null (Because ,We don't need all the information of the interfaces ).	

		+ Call the ioctl function with device name to get the flags of that device .
				
			 To fill the device name into the ifconf structure .
				
				 strncpy(ifr.ifr_name, ifrp->ifr_name, sizeof(ifr.ifr_name));			
			
			 To Get the flags of the given device .
			
				ioctl(fd, SIOCGIFFLAGS, (char *)&ifr)	

			 To check whether the device is in active or not,if the device is not in active then don't get that interface .
				
				    if ((ifr.ifr_flags & IFF_UP) == 0)
					continue;
														 
		+ Get the name of the interface and store that into the character array 'device'.
				
					
			strncpy(device, ifr.ifr_name, sizeof(ifr.ifr_name));
			
									
		+ Call the ioctl function with device name to get the Address of that device .
	
				ioctl(fd, SIOCGIFADDR, (char *)&ifr)

		+ Finally store the address and name of the each interface into the  'ifaddrlist' structure array ,and return to the 
	called function ( The return value is the no of interface we got ).


  Return the interface list
	 	
	struct ifaddrlist
	{
		u_int32_t addr;
		char *device;
	};
	
	
	
 */
int
ifaddrlist(register struct ifaddrlist **ipaddrp, register char *errbuf)
{
	// fd is socket descriptor .
	// nipaddr is number of interfaces .
	register int fd,nipaddr;
#ifdef HAVE_SOCKADDR_SA_LEN
	register int n;
#endif
	/*
 		struct ifreq
		{
			//IFNAMESIZ 16 

		       char ifr_name[IFNAMSIZ]; // Interface name 
		       union
		       {
			   struct sockaddr ifr_addr;			// size is 16 byte
			   struct sockaddr ifr_dstaddr;
			   struct sockaddr ifr_broadaddr;
			   struct sockaddr ifr_netmask;
			   struct sockaddr ifr_hwaddr;		
			   short           ifr_flags;	
			   int             ifr_ifindex;
			   int             ifr_metric;
			   int             ifr_mtu;
			   struct ifmap    ifr_map;			// Size is 16 byte
			   char            ifr_slave[IFNAMSIZ];
			   char            ifr_newname[IFNAMSIZ];
			   char           *ifr_data;
		       };
           	};
		

	*/

	/*
 		ifrp => Temporary 'freq 'structure pointer .
		ifend => Indicate the end of Interface list .
		ifnext => 'ifreq' structure pointer which is used to read through the interface lists .
	
	*/
	register struct ifreq *ifrp, *ifend, *ifnext, *mp; 
	/*
		 struct sockaddr_in
		 {
              	  	sa_family_t    sin_family; // address family: AF_INET 
                	in_port_t      sin_port;   // port in network byte order 
               	 	struct in_addr sin_addr;   // internet address 
           	};

	*/
	register struct sockaddr_in *sin;
	register struct ifaddrlist *al;
	/*		
		struct ifconf
		{
                      int                 ifc_len; // size of buffer 
                      union 
		      {
                          char           *ifc_buf; // buffer address 
                          struct ifreq   *ifc_req; // array of structures 
                      };
                  };
	*/
	struct ifconf ifc;
	// ibuf[819]
	// size of struct ifreq is 40 bytes .

	/* 
		Why the maximum is 819 ?

						
	*/
	struct ifreq ibuf[(32 * 1024) / sizeof(struct ifreq)], ifr;

#define MAX_IPADDR (sizeof(ibuf) / sizeof(ibuf[0]))
	
	/*
 		sizeof(ibuf) => 32760
		sizeof(ibuf[0]) => 40 
				
 			 32760/40 = 819
		
		MAX_IPADDR => 819
		
	*/

	/*	
		struct ifaddrlist
		{
			u_int32_t addr;
			char *device;
		};
		
	*/
	
	static struct ifaddrlist ifaddrlist[MAX_IPADDR]; // interface list

	char device[sizeof(ifr.ifr_name) + 1];	// Device name
	/*
 		Why we need socket descriptor ?
		 + In unix every device accessed as a file . So ,if we need to access any device ,we should open that device .
		 + So ,for the network devices such as ethernet and wireless cards a socket descriptor is used with ioctl function.
			
	*/

	fd = socket(AF_INET, SOCK_DGRAM, 0);

	if (fd < 0)
	{
		(void)sprintf(errbuf, "socket: %s", strerror(errno));
		return (-1);
	}

	ifc.ifc_len = sizeof(ibuf);	// 32760
	ifc.ifc_buf = (caddr_t)ibuf;   //  typedef char *caddr_t;	 

	/*
 
	ioctl(fd, SIOCGIFCONF, (char *)&ifc)
		
	
		+ Use of this place the SIOCGIFCONF determines the amount of memory we need .			
		
		Note:

			+ If  ifc_req  is NULL, SIOCGIFCONF returns the necessary buffer
		      size  in  bytes  for  receiving  all  available  addresses  in
		      ifc_len.
	 
			+  Otherwise, ifc_req contains a pointer to an array of
		      ifreq structures to be filled with  all  currently  active  L3
		      interface  addresses.

		ioctl returns zero on success , returns -1 on error .
		
			
						
	*/
	if (ioctl(fd, SIOCGIFCONF,(char *)&ifc) < 0 || ifc.ifc_len < sizeof(struct ifreq))
	{
		if (errno == EINVAL)	// EINVAL Request or argp is not valid.
		{
			(void)sprintf(errbuf,"SIOCGIFCONF: ifreq struct too small (%d bytes)",sizeof(ibuf));
		}
		else
			(void)sprintf(errbuf, "SIOCGIFCONF: %s",
			    strerror(errno));
		
		(void)close(fd);	
					
		return (-1);
	}
	
	
	ifrp = ibuf;		// base address of the interface address list .

	// ifc.ifc_len contains Total length of the buffer in bytes .
 	
	ifend = (struct ifreq *)((char *)ibuf + ifc.ifc_len); 	// end address of interface addresses list 

	al = ifaddrlist; 

	mp = NULL; 	// Variable of 'ifreq' structure .
	nipaddr = 0;	// No of interface addresses 	
	
	for (; ifrp < ifend; ifrp = ifnext) 
	{
#ifdef HAVE_SOCKADDR_SA_LEN
		/*
 			In FreeBSD 'sockaddr' structure is defines like below .
				 
			Struct sockaddr
			{
				unsigned char sa_len;	// Total length
				sa_family_t sockfamily;  //  2 byte
				char sa_data[14];	 // 14 byte
			}
				
			The 'sockaddr' structure is declared inside of the 'ifreq' structure ,so the structure will be differ in 
		  Free BSD system .
			
			 	
		*/		
		
		n = ifrp->ifr_addr.sa_len + sizeof(ifrp->ifr_name);
				
		if (n < sizeof(*ifrp))
			ifnext = ifrp + 1;
		else
			ifnext = (struct ifreq *)((char *)ifrp + n);

		// To ensure the IP address of the interface should be in IPv4 family .
		if (ifrp->ifr_addr.sa_family != AF_INET)
			continue;
			
#else
		ifnext = ifrp + 1;
#endif
		/*
		  Need a template to preserve address info that is used below to locate the next entry. (Otherwise,SIOCGIFFLAGS stomps
	 over it because the requests are returned in a union.)
				
		 */
		strncpy(ifr.ifr_name, ifrp->ifr_name, sizeof(ifr.ifr_name));
		/*
 			 SIOCGIFPFLAGS
			
 			     Get or set extended (private) flags for the device .
			
		*/
		if (ioctl(fd, SIOCGIFFLAGS,(char *)&ifr) < 0)
		{
			if (errno == ENXIO)	//  No such device or address .
				continue;
			(void)sprintf(errbuf, "SIOCGIFFLAGS: %.*s: %s",
			    (int)sizeof(ifr.ifr_name), ifr.ifr_name,
			    strerror(errno));
			(void)close(fd);
			return (-1);
		}

		/* Must be up ,To get active interface only */
		if ((ifr.ifr_flags & IFF_UP) == 0)
			continue;

								
		(void)strncpy(device, ifr.ifr_name, sizeof(ifr.ifr_name));

		// Getting the interface name here .
	
		device[sizeof(device) - 1] = '\0';
#ifdef sun
		/* Ignore sun virtual interfaces */
		if (strchr(device, ':') != NULL)
			continue;
#endif
		/*
 * 			SIOCGIFADDR

				 Get  or set the address of the device using ifr_addr .

		*/
		if (ioctl(fd, SIOCGIFADDR, (char *)&ifr) < 0)
		 {
			(void)sprintf(errbuf, "SIOCGIFADDR: %s: %s",
			    device, strerror(errno));
			(void)close(fd);
			return (-1);
		}
		// To ensure that do not exceed the Maximum of interface  .
		if (nipaddr >= MAX_IPADDR)
		 {
			(void)sprintf(errbuf, "Too many interfaces (%d)",
			    MAX_IPADDR);
			(void)close(fd);
			return (-1);
		}
			
		sin = (struct sockaddr_in *)&ifr.ifr_addr;
		
		// To get the MTU of the interfaces. 
		if (ioctl(fd,SIOCGIFMTU, (char *)&ifr) < 0)
		 {
			(void)sprintf(errbuf, "SIOCGIFMTU: %s: %s",
			    device, strerror(errno));
			(void)close(fd);
			return (-1);
		}
		
		al->mtu=ifr.ifr_mtu; // Storing the interface in array of ifaddrlist strucuture of the interface.
		
		/* Store the Interface address and name */
		al->addr = sin->sin_addr.s_addr;
		al->device = strdup(device);
		++al; // To move to the  next block of ifaddrlist strucutre to store next interface.
		++nipaddr;
	}
	(void)close(fd);

	*ipaddrp = ifaddrlist;
	return (nipaddr);
}
