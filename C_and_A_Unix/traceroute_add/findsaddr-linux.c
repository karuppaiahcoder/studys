/*
 * Copyright (c) 2000
 *	The Regents of the University of California.  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *	This product includes software developed by the Computer Systems
 *	Engineering Group at Lawrence Berkeley Laboratory.
 * 4. Neither the name of the University nor of the Laboratory may be used
 *    to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#ifndef lint
static const char rcsid[] =
    "@(#) $Id: findsaddr-linux.c,v 1.1 2000/11/23 20:17:12 leres Exp $ (LBL)";
#endif

/* XXX linux is different (as usual) */

#include <sys/param.h>
#include <sys/file.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#ifdef HAVE_SYS_SOCKIO_H
#include <sys/sockio.h>
#endif
#include <sys/time.h>				/* concession to AIX */

#if __STDC__
struct mbuf;
struct rtentry;
#endif

#include <net/if.h>
#include <netinet/in.h>

#include <arpa/inet.h>

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "gnuc.h"
#ifdef HAVE_OS_PROTO_H
#include "os-proto.h"
#endif

#include "findsaddr.h"
#include "ifaddrlist.h"
#include "traceroute.h"

static const char route[] = "/proc/net/route";

// kannan

/*
	
	findsaddr()
		This function is used to find the source address to the given destination address .
				
 */
const char *findsaddr(register const struct sockaddr_in *to,register struct sockaddr_in *from,int *mtu)
{
	register int i, n;
	register FILE *f;		// file pointer for /proc/net/route
	register u_int32_t mask;	// Subnet mask 
	/*
 		dest => Destination address in route entry table .
		tmask => Subnet mask in route entry table .
	*/
	u_int32_t dest, tmask;
	/*
		struct ifaddrlist
		{
			u_int32_t addr;	// Address of the device 
			char *device;  // name of the device 
		};

	*/
	struct ifaddrlist *al;	// pointer to the ifaddrlist structure .
	/*
 		buf => It contains a route entry .
		tdevice => device name taken from the route entry (temporary).
		device => device name taken from the router entry (final one).
	*/
	char buf[256], tdevice[256], device[256]; 	
	static char errbuf[132]; // Errbuf contains error string .
	
	// Here ,We opening a file which has the route table information .
	if ((f = fopen(route, "r")) == NULL) {
		sprintf(errbuf, "open %s: %.128s", route, strerror(errno));
		return (errbuf);
	}

	/* Find the appropriate interface */
	n = 0;	// it has no of route entries . 
	mask = 0; 
	device[0] = '\0';
	/*
 		 Read all the entry from the route entry table .
	*/	
	while (fgets(buf, sizeof(buf), f) != NULL)
	{
		++n;	// no of entry
		if (n == 1 && strncmp(buf, "Iface", 5) == 0)	// skip the header information .
			continue;
		/*
 			To get the device ,destination and the mask from the route entry .
			%*s => used to skip the particular field from the entry.	
		*/
		if ((i = sscanf(buf, "%s %x %*s %*s %*s %*s %*s %x",tdevice, &dest, &tmask)) != 3)
			return ("junk in buffer");

		/*
				
		     Kernel IP routing table

			Destination     Gateway         Genmask         Flags   MSS Window  irtt Iface   // skipped 
			0.0.0.0         192.168.2.10    0.0.0.0         UG        0 0          0 enp0s3
			1.1.1.0         0.0.0.0         255.255.255.0   U         0 0          0 enp0s3
			169.254.0.0     0.0.0.0         255.255.0.0     U         0 0          0 enp0s3
			192.168.2.0     0.0.0.0         255.255.255.0   U         0 0          0 enp0s3

			
 			to->sin_addr.saddr & tmask == dest		
			
				The above condition is used to choose the gateway according to the destination ip.
			
			For example
					dest => 192.168.2.0 
					
					to->sin_addr.saddr => 192.168.2.55 
						
					tmask => 255.255.255.0 
					
					so,
						192.168.2.55 & 255.255.255.0 => 192.168.2.0 
								
						192.168.2.0 == 192.168.2.0  ( matched )
						
					The result indicates that the destination ip is not in outside of the 2 dot network.if the 
				result is not matched then it means the destination ip is in outside of the 2 dot network.
								
				
			tmask > mask || mask ==0 
				current route entry gateway mask should be greater than the previous gateway mask.
				
				Why it is ?
					To choose the best path. 				
	
		*/
		
		if ((to->sin_addr.s_addr & tmask) == dest &&(tmask > mask || mask == 0)) 
		{
			mask = tmask; // To get the best mask.
			strcpy(device, tdevice);
		}
	}
	fclose(f); // close the /proc/net/route file.

	if (device[0] == '\0')
		return ("Can't find interface");

        /* Get the interface address list */
	if ((n = ifaddrlist(&al, errbuf)) < 0)
		return (errbuf);

	if (n == 0)
		return ("Can't find any network interfaces");

	/* Find our appropriate source address */
	for (i = n; i > 0; --i, ++al)
		if (strcmp(device, al->device) == 0)
			break;
	if (i <= 0)
	 {
		sprintf(errbuf, "Can't find interface \"%.32s\"", device);
		return (errbuf);
	}
//	printf("mtu=%d\n",al->mtu);
        *mtu=al->mtu;

	setsin(from, al->addr); // To store the device address into the 'from' structure .
										
	return (NULL);
}
