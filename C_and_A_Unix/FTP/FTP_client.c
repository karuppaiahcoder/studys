/*
		File Tranfer Protocol client (FTP-client)

	Algorithm:
	``````````
		socket,bind and accept:
		```````````````````````
			+ create_socket()
				-I wrote this function to create the client socket.

				-using followign functions,

					socket();
					connect();
					fcntl();
					
			+ Then user give the command fro stdin, I use the sscanf to split the commands and options.

			The user will be use a following commands.

	
				list	=> this command used to list the client current working directory from server.

					=> This command syntax:

							list
						
								=>Not path maitainance,so use this command properly.
				pwd	=> This command used to print the client current working directory path.

				cd	=>This command change directory to server.

					syntax:
					```````	
							cd
							cd /
							cd ./
							cd ..
							cd ./karuppaiah (or) karuppaiah
							cd /karuppaiah/vijay

				stor	=> This command stor the file to server.

				retr 	=> This usage get the file from server.
			
*/

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>
#include<fcntl.h>
#include<sys/socket.h>
#include<netinet/in.h>
#include<sys/select.h>
#include<sys/time.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<setjmp.h>
#include<signal.h>
int c_fd;	//client socket fd

int error_log(char *ptr)
{
	printf("%s",ptr);
	exit(1);
}

int help()
{
	printf("pwd   : present working directory of the particular client.\n");
	printf("list  : list all files in that directory.\n");
	printf("stor  : upload a file from client to server.\n");
	printf("retr  : download a file from server to client.\n");
	printf("cd    : change directory (note user cannot move beyond the server's home directory).\n");
	printf("exit  : logout.\n");
}

int creat_socket(int port)
{
	int c_fd;
	struct sockaddr_in s_inf;
	s_inf.sin_family=AF_INET;
	s_inf.sin_port=htons(port);
	s_inf.sin_addr.s_addr=INADDR_ANY;
	if((c_fd=socket(AF_INET,SOCK_STREAM,0))==-1)
		error_log("socket function error []\n");
	if(connect(c_fd,(struct sockaddr *)&s_inf,sizeof(s_inf))==-1)
		error_log("connect function error []\n");
	if(fcntl(c_fd,F_SETFL,O_NONBLOCK)==-1)
		error_log("fcntl function error []\n");
	if(fcntl(0,F_SETFL,O_NONBLOCK)==-1)
		error_log("fcntl function error []\n");
	return c_fd;
}

int prom()
{
	printf("FTP_$ ");
	fflush(stdout);
}

char file_name[255];
char file_name1[255];

int verify_file_name(char *file_2,char *file_1)	//verify the file name already exist or not.
{
	char tmp[255]={};
	if(file_1[0]=='\0')
	{
		printf("Give source file name\n");
		prom();
		return -1;
	}
	else if(file_2[0]!='\0'&&access(file_2,F_OK)!=-1)
	{
		if(file_2[strlen(file_2)-1]=='/')
		{
			sprintf(tmp,"%s%s",file_2,file_1);
			if(access(tmp,F_OK)!=-1)
			{
				printf("%s:this file name already exist\n",tmp);
				prom();
				return -1;
			}
			strcpy(file_name,tmp);
		}else
		{
			printf("%s:this file name already exist\n",file_2);
			prom();
			return -1;
		}
	}else if(file_2[0]=='\0')
	{
		if(access(file_1,F_OK)!=-1)
		{
			printf("%s:this file name already exist\n",file_1);
			prom();
			return -1;
		}
		strcpy(file_name,file_1);
	}else
		strcpy(file_name,file_2);
	return 1;
}		

int file_download()	//retr command process get the file from server.
{
	char cont[2048]={};
	if(fcntl(c_fd,F_SETFL,O_RDWR)==-1)
		error_log("fcntl function error []\n");
	int fd;
	if((fd=open(file_name,O_CREAT|O_WRONLY|O_APPEND,0666))==-1)
		error_log("open function error \n");
	if(write(c_fd,"SIG_ACK\n",3+3)==-1)
		error_log("ACK signal write error\n");
	while(read(c_fd,cont,2014)>0)
	{
		if(strstr(cont,"SIG_:?)$"))
		{
			if(fcntl(c_fd,F_SETFL,O_NONBLOCK)==-1)
				error_log("fcntl function error []\n");
			prom();
		}else
		{	
			if(write(fd,cont,strlen(cont))==-1)
				error_log("write function error \n");
			if(write(c_fd,"ACK\n",3+3)==-1)
				error_log("ACK signal write error\n");
		}
		sleep(1);
		bzero(cont,2014);
	}
	close(fd);
	bzero(file_name,255);
}
						
int exist_file(char *file_1)	//check file already exist or not
{
	struct stat t;
	if(file_1[0]=='\0')
	{
		printf("Give source file name\n");
		prom();
		return -1;
	}
	if(access(file_1,F_OK|R_OK)==-1)
	{
		printf("%s :This file name not exist\n",file_1);
		prom();
		return -1;
	}
	stat(file_1,&t);
	if(S_ISDIR(t.st_mode))
	{
		printf("%s :This is a directory not file\n",file_1);
		prom();
		return -1;
	}
	strcpy(file_name1,file_1);
	return 1;
}

int upload_file()	//stor command process stor the file to server.
{
	FILE *fp;
	char cont[2048]={},err[15]={};
	if((fp=fopen(file_name1,"r"))==NULL)
		error_log("Fopen function error \n");
	if(fcntl(c_fd,F_SETFL,O_RDWR)==-1)
		error_log("fcntl function error []\n");
	while(fgets(cont,2040,fp)!=NULL)
	{
		if(write(c_fd,cont,strlen(cont))==-1)
			error_log("write function error\n");
		read(c_fd,err,15); //wait for ACK
		bzero(cont,2048);
		sleep(1);
	}
	if(write(c_fd,"SIG_/:)?#\n",11)==-1)
		error_log("Write function error\n");
	if(fcntl(c_fd,F_SETFL,O_NONBLOCK)==-1)
		error_log("fcntl function error []\n");
	fclose(fp);
	prom();
}

jmp_buf env;

void handler()
{
	char tmp[1024]={};
	if(write(c_fd,"$_SIG_INT_$\n",16)==-1)
		error_log("write function error \n");
	if(file_name[0]!='\0')
		unlink(file_name);
	if(fcntl(c_fd,F_SETFL,O_NONBLOCK)==-1)
		error_log("fcntl function error []\n");
	printf("\n");
	prom();
	siglongjmp(env,1);
}

int main(int argc,char *argv[])
{
	c_fd=creat_socket(8484); //default port number 8484
//	int c_fd=creat_socket(8686); //default port number 8484
	char cont[2024]={},data[1024]={},cmd[255]={},file_1[255]={},file_2[255]={};
	fd_set rfd;
	struct timeval t;
	int promt;
	signal(SIGINT,handler);
	printf("You want know about commands details just type 'help' command\n");
	sigsetjmp(env,1);
	while(1)
	{
		FD_ZERO(&rfd);
		FD_SET(c_fd,&rfd);
		FD_SET(0,&rfd);
		t.tv_sec=0;
		t.tv_usec=1;
		if(select(c_fd+1,&rfd,NULL,NULL,&t)>0)
		{
			if(FD_ISSET(c_fd,&rfd))
			{
				bzero(cont,2024);
				if(read(c_fd,cont,2024)>0)
				{
					if(strstr(cont,"SIG_:?)$"))	//retr SIG_:?)$ is identification
						file_download();
					else if(strstr(cont,"SIG_/:)?#"))//stor	SIG_/:)?# identification
						upload_file();
					else if(strstr(cont,"end"))	//promt FTP_$ 
						prom();
					else
					{
						printf("%s",cont);
						fflush(stdout);
					}
				}else
					error_log("Connection closed by foreign host\n");	
			}
			if(FD_ISSET(0,&rfd))
			{
				bzero(data,1024);
				bzero(cont,1024);
				bzero(cmd,255);
				bzero(file_1,255);
				bzero(file_2,255);
				if(read(0,data,1024)>1)
				{
					sscanf(data,"%s %s %s",cmd,file_1,file_2);
					if(!strcmp(cmd,"cd"))
					{
						if(file_1[0]!='\0'&&file_1[strlen(file_1)-1]!='/')
							file_1[strlen(file_1)]='/';
						sprintf(cont,"%s %s",cmd,file_1);
					}else if(!strcmp(cmd,"retr"))
					{
						if(verify_file_name(file_2,file_1)==-1)//valid file name
							continue;
						sprintf(cont,"%s %s",cmd,file_1);
					}else if(!strcmp(cmd,"stor"))
					{
						if(exist_file(file_1)==-1)	//valid file name
							continue;
						if(file_2[0]=='\0')		//second file checking
							sprintf(cont,"%s %s",cmd,file_1);
						else
							sprintf(cont,"%s %s",cmd,file_2);
					}else if(!strcmp(cmd,"help"))
					{
						help();
						prom();
						continue;
					}else if(!strcmp(cmd,"exit"))
					{
						printf("logout\n");
						exit(0);
					}
					else
						sprintf(cont,"%s",data);
					if(write(c_fd,cont,strlen(cont))==-1)
						error_log("write function error []\n");
				}else
					prom();
			}
		}
	}
}
