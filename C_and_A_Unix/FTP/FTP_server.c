/*
		File Tranfer Protocol server (FTP-server)

	Algorithm:
	``````````
		command line argument:
		``````````````````````
			+ get the working directory and second from user by command line argument.

				working directory path:- => Default is current working directory,If user give a path that time take this path with verify that directory exist or not.

				seconds:- =>Default seconds for 60,if user give a second take that with verify integer or not.

		Daemon:
		```````
			umask:	=>Set the umask value zero (umask(0))

			fork(): =>create fork and kill the parent process.

			setsid: =>Change the session id by setsid function.

			fork(): =>Create second fork then kill parent process.

			chdir(): =>Change current working directry. 

			close(): =>Close all descriptors.

			open(): =>Open the stdin,stdout and stderr from /dev/null file.


		Already running:
		````````````````
			+ check this process already running or not.

			+ set the F_SETLF flag to the .FTP.pid file by fcntl function.

			+ Then check that function return value.

		socket,bind and accept:
		```````````````````````
			+ create socket using TCP concept, because FTP is a connection orianted process.

			+ Then bind the socket with name.

			+ The sent the non-blocking to that socket fd.

			+ Then continuesly accept the user request.

		structure:
		``````````
			typedef struct{
				int fd;		=>Client file descriptors.
				char path[];	=>Cleint current working directory.
				int sec;	=>user alive seconds.
			}
		pwd:
		````
			+ Just get the user path from structure, that (c_table[tloc].path), becuase I maitain a each users path in path variable.

		list:
		`````
			+ the popen function will be retrun the file list in user path location directory.

			+ So then read and write the popen retunr file to user fd.
		retr:
		`````
			+ Use the acknpwlagement conecpt, when server ready to send the data that time server send the one signal to client. Then server wait for client acknowlagement signal to send the next value to cleint, this process happaned untile file retrun EOF.

		stor:
		`````
			+ The server whan ready to read the data that time sand the acknowlagement signal to cleint, that cleint read and write the data , then wait for server aknowlagment signal.

		cd:
		```
			+ I maintain the user path, so when user give the cd command and path, that time I verify that path valid or not and add his path variable.

*/

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>
#include<fcntl.h>
#include<sys/socket.h>
#include<netinet/in.h>
#include<sys/select.h>
#include<sys/time.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<sys/sendfile.h>


#define DAEMON_WPATH "."	//daemon working directory we can change this.
char s_path[1255];
int s_sec,e_fd,l_fd;

int error_log(char *ptr)//error loading
{
	write(e_fd,ptr,strlen(ptr));
	exit(1);
}

int log_inf(char *ptr)//error loading
{
	char date_c[30]={},log[120]={};
	FILE *fp=popen("date","r");
	fgets(date_c,30,fp);
	pclose(fp);
	date_c[strlen(date_c)-1]='\0';
	sprintf(log,"%s::%s",date_c,ptr);
	if(write(l_fd,log,strlen(log))==-1)
		error_log("write function error []\n");
}

void verify_arg(int len,char *ptr[])
{
	int i,sec=0,path=0,j=0;
	for(i=1;i<len;i++)
	{
		if(access(ptr[i],X_OK)!=-1)
		{
			strcpy(s_path,ptr[i]);
			if(s_path[strlen(s_path)-1]!='/')
				strcat(s_path,"/");
			path=1;
		}else if(sec==0)
		{
			sec=1;
			while(ptr[i][j]!='\0')
				if(!isdigit(ptr[i][j++]))
				{
					sec=0;
					break;
				}
			if(sec==1)
				s_sec=atoi(ptr[i]);
		}
		if((path==0&&sec==0&&i==1)||((path==0||sec==0)&&i==2))
		{
			printf("Invalid argument\n");
			exit(1);
		}
	}
	if(sec==0)
		s_sec=60;
	if(path==0)
		strcpy(s_path,"./");
}

int daemon_init(void)
{
	umask(0);
	pid_t pid;
	int i,fd;
	if((pid=fork())==-1)	//create child process.
	{
		perror("Fork function error\n");
		exit(1);
	}else if(pid>0)		//exit the parent process.
		exit(1);
	if(setsid()==-1)
	{
		perror("setsid function error\n");
		exit(1);
	}
	if((pid=fork())==-1)	//create child process.
	{
		perror("Fork function error\n");
		exit(1);
	}else if(pid>0)		//exit the parent process.
		exit(1);
	if(chdir(DAEMON_WPATH)==-1)
	{
		perror("chdir function error\n");
		exit(1);
	}
	for(i=0;i<1024;i++)
		close(i);
	if((fd=open("/dev/null",O_RDWR))==-1)
	{
		perror("open function error\n");
		exit(1);
	}
	dup2(fd,1);
	dup2(fd,2);
}

int already_run(void)
{
	char pid[15]={};
	int fd;
	struct flock lk;
	lk.l_type=F_WRLCK;
	lk.l_whence=SEEK_SET;
	lk.l_start=0;
	lk.l_len=10;
	lk.l_pid=getpid();
	if((fd=open(".FTP.pid",O_WRONLY|O_CREAT|O_APPEND,0666))==-1)
		error_log("open function error []\n");
	if(fcntl(fd,F_SETLK,&lk)==-1)		//set the write lock
		error_log("FTP server already running []\n");
	sprintf(pid,"%d",getpid());
	ftruncate(fd,0);
	if(write(fd,pid,strlen(pid))==-1)	//store the process id
		error_log("write function error []\n");
}

int creat_socket(int port)
{
	int s_fd;
	struct sockaddr_in s_inf;
	s_inf.sin_family=AF_INET;
	s_inf.sin_port=htons(port);
	s_inf.sin_addr.s_addr=INADDR_ANY;
	if((s_fd=socket(AF_INET,SOCK_STREAM,0))==-1)
		error_log("socket function error []\n");
	if(bind(s_fd,(struct sockaddr *)&s_inf,sizeof(s_inf))==-1)
		error_log("bind function error []\n");
	if(fcntl(s_fd,F_SETFL,O_NONBLOCK)==-1)
		error_log("fcntl function error []\n");
	return s_fd;
}

typedef struct{
	int fd;		//=>Client file descriptors.
	char path[1255];	//=>Cleint current working directory.
	int sec;	//=>user alive seconds.
}c_struct;
c_struct c_table[1024];
int tloc,maxfd=1005,smax;

int add_user(int c_fd)	//add new users with intialize values.
{
	if(c_fd >= maxfd) //maximum client level cheking process.
		return -1;
	if(smax < c_fd)
		smax=c_fd;
	c_table[tloc].fd=c_fd;
	bzero(c_table[tloc].path,1255);
	strcpy(c_table[tloc].path,s_path);
	c_table[tloc].sec=0;
	tloc++;
	return 0;
}

int kill_client(int l,int len)	//close the client fd
{
	int i,j;
	char log[100]={};
	if(close(c_table[l].fd)==-1)
		error_log("close function error []\n");
	sprintf(log,"Cleint %d died\n",c_table[l].fd);
	log_inf(log);
	for(i=l,j=l+1;j<len;i++,j++)
	{
		c_table[i].fd=c_table[j].fd;
		strcpy(c_table[i].path,c_table[j].path);
		c_table[i].sec=c_table[j].sec;
	}
}

int end(int fd)
{
	write(fd,"end\n",5);
}

int send_file(int c_fd,FILE *fp)	//retr command process
{
	char out[2048]={},ack[15]={};
	if(fork()==0)
	{
		if(write(c_fd,"SIG_:?)$\n",strlen("SIG_:?)$\n")+1)==-1)	//ACK signal
			error_log("file transfer write function error []\n");
		if(fcntl(c_fd,F_SETFL,O_RDWR)==-1)
			error_log("fcntl function error []\n");
		read(c_fd,ack,15);	//wait ACK signal	//get the client signal.
		while(fgets(out,2024,fp)!=NULL)
		{
			if(write(c_fd,out,strlen(out)+1)==-1)	//give the data line by line
				error_log("write function error []\n");
			if(read(c_fd,ack,15)<0) //wait next ACK signal	
			{
				break;
			}
			if(strstr(ack,"$_SIG_INT_$"))
			{
				fclose(fp);
				exit(1);
			}
			bzero(out,2014);
		}
		write(c_fd,"SIG_:?)$\n",strlen("SIG_:?)$\n")+1); //finaly send ACK signal to finish send data
		exit(0);
	}
}

int cd_cmd(char *file_1,c_struct *c_inf)	//cd command implementation
{
	char tmp[1255]={},err[255]={};
	int i;
	if(file_1[0]=='\0' || file_1[0]=='/')	
	{
		if(file_1[0]=='\0' || file_1[1]=='\0')
			sprintf(tmp,"%s",s_path);
		else
			sprintf(tmp,".%s",file_1);
	}else if(file_1[0]=='.')
	{
		char upath[1255]={},*ptr=NULL;
		int i;
		strcpy(upath,c_inf->path);
		if(file_1[0]=='.'&&file_1[1]=='/')
			sprintf(tmp,"%s%s",upath,&file_1[2]);
		else if(!strcmp(file_1,"../"))
		{
			if(strcmp(upath,"./"))
				for(i=strlen(upath)-2;upath[i]!='/';i--)
					upath[i]='\0';
			sprintf(tmp,"%s",upath);
		}
	}else
		sprintf(tmp,"%s%s",c_inf->path,file_1);
	if(access(tmp,X_OK)==-1)	//check the path valid or not from server side.
	{
		sprintf(err,"%s:Not file or directory\nFTP_$ ",file_1);
		if(write(c_inf->fd,err,strlen(err))==-1)
			error_log("write function error []\n");
	}else
	{
		strcpy(c_inf->path,tmp);
		end(c_inf->fd);
	}
}

int load_file(char *path,int c_fd)	//stor command process
{
	int fd;
	char cont[2048]={};
	if(fork()==0)
	{
		if((fd=open(path,O_CREAT|O_WRONLY|O_APPEND,0666))==-1)
			error_log("open function error from load fork []\n");
		if(fcntl(c_fd,F_SETFL,O_RDWR)==-1)
			error_log("fcntl function error from load fork []\n");
		write(c_fd,"SIG_/:)?#\n",strlen("SIG_/:)?#\n")+1); //send ACK to client.
		while(read(c_fd,cont,2024)>0)	//wait for data receiving
		{
			if(strstr(cont,"SIG_/:)?#"))
				break;
			else if(strstr(cont,"$_SIG_INT_$"))
			{
				close(fd);
				unlink(path);
				exit(1);
			}
			else
			{
				if(write(fd,cont,strlen(cont))==-1)
					error_log("Write function error from load fork []\n");
				if(write(c_fd,"ACK\n",3+3)==-1)	//send ACK signal to client
					error_log("write function error from loaf fork []\n");
			}
			bzero(cont,2048);
		}
		exit(1);	
	}
}

int exec_com(char *data,c_struct *c_inf)
{
	char cmd[50]={},file_1[1255]={},err[255]={};
	char out[2048]={},tmp[1255]={};
	FILE *fp;
	struct stat t;
	int fd;
	sscanf(data,"%s %s",cmd,file_1);
	if(!strcmp(cmd,"$_SIG_INT_$"))
		return;
	if(!strcmp(cmd,"list"))		//list command
	{
		sprintf(tmp,"ls %s",c_inf->path);
		if((fp=popen(tmp,"r"))==NULL)
			error_log("popen function error []\n");
		while((fread(out,sizeof(char),1024,fp))>0)
		{
			if(write(c_inf->fd,out,strlen(out))==-1)
				error_log("write function error []\n");
		}
		pclose(fp);
		end(c_inf->fd);
	}else if(!strcmp(cmd,"pwd"))	//pwd command
	{
		sprintf(out,"%s\nFTP_$ ",c_inf->path);
		if(write(c_inf->fd,out,strlen(out))==-1)
			error_log("write function error []\n");
	}else if(!strcmp(cmd,"cd"))	//cd command
		cd_cmd(file_1,c_inf);
	else if(!strcmp(cmd,"stor"))	//stor command
	{	
		sprintf(tmp,"%s%s",c_inf->path,file_1);
		if(!access(tmp,F_OK))
		{
			sprintf(err,"%s :file already exist\nFTP_$ ",file_1);
			if(write(c_inf->fd,err,strlen(err))==-1)
				error_log("write function error []\n");
		}else
			load_file(tmp,c_inf->fd);
	}else if(!strcmp(cmd,"retr"))	//retr command
	{
		sprintf(tmp,"%s%s",c_inf->path,file_1);
		fp=fopen(tmp,"r");
		if(fp==NULL)
		{
			sprintf(err,"%s:file not exist\nFTP_$ ",file_1);
			if(write(c_inf->fd,err,strlen(err))==-1)
				error_log("write function error []\n");
		}else
		{
			stat(tmp,&t);
			if(!S_ISDIR(t.st_mode))
				send_file(c_inf->fd,fp);
			else
			{
				sprintf(err,"%s:its directory not file\nFTP_$ ",file_1);
				if(write(c_inf->fd,err,strlen(err)+1)==-1)
					error_log("write function error []\n");
			}
		}
	}else if(cmd[0]!='\0')
	{
		sprintf(err,"%s:command not fount\nFTP_$ ",cmd); //command not found
		if(write(c_inf->fd,err,strlen(err))==-1)
			error_log("write function error []\n");
	}
}

int recv_cmd(void)	//get the client command
{
	fd_set rfd;
	struct timeval t;
	int i,total=tloc;
	char cmd[1024]={};
	char log[100]={};
	FD_ZERO(&rfd);
	for(i=0;i<total;i++)
		FD_SET(c_table[i].fd,&rfd);
	t.tv_sec=0;
	t.tv_usec=0;
	if(select(smax+1,&rfd,NULL,NULL,&t)>0)
	{
		for(i=0;i<total;i++)
		{
			if(FD_ISSET(c_table[i].fd,&rfd))
			{
				if(read(c_table[i].fd,cmd,1024)>0)
				{
					exec_com(cmd,&c_table[i]);
					sprintf(log,"received command from client %d ::%s",c_table[i].fd,cmd);
					log_inf(log);
					c_table[i].sec=0;
					bzero(cmd,1024);
				}else
				{
					kill_client(i,total);
					i--;
					total--;
				}
			}
		}
	}
	tloc=total;
}

int check_alive(void)	//check client activity
{
	int i,total=tloc;
	char log[100]={};
	for(i=0;i<total;i++)
	{
		c_table[i].sec++;
		if(c_table[i].sec>=s_sec)	//this client not alive,so kill him
		{
			kill_client(i,total);
			sprintf(log,"Cleind died %d\n",c_table[i].fd);
			i--;
			total--;
			log_inf(log);
		}
	}
	tloc=total;
}

int main(int argc,char *argv[])
{
	if(argc>3)
	{
		printf("Usage: %s [directory path] [second]\n",argv[0]);
		exit(1);
	}else if(argc>1)
		verify_arg(argc,argv);
	else
	{
		strcpy(s_path,"./");	//defauld server working directory
		s_sec=60;
	}
//	daemon_init();
	if((e_fd=open(".err_FTP.log",O_WRONLY|O_CREAT|O_APPEND,0666))==-1)
	{
		perror("open function error []\n");
		exit(1);
	}
	if((l_fd=open(".FTP.log",O_WRONLY|O_CREAT|O_APPEND,0666))==-1)
	{
		perror("open function error []\n");
		exit(1);
	}
	already_run();
	int s_fd=creat_socket(8484); //default port number 8484
//	int s_fd=creat_socket(8686); //default port number 8484
	int c_fd;
	char log[100]={};
	struct sockaddr_in c_inf;
	socklen_t size=sizeof(c_inf);
	listen(s_fd,5);
	while(1)
	{
		if((c_fd=accept(s_fd,(struct sockaddr *)&c_inf,&size))>0)
		{
			sprintf(log,"Client accepted %d\n",c_fd);
			log_inf(log);
			if(add_user(c_fd)==-1)
			{
				if(write(c_fd,"request already full,so your request accept fail\n",50)==-1)
					error_log("write function error []\n");
				close(c_fd);
			}
			else if(write(c_fd,"------>login successfully<------\nFTP_$ ",35+6)==-1)
				error_log("write function error []\n");
		}
		recv_cmd();
		check_alive();
		sleep(1);
	}
}
