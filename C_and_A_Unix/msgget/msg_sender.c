/*
	Algorithm:
	``````````
		First I generate the message queue key by ftok() function.

		Create the message queue using msgget()  function.

		That fucion return message queue id.

		Then I get the message from user by stdin and send the message queue id. The message queue assum each data was packets.

		So reader get the packets one by one.
*/

#include<stdio.h>
#include<sys/types.h>
#include<sys/ipc.h>
#include<sys/msg.h>
#include<stdlib.h>
#include<string.h>
int main(void)
{
	int mid,len;
	key_t key;
	char buf[2024]={};
	if((key=ftok("/home",2))==-1) //generate key
	{
		printf("ftok function error\n");
		exit(1);
	}
	if((mid=msgget(key,0666|IPC_CREAT))==-1)//get message queue id
	{
		printf("msgget function error\n");
		exit(1);
	}
	while((len=read(0,buf,2024))>0)
	{
		if(strstr(buf,"::"))	//message format validation
		{
			if(msgsnd(mid,buf,len,IPC_NOWAIT)==-1)	//send data to message queue
				{
					printf("msgsnd function error\n");
					exit(1);
				}
		}else
			printf("Wrong message format usage: username::message\n");
		bzero(buf,2024);
		
	}
}
