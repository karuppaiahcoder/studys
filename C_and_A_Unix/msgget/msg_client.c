/*
program name:ms_client.c
Writer name :Karuppaiah K
usage	    :Receive the server send msg
*/
/*
	algorithm:
	``````````
		First get the port numbet from user by command line arguemnt.

		Create socket:
		`````` ```````
			I create socket by socket function  to client to communicate the server.

			Then get the user name from user using scanf.

		connect server:
		``````` ```````
			When user give the username that time only connect the server by connect function and then send the user name using write function.

		Wait for data: (receive data from server)
		```` ``` `````
			Then client wait and receive the message from server by read function.
*/
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<netinet/in.h>
#include<fcntl.h>
int main(int argc,char *argv[])
{
	char user_name[50]={},buf[2024]={};
	int sfd,len;
	struct sockaddr_in s_inf;
	if(argc!=2)
	{
		printf("Usage :./a.out <port number>\n");
		exit(1);
	}
	if(!isdigit(argv[1][0]))
	{
		printf("Invalid port number\n");
		exit(1);
	}
	s_inf.sin_family=AF_INET;
	s_inf.sin_port=htons(atoi(argv[1]));
	s_inf.sin_addr.s_addr=INADDR_ANY;//inet_addr("192.168.2.60");
	if((sfd=socket(AF_INET,SOCK_STREAM,0))==-1) //create socket
	{
		printf("Socket function error\n");
		exit(1);
	}
	printf("Enter user name: ");
	scanf("%s",user_name);		//get user name
	socklen_t size=sizeof(s_inf);
	if(connect(sfd,(struct sockaddr *)&s_inf,size)==-1) //connect to server
	{
		printf("connect function error\n");
		exit(1);
	}
	if(write(sfd,user_name,strlen(user_name))!=strlen(user_name)) //give the user name to server
	{
		printf("write function error\n");
		exit(1);
	}
	while(read(sfd,buf,2024)>0)	//wait to reaceive the message from server.
	{
		printf("%s",buf);
		bzero(buf,2024);
	}
}
