/*
Program name: msg_server.c
Writer name : Karuappaiah
purpose     : Create server using msgq and TCP concept
*/
/*
	Algorithm:
	``````````
		+ get the port number from user by command line argument.

		+ Create daemon process using six coding rules.

		+ Then verify the already running process.

		+ everything ok means start the server.

		+ The server create socket by socket function and bind by bind function.

		+ Then accept the coming client request, at same time read the message queue by msqrcv function.

		+ I create one structure to store the client information.

			typedef struct{
        			int fd;
			        char name[50];
			}client;

		+ 'fd' its used to store the client socket file descriptor.

		+ 'name' its used for store the user name.

		+ So each user give the user name between two seconds.

		+ I use the msgget function to get the msg id.

		+ when I read a message from message queue, that time I verify the user name and send him socket fd. But user not available in this structure just skipt that message.

		+ Each system call function return values will be check, because  error checking.
	
		+ Per second I check the client status alive or dead. There I use the select function to find which use dead.

		+ Then exist_name() function check the user name already exist or not. User name already exist means, that client can't accept in our server. 

*/

#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<fcntl.h>
#include<unistd.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<netinet/in.h>
#include<sys/ipc.h>
#include<sys/msg.h>
#include<signal.h>
#include<sys/select.h>
#include<sys/time.h>

int e_fd,l_fd;
int error_log(char *ptr)
{
	write(e_fd,ptr,strlen(ptr)); //log the error then exit
	exit(1);
}
int log_info(char *inf)
{
	if(write(l_fd,inf,strlen(inf))!=strlen(inf))  //get log to .msgq.log file
		error_log("Write function error [69]\n");
}
int daemon_init(void)
{
	umask(0);	//change umask zero
	pid_t pid;
	if((pid=fork())<0)
	{
		printf("Fork error [77]\n");
		exit(1);
	}else if(pid>0)	//close the parent 
		exit(1);
	if(setsid()==-1)
	{
		printf("setsid function error [83]\n");
		exit(1);
	}
	if((pid=fork())<0)
	{
		perror("fork error [88]\n");
		exit(1);
	}else if(pid>0)
		exit(1);
	int i=0,fd;
	for(i=0;i<1024;i++)	//close the all descriptor with stdin,stdout and stderr
		if(close(i)==-1)
			perror("close function error [95]\n");
	if((fd=open("/dev/null",O_RDWR))<0)//open /dev/null file to give atdin ,stdout and stderr
	{
		perror("open function error [98]\n");
		exit(1);
	}
	dup2(fd,1);
	dup2(fd,2);
}

int already_run(void)
{
	int pid_fd;
	char pid[10]={};
	struct flock lk;
	lk.l_type=F_WRLCK;
	lk.l_whence=SEEK_SET;
	lk.l_start=0;
	lk.l_len=10;
	lk.l_pid=getpid();

	if((pid_fd=open(".msgq.pid",O_WRONLY|O_CREAT,0666))<0)
		error_log("open error [117]\n");

	if(fcntl(pid_fd,F_SETLK,&lk,NULL)==-1) //lock the file to check already running
		error_log("msgq program already running [120]\n");

	sprintf(pid,"%d",getpid());
	if(write(pid_fd,pid,sizeof(pid))!=sizeof(pid))	//store the process id
		error_log("write function error [124]\n");
}

typedef struct{		//structure use to store client information
	int fd;		//client fd
	char name[50];	//client name
}client;

client table[1024];	//full client entry storage array
int loc=0,max=1004,maxfd;

int send_msg(char *buf)
{
	char *name,cont[2014]={},date_c[30]={},log[2014]={};
	int total=loc,i;
	FILE *fp;
	if((fp=popen("date","r"))==NULL)	//get current date
		error_log("Popen function eror [141]\n");
	if(fgets(date_c,30,fp)==NULL)
		error_log("fgets function error [143]\n");
	if(pclose(fp)==-1)
		error_log("pclose function error [145]\n");
	date_c[strlen(date_c)-1]='\0';
	if(strstr(buf,"::"))
	{
		name=strtok(buf,"::");
		for(i=0;i<total;i++)
		{
			if(!strcmp(table[i].name,name))
			{
				strcpy(cont,strtok(NULL,"\n"));
				cont[strlen(cont)]='\n';
				sprintf(log,"%s::%s:%s",date_c,name,cont);
				if(write(table[i].fd,&cont[1],strlen(&cont[1]))<0) //send mesaage to client
					error_log("write function error [158]\n");
				log_info(log);	//get log
				return;
			}
		}
	}else
	{
		sprintf(log,"%s::Wrong format msg\n",date_c);
		log_info(log);
		return;
	}
	sprintf(log,"%s::warning %s-this user not exist\n",date_c,name);
	log_info(log);
}

int exist_name(char *u_name,int cfd) //check user name unique
{
	int total=loc,i;
	for(i=0;i<total;i++)
	{
		if(!strcmp(table[i].name,u_name))
		{
			if(write(cfd,"This username already exist\n",29)<0)
				error_log("write function error [181]\n");
			close(cfd);
			return -1;
		}
	}
	return 0;
}
		
int client_status(void)
{
	fd_set rfd;
	struct timeval time_v;
	int i,total=loc,j;
	FD_ZERO(&rfd);
	for(i=0;i<total;i++)
		FD_SET(table[i].fd,&rfd);
	time_v.tv_sec=1;
	time_v.tv_usec=2;
	
	if(select(maxfd+1,&rfd,NULL,NULL,&time_v)>0) //check client exit status
	{
		for(i=0;i<total;i++)
		{
			if(FD_ISSET(table[i].fd,&rfd))
			{
				close(table[i].fd);
				for(j=i;j<total;j++)
				{
					table[j].fd=table[j+1].fd;
					strcpy(table[j].name,table[j+1].name);
				}
				i--;
				total--;
			}
		}
	}
	loc=total;
}

int main(int argc,char *argv[])
{
	if(argc!=2)
	{
		printf("Usage :./a.out <port number>\n");
		exit(1);
	}
	if(!isdigit(argv[1][0]))
	{
		printf("Invalid port number\n");
		exit(1);
	}
	daemon_init();
	if((e_fd=open(".err_msgq.log",O_WRONLY|O_APPEND|O_CREAT,0666))<0) //log the all error
	{
		perror("open function error [235]\n");
		exit(1);
	}
	already_run();
	int sfd,mid,cfd;
	struct sockaddr_in s_inf,c_inf;
	socklen_t size;
	char buf[2024]={},u_name[50]={};
	key_t key;
	if((key=ftok("/home",2))==-1)
		error_log("ftok function error [245]\n");
	if((mid=msgget(key,IPC_CREAT))==-1) 	//get the mesage queue id
		error_log("msgget function error [247]\n");
	if((l_fd=open(".msgq.log",O_WRONLY|O_APPEND|O_CREAT,0666))<0) //log the all information
		error_log("haert.log file open error [249]\n");
	s_inf.sin_family=AF_INET;
	s_inf.sin_port=htons(atoi(argv[1]));
	s_inf.sin_addr.s_addr=INADDR_ANY;
	if((sfd=socket(AF_INET,SOCK_STREAM,0))<0)
		error_log("socket function error [254]\n");
	if(bind(sfd,(struct sockaddr *)&s_inf,sizeof(s_inf))==-1)
		error_log("Bind function error [256]\n");
	size=sizeof(s_inf);
	if(fcntl(sfd,F_SETFL,O_NONBLOCK,NULL)<0) //Set nonflock mode t server socket
		error_log("fcntl function error [159]\n");
	listen(sfd,5);
	while(1)
	{
		if((cfd=accept(sfd,(struct sockaddr *)&c_inf,&size))>0)
		{
			if(cfd>maxfd)
				maxfd=cfd;
			sleep(1);
			fcntl(cfd,F_SETFL,O_NONBLOCK,NULL);
			bzero(u_name,50);
			if(read(cfd,u_name,50)>0 && cfd < max)
			{
				if(exist_name(u_name,cfd)==-1)
					goto L;
				if(strlen(u_name)<=25) //check user name length less then 25
				{
					table[loc].fd=cfd;
					bzero(table[loc].name,50);
					strcpy(table[loc].name,u_name);
					loc++;
				}else
				{
					if(write(cfd,"Disconnected (username 25 character)\n",40)<0)
						error_log("write function error [283]\n");
					close(cfd);
				}
			}else
			{
				if(write(cfd,"You are disconnect----> you not give username\n",47)<0)
					error_log("write function error [289]\n");
				close(cfd);
			}
		}
	L:	if(msgrcv(mid,buf,2024,0,IPC_NOWAIT)!=-1)
		{
			send_msg(buf);
			bzero(buf,2024);
		}
		client_status();
	}
}
