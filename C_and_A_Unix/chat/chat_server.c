/*
	Chat application program
	````````````````````````
Algorithm:
``````````
	server:
	```````
		> The server just do a data transfering between two or more users or clients.
		> Actually this server create using TCP concept.
		> The server do this work using following functions.
		Daemonts:
			> The server run daemon process, so I should be create the single instend.
			> Then daemons process create with 6 code rules like,
				umask(0) =>Set the umask zero
				Create the fork and then kill the parent.
				Then create the new session by setsid() function.
				Then create another one fork and kill parent process.
				Then close the all descriptors.
				Change the current working directory.
		Socket:
			> create the sockets by socket system call function.
			> The server accept the any client request so I send the server IP 0.0.0.0
			> Then port number is get from user by command line arguments.
		bind:
			> Then the bind() system call function used to bind the socket with name.
		accept:
			> The accept function every time check the server socket to any client request come this port.
		> Should be nonblock the socket address using fcntl function.
*/
#include<stdio.h>
#include<signal.h>
#include<stdlib.h>
#include<unistd.h>
#include<fcntl.h>
#include<string.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<netinet/ip.h>
#include<sys/select.h>
#include<sys/time.h>
#define DAEMON_PATH "."
int e_fd,l_fd;
void error_log(char *ptr)
{
	write(e_fd,ptr,strlen(ptr));
	exit(1);
}

void log_inf(char *sed,char *rev,char *msg)
{
	char date_c[50]={},data[2064]={};
	FILE *fp;
	/*get current date and time*/
	if((fp=popen("date","r"))==NULL)
		error_log("popen function error [55]\n");
	fgets(date_c,50,fp);
	pclose(fp);
	sprintf(data,"%s:%s:%s:%s\n",sed,rev,msg,date_c);
	if(write(l_fd,data,strlen(data))==-1)
		error_log("write function error [60]\n");
}

int daemon_init()
{
	umask(0); //set  the umask value 0.
	pid_t pid;
	int i,fd;
	if((pid=fork())==-1)
	{
		printf("Fork function error [70]\n");
		exit(1);
	}else if(pid > 0)	//kill the parent process.
		exit(0);
	if(setsid()==-1)  //change new session
	{
		printf("Setsid function error [76]\n");
		exit(1);
	}
	if((pid=fork())==-1)
	{
		perror("Fork function error [81]\n");
		exit(1);
	}else if(pid>0)	//kill the parent process.
		exit(0);
	for(i=0;i<1024;i++)	//close the all descriptors.
		close(i);
	if(chdir(DAEMON_PATH)==-1)
	{
		perror("Chdir function error [89]\n");
		exit(1);
	}
	if((fd=open("/dev/null",O_RDWR))==-1) //open the stdin,stdout and stderr from /dev/null.
	{
		perror("Open function error [94]\n");
		exit(1);
	}
	dup2(fd,1);
	dup2(fd,2);
}

int already_run()
{
	int fd;
	char pid[20]={};
	struct flock lk;
	lk.l_type=F_WRLCK;
	lk.l_whence=SEEK_SET;
	lk.l_start=0;
	lk.l_len=10;
	lk.l_pid=getpid();
	if((fd=open(".chat.pid",O_CREAT|O_WRONLY,0666))==-1)
		error_log("open function error [112]\n");
	if(fcntl(fd,F_SETLK,&lk)==-1)
		error_log("Chat server already running [114]\n");
	ftruncate(fd,0);
	sprintf(pid,"%d",getpid());
	if(write(fd,pid,strlen(pid))==-1)
		error_log("Write function error [118]\n");
}

void verify_port(char *port)	//verify port number only
{
	int i=0;
	while(port[i]!='\0')
		if(!isdigit(port[i++]))
		{
			printf("%s invalid port number [127]\n",port);
			exit(1);
		}
}

int create_socket(char *port)		//create server socket
{
	int s_fd;
	struct  sockaddr_in s_inf;
	/*get server socket TCP fd*/
	if((s_fd=socket(AF_INET,SOCK_STREAM,0))==-1)
		error_log("Socket function error [138]\n");
	s_inf.sin_family=AF_INET;
	s_inf.sin_port=htons(atoi(port));
	s_inf.sin_addr.s_addr=INADDR_ANY;
	if(bind(s_fd,(struct sockaddr *)&s_inf,sizeof(s_inf))==-1)
		error_log("Bind function error [143]\n");
	if(fcntl(s_fd,F_SETFL,O_NONBLOCK)==-1)
		error_log("fcntl function error [145]\n");
	return s_fd;
}

typedef struct{
	int fd;		//each client have unique fd
	char name[50];  //each client have unique name
}c_store;

c_store c_table[1024];	//client details
int tloc=0,maxfd,maxc=1004;

int exist(char *name)	//check user name already exist
{
	int i,total=tloc;
	for(i=0;i<total;i++)
	{
		if(!strcmp(c_table[i].name,name))
			return 1;
	}
	return 0;
}

int add_name(int l,char *name)		//check valid name
{
	if(name[strlen(name)-1]=='\n')
		name[strlen(name)-1]='\0';
	if(strlen(name)>15)
	{
		if(write(c_table[l].fd,"Username too long try again\n\nEnter user name (1 to 15):\n",31+29)==-1)
			error_log("Write function error [175]\n");
	}else
	{
		if(exist(name)==0)
		{
			strcpy(c_table[l].name,name);
			if(write(c_table[l].fd,"----->login successfully<-----\n",32)==-1)
				error_log("Write function error [182]\n");
		}
		else
		{
			if(write(c_table[l].fd,"Username already exist try again\n\nEnter user name (1 to 15):\n",36+29)==-1)
				error_log("Write function error [187]\n");
		}
	}
}

int kill_client(int l)
{
	int total=tloc,i,j;
	close(c_table[l].fd);
	for(i=l,j=l+1;j<total;i++,j++)	//close the exit user fd
	{
		c_table[i].fd=c_table[j].fd;
		strcpy(c_table[i].name,c_table[j].name);
	}
	tloc--;
}

int ls(int fd,int l)
{
	int i=0,total=tloc,reg;
	char name[50]={};
	if(total!=1)
	{
		for(i=0,reg=1;i<total;i++,reg++)	//online user name list.
		{
			sprintf(name,"%d) %s\n",reg,c_table[i].name);
			if(strcmp(c_table[i].name,c_table[l].name))
			{
				if(write(fd,name,strlen(name))==-1)
					error_log("write function error [216]\n");
			}else
					reg--;
			bzero(name,50);
		}
	}else
		if(write(fd,"You only online right now\n",27)==-1)
			error_log("write function error [223]\n");
}
/* user command list and exit*/
int cmd(char *buf,int l)
{
	if(buf[strlen(buf)-1]=='\n')
		buf[strlen(buf)-1]='\0';
	if(!strcmp(buf,"exit"))
		kill_client(l);
	else if(!strcmp(buf,"list"))
	{
		ls(c_table[l].fd,l);
	}
	else	
		return 0;
	return 1;
}

int send_c(char *cont,int sl)
{
	char *rev,*data;
	char msg[2014]={};
	int i,total=tloc;
	if(strstr(cont,":"))
	{
		rev=strtok(cont,":");
		data=strtok(NULL,"\n");
		for(i=0;i<total;i++)
		{
			if(!strcmp(c_table[i].name,rev))	//one to one chatting
			{
				sprintf(msg,"\t%s:%s\n",c_table[sl].name,data);
				if(write(c_table[i].fd,msg,strlen(msg))==-1)
					error_log("write function error [256]\n");
				log_inf(c_table[sl].name,rev,data);
				break;
			}
		}
		if(i==total)
		{
			sprintf(msg,"\t %s was off line,So msg send fail\n",rev);
			if(write(c_table[sl].fd,msg,strlen(msg))==-1)		//user not exist
				error_log("write function error [265]\n");
		}
			
	}else
	{
		sprintf(msg,"\t%s:%s\n",c_table[sl].name,cont);	
		for(i=0;i<total;i++)				//broad coast the message
		{
			if(strcmp(c_table[i].name,c_table[sl].name))
				if(write(c_table[i].fd,msg,strlen(msg))==-1)
					error_log("write function error [275]\n");
		}
		log_inf(c_table[sl].name,"broadcast",cont);
	}
}

int recv_send_msg(void)
{
	fd_set rfd;
	int i,total=tloc,st;
	char buf[2024]={};
	struct timeval t;
	FD_ZERO(&rfd);
	for(i=0;i<total;i++)
		FD_SET(c_table[i].fd,&rfd);
	t.tv_sec=1;
	t.tv_usec=1;
	if(select(maxfd+1,&rfd,NULL,NULL,&t)>0)
	{
		for(i=0;i<total;i++)
		{
			if(FD_ISSET(c_table[i].fd,&rfd))
			{
				if((st=read(c_table[i].fd,buf,2024))>0)
				{
					if(c_table[i].name[0]=='\0')	//get user name
						add_name(i,buf);
					else
					{
						if(cmd(buf,i)==0)	//check msg command or not "list and exit"
							send_c(buf,i);
					}
				}else if(st==0)
					kill_client(i);	//client exit manualu using ctrl+c
				bzero(buf,2014);
			}
		}
	}
}

int main(int argc,char *argv[])
{
	if(argc!=2 || !isdigit(argv[1][0]))
	{
		printf("Usage : ./a.out <port number>\n");
		exit(1);
	}
	verify_port(argv[1]);
	daemon_init();
	/* Open  .chat_err.log file to log the all errors */
	if((e_fd=open(".chat_err.log",O_CREAT|O_WRONLY|O_APPEND,0666))==-1) //error log file
	{
		perror("open function error [327]\n");
		exit(1);
	}
	/*log file format "sender:receiver:messag:date" */
	if((l_fd=open(".chat.log",O_CREAT|O_WRONLY|O_APPEND,0666))==-1) //message send process log file
	{
		perror("open function error [333]\n");
		exit(1);
	}
	already_run();		//check already running process.
	int s_fd,c_fd;
	struct  sockaddr_in c_inf;
	socklen_t size=sizeof(c_inf);
	s_fd=create_socket(argv[1]);
	if(signal(SIGPIPE,SIG_IGN)==SIG_ERR)
		error_log("Signal function error [342]\n");
	listen(s_fd,10);
	while(1)
	{
		if((c_fd=accept(s_fd,(struct sockaddr *)&c_inf,&size))>0) //accept client every time
		{
			if(c_fd>=maxc)	//check maximum clinet count
			{
				if(write(c_fd,"Sorry, Client already full\n",28)==-1)
					error_log("write function error [351]\n");
				close(c_fd);
				goto L;
			}
			if(write(c_fd,"Enter user name (1 to 15):\n",28)==-1)
				error_log("Write function error [356]\n");
			c_table[tloc].fd=c_fd;
			if(c_fd>maxfd)		//longest fd to select function
				maxfd=c_fd;
			bzero(c_table[tloc++].name,50);
		}
		L:recv_send_msg();
	}
}
