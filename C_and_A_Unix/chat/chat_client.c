/*
	chat application client program

	Algorithm:
	``````````
		socket: =>The socket function create new socket to communicate the other computer.

		fcntl:	=>Non block the socket fd and stdin fd, because don't wait for read function.

		select:	=>The select just when server send message or when read function ready to read.

		read and write:
			=>read and write function used to read the data and send the data between server and client.

*/

#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<fcntl.h>
#include<string.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<netinet/ip.h>
#include<sys/select.h>
#include<sys/time.h>
void verify_port(char *port)
{
	int i=0;
	while(port[i]!='\0')
	if(!isdigit(port[i++]))
	{
		printf("%s invalid port number \n",port);
		exit(1);
	}
}

int create_socket(char *port)
{
	int s_fd;
	struct  sockaddr_in s_inf;
	if((s_fd=socket(AF_INET,SOCK_STREAM,0))==-1)
	{
		printf("socket function error\n");
		exit(1);
	}
	s_inf.sin_family=AF_INET;
	s_inf.sin_port=htons(atoi(port));
	s_inf.sin_addr.s_addr=INADDR_ANY;
	if(connect(s_fd,(struct sockaddr *)&s_inf,sizeof(s_inf))==-1)
	{
		printf("connect function error\n");
		exit(1);
	}
	if(fcntl(s_fd,F_SETFL,O_NONBLOCK)==-1)
	{
		printf("fcntl function error\n");
		exit(1);
	}
	return s_fd;
}


int main(int argc,char *argv[])
{
	if(argc!=2 || !isdigit(argv[1][0]))
	{
	printf("Usage : ./a.out <port number>\n");
	exit(1);
	}	
	verify_port(argv[1]);
	int s_fd;
	s_fd=create_socket(argv[1]); //create the client socket
	if(fcntl(0,F_SETFL,O_NONBLOCK)==-1)
	{
		printf("fcntl function error\n");
		exit(1);
	}
	fd_set rfd;
	int i,len;
	char buf[2024]={},cont[2024]={};
	struct timeval t;
	printf("\nWelcome to chat application\n\thelp:-\n\t\tlist ->list online user name\n\t\texit ->logout\n");
	printf("\nChatting format\n\tsingle client:-\n\t\tusername:message\n\tbroadcast :-\n\t\tmessage\n\n");
	while(1)
	{
		FD_ZERO(&rfd);
		FD_SET(s_fd,&rfd);
		FD_SET(0,&rfd);
		t.tv_sec=1;
		t.tv_usec=0;
		if(select(s_fd+1,&rfd,NULL,NULL,&t)>0)
		{
			if(FD_ISSET(s_fd,&rfd))
			{
				if(read(s_fd,buf,2024))
					printf("%s",buf);
				else
				{
					printf("Connection closed by foreign host\n");
					exit(1);
				}
				bzero(buf,2024);
			}
			if(FD_ISSET(0,&rfd))
			{
				if((len=read(0,cont,2024))>1)
					if(write(s_fd,cont,len)==-1)
					{
						printf("Write function error\n");
						exit(1);
					}
				bzero(cont,2024);
			}
		}
	}
}
