# Write a program which gets C file(s) and/or shell script(s) as command line arguments. Remove the comments from those files.
#        - Handle multi-line comments and nested comments.

#!/bin/bash
len=$#;
if [ $len -eq 0 ]
then 
	echo "Please give any script or C programming files";
	exit 1;
fi

func_c()
{
	file=$1;
	use=0;
	while read line
	do
		c1=0;c2=0;c3=0;
		#echo $line
		echo "$line"|grep "//">/dev/null;
		s1=$?
		echo "$line"|grep "/\*">/dev/null;
		s2=$?
		echo "$line"|grep "\*/">/dev/null;
		s3=$?
		val=0;
		if [ $s1 -eq 1 ] && [ $s2 -eq 1 ] && [ $s3 -eq 1 ]
		then
			val=1;
		fi
		if [ $val -eq 0 ]
		then
			for i in "$line"
			do
#				echo [$i]
				echo "$i"|grep "//">/dev/null;
				c1=$?;
				echo "$i"|grep "/\*">/dev/null;
				c2=$?;
				echo "$i"|grep "\*/">/dev/null;
				c3=$?;
				if [ $c1 -eq 0 ] && [ $use -eq 0 ]
				then
					use=0;
					echo "$line"|sed 's/\/\//@/g'|cut -d'@' -f1
					break;
				elif [ $c2 -eq 0 ] && [ $use -eq 0 ]
				then
					use=2;
					echo "$line"|sed 's/\/\*/@/g'|cut -d'@' -f1
					break;
					
				elif [ $c3 -eq 0 ] && [ $use -eq 2 ]
				then
					use=0;
					echo "$line"|sed 's/\*\//@/g'|cut -d'@' -f2
					break;
				fi
			done
			
			elif [ $use -eq 0 ] && [ $val -eq 1 ]
			then	
				echo "$line"
				val=0;
			fi
	done < $file
}


func_s()
{
	file=$1;
		
	while read line
	do
		#echo "$line"
		echo "$line"|grep -E "^[#][^\!]|[ +][#][^\!]">/dev/null;
	#c1=$?;
		if [ $? == 0 ];
		then
			echo $line  | cut -d'#' -f1;

		else
			echo $line;
		fi
		
	done < $file
}


for i in $*
do
	echo $i;
	file $i|grep "shell" > /dev/null;
	f=$?;
	
	if [[ $i == *.c ]]
	then
#		echo "Hello C"
		func_c $i;
	elif [ $f -eq 0 ]
	then
#		echo "Hello script"
		func_s $i;
	else
		echo "This is a Text"
	fi
done

