# Given a process id or multiple process id as command line arguments, tabulate with below details,
#       - Process ID
#       - Command name and the complete command line arguments passed to that process.
#       - The Present/Current working directory.
#       - Number of file's opened by the process, and the file path's separated by |.

#!/bin/bash
arg=$#;
echo "$arg";
if [ $arg -eq 0 ]
then
	echo "Give process id in Command line argument";
	exit 0;
fi

for i in $*
do
	echo "$i"
	cd /proc;
	if [ -d "$i" ]
	then
		cd $i;
		echo -e "\tPID:\t$i\t\t\t\tCMD line:`cat cmdline`";
		link=`readlink -f cwd`;
		echo -e "\n\tWorking directory:$link";
		echo -e "\n\n\t.................Open Files.....................";
		cd fd;
		for j in $ls*
		do
			if [ $j == '0' ]
			then
				echo -e "\t\t\tstdin"; 
			elif [ $j == '1' ]
			then
				echo -e "\t\t\tstdout";
			elif [ $j == '2' ]
			then
				echo -e "\t\t\tstderr";
			else
				echo -e "\t\t\t`readlink -f $j`";
			fi
		done
		echo -e "\t.................................................";

	else
		echo "No such file or directory"
	fi
done
