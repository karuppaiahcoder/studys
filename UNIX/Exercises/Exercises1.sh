#!/bin/bash

# 1) Automatically Decompressing Files

#    Given a list of filenames as input, this script queries each target file (parsing the output of the file command) for the type of compression used on it. Then the script automatically invokes the appropriate decompression command (gunzip, bunzip2, unzip, uncompress, or whatever). If a target file is not compressed, the script emits a warning message, but takes no other action on that particular file.


length=$#;
i=1;
while [ $length -ge $i ]
do
	filename=$(eval echo "\$$i");
	ftype=$(file $filename | cut -d":" -f2 | cut -d" " -f2);
	case "$ftype" in
	
		bzip2)
			echo "Decompress the bzip2 file";
			bunzip2 -d $filename;
			;;
		gzip)
			echo "Decompress the gzip file";
			gunzip $filename;
			;;
		*)
			echo "($filename) It is not compress file";
			;;
	esac
	let i=$i+1;
done
