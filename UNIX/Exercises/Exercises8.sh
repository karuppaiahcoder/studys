
# 2.)Parsing
#                parse /etc/passwd , and output its contents in nice , easy-to-read tabular form .

#!/bin/bash
cat /etc/passwd > file.txt;

cut -d":" -f1,3,4 <file.txt|tr ":" "\n"|pr -t -a -3 > sample.txt;
cut -d":" -f5,6,7 <file.txt|tr ":" "\n"|pr -t -a -3 >sample1.txt;

echo -e "USER\t\t\tUID\t\t\tGID\tPROCESS\t\t\tPATH\t\t\tSHELL";
echo -e "~~~~\t\t\t~~~\t\t\t~~~\t~~~~~~~\t\t\t~~~~\t\t\t~~~~~";
paste sample.txt sample1.txt

rm file.txt sample.txt sample1.txt;
