# Find all the broken links that are available from a directory recursively. If no arguments is passed, the script should act on current directory, if argument is passed, then it should act on that given directory.

#!/bin/bash

len=$#;
echo "$len";
if [ $len -eq 0 ]
then
	find . -type l -exec file {} \; | grep "broken"
else
	find $1 -type l -exec file {} \; | grep "broken"
fi
	
