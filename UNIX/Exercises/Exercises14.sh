#Write a script to implement stack operations in bash like,
#        - Push
#        - Pop
#        - list
#        ( Handle stack empty and full conditions ).
#        By default 50 elements can be added into stack. It can be changed via command line argument -n

#!/bin/bash
declare array;
declare id=0;

push()
{
	if [ $id -le 50 ]
	then
	array[$id]=$1;
	let id=$id+1;
	else
		echo "Stack full (max 50)"
	fi
}

pop()
{
	if [ $id -eq 0 ]	
	then
		echo "Stack empty"
	else
		let id=$id-1;
		echo "${array[$id]}"
	fi
}

while :
do
	echo -e "\t1.push\n\t2.pop\n\t3.exit"
	read opt;

	if [[ "$opt" == "1" ]]
	then
		echo "Enter the value"
		read val
		push $val;

	elif [[ "$opt" == "2" ]]
	then
		pop;

	elif [[ "$opt" == "3" ]]
	then
		echo -e "\t\tThank you"
		exit 0;
	else
		echo "Invalid option!!!!"
	fi
done

