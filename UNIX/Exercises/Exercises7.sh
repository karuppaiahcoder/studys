# 1.)Integer or String
#	Write a script function that determine if an argument passed to it is an integer or a string . the function will return TRUE(0) if passed an integer , and FALSE (1) if passed a string .

#       Hint:
#                + What does The following expression return when $1 is not an integer .

#                                expr $1 + 0

#!/bin/bash
expr $1 + 0 2>/dev/null 1>/dev/null
val=$?;
if [[ $val -eq 2 ]]
then 
	echo "False";
else
	echo "True";
fi
