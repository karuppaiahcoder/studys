#!/bin/bash

#5) To Write the shell program which produces a report from the output of ls -l in the following format about current directory.
#	       Explanation:
#       	       1.To produces a report for the output of ls -l command.
#    		       2.The report should gives the information by the following.
#               	     i.No.of Regular files in a current directory
#	                     ii.No.of directories in a current directory
#         		     iii.No.of symbolic link files in a current directory
#                            iv.Then the Total size of regular files in a current directory.

#!/bin/bash

reg=0;size=0;sym=0;dir=0;

for i in `ls -F`
do
        val=${i: -1};
        if [ $val == "/" ]
        then
                let dir=$dir+1;
        elif [ $val == "@" ]
        then
                let sym=$sym+1;
        else
                let reg=$reg+1;
		temp=$(ls -l $i|tr -s " " ":" |cut -d":" -f5)
		let size=$size+$temp;
        fi
done

echo "directory count           = $dir"
echo "regular file count        = $reg"
echo "regular file size         = $size"
echo "symbolic file count       = $sym"

