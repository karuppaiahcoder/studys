#  3.)Testing Passwords

#                Write  a script to check and validate passwords . The object is to flag "weak" or easily guessed password Candidates.
#                A trial password will be input to the script as a command-line parameter.
#                To be considered acceptable , a password must be the following qualifications :

#                        + Minimum length of 8 characters
#                        + Must contain at least one numeric character .
#                        + must Contain at least one of the following non-alphabetic characters
#                                        @, # ,$ ,% ,&, *, + , - , = .

#                optional :

#                        + Do a dictionary check on every sequence of at least four consecutive alphabetic characters in the
#                password under test . This will eliminate passwords containing embedded "words" found in a standard dictionary .

#                        + Enable the script to check all the passwords on your system . These do not reside in /etc/password .

#                        + This exercise tests mastery of Regular Expressions .

#!/bin/bash

mylen=${#1};
if [[ $mylen -lt 7 ]]
then
	echo "minimum 8 character"
	exit 1;
fi

echo "$1"|grep "[0-9]">/dev/null

num="$?";

echo "$1"|grep "[[:punct:]]">/dev/null

pun="$?";

if [ $num -eq 0 -a $pun -eq 0 ]
then
	echo "$1 This Password validate";
else
	echo "$1 This Password Not valid";
	echo "Password format least 8character with number and spacial character also!!!";
fi
