#!/bin/bash

# 4) To create a clock.
#        Explanation:

#                1.To create a clock.
#                2.The clock should be displayed in the center or corner of the screen.
#                3.Screen should  contains only the clock.All other things should be cleared.
#                3.And the time should be updated.


while : 
do
	setterm -reset;
	clear;
	row=`tput lines`
	column=`tput cols`
	let row=$row/2;
	let column=$column/2;
#	echo "row=$row column=$column"
	tput cup $row $column;
	date +%T;
	sleep 1;
	trap "reset;exit;" SIGINT
done
