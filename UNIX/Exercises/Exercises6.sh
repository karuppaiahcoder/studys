#!/bin/bash

# 6) To Write a script that will count the number of files in each of your subdirectories.
#        Explanation:

#                1.To count the number of subdirectories and files in a given directory.
#                2.If the argument is passed then it performs the operation for that particular directory.
#                3.If the argument is Omitted then it performs the operation for home directory.
#                4.The operation should be performed recursively

len=$#;
if [ $len -gt 0 ]
then
	path=$1;
else
	path=$HOME;
fi
fcount=0;dcount=0;

fcount=$(find $path -type f 2>/dev/null | wc -l)
dcount=$(find $path -type d 2>/dev/null | wc -l)

if [ $fcount -gt 0 ] || [ $dcount -gt 0 ]
then
	echo "Total file count      = $fcount"
	echo "Total directory count = $dcount"
else
	echo "Path error"
fi
