#!/bin/bash
#<< comm
#	Algorithm:
#	----------
#		-First get the two valid date.
#comm
trap "return" 2

declare array=(0 31 28 31 30 31 30 31 31 30 31 30 31)
declare -i day
declare -i mon
declare -i year
valid()
{
	dd=$1;
	echo "$dd"|grep -E "([0-2][0-9]|3[0-1])/(1[0-2]|0[0-9])/....">/dev/null
	if [ $? -eq 0 ]
	then
		day=`echo "$dd"|cut -d'/' -f1|bc`
		mon=`echo "$dd"|cut -d'/' -f2|bc`
		year=`echo "$dd"|cut -d'/' -f3|bc`
		opt=${array[$mon]}
		if [ $day -gt $opt ]
		then
			echo "Invalid date format"
			exit 0;
		fi
	else
		echo "Invalid date format"
		exit 0;
	fi
}

leaf=0;

leaf_year()
{
	ly=$1;
	let val=$ly%4;
	if [ $val -eq 1 ]
	then
		let leaf=$leaf+1;
	fi
}

echo "Enter the first date"
read fdate
valid $fdate
fd=$day;fm=$mon;fy=$year;

echo "Enter the second date"
read sdate
valid $sdate
sd=$day;sm=$mon;sy=$year

let days=${array[$fm]}-$fd;
let days=$days+1

if [ $fm -eq 12 ]
then
	fm=1
	let fy=$fy+1;
else
	let fm=$fm+1;
fi

if [ $sm -eq 1 ]
then
	use=$sm
	sm=12;
	let sy=$sy-1;
else
	use=$sm
	let sm=$sm-1;
fi

while :
do
	let days=$days+${array[$fm]};
	if [ $fy -ge $sy ] && [ $fm -ge $sm ]
	then
		break;
	fi
	if [ $fm -eq 12 ]
	then
		fm=1;let fy=$fy+1;
		let days=$days+${array[$fm]};
		leaf_year $fy
	fi
	let fm=$fm+1;
#echo $days
done

sm=$use;
#let days=$days+${array[$sm]};
let days=$days+$sd;

let days=$days+$leaf
echo $days
