
<<comm
4. Optimize html file ( 15 )
        script name:    htmloptim
        purpose:        optimize html file by reducing its size
        version:        1.0
        usage:          htmloptim [-hlrdspbtm] <file> [file...]

        options:
                -r, replace the input file, default output is stdo
                -d, Removing new line
                -t, remove empty space between > and < (end and begin tag indicators)
                -s, remove leading spaces and tabs
                -p, remove trailing spaces and tabs
                -b, remove blank lines and lines containing only spaces or tabs
                -h, usage and options (help)
                -l, see this script"

        manual:

        DESCRIPTION

        htmloptim reduces the size of html file by optimizing its content. This is
        done by removing unnecessary characters like spaces, tabs, line feeds or
        blank lines.

        If the -r option is used, the optimized version will replace the original
        only if it is smaller.

        Contents between <pre> <listing> <plaintext> and <xmp> tags should not be touched for any reason.
comm

#!/bin/bash

new_line()
{
	echo "vijay"
	echo "$1"|grep -E "\-.*d">/dev/null
	if [ $? -eq 0 ]
	then
		for i in $*
		do
			val=`echo "$i"|grep -E "\-.*"`
			len=${#val}
			if [ $len -ge 1 ]
			then
				continue;
			fi
			if [ -e "$i" ]
			then
				cat $i|tr "\n" " "
			else
				echo "$i  File did not exist"
			fi
		done
		echo " "
	fi
}
<< comm
remove_empty()
{
	echo "$1"|grep -E "\-.*t">/dev/null
	
	if [ $? -eq 0 ]
	then
#		for i in $*
#		do
			
#		done
	fi
}
comm

remove_leting()
{
	touch output.txt
	echo "$1"|grep -E "\-.*s">/dev/null
	if [ $? -eq 0 ]
	then
		for i in $*
		do
			if [[ "$i" == "$1" ]]
			then
				continue
			fi
		while read line
		do
			declare array
			use="0";	
			k=0;
			for j in $line
			do
				if [[ "$j" == " " ]] && [[ "$j" == "\t" ]] && [[ "$use" == "0" ]]
				then
					continue
				else
					use="1";
					array[$k]=$j;
					let k=$k+1;
					fi
			done
				echo "${array[*]}" >>output.txt
		done <$i
		done
	fi
	cat output.txt
	rm output.txt
}

remove_trailing()
{
	echo "$1"|grep -E "\-.*p">/dev/null
	
	if [ $? -eq 0 ]
	then
		for i in $*
		do
			if [[ "$i" == "$1" ]]
			then
				continue
			fi
		while read line
		do
			declare array
			tra=0;
			k=0;
			for j in $line
			do
				if [[ "$j" == " " ]] || [[ "$j" == "\t" ]]
				then
					let tra=$tra+1;
				else
					tra=0;
				fi
				array[$k]=$j;
				let k=$k+1;
			done
			declare temp
			j=0;let k=$k-$tra;
			while :
			do
				temp[$j]=${array[$j]}
				let j=$j+1
				if [ $j -ge $k ]
				then
					break;
				fi
			done
		#	echo "[${temp[*]}]"  #>>output.txt
			echo "[${array[*]}]"  #>>output.txt
		done <$i
		done
		
	fi
}

remove_blanck()
{
	echo "$1"|grep -E "\-.*b">/dev/null
	if [ $? -eq 0 ]
	then
		for i in $*
		do
			val=`echo "$i"|grep -E "\-.*"`
			len=${#val}
			if [ $len -ge 1 ]
			then
				continue;
			fi
			if [ -e "$i" ]
			then
				cat $i|grep -E "[[:alnum:]]"
			else
				echo "$i  File did not exist"
			fi
		done
		
	fi
}

help()
{
	echo "$1"|grep -E "\-.*h">/dev/null
	
	if [ $? -eq 0 ]
	then
		echo "
        options:
                -r, replace the input file, default output is stdo
                -d, Removing new line
                -t, remove empty space between > and < (end and begin tag indicators)
                -s, remove leading spaces and tabs
                -p, remove trailing spaces and tabs
                -b, remove blank lines and lines containing only spaces or tabs
                -h, usage and options (help)
                -l, see this script"
	fi
}

view_self()
{
	echo "$1"|grep -E "\-.*l">/dev/null
	
	if [ $? -eq 0 ]
	then
		file=`echo "$0"|cut -d'/' -f2`
		echo "$file"
		cat $file
	fi
}

len=${#*}
echo $len
if [ $len -ge 1 ]
then
	new_line $*
#	remove_empty $*
	remove_leting $*
	remove_trailing $*
	remove_blanck $*
	help $*
	view_self $*
else
	echo "Give any html file in command line argument"
fi
