
<< comm
1. Implement a logrotate mechanism where-in the script monitors a given directory for any files ending with '.log'. If any '.log' file is growing more than 5MB (can be passed as input from cmdline), rotate the logfile like

        Assuming no other log rotates happened,
                - <FILE>.log will be <FILE>.log.1

        Next time, when <FILE>.log reached 5MB,
                - <FILE>.log.1 will be compressed into <FILE>.log.2.gz
                - <FILE>.log will become <FILE>.log.1
        Next time, when <FILE>.log reached 5MB,
                - <FILE>.log.2.gz will become <FILE>.log.3.gz
                - <FILE>.log.1 will be compressed into <FILE>.log.2.gz
                - FILE>.log will become <FILE>.log.1
        This has to happen continuously. When <FILE>.log.10.gz is present, and once again <FILE>.log reached 5MB, then remove the <FILE>.log.10.gz, and move <FILE>.log.9.gz as <FILE>.log.10.gz
comm
#!/bin/bash
size="5M"
if [ $# -eq 0 ] || [ $# -gt 2 ]
then
	echo -e "\n\nGive the input (directory path) from command line argument:\n\n"
	echo -e "\nYou given Input must be like this formet:\n\n\t\tlog_file_update.sh Directory Path [-nK(or)M]\n\n"
	exit 2;
fi
cd $1 2>/dev/null
if [ $? -eq 1 ]
then
	echo -e "\n\t\t \"$1\" No such file or directory\n"
	exit 2;
fi
echo $2 |grep -E "^[0-9]+[MKmk]$">/dev/null
if [ $? -eq 1 ] && [ $# -eq 2 ]
then
	echo -e "\nSize only MB (M or m)  or KB (k or K)\n"
	echo -e "\n\n===>So now default size 5M<========\n"
elif [ $# -eq 2 ]
then
	opt=`echo $2|tr -d "[0-9]"`
	num=`echo $2|tr -d "[KkMm]"`
	if [[ $opt == "K" ]] || [[ $opt == "k" ]]
	then
		size=`echo $num k|tr -d " "`
	else
		size=`echo $num M|tr -d " "`
	fi	
fi
echo $size
while :
do
	files=`find . -maxdepth 1 -type f -name "*.log" -size +$size|cut -d'/' -f2`
	for i in $files
	do
		temp=`find . -maxdepth 1 -type f -name "$i.*"|cut -d'/' -f2|sort -t'.' -k3 -n -r`
		count=`echo $temp|tr " " "\n"|wc -l`
		let count=$count+1;
		for j in $temp
		do
			if [ $count -eq 2 ]
			then
				cfile=`echo $j |cut -d'.' -f1,2`;
				mv $j $cfile.$count;
				gzip $cfile.$count;
				break;
			elif [ $count -eq 11 ]
			then
				continue;
			fi
			cfile=`echo $j|cut -d'.' -f1,2`;
			mv $j $cfile.$count.gz
			let count=$count-1;
		done
		cp $i $i.1
		>$i
	done
done
